'''
Utilities needed to compute the solution to the multi-agent traffic game

Created on July 15th, 2021

@author: Maximilian Kahn
'''


from equilibrium import utilities
from utilities import occlusion_constants, scenario_utilities, occupancy_map, vehicle_state
from equilibria import equilibria_core
from precomputation import db_precomputation
from occlusion_scenario import occlusion_scenario_builder
from trajectories import trajectory_generation

import ast
import itertools
from copy import deepcopy
import sqlite3
import math
import numpy as np # TEMP

from matplotlib import pyplot as plt

def convert_id_to_code(track_id):
    if abs(track_id) < 10:
        code = f"00{track_id}"
    elif abs(track_id) < 100:
        code = f"0{track_id}"
    elif abs(track_id) < 1000:
        code = f"{track_id}"
    elif abs(track_id) < 10000:
        code = f"{track_id}"
    return code

def get_action_code(current_file_id, subject_vehicle_id, target_vehicle_id, maneuver):
    if subject_vehicle_id == target_vehicle_id:
        subject_code = convert_id_to_code(subject_vehicle_id)
        target_code = f"000"
    else:
        subject_code = convert_id_to_code(subject_vehicle_id)
        target_code = convert_id_to_code(target_vehicle_id)
    l1_action_code = occlusion_constants.L1_ACTION_CODES[maneuver]
    if l1_action_code < 10:
        l1_action_code = f"0{l1_action_code}"
 
    action_code = f"{current_file_id}{subject_code}{target_code}{l1_action_code}"
    return action_code


# Input, x, looks like ([2, 'wait-for-oncoming'], [3, 'follow-lead'], [3, 'wait-for-oncoming'])
# This will return true since there is a repeated track_id
def repeated_track_ids_in_maneuver_combination(x, length):
    unique_track_ids = []
    for track_id, maneuver in x:
        unique_track_ids.append(track_id)

    unique_track_ids = set(unique_track_ids)
    if len(unique_track_ids) != length:
        return True
    else:
        return False


# Extract track_id and maneuver from action code:
# (e.g., return value could (1, 2) meaning track id 1 and maneuver code 2)
# Action code example: 7690010000302
def extract_id_and_maneuver_from_action_code(action_code):
    # Maneuver code is always the last 2 characters in action code
    maneuver = int(action_code[-2:])

    subject_vehicle_is_occluding_vehicle = False

    if action_code[5] == '-':
        track_id = -1 * int(action_code[6:7]) 
        subject_vehicle_is_occluding_vehicle = True
    elif action_code[4] == '-':
        track_id = -1 * int(action_code[5:7]) 
        subject_vehicle_is_occluding_vehicle = True
    elif action_code[3] == '-' and len(action_code) == 12:
        track_id = -1 * int(action_code[4:7]) 
        subject_vehicle_is_occluding_vehicle = True
    elif action_code[3] == '-' and len(action_code) == 13:
        track_id = -1 * int(action_code[4:8]) 
        subject_vehicle_is_occluding_vehicle = True
    else:
        track_id = int(action_code[3:6])


    if subject_vehicle_is_occluding_vehicle and int(action_code[-5:-2]) == 0:
        return (track_id, maneuver)

    elif subject_vehicle_is_occluding_vehicle and int(action_code[-5:-2]) != 0:
        track_id = int(action_code[-5:-2])

    else:
        if action_code[8] == '-':
            track_id = -1 * int(action_code[9:10])
        elif action_code[7] == '-':
            track_id = -1 * int(action_code[8:10])

        elif action_code[6] == '-' and len(action_code) == 12:
            track_id = -1 * int(action_code[7:10])
      
        elif action_code[6] == '-' and len(action_code) == 13:
            track_id = -1 * int(action_code[7:11])
        else:
            track_id = int(action_code[6:9])

        if track_id == 0:
            track_id = int(action_code[3:6])
    # If 0, then this is the subject vehicle
    return (track_id, maneuver)


# Input is a dict representing all the maneuver-utility pairs for one vehicle
# E.g., {'track_speed': 0.9, 'wait-for-oncoming': 0.6}
# Output is a dict containing each maneuver and the probability associated with that maneuver
# E.g., {'track_speed': 0.7, 'wait-for-oncoming': 0.3}
def calculate_maneuver_probability(vehicle_maneuver_utilities):
    maneuver_probabilities = {}
    # Computing sum of e^(-lambda * utility)
    denominator = 0
    for maneuver, utility in vehicle_maneuver_utilities.items():
        denominator += math.exp(-occlusion_constants.LAMBDA * utility)
    for maneuver, utility in vehicle_maneuver_utilities.items():
        maneuver_probabilities[maneuver] = math.exp(-occlusion_constants.LAMBDA * utility) / denominator
    return maneuver_probabilities


# Input is: [subject_vehicle, occluding_vehicle, [relevant_vehicles]]
def compute_all_hit_counts_for_scenario(subject_vehicle, occluding_vehicle, relevant_vehicles):
    # # Since we have already calculated occlusion levels for subject vehicle just use that
    # for track_id, occlusion_level in occlusion_levels:
    scenario = []
    scenario = [subject_vehicle] + [occluding_vehicle] + relevant_vehicles
    scenario_ids = [v.id for v in scenario]
    occluded_vehicles_dict = {}

    vehicle_hit_counts = {}

    for vehicle in scenario:
        other_vehicles = [v for v in scenario if v.id != vehicle.id]
        hit_counts = occlusion_scenario_builder.compute_hit_counts(vehicle, other_vehicles)
        vehicle_hit_counts[vehicle.id] = hit_counts
    return vehicle_hit_counts


def compute_risk_metric(current_file_id, start_scenario_key=None, end_scenario_key=None, return_factored_game_info=False):
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

    conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
    cur_occ_db = conn_occ_db.cursor()

    conn_db = sqlite3.connect(occlusion_constants.DB)
    cur_db = conn_db.cursor()

    execute_cmd = f"SELECT SCENARIO_KEY FROM OCCLUSION_SCENARIO_TRAJECTORIES"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    query_result = set(query_result)
    scenario_keys = [i[0] for i in query_result]
    scenario_keys.sort()

    if start_scenario_key != None:
        scenario_keys = scenario_keys[start_scenario_key:]

    # Retrieve all trajectories for the scenario
    for scenario_key in scenario_keys:

        if end_scenario_key != None and scenario_key >= end_scenario_key:
            return

        is_valid_occlusion_scenario = db_precomputation.retrieve_is_valid_occlusion_scenario(scenario_key)
        if is_valid_occlusion_scenario == 0:
            continue

        print(f"\n----SCENARIO KEY: {scenario_key}; CURRENT_FILE_ID: {current_file_id}----")
        db_precomputation.save_scenario_key_in_collisions(scenario_key)

        current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = scenario_utilities.retrieve_all_maneuvers(current_file_id, scenario_key)

        subject_vehicle_id = ordered_vehicle_ids[0]
        occluding_vehicle_id = ordered_vehicle_ids[1]
        relevant_vehicle_ids = ordered_vehicle_ids[2:]

        hypergame_length = len(ordered_vehicle_ids)
        hypergame_maneuver_combinations = itertools.combinations(track_id_maneuver_pairs, hypergame_length)
        hypergame_maneuver_combinations = [i for i in hypergame_maneuver_combinations if not repeated_track_ids_in_maneuver_combination(i, hypergame_length)]

        scenario = scenario_utilities.setup_scenario(current_file_id, current_time, subject_vehicle_id, relevant_vehicle_ids, occluding_vehicle_id=occluding_vehicle_id)

        # Retrieve occlusion scenarios
        # E.g., factored_occlusion_scenarios = {1: [3], -1: [], 3: [1]}, means vehicle 3 is 
        # occluded from 1 and vehicle 1 is occluded from 3, no vehicles are occluded from -1.
        vehicle_hit_counts = db_precomputation.retrieve_vehicle_hit_counts(scenario_key)

        factored_games = compute_factored_games(scenario_key=scenario_key, vehicle_hit_counts=None)
        # print(f"FACTORED GAMES: {factored_games}")

        # Remove any vehicle that is too far away
        # If a vehicle is too far then we don't include it in any of the games. This is fine for safety as it is too far away to effect
        # the outcome of the games.
        vehicles_too_far_away = []
        for vehicle in scenario:
            if len(factored_games[vehicle.id]) == len(ordered_vehicle_ids) - 1:
                vehicles_too_far_away.append(vehicle)

        if vehicles_too_far_away:   
            for vehicle in vehicles_too_far_away:
                scenario.remove(vehicle)
                ordered_vehicle_ids.remove(vehicle.id)
                factored_games.pop(vehicle.id)
            vehicles_too_far_away_ids = [vehicle.id for vehicle in vehicles_too_far_away]
            
            # Need to recompute factored games, taking into account the removed vehicles
            for track_id in ordered_vehicle_ids:
                factored_games[track_id] = [veh_id for veh_id in factored_games[track_id] if veh_id not in vehicles_too_far_away_ids]

        ordered_hypergame_maneuver_combinations = []

        # E.g., hypergame_maneuver_combinations: [((1, 'follow_lead'), (-1, 'proceed-turn'), (3, 'track_speed')), ((-1, 'proceed-turn'), (1, 'track_speed'), (3, 'track_speed'))
        for maneuver_combination in hypergame_maneuver_combinations:
            ordered_maneuver_combination = []
            for track_id in ordered_vehicle_ids:
                for id_maneuver in maneuver_combination:
                    if id_maneuver[0] == track_id:
                        ordered_maneuver_combination.append(id_maneuver)
                if ordered_maneuver_combination not in ordered_hypergame_maneuver_combinations:
                    ordered_hypergame_maneuver_combinations.append(ordered_maneuver_combination)

        # First we calculate the risk with the hypergame, where all vehicles are included:
        hypergame_payoff_dict = {}

        # This is dict where the key is the hypergame_maneuver_set and the value is the maneuver_to_trajectory_index_dict
        # We need this because the specific trajectory to represent each maneuver depends on the maneuver combination that the 
        # vehicles take.
        all_maneuver_to_trajectory_index_dict = {}
        for hypergame_maneuver_set in ordered_hypergame_maneuver_combinations:

            maneuver_to_trajectory_index_dict = {}
            vehicle_maneuver_dict = {}

            # A hypergame maneuver set is a combination of maneuvers for each vehicle in the scenario:
            # E.g., ((1,2), (2,3), (-3,1)) could be one combination of maneuvers for each vehicle in the scenario with vehicles 1, 2, and -3.

            for id_maneuver in hypergame_maneuver_set:
                vehicle_maneuver_dict[id_maneuver[0]] = [id_maneuver[1]]

            # vehicle maneuvers looks like: {1: ['track_speed', 'follow-lead'], 2: ['wait_for_oncoming_vehicle'], -3: ['follow-lead']}
            trajectories_dict = scenario_utilities.retrieve_trajectories(current_file_id, scenario_key, ordered_vehicle_ids, vehicle_maneuver_dict)

            payoff_dict_key, payoff_dict_value, chosen_trajectories = compute_chosen_trajectories(current_file_id, trajectories_dict, subject_vehicle_id, ordered_vehicle_ids, vehicle_maneuver_dict)

            # This dictionary looks like: {(1, 'follow_lead'): (1, 'aggressive', 0), (-6, 'wait_for_lead_to_cross'): (-6, 'aggressive', 1), (3, 'track_speed'): (3, 'normal', 0)}
            # The key is a track_id, maneuver tuple, the value is a track_id, level, index tuple
            # The point of this dict is to recover the chosen trajectory for the maneuver to do a collision check and compute collision details
            for id_maneuver, chosen_trajectory_key in zip(hypergame_maneuver_set, list(chosen_trajectories.keys())):
                if id_maneuver not in list(maneuver_to_trajectory_index_dict.keys()):                        
                    maneuver_to_trajectory_index_dict[id_maneuver] = chosen_trajectory_key

            all_maneuver_to_trajectory_index_dict[tuple(hypergame_maneuver_set)] = maneuver_to_trajectory_index_dict
            hypergame_payoff_dict[payoff_dict_key] = payoff_dict_value


        if return_factored_game_info:
            return all_maneuver_to_trajectory_index_dict

        num_players = len(ordered_vehicle_ids)
        player_actions = [list(set([k[i] for k in hypergame_payoff_dict.keys()])) for i in np.arange(num_players)]  

   
        eq = equilibria_core.EquilibriaCore(num_players,hypergame_payoff_dict,len(hypergame_payoff_dict),player_actions[0],False)
        nash_equilibria = eq.calc_pure_strategy_nash_equilibrium_exhaustive()

        # We cannot play out this game since there are no nash equilibria
        if not nash_equilibria:
            print(f"No hypergame Nash Equilibria could be generated for scenario {scenario_key}. Skipping the scenario.")
            continue

        # This stores every vehicles maneuvers in the form
        # e.g., {1: ['track_speed', 'follow-lead'], 2: ['follow-lead']}
        vehicle_maneuvers = {}
        for index in range(len(ordered_vehicle_ids)):
            track_id = ordered_vehicle_ids[index]
            vehicle_maneuvers[track_id] = []
            for maneuver_combination in  ordered_hypergame_maneuver_combinations:
                if maneuver_combination[index][1] not in vehicle_maneuvers[track_id]:
                    vehicle_maneuvers[track_id].append(maneuver_combination[index][1])

        # Compute maneuver probabilities for vehicles that have no occluded vehicles 
        hypergame_maneuver_probabilities = {}
        for track_id, maneuvers in vehicle_maneuvers.items():

            if factored_games[track_id]:
                continue

            subject_vehicle_maneuvers = {}
            subject_vehicle_maneuvers[track_id] = maneuvers
            maneuver_utilities = compute_maneuver_utilities(subject_vehicle_maneuvers, ordered_vehicle_ids, hypergame_payoff_dict, nash_equilibria)

            vehicle_maneuver_utilities = maneuver_utilities[track_id]
            vehicle_maneuver_probabilities = calculate_maneuver_probability(vehicle_maneuver_utilities)

            hypergame_maneuver_probabilities[track_id] = vehicle_maneuver_probabilities


        # vehicle maneuvers looks like: {1: ['track_speed', 'follow-lead'], 2: ['wait_for_oncoming_vehicle'], -3: ['follow-lead']}
        all_trajectories_dict = scenario_utilities.retrieve_trajectories(current_file_id, scenario_key, ordered_vehicle_ids, vehicle_maneuvers)
 
        # Assign leading vehicle attribute
        for vehicle in scenario:
            lead_vehicle_id = scenario_utilities.get_leader_id(vehicle, scenario)
            if lead_vehicle_id:
                for v in scenario:
                    if v.id == lead_vehicle_id:
                        vehicle.set_leading_vehicle(v)
            else:
                vehicle.set_leading_vehicle(None)

        # Note: this requires the original scenario and so needs to be above the has_oncoming assignment below
        factored_game_details = compute_ground_truth_maneuvers_for_factored_games(current_file_id, scenario_key, scenario, ordered_vehicle_ids, factored_games, track_id_maneuver_pairs)
            
        # This is the case if no Nash Equilibria could be found for one of the factored games
        if not factored_game_details:
            print(f"No Nash Equilibria could be found for one of the factored games for scenario {scenario_key}. Skipping the scenario.")
            continue

        factored_ground_truth_maneuvers, factored_game_maneuver_probabilities = factored_game_details
        print(f"FACTORED GAME GROUND-TRUTH MANEUVERS: {factored_ground_truth_maneuvers}")
        factored_ground_truth_maneuver_sets = compute_ground_truth_maneuver_sets(factored_ground_truth_maneuvers, hypergame_length)
        print(f"BEFORE: FACTORED GROUND TRUTH MANEUVER SETS: {factored_ground_truth_maneuver_sets}")

        # Update factored_game_maneuver_probabilities so that it contains all maneuver probabilities
        factored_game_maneuver_probabilities.update(hypergame_maneuver_probabilities)
 
        # Assign has_oncoming_vehicle attribute for vehicles in scenario
        for i in range(0, len(scenario)-1):
            for j in range(i, len(scenario)):
                vehicle_a, vehicle_b = scenario[i], scenario[j]
                has_oncoming_vehicle = scenario_utilities.compute_has_oncoming_vehicle(vehicle_a, vehicle_b)
                if has_oncoming_vehicle:
                    if not vehicle_a.has_oncoming_vehicle:
                        vehicle_a.set_has_oncoming_vehicle(True)
                    if not vehicle_b.has_oncoming_vehicle:
                        vehicle_b.set_has_oncoming_vehicle(True)

        hypergame_ground_truth_maneuvers = compute_ground_truth_maneuvers(ordered_vehicle_ids, scenario, nash_equilibria, hypergame_payoff_dict, factored_game_subject_id=None)
        hypergame_ground_truth_maneuver_sets = compute_ground_truth_maneuver_sets(hypergame_ground_truth_maneuvers, hypergame_length)

        for factored_ground_truth_maneuvers in factored_ground_truth_maneuver_sets:
            for track_id, maneuver in factored_ground_truth_maneuvers.items():
                if maneuver == 'hypergame_maneuver':
                    factored_ground_truth_maneuvers[track_id] = hypergame_ground_truth_maneuvers[track_id].copy().pop()

        print(f"AFTER: FACTORED GAME GROUND-TRUTH MANEUVER SETS: {factored_ground_truth_maneuver_sets}")

        # Calculate all collisions for hypergame
        print(f"COMPUTING ALL HYPERGAME COLLISIONS")
        all_hypergame_filtered_collisions = {}
        all_true_hypergame_emergency_braking_trajectories = {}
        all_hypergame_maneuver_to_trajectory_index_dict = {}

        for hypergame_ground_truth_maneuvers in hypergame_ground_truth_maneuver_sets:

            # To identify different collisions caused by different ground truth maneuver sets
            # The key to the dict says the maneuver set: ((119, 2), (-2047, 6), (100, 2), (125, 3))
            collision_identifier = [(track_id, occlusion_constants.L1_ACTION_CODES[maneuver]) for track_id, maneuver in hypergame_ground_truth_maneuvers.items()]
            collision_identifier = tuple(collision_identifier)

            hypergame_ground_truth_maneuvers_set = tuple([(track_id, maneuver) for track_id, maneuver in hypergame_ground_truth_maneuvers.items()])

            # For hypergame, find ground truth maneuver_to_trajectory_index_dict: which is called hypergame_maneuver_to_trajectory_index_dict
            for maneuver_combination, maneuver_to_trajectory_index_dict in all_maneuver_to_trajectory_index_dict.items():
                if hypergame_ground_truth_maneuvers_set == maneuver_combination:
                    hypergame_maneuver_to_trajectory_index_dict = maneuver_to_trajectory_index_dict
                    break

            all_hypergame_maneuver_to_trajectory_index_dict[collision_identifier] = hypergame_maneuver_to_trajectory_index_dict
            hypergame_all_collisions, hypergame_emergency_braking_trajectories = compute_all_collisions(current_file_id, ordered_vehicle_ids, hypergame_ground_truth_maneuvers, all_trajectories_dict, hypergame_maneuver_to_trajectory_index_dict, scenario)
            hypergame_filtered_collisions, true_hypergame_emergency_braking_trajectories = filter_collisions(ordered_vehicle_ids, hypergame_all_collisions, hypergame_emergency_braking_trajectories)
            
            if hypergame_filtered_collisions:
                all_hypergame_filtered_collisions[collision_identifier] = hypergame_filtered_collisions
                all_true_hypergame_emergency_braking_trajectories[collision_identifier] = true_hypergame_emergency_braking_trajectories

        print(f"ALL HYPERGAME FILTERED COLLISIONS: {hypergame_filtered_collisions}")

        # Calculate all collisions for factored game
        print(f"COMPUTING ALL FACTORED GAME COLLISIONS")
        all_factored_game_filtered_collisions = {}
        all_true_factored_game_emergency_braking_trajectories = {}
        all_factored_game_maneuver_to_trajectory_index_dict = {}

        for factored_ground_truth_maneuvers in factored_ground_truth_maneuver_sets:

            # To identify different collisions caused by different ground truth maneuver sets
            # The key to the dict says the maneuver set: ((119, 2), (-2047, 6), (100, 2), (125, 3))
            collision_identifier = [(track_id, occlusion_constants.L1_ACTION_CODES[maneuver]) for track_id, maneuver in factored_ground_truth_maneuvers.items()]
            collision_identifier = tuple(collision_identifier)

            factored_ground_truth_maneuvers_set = tuple([(track_id, maneuver) for track_id, maneuver in factored_ground_truth_maneuvers.items()])
        
            for maneuver_combination, maneuver_to_trajectory_index_dict in all_maneuver_to_trajectory_index_dict.items():
                if factored_ground_truth_maneuvers_set == maneuver_combination:
                    factored_game_maneuver_to_trajectory_index_dict = maneuver_to_trajectory_index_dict
                    break


            all_factored_game_maneuver_to_trajectory_index_dict[collision_identifier] = factored_game_maneuver_to_trajectory_index_dict
            factored_game_all_collisions, factored_game_emergency_braking_trajectories = compute_all_collisions(current_file_id, ordered_vehicle_ids, factored_ground_truth_maneuvers, all_trajectories_dict, factored_game_maneuver_to_trajectory_index_dict, scenario)

            factored_game_filtered_collisions, true_factored_game_emergency_braking_trajectories = filter_collisions(ordered_vehicle_ids, factored_game_all_collisions, factored_game_emergency_braking_trajectories)

            if factored_game_filtered_collisions:
                all_factored_game_filtered_collisions[collision_identifier] = factored_game_filtered_collisions
                all_true_factored_game_emergency_braking_trajectories[collision_identifier] = true_factored_game_emergency_braking_trajectories

        print(f"ALL FACTORED GAME FILTERED COLLISIONS")
        print(all_factored_game_filtered_collisions)

        if all_hypergame_filtered_collisions:

            all_hypergame_trajectory_indices = {}
            for collision_identifier, hypergame_filtered_collisions in all_hypergame_filtered_collisions.items():
                # Convert maneuvers to coded form since SQL cannot store text in the form 'text'
                # [track_id_a, track_id_b, collision_id, 'emergency_braking', 'emergency_braking', maneuver_a, maneuver_b, impact_velocity_a, impact_velocity_b, time_to_collision]
                for collision in hypergame_filtered_collisions:
                    collision[3] = occlusion_constants.L1_ACTION_CODES[collision[3]]
                    collision[4] = occlusion_constants.L1_ACTION_CODES[collision[4]]
                    collision[5] = occlusion_constants.L1_ACTION_CODES[collision[5]]
                    collision[6] = occlusion_constants.L1_ACTION_CODES[collision[6]]

                # hypergame_maneuver_to_trajectory_index_dict: {(11, 'follow_lead_into_intersection'): (11, 2), (-8, 'wait_for_lead_to_cross'): (-8, 1), (3, 'track_speed'): (3, 2), (1, 'follow_lead'): (1, 1), (20, 'track_speed'): (20, 2)}
                hypergame_trajectory_indices = [] # {track_id: {maneuver_code: [traj_level_code, traj_index]}
                hypergame_maneuver_to_trajectory_index_dict = all_hypergame_maneuver_to_trajectory_index_dict[collision_identifier]
                for id_maneuver, trajectory_index in hypergame_maneuver_to_trajectory_index_dict.items():
                    track_id = id_maneuver[0]
                    coded_maneuver = occlusion_constants.L1_ACTION_CODES[id_maneuver[1]]
                    hypergame_trajectory_indices.append([track_id, coded_maneuver, trajectory_index[1]])

                all_hypergame_trajectory_indices[collision_identifier] = hypergame_trajectory_indices

            # Save hypergame collision
            db_precomputation.save_hypergame_collision_details(scenario_key, all_hypergame_filtered_collisions, all_hypergame_trajectory_indices, all_true_hypergame_emergency_braking_trajectories)


        if all_factored_game_filtered_collisions:

            # Same format as factored_game_maneuver_probabilities but 
            # maneuvers are coded, e.g., {track_id: {maneuver_code: prob.}, ...}
            all_coded_maneuver_probabilities = {} 
            for track_id, maneuver_probabilities in factored_game_maneuver_probabilities.items():
                coded_maneuver_probabilities = {}
                for maneuver, probability in maneuver_probabilities.items():
                    coded_maneuver = occlusion_constants.L1_ACTION_CODES[maneuver]
                    coded_maneuver_probabilities[coded_maneuver] = probability
                all_coded_maneuver_probabilities[track_id] = coded_maneuver_probabilities
            # The probabilities are not for each vehicle, only for the vehicles that have 
            # occluded vehicles, otherwise the actions for the hypergame are used.
            print(f"CODED MANEUVER PROBABILITIES: {all_coded_maneuver_probabilities}")

            occlusion_caused_collisions = {}
            all_factored_game_trajectory_indices = {}
            for collision_identifier, factored_game_filtered_collisions in all_factored_game_filtered_collisions.items():
                occlusion_caused_collisions[collision_identifier] = {}
                # Convert maneuvers to coded form since SQL cannot store text in the form 'text'
                for collision in factored_game_filtered_collisions:
                    collision[3] = occlusion_constants.L1_ACTION_CODES[collision[3]]
                    collision[4] = occlusion_constants.L1_ACTION_CODES[collision[4]]
                    collision[5] = occlusion_constants.L1_ACTION_CODES[collision[5]]
                    collision[6] = occlusion_constants.L1_ACTION_CODES[collision[6]]

                    collision_id = collision[2]
                    factored_game = compute_factored_games(scenario_key=scenario_key)
                    track_id_a = collision[0]
                    track_id_b = collision[1]
                    vehicle_a_occluded_vehicles = factored_game[track_id_a]
                    vehicle_b_occluded_vehicles = factored_game[track_id_b]
                    if track_id_b in vehicle_a_occluded_vehicles or track_id_a in vehicle_b_occluded_vehicles:
                        occlusion_caused_collisions[collision_identifier][collision_id] = 1
                    else:
                        occlusion_caused_collisions[collision_identifier][collision_id] = 0

                factored_game_trajectory_indices = [] # {track_id: {maneuver_code: [traj_level_code, traj_index]}
                factored_game_maneuver_to_trajectory_index_dict = all_factored_game_maneuver_to_trajectory_index_dict[collision_identifier]

                for id_maneuver, trajectory_index in factored_game_maneuver_to_trajectory_index_dict.items():
                    track_id = id_maneuver[0]
                    coded_maneuver = occlusion_constants.L1_ACTION_CODES[id_maneuver[1]]
                    factored_game_trajectory_indices.append([track_id, coded_maneuver, trajectory_index[1]])

                all_factored_game_trajectory_indices[collision_identifier] = factored_game_trajectory_indices

            db_precomputation.save_factored_game_collision_details(scenario_key, all_factored_game_filtered_collisions, all_factored_game_trajectory_indices, \
                                                                    all_true_factored_game_emergency_braking_trajectories, occlusion_caused_collisions)
            db_precomputation.save_maneuver_probabilities(scenario_key, all_coded_maneuver_probabilities)


# Probably want to save some values here, such as maneuver probabilities 
# track_id_maneuver_pairs example : [(2, 'follow_lead_into_intersection'), (8, 'proceed-turn'), (3, 'track_speed'), (2, 'proceed-turn'), (1, 'follow_lead'), (8, 'wait-for-oncoming'), (1, 'track_speed'), (-13, 'proceed-turn'), (-13, 'wait_for_lead_to_cross')]
def compute_ground_truth_maneuvers_for_factored_games(current_file_id, scenario_key, scenario, ordered_vehicle_ids, factored_games, track_id_maneuver_pairs):
    ground_truth_maneuvers = {}

    # We don't allow any vehicle to use a follow maneuver if it's occluded vehicle includes it's lead vehicle
    # This may result in an error if the vehicle only can use follow maneuvers. Though they if they use follow-lead
    # they should also be able to use track_speed.
    # See if lead vehicle is occluded 

    lead_vehicle_occluded = [] # Record vehicle ids with lead vehicles occluded
    for vehicle in scenario:
        if vehicle.leading_vehicle:
            if vehicle.leading_vehicle.id in factored_games[vehicle.id]:
                lead_vehicle_occluded.append(vehicle.id)

    maneuver_probabilities = {}

    for subject_vehicle_id, occluded_vehicles in factored_games.items():

        if not occluded_vehicles:
            ground_truth_maneuvers[subject_vehicle_id] = {'hypergame_maneuver'}
            continue

        # Get factored ordered_vehicle_ids for this particular occlusion scenario
        factored_ordered_vehicle_ids = [subject_vehicle_id]
        for track_id in ordered_vehicle_ids:
            if track_id != subject_vehicle_id and track_id not in occluded_vehicles:
                factored_ordered_vehicle_ids.append(track_id)

        game_length = len(factored_ordered_vehicle_ids)

        # Get the track_id_maneuver_pairs for this factored game
        factored_track_id_maneuver_pairs = []
        for track_id_maneuver_pair in track_id_maneuver_pairs:
            if track_id_maneuver_pair[0] in factored_ordered_vehicle_ids:

                # The vehicle can't use follow maneuvers if it's lead vehicle is occluded
                if track_id_maneuver_pair[0] in lead_vehicle_occluded and track_id_maneuver_pair[1] in occlusion_constants.FOLLOW_ACTIONS:
                   continue

                factored_track_id_maneuver_pairs.append(track_id_maneuver_pair)

        maneuver_combinations = itertools.combinations(factored_track_id_maneuver_pairs, game_length)
        maneuver_combinations = [i for i in maneuver_combinations if not repeated_track_ids_in_maneuver_combination(i, game_length)]

        # Order maneuver combination so order of vehicles is consistent with factored_order_vehicle_ids
        ordered_maneuver_combinations = []
        for maneuver_combination in maneuver_combinations:
            ordered_combination = []
            for track_id in factored_ordered_vehicle_ids:
                for id_maneuver in maneuver_combination:
                    if id_maneuver[0] == track_id:
                        ordered_combination.append(id_maneuver)
                if ordered_combination not in ordered_maneuver_combinations:
                    ordered_maneuver_combinations.append(ordered_combination)

        factored_payoff_dict = {}
        for maneuver_combination in ordered_maneuver_combinations:
            vehicle_maneuver_dict = {}
            for id_maneuver in maneuver_combination:
                vehicle_maneuver_dict[id_maneuver[0]] = [id_maneuver[1]]
            # vehicle maneuvers looks like: {1: ['track_speed', 'follow-lead'], 2: ['wait_for_oncoming_vehicle'], -3: ['follow-lead']}
           
            trajectories_dict = scenario_utilities.retrieve_trajectories(current_file_id, scenario_key, factored_ordered_vehicle_ids, vehicle_maneuver_dict)

            factored_payoff_dict_key, factored_payoff_dict_value, chosen_trajectories = compute_chosen_trajectories(current_file_id, trajectories_dict, subject_vehicle_id, factored_ordered_vehicle_ids, vehicle_maneuver_dict)

            factored_payoff_dict[factored_payoff_dict_key] = factored_payoff_dict_value

        factored_num_players = len(factored_ordered_vehicle_ids)
        factored_player_actions = [list(set([k[i] for k in factored_payoff_dict.keys()])) for i in np.arange(factored_num_players)]

        eq = equilibria_core.EquilibriaCore(factored_num_players,factored_payoff_dict,len(factored_payoff_dict),factored_player_actions[0],False)
        factored_nash_equilibria = eq.calc_pure_strategy_nash_equilibrium_exhaustive()


        if not factored_nash_equilibria:
            return None

        # Assemble scenario
        factored_scenario = deepcopy(scenario)
        factored_scenario = [v for v in factored_scenario if v.id in factored_ordered_vehicle_ids]

        # Assign leading vehicle attribute
        for vehicle in factored_scenario:
            lead_vehicle_id = scenario_utilities.get_leader_id(vehicle, factored_scenario)
            if lead_vehicle_id != None:
                for v in factored_scenario:
                    if v.id == lead_vehicle_id:
                        vehicle.set_leading_vehicle(v)
            else:
                vehicle.set_leading_vehicle(None)

        for i in range(0, len(factored_scenario)-1):
            for j in range(i, len(factored_scenario)):
                vehicle_a, vehicle_b = factored_scenario[i], factored_scenario[j]
                has_oncoming_vehicle = scenario_utilities.compute_has_oncoming_vehicle(vehicle_a, vehicle_b)
                if has_oncoming_vehicle:
                    if not vehicle_a.has_oncoming_vehicle:
                        vehicle_a.set_has_oncoming_vehicle(True)
                    if not vehicle_b.has_oncoming_vehicle:
                        vehicle_b.set_has_oncoming_vehicle(True)

        computed_maneuvers = compute_ground_truth_maneuvers(factored_ordered_vehicle_ids, factored_scenario, factored_nash_equilibria, factored_payoff_dict, factored_game_subject_id=subject_vehicle_id)

        ground_truth_maneuvers[subject_vehicle_id] = computed_maneuvers[subject_vehicle_id]

        # Get all of subject vehicle's factored game maneuvers
        # ['76900700004', '76900700003']
        subject_vehicle_maneuver_action_codes = factored_player_actions[0]

        maneuvers = []
        for action_code in subject_vehicle_maneuver_action_codes:
            track_id, maneuver_code = extract_id_and_maneuver_from_action_code(action_code)
            maneuver = occlusion_constants.L1_MANEUVERS[maneuver_code]
            maneuvers.append(maneuver)

        subject_vehicle_maneuvers = {}
        subject_vehicle_maneuvers[subject_vehicle_id] = maneuvers
        maneuver_utilities = compute_maneuver_utilities(subject_vehicle_maneuvers, factored_ordered_vehicle_ids, factored_payoff_dict, factored_nash_equilibria)

        vehicle_maneuver_utilities = maneuver_utilities[subject_vehicle_id]
        vehicle_maneuver_probabilities = calculate_maneuver_probability(vehicle_maneuver_utilities)

        maneuver_probabilities[subject_vehicle_id] = vehicle_maneuver_probabilities

    return ground_truth_maneuvers, maneuver_probabilities

# E.g., turns {119: {'follow_lead_into_intersection', 'proceed-turn'}, -2047: 'hypergame_maneuver', 100: 'hypergame_maneuver', 125: {'track_speed'}}
# into [{119: 'follow_lead_into_intersection', -2047: 'hypergame_maneuver', 100: 'hypergame_maneuver', 125: 'track_speed'}, 
#       {119: 'proceed-turn', -2047: 'hypergame_maneuver', 100: 'hypergame_maneuver', 125: 'track_speed'}]
def compute_ground_truth_maneuver_sets(ground_truth_maneuvers, game_length):
    track_id_maneuver_pairs = []
    for track_id, maneuvers in ground_truth_maneuvers.items():
        for maneuver in maneuvers:
            track_id_maneuver_pairs.append((track_id, maneuver))

    maneuver_combinations = itertools.combinations(track_id_maneuver_pairs, game_length)
    maneuver_combinations = [i for i in maneuver_combinations if not repeated_track_ids_in_maneuver_combination(i, game_length)]

    ground_truth_maneuver_sets = []
    for maneuver_combination in maneuver_combinations:
        ground_truth_maneuver = {}
        for track_id_maneuver in maneuver_combination:
            ground_truth_maneuver[track_id_maneuver[0]] = track_id_maneuver[1]

        ground_truth_maneuver_sets.append(ground_truth_maneuver)
    
    return ground_truth_maneuver_sets


def compute_ground_truth_maneuvers(ordered_vehicle_ids, scenario, nash_equilibria, payoff_dict, factored_game_subject_id=None):
    
    all_ground_truth_maneuvers = {}
    maximum_utility_nash_equilibria = compute_maximum_utility_nash_equilibria(nash_equilibria)

    # Get the maneuver for each vehicle for the hypergame
    for vehicle in scenario:

        # If we are computing ground-truth maneuvers for a factored game, 
        # then only compute them for the subject vehicle for that game
        if factored_game_subject_id:
            if vehicle.id != factored_game_subject_id:
                continue

        rule_based_maneuvers = scenario_utilities.get_rule_action(vehicle)

        track_id_index = [index for index in range(len(ordered_vehicle_ids)) if ordered_vehicle_ids[index] == vehicle.id]
        track_id_index = track_id_index[0]
        nash_equilibria_maneuvers = extract_maneuvers_from_nash_equilibria(track_id_index, nash_equilibria)

        vehicle_maneuvers = {vehicle.id: nash_equilibria_maneuvers}
    
        maneuver_utilities = compute_maneuver_utilities(vehicle_maneuvers, ordered_vehicle_ids, payoff_dict, nash_equilibria)

        if len(rule_based_maneuvers) == 1:
        #     # Check if this maneuver is used in any of the nash equilibria for this vehicle. If so, then use this maneuver.
        #     # If not, take the maneuver with the highest utility value (across nash equilibria) for this vehicle.
            if rule_based_maneuvers[0] in nash_equilibria_maneuvers:
                ground_truth_maneuvers = [rule_based_maneuvers[0]]
            else:
                ground_truth_maneuvers = []
                for maximum_utility_nash_equilibrium in maximum_utility_nash_equilibria:

                    id_maneuver_code = extract_id_and_maneuver_from_action_code(maximum_utility_nash_equilibrium[track_id_index])
                    ground_truth_maneuver = occlusion_constants.L1_MANEUVERS[id_maneuver_code[1]]
                    ground_truth_maneuvers.append(ground_truth_maneuver) 

        elif len(rule_based_maneuvers) > 1:
            potential_ground_truth_maneuvers = []
            # Check which rule-based actions are also nash equilibria actions. If there are multiple that satisfy this criteria,
            # check their utilities and take the max across nash equilibria. Like above, if there are no rule-based actions that
            # correspond with nash equilibria actions (for this vehicle) then take maneuverwith the highest utility
            for rule_based_maneuver in rule_based_maneuvers:
                if rule_based_maneuver in nash_equilibria_maneuvers:
                    potential_ground_truth_maneuvers.append(rule_based_maneuver)

            if len(potential_ground_truth_maneuvers) == 1:
                ground_truth_maneuvers = potential_ground_truth_maneuvers

            elif len(potential_ground_truth_maneuvers) > 1:
                maximum_utility = -math.inf
                ground_truth_maneuvers = []
                # Loop through rule-based maneuvers and take maximum utility
                for potential_maneuver in potential_ground_truth_maneuvers:
                    if maneuver_utilities[vehicle.id][potential_maneuver] > maximum_utility:
                        maximum_utility = maneuver_utilities[vehicle.id][potential_maneuver]
                        ground_truth_maneuvers = []
                        ground_truth_maneuvers.append(potential_maneuver)

                    elif maneuver_utilities[vehicle.id][potential_maneuver] == maximum_utility:
                        ground_truth_maneuvers.append(potential_maneuver)

            # If none exist, just pick maneuver from max combined utility nash equilibrium
            elif len(potential_ground_truth_maneuvers) == 0:
                ground_truth_maneuvers = []
                for maximum_utility_nash_equilibrium in maximum_utility_nash_equilibria:
                    id_maneuver_code = extract_id_and_maneuver_from_action_code(maximum_utility_nash_equilibrium[track_id_index])
                    ground_truth_maneuver = occlusion_constants.L1_MANEUVERS[id_maneuver_code[1]]
                    ground_truth_maneuvers.append(ground_truth_maneuver) 

        elif len(rule_based_maneuvers) == 0:
            ground_truth_maneuvers = []
            for maximum_utility_nash_equilibrium in maximum_utility_nash_equilibria:
                id_maneuver_code = extract_id_and_maneuver_from_action_code(maximum_utility_nash_equilibrium[track_id_index])
                ground_truth_maneuver = occlusion_constants.L1_MANEUVERS[id_maneuver_code[1]]
                ground_truth_maneuvers.append(ground_truth_maneuver) 

            # Like above, if there are no rule-based actions that correspond with nash equilibria actions (for this vehicle) 
            # then take maneuver with the highest utility

        ground_truth_maneuvers = set(ground_truth_maneuvers)
        all_ground_truth_maneuvers[vehicle.id] = ground_truth_maneuvers
    
    return all_ground_truth_maneuvers

# Get all maneuvers track_id makes in all nash equilibria that exist
# track_id_index is the index of track_id in ordered_vehicle_ids
def extract_maneuvers_from_nash_equilibria(track_id_index, nash_equilibria):
    maneuvers = []
    for action_codes in list(nash_equilibria.keys()):
        action_code = action_codes[track_id_index]
        id_and_maneuver_code_tuple = extract_id_and_maneuver_from_action_code(action_code)
        maneuvers.append(occlusion_constants.L1_MANEUVERS[id_and_maneuver_code_tuple[1]])
    return set(maneuvers)

# Return is nash equilibrium with maximum utility
def compute_maximum_utility_nash_equilibria(nash_equilibria):
    maximum_utility_nash_equilibria = []
    maximum_utility = -math.inf
    for action_codes, nash_equilibrium_utilities in nash_equilibria.items():

        if sum(nash_equilibrium_utilities) > maximum_utility:
            maximum_utility = sum(nash_equilibrium_utilities)
            maximum_utility_nash_equilibria = []
            maximum_utility_nash_equilibria.append(action_codes)

        elif sum(nash_equilibrium_utilities) == maximum_utility:
            maximum_utility_nash_equilibria.append(action_codes)
            
    return maximum_utility_nash_equilibria

# vehicle_maneuvers is a dict of track id, maneuver lists: {1: [track_speed], 2: [follow-lead, wait-for-oncoming-vehicle]}
def compute_maneuver_utilities(vehicle_maneuvers, ordered_vehicle_ids, payoff_dict, nash_equilibria):

    # This dict stores the same nash equilibria information as nash_equilibria but the action codes have been changed to id-maneuver coded forms
    # E.g., {('76911900007', '769119-204707', '76911910002'): [1, 1, 1]}
    # to {((119, 7), (-2047, 7), (100, 2)): [1, 1, 1]}
    nash_equilibria_coded_dict = {}
    for action_codes, nash_equilibrium_utilities in nash_equilibria.items():
        nash_equilibrium_id_maneuver_pairs = []
        for action_code in action_codes:
            id_maneuver_pair = extract_id_and_maneuver_from_action_code(action_code)
            nash_equilibrium_id_maneuver_pairs.append(id_maneuver_pair)
        nash_equilibrium_id_maneuver_pairs = tuple(nash_equilibrium_id_maneuver_pairs)
        nash_equilibria_coded_dict[nash_equilibrium_id_maneuver_pairs] = nash_equilibrium_utilities

    payoff_coded_dict = {}
    for action_codes, payoff_utilities in payoff_dict.items():
        id_maneuver_pairs = []
        for action_code in action_codes:
            id_maneuver_pair = extract_id_and_maneuver_from_action_code(action_code)
            id_maneuver_pairs.append(id_maneuver_pair)
        id_maneuver_pairs = tuple(id_maneuver_pairs)
        payoff_coded_dict[id_maneuver_pairs] = payoff_utilities

    maneuver_utilities = {}
    for index in range(len(ordered_vehicle_ids)):
        track_id = ordered_vehicle_ids[index]

        # Sometimes we just want the utilities for one vehicle.
        # Note: vehicle maneuvers determines which maneuvers have utilities computed for them
        if track_id not in list(vehicle_maneuvers.keys()):
            continue

        maneuvers = vehicle_maneuvers[track_id]

        maneuver_utilities_for_track_id = {}
        for maneuver in maneuvers:

            maneuver_code = occlusion_constants.L1_ACTION_CODES[maneuver]
            track_id_maneuver_code = (track_id, maneuver_code)
            
            # Stores difference between particular nash equilibrium and maneuver combination
            nash_equilibria_distances = {}

            # e.g., nash_equilibrium = ((3, 3), (-2, 2), (1, 3)), nash_equilibrium_utilities = [1, -0.9973356866608549, -0.9976377481659366]
            for nash_equilibrium, nash_equilibrium_utilities in nash_equilibria_coded_dict.items():
                nash_equilibrium_utility = nash_equilibrium_utilities[index]

                payoff_dict_key = list(nash_equilibrium).copy()
                payoff_dict_key[index] = track_id_maneuver_code
                payoff_dict_key = tuple(payoff_dict_key)  
                action_utility = payoff_coded_dict[payoff_dict_key][index]

                distance_to_nash_equilibrium_utility = abs(nash_equilibrium_utility - action_utility)
  
                nash_equilibria_distances[nash_equilibrium] = (action_utility, distance_to_nash_equilibrium_utility)
            
            # E.g., {((1, 3), (3, 3), (-1, 2)): (-0.9956469092808533, 0.00012381575849462756)}
            min_distance = math.inf
            for action_utility_and_distance in list(nash_equilibria_distances.values()):
                if action_utility_and_distance[1] < min_distance:
                    min_distance = action_utility_and_distance[1]
                    chosen_action_utility = action_utility_and_distance[0]

            maneuver_utilities_for_track_id[maneuver] = chosen_action_utility

        maneuver_utilities[track_id] = maneuver_utilities_for_track_id

    return maneuver_utilities


# This determines which trajectory to use for the subject vehicle's maneuver with target vehicle maneuvers. 
# The order of maneuvers in target_vehicle_maneuvers matches with the order in target_vehicles
# In this case, subject_vehicle_id refers to the subject vehicle for the base scenario, NOT the "subject vehicle"
# vehicle for each occlusion scenario.
def compute_chosen_trajectories(current_file_id, trajectories_dict, subject_vehicle_id, vehicle_ids, vehicle_maneuvers):
    util_obj = utilities.Utilities()
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()

    # Stores every trajectory safety utility pair
    safety_utilities_pair_dict = {}
    # Stores each trajectory id: (track_id, level, trajectory_index)
    trajectory_ids = []

    # Compute safety utility for each traj pair
    for track_id_index_a in range(0, len(vehicle_ids)-1):
        track_id_a = vehicle_ids[track_id_index_a]
        maneuver_a = vehicle_maneuvers[track_id_a][0]
        trajectories_a = trajectories_dict[track_id_a][maneuver_a]

        for trajectory_index_a in range(len(trajectories_a)):
            trajectory_a = trajectories_a[trajectory_index_a]

            for track_id_index_b in range(track_id_index_a+1, len(vehicle_ids)):
                track_id_b = vehicle_ids[track_id_index_b]
                maneuver_b = vehicle_maneuvers[track_id_b][0]
                trajectories_b = trajectories_dict[track_id_b][maneuver_b]

                for trajectory_index_b in range(len(trajectories_b)):
                    trajectory_b = trajectories_b[trajectory_index_b] 

                    # Compute safety utility
                    dist_gap = util_obj.calc_dist_gap(trajectory_a, trajectory_b)
                    safety_utility = util_obj.calc_safe_payoff(dist_gap)

                    trajectory_id1 = (track_id_a, trajectory_index_a)
                    trajectory_id2 = (track_id_b, trajectory_index_b)

                    safety_utilities_pair_dict[(trajectory_id1, trajectory_id2)] = safety_utility

                    trajectory_ids.append(trajectory_id1)
                    trajectory_ids.append(trajectory_id2)

    # Format is: trajectory_id1 = (track_id, level, trajectory_index)
    # E.g., trajectory_ids = [(1, 'normal', 0), (2, 'aggressive', 2), (1, 'aggressive', 1), ...]
    trajectory_ids = set(trajectory_ids)
    # Holds the "true" safety utility for each trajectory
    true_safety_utilities_dict = {}
    # Computing actual safety utility value for each trajectory by finding minimum safety utility for trajectory
    for track_id in vehicle_ids:
        # This is the trajectory_ids for this particular track_id
        trajectory_ids_for_track_id = [trajectory_id for trajectory_id in trajectory_ids if trajectory_id[0] == track_id]
        for trajectory_id in trajectory_ids_for_track_id:
            minimum_safety_utility = math.inf

            for trajectory_id_pairs in list(safety_utilities_pair_dict.keys()):
                if trajectory_id in trajectory_id_pairs and safety_utilities_pair_dict[trajectory_id_pairs] < minimum_safety_utility:
                    minimum_safety_utility = safety_utilities_pair_dict[trajectory_id_pairs]
                    true_safety_utilities_dict[trajectory_id] = minimum_safety_utility

    chosen_trajectories = {}
    # Choosing maximum utility trajectory for each maneuver
    for track_id in vehicle_ids:
        maneuver = vehicle_maneuvers[track_id][0]
        maximum_utility_value = -math.inf
        # This is the trajectory_ids for this particular track_id
        trajectory_ids_for_track_id = [trajectory_id for trajectory_id in trajectory_ids if trajectory_id[0] == track_id]

        if len(trajectory_ids_for_track_id) == 0:
            raise Exception(f"Length of trajectory_ids_for_track_id is equal 0. subject_vehicle_id is : {subject_vehicle_id}, vehicle_ids are: {vehicle_ids}")
        
        maximum_utility_trajectory_id = None
        for trajectory_id in trajectory_ids_for_track_id:
            safety_utility = true_safety_utilities_dict[trajectory_id]
            # index by track_id, level, and trajectory index
            trajectory = trajectories_dict[trajectory_id[0]][maneuver][trajectory_id[1]]
            traj_length = util_obj.calc_traj_length(trajectory)
            progress_utility = util_obj.progress_payoff_dist(traj_length, 'agent_2')
            total_utility = util_obj.combine_utils(progress_utility, safety_utility, occlusion_constants.SAFETY_THRESHOLD)
            if total_utility > maximum_utility_value:
                maximum_utility_value = total_utility
                maximum_utility_trajectory_id = trajectory_id
        chosen_trajectories[maximum_utility_trajectory_id] = maximum_utility_value

    payoff_dict_key = []
    payoff_dict_value = []

    for trajectory_id, maximum_utility in chosen_trajectories.items():

        target_vehicle_id = trajectory_id[0]
        maneuver = vehicle_maneuvers[target_vehicle_id][0]
        action_code = get_action_code(current_file_id, subject_vehicle_id, target_vehicle_id, maneuver)

        payoff_dict_key.append(action_code)
        payoff_dict_value.append(maximum_utility)

    return tuple(payoff_dict_key), payoff_dict_value, chosen_trajectories


# E.g., collision_details = [[1,2, 'maneuver1', 'maneuver2', speed1, speed2, collision_time(between 0 and traj horizon)], ...]
# collision_details = compute_collision_details(all_trajectories_dict, maneuver_to_trajectory_index_dict)

# E.g., maneuver_to_trajectory_index_dict = {(1, 'follow_lead'): (1, 'aggressive', 0), (-6, 'wait_for_lead_to_cross'): (-6, 'aggressive', 1), (3, 'track_speed'): (3, 'normal', 0)}
# all_trajectories_dict is a dict with track_id keys. The value is a dict with levels as the key and the value is a list of trajectories
# ordered_hypergame_maneuver_combinations: [[(1, 'track_speed'), (3, 'track_speed'), (-1, 'proceed-turn')], [(1, 'follow_lead'), (3, 'track_speed'), (-1, 'proceed-turn')]]
# Example return value: [[1, -1, 'track_speed', 'proceed-turn', 16.929635360378597, 7.860411808911558, 0.2], [1, -1, 'follow_lead', 'proceed-turn', 16.814012596833372, 7.860411808911558, 0.2]]
def compute_all_collisions(current_file_id, ordered_vehicle_ids, vehicle_maneuvers, all_trajectories_dict, maneuver_to_trajectory_index_dict, scenario):
    # TODO ASSUME VEHICLE_MANEUVERS IS ONE MANEUVER PER TRACK ID.
    collision_id = 0

    emergency_braking_trajectories = {} # Format is {collision_id: (traj_1, traj_2)}
    all_collisions = []

    # Construct selected trajectories dict
    # which stores the selected trajectory for each vehicle
    # Eg. {track_id: traj, ...}
    selected_trajectories = {}

    for track_id in ordered_vehicle_ids:
        maneuver = vehicle_maneuvers[track_id]
        _, index = maneuver_to_trajectory_index_dict[(track_id, maneuver)]
        trajectory = all_trajectories_dict[track_id][maneuver][index]
        selected_trajectories[track_id] = trajectory

    for i in range(0, len(ordered_vehicle_ids) - 1):
        track_id_a = ordered_vehicle_ids[i]
        maneuver_a = vehicle_maneuvers[track_id_a]
        for j in range(i+1, len(ordered_vehicle_ids)):
            track_id_b = ordered_vehicle_ids[j]
            maneuver_b = vehicle_maneuvers[track_id_b]

            trajectory_a = selected_trajectories[track_id_a]
            trajectory_b = selected_trajectories[track_id_b]

            collision_impact_details = scenario_utilities.collision_check(trajectory_a, trajectory_b)

            if collision_impact_details:
                # Checks if colliding vehicles can see each other, if they can, emergency brake.
                # If they cannot see each other then continue along the collision trajectory until
                # they can or until they collide. Once they can see each other, there is a 1.5 response time delay.
                traffic_information = [track_id_a, track_id_b, maneuver_a, maneuver_b]

                collision_details_with_trajectories = handle_collisions(current_file_id, collision_id, scenario[0].current_time, ordered_vehicle_ids, collision_impact_details, traffic_information, selected_trajectories, scenario)

                # This means collision can be avoided
                if type(collision_details_with_trajectories) == bool:
                    print(f"No Collision: Collision between {track_id_a} and {track_id_b} can be avoided.")
                    continue
                else:
                    collision_details = collision_details_with_trajectories[0]
                    emergency_braking_trajectories[collision_id] = collision_details_with_trajectories[1]
                    all_collisions.append(collision_details)

                    collision_id += 1

    return all_collisions, emergency_braking_trajectories


# Checks if colliding vehicles can see each other, if they can, emergency brake.
# If they cannot see each other then continue along the collision trajectory until
# they can or until they collide. Once they can see each other, there is a 1.5 response time delay.
# [0.0, 538830.52, 4814012.16, 16.994444444444444, -2.053959551935696, 0, 0.04318783942461197, -0.98052867145024]
def handle_collisions(current_file_id, collision_id, current_time, ordered_vehicle_ids, collision_impact_details, traffic_information, selected_trajectories, scenario):
    impact_velocity_a, impact_velocity_b, original_time_to_collision = collision_impact_details
    track_id_a, track_id_b, maneuver_a, maneuver_b = traffic_information
    # FIRST THING IS TO CHECK WHEN BOTH VEHICLES CAN SEE EACH OTHER ALONG THEIR COLLISION TRAJECTORIES:
    # [time, x, y, speed, acceleration, jerk, lateral acceleration, yaw, trajectory length]

    updated_scenario = []
    for track_id in ordered_vehicle_ids:
        v = vehicle_state.VehicleState()
        v.set_id(track_id)
        updated_scenario.append(v)

    for i in range(len(selected_trajectories[track_id_a])):
        # This is time along trajectory
        time = selected_trajectories[track_id_a][i][0]
        if time + occlusion_constants.EMERGENCY_BRAKING_DELAY >= original_time_to_collision:
            # Collision is before drivers can react so return the base collision
            collision_details = [track_id_a, track_id_b, collision_id, maneuver_a, maneuver_b, maneuver_a, maneuver_b, impact_velocity_a, impact_velocity_b, original_time_to_collision]
            print(f"Collision: {track_id_a} and {track_id_b} do not have enough time to prevent original collision.")
            return (collision_details, (selected_trajectories[track_id_a], selected_trajectories[track_id_b]))

        # We have already tested for timestep 0 as that is the base case
        if i == 0:
            continue

        # Update vehicle states for new time
        for v in updated_scenario:
            trajectory = selected_trajectories[v.id]
            timestep = trajectory[i]
            v.set_x(timestep[1])
            v.set_y(timestep[2])
            angle = scenario_utilities.angle_check(timestep[-2])
            v.set_angle(angle)

        # Compute updated factored game for vehicle a
        other_vehicles_a = []
        for v in updated_scenario:
            if v.id != track_id_a:
                other_vehicles_a.append(v)
            else:
                subject_vehicle_a = v
        hit_counts_a = occlusion_scenario_builder.compute_hit_counts(subject_vehicle_a, other_vehicles_a)
        factored_game_a = compute_factored_game(hit_counts_a)

        # Compute updated factored game for vehicle b
        other_vehicles_b = []
        for v in updated_scenario:
            if v.id != track_id_b:
                other_vehicles_b.append(v)
            else:
                subject_vehicle_b = v
        hit_counts_b = occlusion_scenario_builder.compute_hit_counts(subject_vehicle_b, other_vehicles_b)
        factored_game_b = compute_factored_game(hit_counts_b)

        trajectory_a = selected_trajectories[track_id_a].copy()
        trajectory_b = selected_trajectories[track_id_b].copy()

        # This is the index of the ground-truth trajectory where the emergency braking trajectory starts
        start_emergency_braking_index = i + int(occlusion_constants.EMERGENCY_BRAKING_DELAY * 10)

        # If both colliding vehicles are no longer occluded from each other
        if track_id_b not in factored_game_a and track_id_a not in factored_game_b:

            vehicle_a = create_vehicle_for_emergency_braking(track_id_a, i, time, trajectory_a, scenario)
            vehicle_b = create_vehicle_for_emergency_braking(track_id_b, i, time, trajectory_b, scenario)
            collision_details_with_trajectories = can_collision_be_avoided_neither_car_occluded(current_file_id, vehicle_a, vehicle_b, trajectory_a, trajectory_b, start_emergency_braking_index)

            #This means collision can be avoided
            if type(collision_details_with_trajectories) == bool:
                return collision_details_with_trajectories
            else:
                collision_details = collision_details_with_trajectories[0]
                impact_velocity_a = collision_details[0]
                impact_velocity_b = collision_details[1]
                # collision_details[2] is the time along the emergency braking traj which starts at time + occlusion_constants.EMERGENCY_BRAKING_DELAY along
                # the original trajectories of both vehicles
                time_to_collision = time + occlusion_constants.EMERGENCY_BRAKING_DELAY + collision_details[2]

                collision_details = [track_id_a, track_id_b, collision_id, 'emergency_braking', 'emergency_braking', maneuver_a, maneuver_b, impact_velocity_a, impact_velocity_b, time_to_collision]
                
                print(f"Collision: Neither vehicle is occluded and there was a collision.")

                trajectory_a = trajectory_a[:start_emergency_braking_index] + collision_details_with_trajectories[1][0]
                trajectory_b = trajectory_b[:start_emergency_braking_index] + collision_details_with_trajectories[1][1]

                return (collision_details, (trajectory_a, trajectory_b))

        # If vehicle a is no longer occluded from vehicle b
        # but vehicle b is still occluded from vehicle a
        elif track_id_a not in factored_game_b:
            vehicle_b = create_vehicle_for_emergency_braking(track_id_b, i, time, selected_trajectories[track_id_b], scenario)

            collision_details_with_trajectories = can_collision_be_avoided_one_car_occluded(current_file_id, vehicle_b, trajectory_b, trajectory_a, start_emergency_braking_index)
            #This means collision can be avoided
            if type(collision_details_with_trajectories) == bool:
                return collision_details_with_trajectories
            # If collision_details_with_trajectories is not a bool then it must be a tuple of the collision details
            # Check if we have enough time to emergency brake at next timestep
            # If not, then this is the best we can do, so return this collision.
            next_time = selected_trajectories[track_id_a][i+1][0]
            if i == len(selected_trajectories[track_id_a]) - 1 or next_time + occlusion_constants.EMERGENCY_BRAKING_DELAY >= original_time_to_collision:

                collision_details = collision_details_with_trajectories[0] 
                impact_velocity_a = collision_details[0]
                impact_velocity_b = collision_details[1]
                # Do I add current traj time here?
                time_to_collision = time + occlusion_constants.EMERGENCY_BRAKING_DELAY + collision_details[2] 

                collision_details = [track_id_a, track_id_b, collision_id, maneuver_a, 'emergency_braking', maneuver_a, maneuver_b, impact_velocity_a, impact_velocity_b, time_to_collision]
                
                emergency_braking_traj_b = collision_details_with_trajectories[1]
                trajectory_b = trajectory_b[:start_emergency_braking_index] + emergency_braking_traj_b
                print(f"Collision: vehicle {track_id_a} is no longer occluded from vehicle {track_id_b}.")
                return (collision_details, (trajectory_a, trajectory_b))

            else:
                continue

        # If vehicle b is no longer occluded from vehicle a
        # but vehicle a is still occluded from vehicle b
        elif track_id_b not in factored_game_a:

            vehicle_a = create_vehicle_for_emergency_braking(track_id_a, i, time, selected_trajectories[track_id_a], scenario)

            collision_details_with_trajectories = can_collision_be_avoided_one_car_occluded(current_file_id, vehicle_a, trajectory_a, trajectory_b, start_emergency_braking_index)
            #This means collision can be avoided
            if type(collision_details_with_trajectories) == bool:
                return collision_details_with_trajectories
            # If collision_details_with_trajectories is not a bool then it must be a tuple of the collision details
            # Check if we have enough time to emergency brake at next timestep
            # If not, then this is the best we can do, so return this collision.
            next_time = selected_trajectories[track_id_a][i+1][0]
            if i == len(selected_trajectories[track_id_a]) - 1 or next_time + occlusion_constants.EMERGENCY_BRAKING_DELAY >= original_time_to_collision:

                collision_details = collision_details_with_trajectories[0] 
                impact_velocity_a = collision_details[0]
                impact_velocity_b = collision_details[1]
                time_to_collision = time + occlusion_constants.EMERGENCY_BRAKING_DELAY + collision_details[2] 

                collision_details = [track_id_a, track_id_b, collision_id, 'emergency_braking', maneuver_b, maneuver_a, maneuver_b, impact_velocity_a, impact_velocity_b, time_to_collision]
                
                emergency_braking_traj_a = collision_details_with_trajectories[1]
                trajectory_a = trajectory_a[:start_emergency_braking_index] + emergency_braking_traj_a

                print(f"Collision: vehicle {track_id_b} is no longer occluded from vehicle {track_id_a}.")

                return (collision_details, (trajectory_a, trajectory_b))
            else:
                continue
         

# Time is the time in the trajectory we are currently at
def create_vehicle_for_emergency_braking(track_id, index, time, trajectory, scenario):
    # Time duration until emergency maneuver 
    # If time + occlusion_constants.EMERGENCY_BRAKING_DELAY exceeds trajectory then we just take last timestep in trajectory by using
    # min distance calculation below

    emergency_maneuver_start_index = index + int(occlusion_constants.EMERGENCY_BRAKING_DELAY * 10)
    print(f"emergency_maneuver_start_index: {emergency_maneuver_start_index}")
    vehicle_details = trajectory[emergency_maneuver_start_index]

    for v in scenario:
        if v.id == track_id:
            segment_seq = v.segment_seq
            current_time = v.current_time + time + occlusion_constants.EMERGENCY_BRAKING_DELAY
            leading_vehicle = v.leading_vehicle

    # Get direction for vehicle state objects
    for segment_seq_direction in occlusion_constants.SEGMENT_SEQUENCES:
        if segment_seq == segment_seq_direction[0]:
            direction = segment_seq_direction[1]

    # trajectories: [time, x, y, speed, acceleration, jerk, lateral acceleration, yaw, trajectory length]
    vehicle = vehicle_state.VehicleState()
    vehicle.set_id(track_id)
    vehicle.set_current_time(current_time) 
    vehicle.set_x(vehicle_details[1])
    vehicle.set_y(vehicle_details[2])
    vehicle.set_speed(vehicle_details[3])
    vehicle.set_segment_seq(segment_seq)
    vehicle.set_direction(direction)
    # NOTE: No need to alter leading vehicle as we are only generating wait trajectories
    vehicle.set_leading_vehicle(leading_vehicle)
    angle = scenario_utilities.angle_check(vehicle_details[-2])
    vehicle.set_angle(angle)
    return vehicle

# Takes all collisions, e.g., [[1, -1, 'track_speed', 'proceed-turn', 16.929635360378597, 7.860411808911558, 0.2], [1, -1, 'follow_lead', 'proceed-turn', 16.814012596833372, 7.860411808911558, 0.2]]
# And returns all feasible collisions. Return value is guaranteed to only have at most 1 collision for each unique pair of vehicles.
# If collision between vehicles happen at the same time, take the collision with higher severity
def filter_collisions(ordered_vehicle_ids, all_collisions, emergency_braking_trajectories=None):

    if len(all_collisions) == 1 and emergency_braking_trajectories != None:
        return all_collisions, emergency_braking_trajectories

    elif len(all_collisions) == 1 and emergency_braking_trajectories == None:
        return all_collisions

    true_emergency_braking_trajectories = {}
    for track_id in ordered_vehicle_ids:
        # Get all collisions that contain track id
        filtered_collisions = [collision for collision in all_collisions if track_id in [collision[0], collision[1]]] 
  
        if not filtered_collisions:
            continue

        filtered_collisions.sort(key=lambda x:x[-1])
        earliest_collision_time = filtered_collisions[0][-1]

        # Get indices of all collisions that share the same early crash time
        indices_of_same_collision_time = [index for index in range(len(filtered_collisions)) if filtered_collisions[index][-1] == earliest_collision_time]
        
        # This is the collision that happens the earliest in time and with highest impact speed (across both colliding vehicles)
        most_severe_collision = filtered_collisions[0]

        if len(indices_of_same_collision_time) > 1:
            maximum_recorded_impact_speed = -math.inf
            for i in indices_of_same_collision_time:
                testing_collision = filtered_collisions[i]
                max_impact_speed = max(testing_collision[7], testing_collision[8])
                if max_impact_speed > maximum_recorded_impact_speed:
                    maximum_recorded_impact_speed = max_impact_speed
                    most_severe_collision = filtered_collisions[i]

        #filtered collisions will hold all the collisions that either occurred later or are of a lower severity, so we remove
        # "most_severe_collision" which is the collision that occurred first and with the highest severity
        filtered_collisions.remove(most_severe_collision)

        # We come out of this with one collision involving track_id (even if there are 
        # multiple collisions  at one time and with same impact velocity)
        for removed_collision in filtered_collisions:
            all_collisions.remove(removed_collision)

    if emergency_braking_trajectories != None:
        # This loops through all collisions, but not all collisions have an emergency braking trajectory 
        # So just check for one first.
        for collision in all_collisions:
            collision_id = collision[2]
            # if collision_id in list(emergency_braking_trajectories.keys()):
            true_emergency_braking_trajectories[collision_id] = emergency_braking_trajectories[collision_id]
        return all_collisions, true_emergency_braking_trajectories

    else:
        return all_collisions


def compute_factored_game(hit_counts):
    occluded_vehicles = []
    for target_vehicle_id, hit_count in hit_counts.items():
        if type(hit_count) == int:
            if hit_count <= occlusion_constants.MINIMUM_HIT_COUNT_FOR_OCCLUSION:
                occluded_vehicles.append(target_vehicle_id)
        else:
            # If hit count is equal to -1 it means that the vehicle is out of bounds (outside of the occupancy map boundary)
            # and is occluded.
            if hit_count == -1:
                occluded_vehicles.append(target_vehicle_id)
    return occluded_vehicles


def compute_factored_games(scenario_key=None, vehicle_hit_counts=None):
    if scenario_key != None and vehicle_hit_counts == None:
        vehicle_hit_counts = db_precomputation.retrieve_vehicle_hit_counts(scenario_key)
    occluded_vehicles_dict = {}
    for track_id, hit_counts in vehicle_hit_counts.items():
        occluded_vehicles = compute_factored_game(hit_counts)
        occluded_vehicles_dict[track_id] = occluded_vehicles
    return occluded_vehicles_dict


def can_collision_be_avoided_one_car_occluded(current_file_id, vehicle_a, trajectory_a, trajectory_b, start_emergency_braking_index):
    
    path_a = [(timestep[1], timestep[2]) for timestep in trajectory_a]
    vehicle_a_trajectories = trajectory_generation.generate_trajectories(current_file_id, vehicle_a, 'emergency_braking', vehicle_a.speed, path=path_a, start_emergency_braking_index=start_emergency_braking_index)
    
    if not vehicle_a_trajectories:
        raise Exception(f"Could not generate emergency braking maneuver. Vehicle details: {vehicle_a}")

    trajectories_a = []
    for level, trajectories in vehicle_a_trajectories.items():
        trajectories_a += trajectories

    collision_can_be_avoided = False
    lowest_impact_speed = math.inf
    for trajectory_a in trajectories_a:
        collision_details = scenario_utilities.collision_check(trajectory_a, trajectory_b[start_emergency_braking_index:])

        if not collision_details:
            return True
        else:
            impact_speed = max(collision_details[0], collision_details[1])
            if impact_speed < lowest_impact_speed:
                lowest_impact_speed = impact_speed
                # Assume agent is trying to maximize utility and so minimize impact speed maximizes utility
                ground_truth_collision = collision_details
                emergency_braking_traj_a = trajectory_a

        return (ground_truth_collision, emergency_braking_traj_a)

def can_collision_be_avoided_neither_car_occluded(current_file_id, vehicle_a, vehicle_b, trajectory_a, trajectory_b, start_emergency_braking_index):

    path_a = [(timestep[1], timestep[2]) for timestep in trajectory_a]
    path_b = [(timestep[1], timestep[2]) for timestep in trajectory_b]
    vehicle_a_trajectories = trajectory_generation.generate_trajectories(current_file_id, vehicle_a, 'emergency_braking', vehicle_a.speed, path=path_a, start_emergency_braking_index=start_emergency_braking_index)
    vehicle_b_trajectories = trajectory_generation.generate_trajectories(current_file_id, vehicle_b, 'emergency_braking', vehicle_b.speed, path=path_b, start_emergency_braking_index=start_emergency_braking_index)

    if not vehicle_a_trajectories:
        raise Exception(f"Could not generate a decelerate to stop maneuver for: {vehicle_a.id}")
    if not vehicle_b_trajectories:
        raise Exception(f"Could not generate a decelerate to stop maneuver for: {vehicle_b.id}")

    collision_can_be_avoided = False

    levels_a = list(vehicle_a_trajectories.keys())
    levels_b = list(vehicle_b_trajectories.keys())

    lowest_impact_speed = math.inf
    for level_a in levels_a:
        trajectories_a = vehicle_a_trajectories[level_a]
        for trajectory_a in trajectories_a:
            for level_b in levels_b:
                trajectories_b = vehicle_b_trajectories[level_b]
                for trajectory_b in trajectories_b:
                    collision_details = scenario_utilities.collision_check(trajectory_a, trajectory_b)

                    if not collision_details:
                        return True
                    else:
                        impact_speed = max(collision_details[0], collision_details[1])
                        if impact_speed < lowest_impact_speed:
                            lowest_impact_speed = impact_speed
                            # Assume agent is trying to maximize utility and so minimize impact speed maximizes utility
                            ground_truth_collision = collision_details

                            emergency_braking_traj_a = trajectory_a
                            emergency_braking_traj_b = trajectory_b

    return (ground_truth_collision, (emergency_braking_traj_a, emergency_braking_traj_b))

