
'''
Occlusion Scenario Builder

Created on May 10, 2021

@author: Maximilian Kahn
'''

from utilities import occlusion_constants, scenario_utilities, occupancy_map, raycast, vehicle_state
from trajectories import trajectory_generation
import sqlite3
import ast
from scipy import interpolate
import copy
import math
import numpy as np
import random
from shapely.geometry import Point, LineString
from shapely.geometry.polygon import Polygon
from matplotlib import pyplot as plt


def compute_angle(p1,p2):
    x = p2[0] - p1[0]
    y = p2[1] - p1[1]
    angle = math.atan2(y,x)
    if angle < 0:
        angle = angle + 2 * occlusion_constants.PI
    return angle


"""
    make_circle: Outputs points on a circle with the given radius centred
    at (x_offset, y_offset). The number n determines how many equally
    spaced points will be created on the circle.
"""
def make_circle(radius,x_offset,y_offset,n=occlusion_constants.FOV_CIRCLE_POINTS):
    return [(math.cos(2*occlusion_constants.PI/n*i)*radius + x_offset, math.sin(2*occlusion_constants.PI/n*i)*radius + y_offset) for i in range(0,n+1)]


def angle_check(angle):
    if angle > 2 * occlusion_constants.PI:
        return angle - 2 * occlusion_constants.PI
    elif angle < 0:
        return angle + 2 * occlusion_constants.PI
    else:
        return angle


def filter_centreline_points(subject_vehicle, relevant_vehicles, raycast_polygon_points, vehicle_bounding_box_polygons, current_time, vehicle_type):
    # Relevant vehicles + subject vehicles
    number_of_vehicles = len(relevant_vehicles) + 1
    sequences_and_centrelines = scenario_utilities.get_sequences_and_centrelines(subject_vehicle, current_time)

    polygon_fov_list = []
    for points in raycast_polygon_points:
        # Triangle polygon fov region (so we can check if the occluding vehicle centre point is within this region)
        p0 = points[0][0] / occlusion_constants.MAP_SCALE
        p1 = points[0][1] / occlusion_constants.MAP_SCALE
        p2 = points[1][0] / occlusion_constants.MAP_SCALE
        p3 = points[1][1] / occlusion_constants.MAP_SCALE
        polygon = Polygon([(subject_vehicle.x, subject_vehicle.y), (p0, p1), (p2, p3)])
        polygon_fov_list.append(polygon)

    # Each key is a sequence and the value is a list of tuples of coordinate and angle
    filtered_points = {}
  
    for sequence_and_centreline in sequences_and_centrelines:
        sequence = sequence_and_centreline[0]
        centreline = sequence_and_centreline[1]
        last_point_within = False

        for i in range(len(centreline)):
            p = centreline[i]
            point = Point(p)
            counter = 0

            for polygon in polygon_fov_list:
                if polygon.contains(point):

                    angle = compute_angle_on_centreline(i, centreline)
                    # Add point to the sequence in the dictionary
                    if str(sequence) in list(filtered_points.keys()):
                        filtered_points[str(sequence)].append((p, angle))
                    else:
                        filtered_points[str(sequence)] = []
                        filtered_points[str(sequence)].append((p, angle))

                    if not last_point_within and i > 0:
                        filtered_points[str(sequence)].append((centreline[i-1], angle))

                    # If point is in even one of the polygons we include it.
                    last_point_within = True
                    break
                else:
                    counter += 1
                # This should mean all the rest of the points are not in region since the lines are curved but mostly straight
                if counter == len(polygon_fov_list) - 1:
                    if last_point_within == True:
                        filtered_points[str(sequence)].append((p, angle))

                    last_point_within = False


    # Filter out occluding vehicles that are too close to the vehicles in the scenario
    candidate_occluding_vehicles = []

    for sequence, potential_spawn_points in filtered_points.items():
        sequence = ast.literal_eval(sequence)
        for potential_spawn_point in potential_spawn_points:
            # total_number_of_centreline_points += len(potential_spawn_point)
            point = potential_spawn_point[0]
            angle = potential_spawn_point[1]

            occluding_vehicle_type = vehicle_type
            occluding_vehicle_id = -1
            occluding_vehicle = vehicle_state.VehicleState(occluding_vehicle_type)
            occluding_vehicle.set_id(occluding_vehicle_id)
            occluding_vehicle.set_x(point[0])
            occluding_vehicle.set_y(point[1])

            centreline = scenario_utilities.get_centreline_from_segment_sequence(sequence)
            path_origin = centreline[0]
            occluding_vehicle.set_path_origin(path_origin)

            occluding_vehicle.set_segment_seq(sequence)
            occluding_vehicle.set_angle(angle)
            occluding_vehicle.set_current_time(current_time)
            occluding_vehicle.set_gate_crossing_times((None, None))

            # Set direction
            direction = None
            for segment_sequence in occlusion_constants.SEGMENT_SEQUENCES:
                if sequence == segment_sequence[0]:
                    direction = segment_sequence[1]
                    occluding_vehicle.set_direction(direction)
                    break
            if direction == None:
                raise(f"Cannot find direction for occluding vehicle with sequence: {sequence}")

            signal = scenario_utilities.get_traffic_signal(current_time, direction)

            occluding_vehicle.set_traffic_light(signal)

            traffic_region_list = None
            segment = scenario_utilities.assign_curent_segment(traffic_region_list,occluding_vehicle,simulation=False)

            occluding_vehicle.set_current_segment(segment)

            occluding_vehicle_polygon = Polygon(occluding_vehicle.get_bounding_box_points(for_occ_map=False))

            counter = 0
            for polygon in vehicle_bounding_box_polygons:
                if occluding_vehicle_polygon.distance(polygon) >= occlusion_constants.MINIMUM_SPAWNING_DISTANCE:
                    counter += 1
                else:
                    break

            if counter == number_of_vehicles:
                candidate_occluding_vehicles.append(occluding_vehicle)

    return candidate_occluding_vehicles


# This returns the occlusion level for the input subject vehicle and all relevant vehicles (including occluding vehicle too)
def compute_hit_counts(subject_vehicle, relevant_vehicles):

    scenario = [subject_vehicle] + relevant_vehicles
    occ_map = occupancy_map.OccupancyMap()
    out_of_bounds_vehicles = occ_map.make_occupancy_map(scenario)

    naturally_occluded_out_of_bounds_vehicles = []
    # Checking if out of bounds vehicles are subject to natural occlusion. I.e., another relevant vehicle is occluding them.
    for v in relevant_vehicles:
        if v.id in out_of_bounds_vehicles:
            print(f"Performing an occlusion check on the out of bounds vehicle {v.id}")
            line = LineString([(subject_vehicle.x, subject_vehicle.y), (v.x, v.y)])
            for v2 in relevant_vehicles:
                if v.id != v2.id:
                    relevant_vehicle_polygon = Polygon(v2.get_bounding_box_points(for_occ_map=False))
                    if relevant_vehicle_polygon.intersects(line):
                        naturally_occluded_out_of_bounds_vehicles.append(v.id)


    # Computing attention values for each vehicle in the scenario excluding all out of bounds vehicles
    relevant_vehicle_attention = {}
    if len(relevant_vehicles) > 1:
        minimum_distance_to_sv = math.inf
        # Stores relevant vehicle distances to the subject vehicle
        relevant_vehicle_distances = {}
        distance_summed_to_subject_vehicle = 0
        relevant_vehicle_distances_order = []
        for v in relevant_vehicles:
            # Here we only compute attention for those vehicles in the map
            dist_to_sv = scenario_utilities.compute_distance((subject_vehicle.x, subject_vehicle.y), (v.x, v.y))
            relevant_vehicle_distances[v.id] = dist_to_sv
            distance_summed_to_subject_vehicle += dist_to_sv
            if dist_to_sv < minimum_distance_to_sv:
                minimum_distance_to_sv = dist_to_sv
   
        # Catch if all relevant agents are too far away
        if minimum_distance_to_sv >= occlusion_constants.MAXIMUM_DISTANCE_FROM_SUBJECT_VEHICLE:
            raise Exception(f"Closest relevant vehicle is farther away than the maximum distance, {occlusion_constants.MAXIMUM_DISTANCE_FROM_SUBJECT_VEHICLE}, to the subject vehicle.")

        attention_total = 0
        for id, distance in relevant_vehicle_distances.items():
            attention = (distance_summed_to_subject_vehicle - distance) / distance_summed_to_subject_vehicle
            attention_total += attention
            relevant_vehicle_attention[id] = attention

        # Normalize attention values
        for id, attention in relevant_vehicle_attention.items():
            relevant_vehicle_attention[id] = relevant_vehicle_attention[id] / attention_total
    else:
        relevant_vehicle_attention[relevant_vehicles[0].id] = 1

        # If vehicle in naturally occluded out of bounds ,then factor out. Else include in game.
        print(naturally_occluded_out_of_bounds_vehicles)
        print(relevant_vehicle_attention)

    subject_vehicle_raycasts = []
    raycast_polygon_points = []

    for v in relevant_vehicles:
        subject_vehicle_raycast, right_point_index, left_point_index = compute_raycast(subject_vehicle, v, relevant_vehicle_attention[v.id])
        raycast_polygon_points.append([subject_vehicle_raycast[right_point_index][-1], subject_vehicle_raycast[left_point_index][-1]])
        subject_vehicle_raycasts += subject_vehicle_raycast

    occ_map.add_raycast(subject_vehicle_raycasts, subject_vehicle.id)

    hit_counts = occ_map.compute_hit_count(subject_vehicle_raycasts, subject_vehicle.id, relevant_vehicles)


    # If hit count is equal to -1 it means that the vehicle is out of bounds (outside of the occupancy map boundary)
    # and is occluded.
    for track_id in naturally_occluded_out_of_bounds_vehicles:
        hit_counts[track_id] = -1

    return hit_counts

def spawn_occluding_vehicle(subject_vehicle, relevant_vehicles, current_time, occluding_vehicle_type='car'):
 
     # Compute polygons for the subject vehicle and each relevant vehicle in the scenario
    vehicle_bounding_box_polygons = [Polygon(subject_vehicle.get_bounding_box_points(for_occ_map=False))]
    for vehicle in relevant_vehicles:
        bounding_box_polygon = Polygon(vehicle.get_bounding_box_points(for_occ_map=False))
        vehicle_bounding_box_polygons.append(bounding_box_polygon)

    scenario = [subject_vehicle] + relevant_vehicles
    unoccluded_occ_map = occupancy_map.OccupancyMap()
    out_of_bounds_vehicles = unoccluded_occ_map.make_occupancy_map(scenario)

    naturally_occluded_out_of_bounds_vehicles = []
    # Checking if out of bounds vehicles are subject to natural occlusion. I.e., another relevant vehicle is occluding them.
    for v in relevant_vehicles:
        if v.id in out_of_bounds_vehicles:
            print(f"Performing an occlusion check on the out of bounds vehicle {v.id}")
            line = LineString([(subject_vehicle.x, subject_vehicle.y), (v.x, v.y)])
            for v2 in relevant_vehicles:
                if v.id != v2.id:
                    relevant_vehicle_polygon = Polygon(v2.get_bounding_box_points(for_occ_map=False))
                    if relevant_vehicle_polygon.intersects(line):
                        naturally_occluded_out_of_bounds_vehicles.append(v.id)

    # Computing attention values for each vehicle in the scenario excluding all out of bounds vehicles
    relevant_vehicle_attention = {}
    # if len(relevant_vehicles) - len(out_of_bounds_vehicles) > 1:
    if len(relevant_vehicles) > 1:
        minimum_distance_to_sv = math.inf
        # Stores relevant vehicle distances to the subject vehicle
        relevant_vehicle_distances = {}
        distance_summed_to_subject_vehicle = 0
        relevant_vehicle_distances_order = []
        for v in relevant_vehicles:
            # Here we only compute attention for those vehicles in the map
            dist_to_sv = scenario_utilities.compute_distance((subject_vehicle.x, subject_vehicle.y), (v.x, v.y))
            relevant_vehicle_distances[v.id] = dist_to_sv
            distance_summed_to_subject_vehicle += dist_to_sv
            if dist_to_sv < minimum_distance_to_sv:
                minimum_distance_to_sv = dist_to_sv
   
        # Catch if all relevant agents are too far away
        if minimum_distance_to_sv >= occlusion_constants.MAXIMUM_DISTANCE_FROM_SUBJECT_VEHICLE:
            raise Exception(f"Closest relevant vehicle is farther away than the maximum distance, {occlusion_constants.MAXIMUM_DISTANCE_FROM_SUBJECT_VEHICLE}, to the subject vehicle.")

        attention_total = 0
        for id, distance in relevant_vehicle_distances.items():
            attention = (distance_summed_to_subject_vehicle - distance) / distance_summed_to_subject_vehicle
            attention_total += attention
            relevant_vehicle_attention[id] = attention

        # Normalize attention values
        for id, attention in relevant_vehicle_attention.items():
            relevant_vehicle_attention[id] = relevant_vehicle_attention[id] / attention_total
    else:
        relevant_vehicle_attention[relevant_vehicles[0].id] = 1

    subject_vehicle_raycasts = []
    raycast_polygon_points = []

    for v in relevant_vehicles:
        subject_vehicle_raycast, right_point_index, left_point_index = compute_raycast(subject_vehicle, v, relevant_vehicle_attention[v.id])
        raycast_polygon_points.append([subject_vehicle_raycast[right_point_index][-1], subject_vehicle_raycast[left_point_index][-1]])
        subject_vehicle_raycasts += subject_vehicle_raycast

    unoccluded_occ_map.add_raycast(subject_vehicle_raycasts, subject_vehicle.id)
    unnoccluded_hit_counts = unoccluded_occ_map.compute_hit_count(subject_vehicle_raycasts, subject_vehicle.id, relevant_vehicles)



    # Returns all the centreline points within the subject vehicle's FOV region and not intersecting with one of the vehicle's in the scenario
    candidate_occluding_vehicles = filter_centreline_points(subject_vehicle, relevant_vehicles, raycast_polygon_points, vehicle_bounding_box_polygons, current_time, occluding_vehicle_type)

    print(f"NUMBER OF CANDIDATE OCCLUDING VEHICLES: {len(candidate_occluding_vehicles)}")

    naturally_occluded_vehicles = []
    occluding_vehicles = []
    out_of_bounds_vehicles_dict = {}

    # Perform a "cheap" raycast to see if occluding vehicle occludes relevant vehicle
    for v in relevant_vehicles:
        # Get the naturally occluded vehicles
        if unnoccluded_hit_counts[v.id] == 0:

            naturally_occluded_vehicles.append(v.id)

        if v.id not in naturally_occluded_vehicles and v.id not in naturally_occluded_out_of_bounds_vehicles:

            line = LineString([(subject_vehicle.x, subject_vehicle.y), (v.x, v.y)])

            for occluding_vehicle in candidate_occluding_vehicles:

                occluding_vehicle_polygon = Polygon(occluding_vehicle.get_bounding_box_points(for_occ_map=False))

                if occluding_vehicle_polygon.intersects(line) and str(v.segment_seq) != str(occluding_vehicle.segment_seq):

                    occluding_vehicles.append(occluding_vehicle)

                    if v.id in out_of_bounds_vehicles:

                        print(f"Note: found an occluding vehicle for the out of bounds vehicle {v.id}.")

                        if (occluding_vehicle.x, occluding_vehicle.y) in list(out_of_bounds_vehicles_dict.keys()):
                            out_of_bounds_vehicles_dict[(occluding_vehicle.x, occluding_vehicle.y)].append(v.id)
                        else: 
                            out_of_bounds_vehicles_dict[(occluding_vehicle.x, occluding_vehicle.y)] = []
                            out_of_bounds_vehicles_dict[(occluding_vehicle.x, occluding_vehicle.y)].append(v.id)


    print(f"NUMBER OF OCCLUDING VEHICLES: {len(occluding_vehicles)}")
    occlusion_levels = {}

    # This stores the coordinates of the bounding box for the occluding vehicle
    previous_occluding_vehicle_coordinates = []
    # Rename for clarity
    occluded_occ_map = unoccluded_occ_map

    for occluding_vehicle in occluding_vehicles:
        occlusion_levels[(occluding_vehicle.x, occluding_vehicle.y)] = {}
        # Remove the bounding box for the previous occluding vehicle from the grid.
        if previous_occluding_vehicle_coordinates:
            for coordinate in previous_occluding_vehicle_coordinates:
                occluded_occ_map.grid[coordinate] = [[],[]]

        _, previous_occluding_vehicle_coordinates = occluded_occ_map.make_occupancy_map([occluding_vehicle],pass_bb_box_back=True)
        
        relevant_vehicles.append(occluding_vehicle)
        occluded_occ_map.add_raycast(subject_vehicle_raycasts, subject_vehicle.id)
        occluded_hit_counts = occluded_occ_map.compute_hit_count(subject_vehicle_raycasts, subject_vehicle.id, relevant_vehicles)


        relevant_vehicles.pop()

        for unnoccluded_info, occluded_info in zip(unnoccluded_hit_counts.items(), occluded_hit_counts.items()):
            # Make sure the compared hit counts come from the same vehicle
            if unnoccluded_info[0] != occluded_info[0]:
                raise Exception(f"Error: Comparing hit counts for two difference vehicle ids: {unnoccluded_info[0]} and {occluded_info[0]}.")

            # If the relevant vehicle (whose id is denoted as unnoccluded_info[0]) is out of bounds then we compute occlusion as either 1 
            # if an occluding vehicle exists or 0 otherwise using a cheap raycast (i.e., a line intersection check). If the relevant vehicle
            # if naturally occluded then its unnoccluded hit count will be 0 so we manually set occlusion level to 0.
            if unnoccluded_info[0] not in out_of_bounds_vehicles and unnoccluded_info[0] not in naturally_occluded_out_of_bounds_vehicles and \
               unnoccluded_info[0] not in naturally_occluded_vehicles:

                occlusion_levels[(occluding_vehicle.x, occluding_vehicle.y)][unnoccluded_info[0]] = (unnoccluded_info[1] - occluded_info[1]) / unnoccluded_info[1]

        # Append the occluding vehicle hit count so we have this information to work with.
        occlusion_levels[(occluding_vehicle.x, occluding_vehicle.y)][occluding_vehicle.id] = occluded_hit_counts[-1]


        # Add the occluded out of bounds relevant vehicles here to the occlusion_levels dict
        for occluding_vehicle_coordinates in list(out_of_bounds_vehicles_dict.keys()):
            if occluding_vehicle_coordinates not in list(occlusion_levels.keys()):
                occlusion_levels[occluding_vehicle_coordinates] = {}
            for occluded_relevant_vehicle_ids in list(out_of_bounds_vehicles_dict.values()):
                for occluded_relevant_vehicle_id in occluded_relevant_vehicle_ids:
                    occlusion_levels[occluding_vehicle_coordinates][occluded_relevant_vehicle_id] = 1.0

        if len(list(occlusion_levels.keys())) == 0:
            if len(naturally_occluded_vehicles) != 0 or len(naturally_occluded_out_of_bounds_vehicles) != 0:
                occlusion_levels["None"] = {}
                for id in naturally_occluded_vehicles:
                    occlusion_levels["None"][id] = 1.0
                for id in naturally_occluded_out_of_bounds_vehicles:
                    occlusion_levels["None"][id] = 1.0
        else:
            # Add naturally occluded out of bounds vehicles to the occlusion levels dictionary.
            for occluding_vehicle_coordinates in list(occlusion_levels.keys()):
                for id in naturally_occluded_vehicles:
                    occlusion_levels[occluding_vehicle_coordinates][id] = 1.0
                for id in naturally_occluded_out_of_bounds_vehicles:
                    occlusion_levels[occluding_vehicle_coordinates][id] = 1.0

    print(f"OCCLUSION LEVELS: {occlusion_levels}")
    print(f"-----------------------------------\n")

    return occluding_vehicles, occlusion_levels


def generate_occlusion_scenarios():

        time_list = scenario_utilities.generate_time_list()

        for current_time in time_list:
            print(f"CURRENT_TIME")

            subject_vehicle_list = scenario_utilities.retrieve_subject_vehicle_list(current_time)

            for subject_vehicle_id in subject_vehicle_list:

                subject_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(subject_vehicle_id[0], current_time)
                relevant_vehicles = scenario_utilities.retrieve_relevant_vehicles(subject_vehicle_id[0], current_time)

                if not relevant_vehicles:
                    continue

                occluding_vehicles, occlusion_levels = spawn_occluding_vehicle(subject_vehicle, relevant_vehicles, current_time, occluding_vehicle_type='car')




def compute_angle_on_centreline(current_index, centreline):
    # Get current point and next point if not last point
    if current_index == len(centreline) - 1:
        spawn_point = centreline[current_index]
        p2 = centreline[current_index - 1]
        angle = compute_angle(p2, spawn_point,)
    else:
        spawn_point = centreline[current_index]
        p2 = centreline[current_index + 1]
        angle = compute_angle(spawn_point, p2)
    return angle

# Computes the raycasts from vehicle1 to vehicle2
def compute_raycast(vehicle1, vehicle2, attention):
    p0 = (vehicle1.x, vehicle1.y)
    p1 = (vehicle2.x, vehicle2.y)
    v1_v2_angle = compute_angle(p0,p1)
    vehicle1_x = int(round(vehicle1.x)) * occlusion_constants.MAP_SCALE
    vehicle1_y = int(round(vehicle1.y)) * occlusion_constants.MAP_SCALE
 
    v1_fov = make_circle(vehicle1.fov_distance, vehicle1_x, vehicle1_y)

    # Make sure attention reduction is working!
    v1_min_angle = int(round(angle_check(v1_v2_angle - occlusion_constants.FOV_BUDGET * attention) * 180 / occlusion_constants.PI)) * occlusion_constants.FOV_CIRCLE_POINTS_SCALE
    v1_max_angle = int(round(angle_check(v1_v2_angle + occlusion_constants.FOV_BUDGET * attention) * 180 / occlusion_constants.PI)) * occlusion_constants.FOV_CIRCLE_POINTS_SCALE

    if v1_min_angle > v1_max_angle and v1_min_angle != occlusion_constants.FOV_CIRCLE_POINTS:

        v1_fov_0_to_max = v1_fov[0:v1_max_angle]
        v1_fov_min_to_end = v1_fov[v1_min_angle:-1]

        v1_fov_min_to_end.reverse()

        # This goes from 0 to max_angle-1 (since we are starting at 0), and then from the last index (-1) to min_angle-1
        v1_fov = v1_fov_0_to_max + v1_fov_min_to_end

        # These two points are to record the end points for the raycast polygon.
        right_point_index = v1_max_angle-1
        left_point_index = len(v1_fov)-1
    
    else:
        # Since 720 is the same as 0, we set min angle to 0. 
        if v1_min_angle == occlusion_constants.FOV_CIRCLE_POINTS:
            v1_min_angle = 0

        v1_fov = v1_fov[v1_min_angle:v1_max_angle]
        # These two points are to record the end points for the raycast polygon.
        right_point_index = 0
        left_point_index = len(v1_fov)-1

    vehicle1_raycasts = []
    for point in v1_fov:
        fov_x = int(round(point[0]))
        fov_y = int(round(point[1]))
        ray = raycast.compute_FVTA_line(vehicle1_x, vehicle1_y, fov_x, fov_y)
        vehicle1_raycasts.append(ray)

    return vehicle1_raycasts, right_point_index, left_point_index


def generate_scenarios(current_time):
    base_scenario = scenario_utilities.get_all_vehicles_at_current_time(current_time)

    scenario_list = []
    # Stores the leading vehicles for the subject vehicle
    leading_vehicles = {}
    for v in base_scenario:

        try:
            leading_vehicles[v.id] = v.leading_vehicle.id
        except:
            leading_vehicles[v.id] = None
      
        relevant_vehicles = scenario_utilities.get_relevant_vehicles(v)

        reduced_relevant_vehicles_ids = scenario_utilities.reduce_relev_agents(v.id, current_time, relevant_vehicles)
 
        reduced_relevant_vehicles_ids.insert(0,v.id)

        scenario_list.append(reduced_relevant_vehicles_ids)

    return scenario_list, leading_vehicles



