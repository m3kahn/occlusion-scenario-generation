# Dynamic-Occlusion-Aware Risk Identification for Autonomous Vehicles Using Hypergames

Perform dynamic occlusion risk identification for strategic planners by (1) automatically generating dynamic occlusion situations by injecting synthetic occluding vehicles into naturalistic traffic scenes and (2) evaluating strategic planner performance on generated dynamic occlusion situations. We use the theory of hypergames to incorporate occlusion into the game-theoretic simulation. For more details about our approach, please refer to our paper, [I Know You Can't See Me: Dynamic Occlusion-Aware Safety Validation of Strategic Planners for Autonomous Vehicles Using Hypergames](https://arxiv.org/abs/2109.09807).

# Project Overview

This project is split into multiple directories.

1. [occlusion-scenarios](https://git.uwaterloo.ca/m3kahn/occlusion-scenario-generation/-/tree/master/occlusion_scenario): contains functions for injecting synthetic occluding vehicles into naturalistic traffic scenes to create realistic dynamic occlusion situations.
2. [precomputation](https://git.uwaterloo.ca/m3kahn/occlusion-scenario-generation/-/tree/master/precomputation): contains wrapper functions used to generate and save intersection map centreline points, vehicle trajectories, relevant vehicles for each partial scene, etc., to the database. 
3. [game](https://git.uwaterloo.ca/m3kahn/occlusion-scenario-generation/-/tree/master/game): contains functions used to solve the level-0 and level-1 hypergames.
4. [trajectories](https://git.uwaterloo.ca/m3kahn/occlusion-scenario-generation/-/tree/master/trajectories): contains the function that is used to generate trajectories for each available manoeuvre in the traffic game.
5. [utilities](https://git.uwaterloo.ca/m3kahn/occlusion-scenario-generation/-/tree/master/utilities): contains utility functions used in the project.
6. [OCC Videos](https://git.uwaterloo.ca/m3kahn/occlusion-scenario-generation/-/tree/master/OCC%20Videos): contains video of the occlusion-caused collisions we recorded while running simulation testing.
7. [Speed Distributions](https://git.uwaterloo.ca/m3kahn/occlusion-scenario-generation/-/tree/master/Speed%20Distributions): contains images of the speed distributions, conditioned on factors like traffic light state and if there is a vehicle present in a conflicting lane, for each lanelet in the intersection map.


# Installation Instructions and Minimal Working Example

Installation instructions and a minimal working example of the project (which includes retrieving an occlusion situation from the database, generating trajectories for each vehicle, and finding the set of Nash Equilibria for the traffic game) can all be found in the [tutorial](https://git.uwaterloo.ca/m3kahn/occlusion-scenario-generation/-/blob/master/tutorial.py).



