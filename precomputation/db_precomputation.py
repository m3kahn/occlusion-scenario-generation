
'''
Used to precompute trajectories, relevant vehicles, etc., for DOR-based risk identification

Created on May 10, 2021

@author: Maximilian Kahn
'''

from occlusion_scenario import occlusion_scenario_builder
from utilities import occlusion_constants, scenario_utilities, occupancy_map
from trajectories import trajectory_generation
from occlusion_scenario import occlusion_scenario_builder
from game import game_utilities
from equilibria import equilibria_core
from matplotlib import pyplot as plt
import numpy as np
from scipy import interpolate
import sqlite3
import ast
import itertools


def filter_path(path, threshold_distance):
    if len(path) <= 2:
        return path

    removed_points = []
    not_done = False
    index = 0
    not_done = True

    while not_done:
        if scenario_utilities.compute_distance(path[index],path[index + 1]) < threshold_distance:
            next_index = index + 1
            removed_points.append(path[next_index])
            next_index_too_close = True

            while next_index_too_close:

                if next_index != len(path) - 1:
                    if scenario_utilities.compute_distance(path[index],path[next_index + 1]) < threshold_distance:
                        next_index += 1
                        removed_points.append(path[next_index])
                    else:
                        next_index_too_close = False
                else:
                    next_index_too_close = False

            for point in removed_points:
                path.remove(point)

            removed_points.clear()

        if index >= len(path) - 2:
            not_done = False
        else:
            index += 1
    return path


# Returns a set of x,y coordinates (determined by number_of_points) that represent the centreline.
def generate_centreline(traffic_region_sequence, step=0.5, threshold_distance=10):
    x_points = []
    y_points = []
    with sqlite3.connect(occlusion_constants.DB) as conn:
        c = conn.cursor()
        x_points = []
        y_points = []
        for seg in traffic_region_sequence:
            
            elements = "X_POSITIONS, Y_POSITIONS"
            execute_cmd = f"SELECT {elements} FROM TRAFFIC_REGIONS_DEF WHERE NAME='{seg}' AND REGION_PROPERTY='center_line'"

            c.execute(execute_cmd)
            points_unformatted = c.fetchall()

            x = ast.literal_eval(points_unformatted[0][0])
            y = ast.literal_eval(points_unformatted[0][1])

            # For merging two lists use the += operator
            x_points += x
            y_points += y
    last_point_on_path = (x_points[-1], y_points[-1])
    indices = np.arange(1,len(x_points)+1)
    cs_x = interpolate.CubicSpline(indices, x_points)
    cs_y = interpolate.CubicSpline(indices, y_points)
    indices = np.arange(1, len(indices), step)
    x_plot_points = cs_x(indices)
    y_plot_points = cs_y(indices)
    # Testing filtering path (i.e., removing points that are too close together)
    path = list(zip(x_plot_points, y_plot_points))
    path = filter_path(path, threshold_distance)
    # Make sure last point from original path is included in centreline path.
    if path[-1] != last_point_on_path:
        path.append(last_point_on_path)
    return path


def regenerate_centreline(centreline, step=0.1, threshold_distance=2):
    last_point_on_path = centreline[-1]
    indices = np.arange(1,len(centreline)+1)
    x_points = [i[0] for i in centreline]
    y_points = [i[1] for i in centreline]
    cs_x = interpolate.CubicSpline(indices, x_points)
    cs_y = interpolate.CubicSpline(indices, y_points)
    indices = np.arange(1, len(indices), step)
    x_plot_points = cs_x(indices)
    y_plot_points = cs_y(indices)
    # Testing filtering path (i.e., removing points that are too close together)
    path = list(zip(x_plot_points, y_plot_points))
    path = filter_path(path, threshold_distance)
    return path


def generate_and_save_centrelines():
    conn = sqlite3.connect(occlusion_constants.DB)
    cur = conn.cursor()
    for sequence in occlusion_constants.SEGMENT_SEQUENCES:
        name = f'{sequence[0]}'
        shape = f"line"
        region_property = f"segment_seq_center_line"
        if sequence[0] == ['ln_s_2', 'prep-turn_s', 'exec-turn_s', 'ln_w_-1']:
            centreline = generate_centreline(sequence[0], occlusion_constants.CENTRELINE_STEPSIZE, occlusion_constants.CENTRELINE_POINTS_THRESHOLD_DISTANCE)
            centreline = regenerate_centreline(centreline)
            centreline = filter_path(centreline, 12)
            centreline = centreline[0:1] + [(538846.5, 4813998), (538835, 4814006)] + centreline[3:5] + [(538788, 4813992)]
            centreline = regenerate_centreline(centreline)
        elif sequence[0] == ['ln_s_1', 'prep-turn_s', 'exec-turn_s', 'ln_w_-2']:
            centreline = generate_centreline(sequence[0], occlusion_constants.CENTRELINE_STEPSIZE, occlusion_constants.CENTRELINE_POINTS_THRESHOLD_DISTANCE)
            centreline = regenerate_centreline(centreline)
            centreline = filter_path(centreline, 12)
            centreline_addition = [(538825, 4814010), (538815, 4814009), (538805, 4814006), (538798, 4814002), (538787.5, 4813997)]
            centreline = centreline[0:3] + centreline_addition
            centreline = regenerate_centreline(centreline)
        else:
            centreline = generate_centreline(sequence[0], occlusion_constants.CENTRELINE_STEPSIZE, occlusion_constants.CENTRELINE_POINTS_THRESHOLD_DISTANCE)
            centreline = regenerate_centreline(centreline)
            if sequence[0] == ['ln_s_1', 'prep-turn_s', 'exec-turn_s', 'ln_w_-1']:
                centreline += [(538788, 4813992)]
        x_points = [i[0] for i in centreline]
        y_points = [i[1] for i in centreline]
        x_points = f"{x_points}"
        y_points = f"{y_points}"
        cur.execute("INSERT INTO TRAFFIC_REGIONS_DEF (NAME,SHAPE,REGION_PROPERTY,X_POSITIONS,Y_POSITIONS) VALUES (?,?,?,?,?)", (name,shape,region_property,x_points,y_points))
    conn.commit()
    conn.close()
    return


def generate_and_save_relevant_agents():
    conn = sqlite3.connect(occlusion_constants.DB)
    cur = conn.cursor()
    # Create relevant agents table in database
    cmd = f"CREATE TABLE RELEVANT_AGENTS(TRACK_ID INT, TIME NUMERIC, LEAD_VEHICLE INT, RELEV_AGENT_IDS TEXT)"   
    cur.execute(cmd)
    time_list = scenario_utilities.generate_time_list()
    index = 0 
    for i in range(len(time_list)):
        time = time_list[i]
        # Generate scenarios should be moved here too
        scenario_list, leading_vehicles = occlusion_scenario_builder.generate_scenarios(time)
        for scenario in scenario_list:
            track_id = scenario[0]
            lead_vehicle = None
            relev_agent_ids = None

            if len(scenario) > 1:
                relev_agent_ids = scenario[1:]
                relev_agent_ids = f"{relev_agent_ids}"
                lead_vehicle = leading_vehicles[track_id]

            cur.execute("INSERT INTO RELEVANT_AGENTS (TRACK_ID, TIME, LEAD_VEHICLE, RELEV_AGENT_IDS) VALUES (?,?,?,?)", (track_id, time, lead_vehicle, relev_agent_ids))
        index += 1  
        print(f"FINISHED COMPUTING SCENARIO FOR TIME {time}; {round(index / len(time_list) * 100, 2)}% PERCENT COMPLETED DATABASE ID {occlusion_constants.CURRENT_FILE_ID}.")
        conn.commit()
    conn.close()
    return


# This assumes the table has already been created
# The input time must be an element in time_list
def generate_and_save_relevant_agents_from_time(time):
    conn = sqlite3.connect(occlusion_constants.DB)
    cur = conn.cursor()
    time_list = scenario_utilities.generate_time_list()
    index = time_list.index(time)
    time_list = time_list[index:]
    print(time_list)
    index = 0 
    for i in range(len(time_list)):
        time = time_list[i]
        # Generate scenarios should be moved here too
        scenario_list, leading_vehicles = occlusion_scenario_builder.generate_scenarios(time)
        for scenario in scenario_list:
            track_id = scenario[0]
            lead_vehicle = None
            relev_agent_ids = None

            if len(scenario) > 1:
                relev_agent_ids = scenario[1:]
                relev_agent_ids = f"{relev_agent_ids}"
                lead_vehicle = leading_vehicles[track_id]
            cur.execute("INSERT INTO RELEVANT_AGENTS (TRACK_ID, TIME, LEAD_VEHICLE, RELEV_AGENT_IDS) VALUES (?,?,?,?)", (track_id, time, lead_vehicle, relev_agent_ids))
        index += 1  
        print(f"FINISHED COMPUTING SCENARIO FOR TIME {time}; {round(index / len(time_list) * 100, 2)}% PERCENT COMPLETED DATABASE ID {occlusion_constants.CURRENT_FILE_ID}.")
        conn.commit()
    conn.close()
    return


def generate_and_save_occluding_vehicles(vehicle_type):
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()

    cmd = f"CREATE TABLE OCCLUDING_VEHICLES(TIME NUMERIC, X NUMERIC, Y NUMERIC, ANGLE NUMERIC, " \
          f"ASSIGNED_SEGMENT TEXT, SEGMENT_SEQ TEXT, DIRECTION TEXT, TRAFFIC_SIGNAL TEXT, TYPE TEXT, SUBJECT_VEHICLE_ID INT, OCCLUSION_LEVEL TEXT)"
    cur.execute(cmd)

    time_list = scenario_utilities.generate_time_list()
    index = 0
    for current_time in time_list:
        subject_vehicle_list = scenario_utilities.retrieve_subject_vehicle_list(current_time)   

        for id in subject_vehicle_list:

            subject_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(id[0], current_time)
            relevant_vehicles = scenario_utilities.retrieve_relevant_vehicles(id[0], current_time)

            # If there are no valid relevant_vehicles (i.e., we do not include vehicles that move from East to North in this analysis)
            if not relevant_vehicles:
                continue
                
            occluding_vehicles, occlusion_levels = occlusion_scenario_builder.spawn_occluding_vehicle(subject_vehicle, relevant_vehicles, current_time, occluding_vehicle_type=vehicle_type)

            if occluding_vehicles:
                for occluding_vehicle in occluding_vehicles:

                    occlusion_level = occlusion_levels[(occluding_vehicle.x, occluding_vehicle.y)]
                    cur.execute("INSERT INTO OCCLUDING_VEHICLES(TIME, X, Y, ANGLE, ASSIGNED_SEGMENT, SEGMENT_SEQ, DIRECTION, TRAFFIC_SIGNAL, \
                                 TYPE, SUBJECT_VEHICLE_ID, OCCLUSION_LEVEL) VALUES (?,?,?,?,?,?,?,?,?,?,?)", 
                                 (occluding_vehicle.current_time, occluding_vehicle.x, occluding_vehicle.y, occluding_vehicle.angle, occluding_vehicle.current_segment, \
                                  str(occluding_vehicle.segment_seq), occluding_vehicle.direction, occluding_vehicle.signal, vehicle_type, subject_vehicle.id, str(occlusion_level)))

        index += 1
        print(f"FINISHED COMPUTING OCCLUSION SCENARIO FOR TIME {current_time}; {round(index / len(time_list) * 100, 2)}% PERCENT COMPLETED DATABASE ID {occlusion_constants.CURRENT_FILE_ID}.")
        conn.commit()

    conn.close()
   

def save_is_valid_occlusion_scenario(scenario_key, is_valid_occlusion_scenario):
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()
    cmd = f"UPDATE main.ALL_SYNTHETIC_SCENARIOS SET IS_VALID_OCCLUSION_SCENARIO={is_valid_occlusion_scenario} WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    conn.commit()
    conn.close()


def retrieve_is_valid_occlusion_scenario(scenario_key):
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()
    cmd = f"SELECT IS_VALID_OCCLUSION_SCENARIO FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    query_result = cur.fetchall()
    conn.close()
    return query_result[0][0]


def compute_and_save_vehicle_hit_counts(current_file_id, start_scenario_key=None):
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)
    conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
    cur_occ_db = conn_occ_db.cursor()

    execute_cmd = f"SELECT SCENARIO_KEY FROM OCCLUSION_SCENARIO_TRAJECTORIES"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    query_result = set(query_result)
    scenario_keys = [i[0] for i in query_result]
    scenario_keys.sort()

    if start_scenario_key:
        scenario_keys = scenario_keys[start_scenario_key:]

    # Retrieve all trajectories for the scenario
    for scenario_key in scenario_keys:
        is_valid_occlusion_scenario = retrieve_is_valid_occlusion_scenario(scenario_key)
        if is_valid_occlusion_scenario == 0:
            continue

        print(f"SCENARIO KEY: {scenario_key}, CURRENT_FILE_ID: {current_file_id}")

        current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = scenario_utilities.retrieve_all_maneuvers(current_file_id, scenario_key)

        subject_vehicle_id = ordered_vehicle_ids[0]
        occluding_vehicle_id = ordered_vehicle_ids[1]
        relevant_vehicle_ids = ordered_vehicle_ids[2:]

        subject_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(subject_vehicle_id, current_time)

        print(F"{subject_vehicle.id}: {subject_vehicle.x}, {subject_vehicle.y}")

        # Create occluding vehicle from db
        execute_cmd = f"SELECT * FROM OCCLUDING_VEHICLES WHERE TRACK_ID={occluding_vehicle_id}"
        cur_occ_db.execute(execute_cmd)
        query_result = cur_occ_db.fetchall()
        occluding_vehicle = scenario_utilities.setup_occluding_vehicle_from_db(query_result[0])
        # Create relevant_vehicles from db
        relevant_vehicles = []
        for track_id in relevant_vehicle_ids:
            relevant_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(track_id, current_time)
            relevant_vehicles.append(relevant_vehicle)
        
        # E.g., vehicle_hit_counts = {2: {1: 23, 3: 10}, 1: {2: 1, 3: 0}} shows how many rays hit each vehicle 
        vehicle_hit_counts = game_utilities.compute_all_hit_counts_for_scenario(subject_vehicle, occluding_vehicle, relevant_vehicles)
        print(f"vehicle hit counts: {vehicle_hit_counts}\n")
        # save_vehicle_hit_counts(scenario_key, vehicle_hit_counts)

    print(f"Finished computing vehicle hit counts for database {current_file_id}.")

def save_vehicle_hit_counts(scenario_key, vehicle_hit_counts):
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()
    cmd = f"UPDATE main.ALL_SYNTHETIC_SCENARIOS SET VEHICLE_HIT_COUNTS='{str(vehicle_hit_counts)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    conn.commit()
    conn.close()


def retrieve_vehicle_hit_counts(scenario_key):
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()
    cmd = f"SELECT VEHICLE_HIT_COUNTS FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    query_result = cur.fetchall()

    if query_result[0][0]:
        query_result = ast.literal_eval(query_result[0][0])
    else:
        query_result = query_result[0][0]
    conn.close()
    return query_result


def generate_trajectories_tables():
    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:
        occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
        conn = sqlite3.connect(occlusion_constants.OCC_DB)
        cur = conn.cursor()
        cmd = f"CREATE TABLE BASE_SCENARIO_TRAJECTORIES (MIN_SCENARIO_KEY INTEGER, MAX_SCENARIO_KEY INTEGER, TRACK_ID INTEGER, MANEUVER TEXT, TRAJECTORIES TEXT)"
        cur.execute(cmd)
        conn.commit()
        conn.close()

    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:
        occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
        conn = sqlite3.connect(occlusion_constants.OCC_DB)
        cur = conn.cursor()
        cmd = f"CREATE TABLE OCCLUSION_SCENARIO_TRAJECTORIES (SCENARIO_KEY INTEGER, TRACK_ID INTEGER, MANEUVER TEXT, TRAJECTORIES TEXT)"
        cur.execute(cmd)
        conn.commit()
        conn.close()

def save_base_scenario_trajectories(min_scenario_key, max_scenario_key, track_id, maneuver, trajectories):
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()
    cur.execute("INSERT INTO BASE_SCENARIO_TRAJECTORIES (MIN_SCENARIO_KEY, MAX_SCENARIO_KEY, TRACK_ID, MANEUVER, TRAJECTORIES) VALUES (?,?,?,?,?)", 
               (min_scenario_key, max_scenario_key, track_id, maneuver, str(trajectories)))
    conn.commit()
    conn.close()


def save_occlusion_scenario_trajectories(scenario_key, track_id, maneuver, trajectories):
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()
    cur.execute("INSERT INTO OCCLUSION_SCENARIO_TRAJECTORIES (SCENARIO_KEY, TRACK_ID, MANEUVER, TRAJECTORIES) VALUES (?,?,?,?)", 
               (scenario_key, track_id, maneuver, str(trajectories)))
    conn.commit()
    conn.close()



# Run this to generate the base and occlusion scenario trajectories tables and the synthetic scenarios table
def generate_trajectories_and_synthetic_scenarios_tables():
    generate_synthetic_scenarios_table()
    generate_trajectories_tables()


def generate_synthetic_scenarios_table():
    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:
        occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
        conn = sqlite3.connect(occlusion_constants.OCC_DB)
        cur = conn.cursor()
        cmd = f"CREATE TABLE ALL_SYNTHETIC_SCENARIOS (SCENARIO_KEY INTEGER, SUBJECT_VEHICLE_ID INTEGER, OCCLUDING_VEHICLE_ID INTEGER, TIME NUMERIC, INITIAL_SPEED_KEY INTEGER, OCCLUDING_VEHICLE_SPEED NUMERIC, IS_VALID_OCCLUSION_SCENARIO INTEGER, VEHICLE_HIT_COUNTS TEXT)"
        cur.execute(cmd)
        conn.commit()
        conn.close()

        
def save_synthetic_scenario(scenario_key, subject_vehicle_id, occluding_vehicle_id, time, initial_speed_key, occluding_vehicle_speed, is_valid_occlusion_scenario=None, vehicle_hit_counts=None):
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()
    cur.execute("INSERT INTO ALL_SYNTHETIC_SCENARIOS (SCENARIO_KEY, SUBJECT_VEHICLE_ID, OCCLUDING_VEHICLE_ID, TIME, INITIAL_SPEED_KEY, OCCLUDING_VEHICLE_SPEED, IS_VALID_OCCLUSION_SCENARIO, VEHICLE_HIT_COUNTS) VALUES (?,?,?,?,?,?,?,?)", 
               (scenario_key, subject_vehicle_id, occluding_vehicle_id, time, initial_speed_key, occluding_vehicle_speed, is_valid_occlusion_scenario, vehicle_hit_counts))
    conn.commit()
    conn.close()


def generate_initial_speeds_table():
    insert_section = f"(INITIAL_SPEED_KEY INTEGER, SEGMENT TEXT, TRAFFIC_LIGHT TEXT, DEDICATED_GREEN_FLAG INTEGER, HAS_CONFLICT_FLAG INTEGER, JUST_TURNED_GREEN_FLAG INTEGER, MIN_SPEED NUMERIC, " \
                     f"MAX_SPEED NUMERIC, MEAN_SPEED NUMERIC, MEDIAN_SPEED NUMERIC, STANDARD_DEVIATION NUMERIC, TOTAL_COUNT INTEGER)"
    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:
        occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
        conn = sqlite3.connect(occlusion_constants.OCC_DB)
        cur = conn.cursor()
        cmd = f"CREATE TABLE INITIAL_SPEEDS {insert_section}"
        cur.execute(cmd)
        conn.commit()
        conn.close()


def copy_initial_speeds_table():
    base_table_id = "769" # We are copying contents of INITIAL_SPEEDS table from 769 to other databases
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{base_table_id}/uni_weber_{base_table_id}_occluding_vehicles.db"
    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:
        if current_file_id == base_table_id:
            continue
        file_name = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
        conn = sqlite3.connect(occlusion_constants.OCC_DB)
        cur = conn.cursor()
        print(current_file_id)
        print(file_name)
        cmd = f"ATTACH DATABASE '{file_name}' AS new_db"
        cur.execute(cmd)
        cmd = f"INSERT INTO new_db.INITIAL_SPEEDS SELECT * FROM main.INITIAL_SPEEDS;"
        cur.execute(cmd)
        conn.commit()
        cmd = f"DETACH new_db"
        cur.execute(cmd)
        conn.close()

# Used to add track id values to the OCCLUDING VEHICLES table
def add_track_id_for_occluding_vehicles():
    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:
        occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
        conn = sqlite3.connect(occlusion_constants.OCC_DB)
        cur = conn.cursor()
        cur.execute(f"SELECT TIME FROM OCCLUDING_VEHICLES")
        query_result = cur.fetchall()
        track_id = -1
        for i in range(1, len(query_result)+1):
            cur.execute(f"UPDATE main.OCCLUDING_VEHICLES SET TRACK_ID={track_id} WHERE _rowid_='{i}'")
            track_id -= 1
        conn.commit()
        conn.close()


def save_scenario_key_in_collisions(scenario_key):
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()
    cmd = f"SELECT SCENARIO_KEY FROM COLLISIONS"
    cur.execute(cmd)
    query_result = cur.fetchall()
    if not query_result:
        cur.execute("INSERT INTO COLLISIONS (SCENARIO_KEY) VALUES (?)", (str(scenario_key),))
        conn.commit()
        conn.close()
    else: 
        scenario_keys = [i[0] for i in query_result]
        # If scenario key already in the table, do nothing
        if scenario_key in scenario_keys:
            return
        cur.execute("INSERT INTO COLLISIONS (SCENARIO_KEY) VALUES (?)", (str(scenario_key),))
        conn.commit()
        conn.close()


def save_hypergame_collision_details(scenario_key, hypergame_collisions, hypergame_traj_indices, hypergame_emergency_breaking_trajs):
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()
    cmd = f"UPDATE main.COLLISIONS SET HYPERGAME_COLLISIONS='{str(hypergame_collisions)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    cmd = f"UPDATE main.COLLISIONS SET HYPERGAME_TRAJ_INDICES='{str(hypergame_traj_indices)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    cmd = f"UPDATE main.COLLISIONS SET HYPERGAME_EMERGENCY_BRAKING_TRAJS='{str(hypergame_emergency_breaking_trajs)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    conn.commit()
    conn.close()


def save_factored_game_collision_details(scenario_key, factored_game_collisions, factored_game_traj_indices, factored_game_emergency_breaking_trajs, occlusion_caused_collisions):
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()
    cmd = f"UPDATE main.COLLISIONS SET FACTORED_GAME_COLLISIONS='{str(factored_game_collisions)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    cmd = f"UPDATE main.COLLISIONS SET FACTORED_GAME_TRAJ_INDICES='{str(factored_game_traj_indices)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    cmd = f"UPDATE main.COLLISIONS SET FACTORED_GAME_EMERGENCY_BRAKING_TRAJS='{str(factored_game_emergency_breaking_trajs)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    cmd = f"UPDATE main.COLLISIONS SET OCCLUSION_CAUSED_COLLISIONS='{str(occlusion_caused_collisions)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    conn.commit()
    conn.close()


def save_maneuver_probabilities(scenario_key, maneuver_probabilities):
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()
    cmd = f"UPDATE main.COLLISIONS SET MANEUVER_PROBABILITIES='{str(maneuver_probabilities)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    conn.commit()
    conn.close()


def save_occlusion_caused_collisions(scenario_key, occlusion_caused_collisions):
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()
    cmd = f"UPDATE main.COLLISIONS SET OCCLUSION_CAUSED_COLLISIONS='{str(occlusion_caused_collisions)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    conn.commit()
    conn.close()

def generate_collisions_tables():
    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:
        occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
        conn = sqlite3.connect(occlusion_constants.OCC_DB)
        cur = conn.cursor()
        # Create relevant agents table in database
        cmd = f"CREATE TABLE COLLISIONS (SCENARIO_KEY INTEGER, HYPERGAME_COLLISIONS TEXT, FACTORED_GAME_COLLISIONS TEXT, OCCLUSION_CAUSED_COLLISIONS TEXT, MANEUVER_PROBABILITIES TEXT, \
                HYPERGAME_TRAJ_INDICES TEXT, FACTORED_GAME_TRAJ_INDICES TEXT, HYPERGAME_EMERGENCY_BRAKING_TRAJS TEXT, FACTORED_GAME_EMERGENCY_BRAKING_TRAJS TEXT)"   
        cur.execute(cmd)
        conn.commit()
        conn.close()


def generate_and_save_trajectories(current_file_id, start_time_index=0, end_time_index=None, scenario_key=0):
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

    time_list = scenario_utilities.generate_time_list()
    if end_time_index == None:
        end_time_index = len(time_list)
    time_list = time_list[start_time_index:end_time_index]
    for current_time in time_list:

        scenario_list = scenario_utilities.retrieve_occlusion_scenarios(current_time)
        for scenario in scenario_list:
            starting_scenario_key = scenario_key
            print(f"STARTING NEW BASE SCENARIO; SUBJECT VEHICLE ID: {subject_vehicle.id}; STARTING SCENARIO KEY: {starting_scenario_key}")
            subject_vehicle = scenario[0]
            relevant_vehicles = scenario[1]
            occluding_vehicles = scenario[2]
            base_scenario = [subject_vehicle] + relevant_vehicles

            # Stores the trajectories described below
            base_scenario_trajectories = {}

            # Generate trajectories for subject and relevant vehicles that don't depend on the 
            # occluding vehicle (all maneuvers except for follow maneuvers)
            for v in base_scenario:
                v.set_leading_vehicle(None)

                base_scenario_trajectories[v.id] = {}
                maneuvers = scenario_utilities.get_available_actions(v)
                print(f"-----------BELOW IS FOR VEHICLE: {v.id}----------")
                print(f"VEHICLE CURRENT SEGMENT: {v.current_segment}")
                print(f"VEHICLE SEGMENT SEQ: {v.segment_seq}")
                print(f"VEHICLE SIGNAL: {v.signal}")
                print(f"BASE SCENARIO MANEUVERS: {maneuvers}")
                maneuvers = scenario_utilities.filter_wait_maneuvers(maneuvers, filter_follow_maneuvers=True)
                print(f"BASE SCENARIO MANEUVERS AFTER FILTERING: {maneuvers}")

                for maneuver in maneuvers:
                    print(f"Working on vehicle {v.id}, maneuver: {maneuver}")
                    if maneuver == 'cut-in':
                        continue

                    trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, None)
                    # Relax constraints if we failed to generate trajectories
                    if not trajectories:
                        trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, None, relaxed_constraints=True)
                    # Set speed to something reasonable if we failed to generate trajectories 
                    if not trajectories:
                        if maneuver in occlusion_constants.WAIT_ACTIONS:
                            new_speed = 2.0
                        else:
                            new_speed = 8.0 
                        trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, None, relaxed_constraints=True, new_speed=new_speed)

                    if not trajectories:
                        raise Exception(f"Could not generate trajectories for file id: {current_file_id}, time: {v.current_time}, vehicle: {v.id} for maneuver: {maneuver}, vehicle segment: {v.current_segment}, speed: {v.speed}")
                    sorted_trajectories = scenario_utilities.filter_trajectories(trajectories, maneuver)
                    base_scenario_trajectories[v.id][maneuver] = sorted_trajectories
                    
            for occluding_vehicle in occluding_vehicles:

                print(f"\n***---NEW OCCLUSION SCENARIO: TIME {current_time}, FILE ID: {current_file_id}---***")
                vehicle_list = []
                vehicle_list.append(subject_vehicle)
                vehicle_list.append(occluding_vehicle)
                vehicle_list += relevant_vehicles

                # Set leaders for each vehicle in scenario
                for v in vehicle_list:
                    leader_id = scenario_utilities.get_leader_id(v, vehicle_list)
                    if leader_id:
                        for veh in vehicle_list:
                            if veh.id == leader_id:
                                v.set_leading_vehicle(veh)
                                break
                    else:
                        v.set_leading_vehicle(None)

                # Retrieve traffic information for occluding vehicle speed generation
                dedicated_green, has_conflict, just_turned_green = scenario_utilities.retrieve_traffic_conditions_for_initial_speed(current_file_id, occluding_vehicle, vehicle_list)
                speed = scenario_utilities.get_initial_speed_for_occluding_vehicle(occluding_vehicle.current_segment, occluding_vehicle.signal, dedicated_green, has_conflict, just_turned_green)

                # Create scenarios based on the occluding vehicle's initial speed
                occluding_vehicle_initial_speeds = []

                # Stores the occluding vehicle initial speeds in a list of tuples of the form:
                # [(speed_key, speed)]. The speed key tells you what entry in the initial speeds 
                # table to look at to generate the speed.
                if type(speed) == dict and speed['go'] != speed['stop']:
                    occluding_vehicle_initial_speeds.append((speed['go_speed_initial_key'], speed['go']))
                    occluding_vehicle_initial_speeds.append((speed['stop_speed_initial_key'], speed['stop']))
                else:
                    occluding_vehicle_initial_speeds.append((speed['go_speed_initial_key'], speed['go']))
               
                # This is true if all maneuvers could be generated for at least one of the occluding vehicle speeds
                successful_occlusion_scenario = False

                # Store the initial speeds tested 
                for key_speed_tuple in occluding_vehicle_initial_speeds:
                    initial_speed_key = key_speed_tuple[0]
                    occluding_vehicle_initial_speed = key_speed_tuple[1]

                    # If occluding vehicle has a lead vehicle, occluding vehicle's speed 
                    # cannot be greater than lead vehicle's speed.
                    # This will hopefully prevent collisions where the occluding vehicle
                    # rear-ends the lead vehicle.
                    if occluding_vehicle.leading_vehicle and occluding_vehicle_initial_speed > occluding_vehicle.leading_vehicle.speed:
                            occluding_vehicle_initial_speed = occluding_vehicle.leading_vehicle.speed
                    occluding_vehicle.set_speed(occluding_vehicle_initial_speed)

                    occlusion_scenario_trajectories = {}
                    for v in vehicle_list:
                        occlusion_scenario_trajectories[v.id] = {}

                    for v in vehicle_list:
                        maneuvers = scenario_utilities.get_available_actions(v)

                        if v.id < 0:
                            maneuvers = scenario_utilities.filter_wait_maneuvers(maneuvers, filter_follow_maneuvers=False)
                        else:
                            maneuvers = [maneuver for maneuver in maneuvers if maneuver in occlusion_constants.FOLLOW_ACTIONS]

                        original_speed = occluding_vehicle_initial_speed if v.id < 0 else v.speed
                     
                        for maneuver in maneuvers:
                            print(f"Working on vehicle {v.id}, maneuver: {maneuver}")
                            if maneuver == 'cut-in':
                                continue
                            if maneuver in occlusion_constants.FOLLOW_ACTIONS and not v.leading_vehicle:
                                continue

                            print(f"FIRST ATTEMPT")
                            trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, occluding_vehicle_initial_speed)
                            
                            if not trajectories:
                                print(f"SECOND ATTEMPT")
                                trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, occluding_vehicle_initial_speed, relaxed_constraints=True)
                        
                            # Set speed to something reasonable if we failed to generate trajectories 
                            if not trajectories:
                                print(f"THIRD ATTEMPT")
                                changed_lead_vehicle_speed = False

                                if maneuver in occlusion_constants.WAIT_ACTIONS:
                                    reasonable_speed = 2.0
                                    if v.id < 0:
                                        occluding_vehicle_initial_speed = reasonable_speed

                                    trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, occluding_vehicle_initial_speed, relaxed_constraints=True, new_speed=reasonable_speed)

                                elif maneuver in occlusion_constants.FOLLOW_ACTIONS and v.leading_vehicle:

                                    trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, v.leading_vehicle.speed, relaxed_constraints=True, third_attempt=True)

                                else:
                                    reasonable_speed = 8.0 # approx. 29 km/h
                                    if v.id < 0:
                                        occluding_vehicle_initial_speed = reasonable_speed

                                    trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, occluding_vehicle_initial_speed, relaxed_constraints=True, new_speed=reasonable_speed)
                                
                                if not trajectories:
                                    raise Exception(f"Could not generate trajectories for file id: {current_file_id}, time: {v.current_time}, vehicle: {v.id} for maneuver: {maneuver}, vehicle segment: {v.current_segment}, speed: {v.speed}")
                                
                                if v.id < 0:
                                    occluding_vehicle_initial_speed = original_speed

                            sorted_trajectories = scenario_utilities.filter_trajectories(trajectories, maneuver)
                            occlusion_scenario_trajectories[v.id][maneuver] = sorted_trajectories

                    # # Save occlusion scenario trajectories
                    for v in vehicle_list:
                        # Save occlusion scenario trajectories
                        occlusion_scenario_maneuvers = list(occlusion_scenario_trajectories[v.id].keys())
                        for maneuver in occlusion_scenario_maneuvers:
                            trajectories = occlusion_scenario_trajectories[v.id][maneuver]

                            save_format_trajectories = [] 
                            # Convert weird array in timestep to float
                            for i in range(len(trajectories)):
                                traj = trajectories[i]
                                traj = [list(timestep) for timestep in traj]
                                for j in range(len(traj)):
                                    timestep = traj[j]
                                    traj[j][4] = float(timestep[4])
                                save_format_trajectories.append(traj)
                            save_occlusion_scenario_trajectories(scenario_key, v.id, maneuver, save_format_trajectories)
                    save_synthetic_scenario(scenario_key, subject_vehicle.id, occluding_vehicle.id, current_time, initial_speed_key, occluding_vehicle_initial_speed)
                    
                    scenario_key += 1

            # Save base scenario trajectories
            base_scenario_track_ids = list(base_scenario_trajectories.keys())

            for track_id in base_scenario_track_ids:
                maneuvers = list(base_scenario_trajectories[track_id].keys())

                for maneuver in maneuvers:
                    trajectories = base_scenario_trajectories[track_id][maneuver]
                    save_format_trajectories = [] 
                    # Convert weird array in timestep to float
                    for i in range(len(trajectories)):
                        traj = trajectories[i]
                        traj = [list(timestep) for timestep in traj]
                        for j in range(len(traj)):
                            timestep = traj[j]
                            traj[j][4] = float(timestep[4])
                        save_format_trajectories.append(traj)
                    save_base_scenario_trajectories(starting_scenario_key, scenario_key-1, track_id, maneuver, save_format_trajectories)
                    


def compute_and_save_is_valid_occlusion_scenario(current_file_id, start_scenario_key=None):
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)
    conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
    cur_occ_db = conn_occ_db.cursor()
    conn_db = sqlite3.connect(occlusion_constants.DB)
    cur_db = conn_db.cursor()

    execute_cmd = f"SELECT SCENARIO_KEY FROM OCCLUSION_SCENARIO_TRAJECTORIES"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    query_result = set(query_result)
    scenario_keys = [i[0] for i in query_result]
    scenario_keys.sort()

    if start_scenario_key:
        scenario_keys = scenario_keys[start_scenario_key:]

    # Retrieve all trajectories for the scenario
    for scenario_key in scenario_keys:
        print(f"----CURRENT FILE ID: {current_file_id}; SCENARIO KEY: {scenario_key}----")

        current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = scenario_utilities.retrieve_all_maneuvers(current_file_id, scenario_key)

        subject_vehicle_id = ordered_vehicle_ids[0]
        occluding_vehicle_id = ordered_vehicle_ids[1]
        relevant_vehicle_ids = ordered_vehicle_ids[2:]
        hypergame_length = len(ordered_vehicle_ids)

        scenario = scenario_utilities.setup_scenario(current_file_id, current_time, subject_vehicle_id, relevant_vehicle_ids, occluding_vehicle_id=occluding_vehicle_id)

        # Assign leading vehicle attribute
        for vehicle in scenario:
            lead_vehicle_id = scenario_utilities.get_leader_id(vehicle, scenario)
            if lead_vehicle_id:
                for v in scenario:
                    if v.id == lead_vehicle_id:
                        vehicle.set_leading_vehicle(v)
            else:
                vehicle.set_leading_vehicle(None)

        # Assign has_oncoming_vehicle attribute for vehicles in scenario
        for i in range(0, len(scenario)-1):
            for j in range(i, len(scenario)):
                vehicle_a, vehicle_b = scenario[i], scenario[j]
                has_oncoming_vehicle = scenario_utilities.compute_has_oncoming_vehicle(vehicle_a, vehicle_b)
                if has_oncoming_vehicle:
                    if not vehicle_a.has_oncoming_vehicle:
                        vehicle_a.set_has_oncoming_vehicle(True)
                    if not vehicle_b.has_oncoming_vehicle:
                        vehicle_b.set_has_oncoming_vehicle(True)
                        
        hypergame_maneuver_combinations = itertools.combinations(track_id_maneuver_pairs, hypergame_length)
        hypergame_maneuver_combinations = [i for i in hypergame_maneuver_combinations if not game_utilities.repeated_track_ids_in_maneuver_combination(i, hypergame_length)]

        ordered_hypergame_maneuver_combinations = []
        # Elements of hypergame_maneuver_combinations are not ordered and they must be ordered for 
        # the Nash Equilibrium solver to work.
        # E.g., hypergame_maneuver_combinations: [((1, 'follow_lead'), (-1, 'proceed-turn'), (3, 'track_speed')), 
        #                                         ((-1, 'proceed-turn'), (1, 'track_speed'), (3, 'track_speed'))
        for maneuver_combination in hypergame_maneuver_combinations:
            ordered_maneuver_combination = []
            for track_id in ordered_vehicle_ids:
                for id_maneuver in maneuver_combination:
                    if id_maneuver[0] == track_id:
                        ordered_maneuver_combination.append(id_maneuver)
                if ordered_maneuver_combination not in ordered_hypergame_maneuver_combinations:
                    ordered_hypergame_maneuver_combinations.append(ordered_maneuver_combination)

        hypergame_payoff_dict = {}
        all_maneuver_to_trajectory_index_dict = {}

        for hypergame_maneuver_set in ordered_hypergame_maneuver_combinations:
            # A hypergame maneuver set is a combination of maneuvers for each vehicle in the scenario:
            # E.g., ((1,2), (2,3), (-3,1)) could be one combination of maneuvers for each vehicle in the scenario with vehicles 1, 2, and -3.
            vehicle_maneuver_dict = {}
            for id_maneuver in hypergame_maneuver_set:
                vehicle_maneuver_dict[id_maneuver[0]] = [id_maneuver[1]]
   
            # Retrieve trajectories for this set of maneuvers
            trajectories_dict = scenario_utilities.retrieve_trajectories(current_file_id, scenario_key, ordered_vehicle_ids, vehicle_maneuver_dict)

            payoff_dict_key, payoff_dict_value, chosen_trajectories = game_utilities.compute_chosen_trajectories(current_file_id, trajectories_dict, subject_vehicle_id, ordered_vehicle_ids, vehicle_maneuver_dict)

            # maneuver_to_trajectory_index_dict looks like: {(1, 'follow_lead'): (1, 'aggressive', 0), (-6, 'wait_for_lead_to_cross'): (-6, 'aggressive', 1), (3, 'track_speed'): (3, 'normal', 0)}
            # The key is a track_id, maneuver tuple, the value is a track_id, level, index tuple
            # The point of this dict is to recover the chosen trajectory for the maneuver to do a collision check and compute collision details
            maneuver_to_trajectory_index_dict = {}
            for id_maneuver, chosen_trajectory_key in zip(hypergame_maneuver_set, list(chosen_trajectories.keys())):
                if id_maneuver not in list(maneuver_to_trajectory_index_dict.keys()):
                    maneuver_to_trajectory_index_dict[id_maneuver] = chosen_trajectory_key

            hypergame_payoff_dict[payoff_dict_key] = payoff_dict_value

            all_maneuver_to_trajectory_index_dict[tuple(hypergame_maneuver_set)] = maneuver_to_trajectory_index_dict

        num_players = len(ordered_vehicle_ids)
        player_actions = [list(set([k[i] for k in hypergame_payoff_dict.keys()])) for i in np.arange(num_players)]  
   
        eq = equilibria_core.EquilibriaCore(num_players,hypergame_payoff_dict,len(hypergame_payoff_dict),player_actions[0],False)
        nash_equilibria = eq.calc_pure_strategy_nash_equilibrium_exhaustive()

        is_valid_occlusion_scenario = False
        # Skip the scenario if there is no nash equilibria for the hypergame
        if not nash_equilibria:
            save_is_valid_occlusion_scenario(scenario_key, 0)
            print(f"COULD NOT GENERATE NASH EQUILIBRIA FOR THIS SCENARIO\n")
            continue

        # This stores every vehicles' maneuvers in the form
        # e.g., {1: ['track_speed', 'follow-lead'], 2: ['follow-lead']}
        vehicle_maneuvers = {}
        # Since the order is fixed, the index tells us the position of particular vehicle
        for index in range(len(ordered_vehicle_ids)):
            track_id = ordered_vehicle_ids[index]
            vehicle_maneuvers[track_id] = []
            for maneuver_combination in ordered_hypergame_maneuver_combinations:
                if maneuver_combination[index][1] not in vehicle_maneuvers[track_id]:
                    vehicle_maneuvers[track_id].append(maneuver_combination[index][1])

        # vehicle maneuvers looks like: {1: ['track_speed', 'follow-lead'], 2: ['wait_for_oncoming_vehicle'], -3: ['follow-lead']}
        occlusion_scenario_trajectories = scenario_utilities.retrieve_trajectories(current_file_id, scenario_key, ordered_vehicle_ids, vehicle_maneuvers)

        # In order for an occlusion scenario to be valid, it must have either no collision in its hypergame nash equilibria, or,
        # have no collisions involving the occluding vehicle, or, have at least one nash equilibrium where there is at least one 
        # collision pair that does not involve the occluding vehicle.
        # E.g., collision_pairs = [] is a valid occlusion scenario because there are no collision pairs
        #       collision_pairs = [(-1,3), (2,4)] is valid because there is one collision pair that does not involve the occluding vehicle, -1.
        #       collision_pairs = [(-1,3)] is not valid.
        hypergame_ground_truth_maneuvers = game_utilities.compute_ground_truth_maneuvers(ordered_vehicle_ids, scenario, nash_equilibria, hypergame_payoff_dict, factored_game_subject_id=None)
        
        # Take first ground-truth maneuver
        first_maneuver_hypergame_ground_truth_maneuvers = {}
        for track_id, maneuvers in hypergame_ground_truth_maneuvers.items():
            first_maneuver_hypergame_ground_truth_maneuvers[track_id] = maneuvers.pop()

        hypergame_ground_truth_maneuvers_set = set([(track_id, maneuver) for track_id, maneuver in first_maneuver_hypergame_ground_truth_maneuvers.items()])

        # For hypergame, find ground_truth_maneuver_to_trajectory_index_dict
        for maneuver_combination, maneuver_to_trajectory_index_dict in all_maneuver_to_trajectory_index_dict.items():
            if hypergame_ground_truth_maneuvers_set == set(maneuver_combination):
                ground_truth_maneuver_to_trajectory_index_dict = maneuver_to_trajectory_index_dict
                break

        # Compute all collisions   
        all_collisions = []
        # Construct selected trajectories dict which stores the selected trajectory for each vehicle
        # Eg. {track_id: traj, ...}
        selected_trajectories = {}
        for track_id in ordered_vehicle_ids:
            maneuver = first_maneuver_hypergame_ground_truth_maneuvers[track_id]
            _, index = ground_truth_maneuver_to_trajectory_index_dict[(track_id, maneuver)]
            trajectory = occlusion_scenario_trajectories[track_id][maneuver][index]
            selected_trajectories[track_id] = trajectory

        all_collisions = []
        collision_id = 0
        for i in range(0, len(ordered_vehicle_ids) - 1):
            track_id_a = ordered_vehicle_ids[i]
            maneuver_a = first_maneuver_hypergame_ground_truth_maneuvers[track_id_a]
            for j in range(i+1, len(ordered_vehicle_ids)):
                track_id_b = ordered_vehicle_ids[j]
                maneuver_b = first_maneuver_hypergame_ground_truth_maneuvers[track_id_b]

                trajectory_a = selected_trajectories[track_id_a]
                trajectory_b = selected_trajectories[track_id_b]
                collision_impact_details = scenario_utilities.collision_check(trajectory_a, trajectory_b)

                if collision_impact_details:
                    impact_velocity_a, impact_velocity_b, original_time_to_collision = collision_impact_details
                    collision_details = [track_id_a, track_id_b, collision_id, maneuver_a, maneuver_b, maneuver_a, maneuver_b, impact_velocity_a, impact_velocity_b, original_time_to_collision]
                    all_collisions.append(collision_details)
                    collision_id += 1

        hypergame_filtered_collisions = game_utilities.filter_collisions(ordered_vehicle_ids, all_collisions, emergency_braking_trajectories=None)
        print(f"HYPERGAME FILTERED COLLISIONS: {hypergame_filtered_collisions}")

        # Stores tuples of track_ids involved in each accident, e.g., hypergame_collision_pairs = [(-1,3)]
        hypergame_collision_pairs = [(collision[0], collision[1]) for collision in hypergame_filtered_collisions]
        collisions_not_involving_occluding_vehicle = [collision_pair for collision_pair in hypergame_collision_pairs if occluding_vehicle_id not in collision_pair]
        
        if not hypergame_filtered_collisions:
            save_is_valid_occlusion_scenario(scenario_key, 1) 
            print(f"IS VALID OCCLUSION SCENARIO: 1\n")

        # In this case there must be at least one collision pair that does not involve the one occluding vehicle
        elif len(collisions_not_involving_occluding_vehicle) >= 1:
            save_is_valid_occlusion_scenario(scenario_key, 1) 
            print(f"IS VALID OCCLUSION SCENARIO: 1\n")
        # If we get to this point it must be because there is a collision that involves the occluding vehicle
        # We are okay with any collision as long as it happens after some time horizon threshold as this threshold 
        # means the vehicles in the scenario have time to react
        elif hypergame_filtered_collisions[0][-1] >= occlusion_constants.OCCLUSION_SCENARIO_INCLUSION_TIME_HORIZON_THRESHOLD:
            save_is_valid_occlusion_scenario(scenario_key, 1) 
            print(f"IS VALID OCCLUSION SCENARIO: 1\n")
        else:
            save_is_valid_occlusion_scenario(scenario_key, 0) 
            print(f"IS VALID OCCLUSION SCENARIO: 0\n")


  
