

'''
Occupancy Map Definition

Created on May 10, 2021

@author: Maximilian Kahn
'''

from utilities import occlusion_constants, scenario_utilities
from utilities.raycast import compute_bresenham_line, compute_FVTA_line
from occlusion_scenario import occlusion_scenario_builder
import matplotlib.patches as patches
from matplotlib import pyplot as plt
import math
import random

class OccupancyMap:
    """
        Occupancy Map:
               width →
        ---------------------
        |__| ← grid_size    |
        |                   |
        |                   | length ↑
        |                   |
        ---------------------
    """
    def __init__(self, origin=occlusion_constants.OCCUPANCY_MAP_ORIGIN,
                length=occlusion_constants.OCCUPANCY_MAP_LENGTH,
                 width=occlusion_constants.OCCUPANCY_MAP_WIDTH,
                 grid_size=occlusion_constants.OCCUPANCY_MAP_GRID_SIZE):
        self.origin = origin
        self.length = length
        self.width = width
        self.grid_size = grid_size
        self.grid = self.make_grid()

    """
        make_grid: Makes a "grid" in the form of a dictionary where the coordinates
        on the grid are the keys and each coordinate is associated with a list of
        vehicle ids that have occupied that grid square.

        This is an example of a grid in the occupancy map. First we have the coordinates
        of the grid point. Next we have the vehicles occupying the point (the first list)
        Next, we have the vehicles that can see the point (the second list)
        
        grid = {
            (0,0): [[1], [0, 1]]
            .
            .
            (1,0): [[1], [0, 1]]
        }
    """
    def make_grid(self):
        grid = {}
        for y in range(self.origin[1], self.length + self.origin[1] + 1):
            for x in range(self.origin[0], self.width + self.origin[0] + 1):
                grid[(x,y)] = [[],[]]
        return grid

    """
    occupied: Returns true if the point, (px,py) lies within the grid square centred at
    (x,y) with side length, length.
    """
    def occupied(self,px,py,x,y):
        if (px <= x + self.grid_size / 2) and (px >= x - self.grid_size / 2) and \
           (py <= y + self.grid_size / 2) and (py >= y - self.grid_size / 2):
           return True
        else:
           return False

    """
    make_occupancy_map: Appends each vehicle id to each grid they occupy in the occupancy map
    NOTE: Just their boundaries are recorded in the occupancy map
    """
    def make_occupancy_map(self, vehicles, pass_bb_box_back=False):

        out_of_bounds_vehicles = []

        for vehicle in vehicles:
            bounding_box_points = vehicle.get_bounding_box_points()

            l1 = compute_FVTA_line(bounding_box_points[0][0],
                                   bounding_box_points[0][1],
                                   bounding_box_points[1][0],
                                   bounding_box_points[1][1])


            l2 = compute_FVTA_line(bounding_box_points[1][0],
                                   bounding_box_points[1][1],
                                   bounding_box_points[2][0],
                                   bounding_box_points[2][1])

            l3 = compute_FVTA_line(bounding_box_points[2][0],
                                   bounding_box_points[2][1],
                                   bounding_box_points[3][0],
                                   bounding_box_points[3][1])

            l4 = compute_FVTA_line(bounding_box_points[3][0],
                                   bounding_box_points[3][1],
                                   bounding_box_points[0][0],
                                   bounding_box_points[0][1])


            bounding_box = l1 + l2 + l3 + l4

            outside_map = False
            for point in bounding_box:

                # Make sure the bounding box point is within the bounds of the map
                if point[0] > self.width + self.origin[0] or \
                   point[1] > self.length + self.origin[1]:
                    outside_map = True
                    continue
                if point[0] < self.origin[0] or point[1] < self.origin[1]:
                    outside_map = True
                    continue

                if vehicle.id not in self.grid[((point[0]),point[1])][0]:
                    self.grid[((point[0]),point[1])][0].append(vehicle.id)

            if outside_map:
                out_of_bounds_vehicles.append(vehicle.id)

        if pass_bb_box_back == True:
            return out_of_bounds_vehicles, bounding_box

        else:
            return out_of_bounds_vehicles



    """
    compute_hit_count: get how many raycasts intersect with the vehicle with
    the input id.
    This is an example of a grid in the occupancy map. First we have the coordinates
    of the grid point. Next we have the vehicles occupying the point (the first list)
    Next, we have the vehicles that can see the point (the second list)
    grid square = ((78, 98), [[1], [0, 1]])
    """
    def compute_hit_count(self, raycasts, subject_vehicle_id, relevant_vehicles):
        visited_grid_squares = []

        hit_counts = {}
        relevant_vehicle_ids = []
        for v in relevant_vehicles:
            relevant_vehicle_ids.append(v.id)

            hit_counts[v.id] = 0

        for raycast in raycasts:
            # Make sure points all go from subject vehicle to target vehicle
            for point in raycast:
                # Outside grid bounds
                if point[0] > self.width + self.origin[0] or \
                   point[1] > self.length + self.origin[1]:
                    break
                if point[0] < self.origin[0] or point[1] < self.origin[1]:
                    break
                # Case where the subject vehicle can see itself
                if subject_vehicle_id in self.grid[point][0]:
                    continue

                # If we have already visited this point and a vehicle is occupying it then we break
                if len(self.grid[point][0]) > 0 and point in visited_grid_squares:
                    break
                # Counting hit point if vehicle that is not the subject vehicle is occupying the point
                if len(self.grid[point][0]) > 0 and point not in visited_grid_squares:

                    visited_grid_squares.append(point)
                    self.grid[point][1].append(subject_vehicle_id)

                    if hit_counts[self.grid[point][0][0]]:
                        hit_counts[self.grid[point][0][0]] += 1
                    else:
                        hit_counts[self.grid[point][0][0]] = 1
                    break

        return hit_counts

    @staticmethod
    def draw_arrow(vehicle, arrow_size = 1):
        x_centre = vehicle.x * occlusion_constants.MAP_SCALE
        y_centre = vehicle.y * occlusion_constants.MAP_SCALE
        arrow_top = (x_centre + vehicle.length / 8, y_centre)
        arrow_bottom = (x_centre - vehicle.length / 3, y_centre)
        rotated_arrow_top_x = (arrow_top[0] - x_centre) * math.cos(vehicle.angle) - \
                                 (arrow_top[1] - y_centre) * math.sin(vehicle.angle) + x_centre

        rotated_arrow_top_y = (arrow_top[0] - x_centre) * math.sin(vehicle.angle) + \
                                 (arrow_top[1] - y_centre) * math.cos(vehicle.angle) + y_centre

        rotated_arrow_bottom_x = (arrow_bottom[0] - x_centre) * math.cos(vehicle.angle) - \
                              (arrow_bottom[1] - y_centre) * math.sin(vehicle.angle) + x_centre

        rotated_arrow_bottom_y = (arrow_bottom[0] - x_centre) * math.sin(vehicle.angle) + \
                              (arrow_bottom[1] - y_centre) * math.cos(vehicle.angle) + y_centre
        dx = rotated_arrow_top_x - rotated_arrow_bottom_x
        dy = rotated_arrow_top_y - rotated_arrow_bottom_y 
        plt.arrow(rotated_arrow_bottom_x, rotated_arrow_bottom_y, dx, dy, width=arrow_size, head_width=8*arrow_size, color='blue')


    def visualize_occupancy_map(self, subject_vehicle, relevant_vehicles, filename=None, trajectories=None):

        # Create figure and axes
        fig, ax = plt.subplots()
        annotated = []
        
        if trajectories:
            for trajectory in trajectories:
                x_points = [i[1] * occlusion_constants.MAP_SCALE for i in trajectory]
                y_points = [i[2] * occlusion_constants.MAP_SCALE for i in trajectory]
                ax.plot(x_points,y_points, 'red', alpha=0.8, linewidth=0.5)

        for sequence in occlusion_constants.SEGMENT_SEQUENCES:
            centreline = scenario_utilities.get_centreline_from_segment_sequence(sequence[0])
            x_points = [i[0] * occlusion_constants.MAP_SCALE for i in centreline]
            y_points = [i[1] * occlusion_constants.MAP_SCALE for i in centreline]
            ax.plot(x_points,y_points, 'black', alpha=0.4, linewidth=0.2)


        for grid_square in self.grid.items():

            # Draw raycast
            if subject_vehicle.id in grid_square[1][1]:
                plotted_grid_square = patches.Rectangle((grid_square[0][0], grid_square[0][1]), \
                    width=self.grid_size, height=self.grid_size, linewidth=1, facecolor='red')
                ax.add_patch(plotted_grid_square)

            # Plot subject vehicle
            elif subject_vehicle.id in grid_square[1][0]:
                plotted_grid_square = patches.Rectangle((grid_square[0][0], grid_square[0][1]), \
                    width=self.grid_size, height=self.grid_size, linewidth=1, facecolor='red')
                ax.add_patch(plotted_grid_square)

                # This labels each vehicle with their vehicle ID
                if grid_square[1][0][0] not in annotated:
                    plt.text(grid_square[0][0], grid_square[0][1], f"SUBJECT ID: {grid_square[1][0][0]}")
                    annotated.append(grid_square[1][0][0])

                self.draw_arrow(subject_vehicle)

            # Draw black to signal this is bounding box of a vehicle.
            elif grid_square[1][0]:
                    plotted_grid_square = patches.Rectangle((grid_square[0][0], grid_square[0][1]), \
                        width=self.grid_size, height=self.grid_size, linewidth=1, facecolor='black')
                    ax.add_patch(plotted_grid_square)

                    # This labels each vehicle with their vehicle ID
                    if grid_square[1][0][0] not in annotated:
                        plt.text(grid_square[0][0], grid_square[0][1], f"ID: {grid_square[1][0][0]}")
                        annotated.append(grid_square[1][0][0])


                        # Draw arrow from centre of vehicle
                        vehicle_id = grid_square[1][0][0] # WARNING: This should only have one element! Two would mean that a collision occurred.

                        # Add arrows to label the direction of the vehicles
                        for v in relevant_vehicles:
                            if v.id == vehicle_id:
                                self.draw_arrow(v)

        plt.xlim([self.origin[0] + 150, self.width + self.origin[0] - 150])
        plt.ylim([self.origin[1] + 390, self.length + self.origin[1] - 360])

        if filename:
            plt.savefig(filename, format='pdf')
            plt.close()
        else:
            plt.show()


    def add_raycast(self, raycasts, subject_vehicle_id):
        for raycast in raycasts:
            for point in raycast:
                if point[0] > self.width + self.origin[0] or \
                   point[1] > self.length + self.origin[1]:
                    break
                if point[0] < self.origin[0] or point[1] < self.origin[1]:
                    break

                if subject_vehicle_id not in self.grid[point][1]:
                    self.grid[point][1].append(subject_vehicle_id)

                # This checks if the subject vehicle can see this point
                # and there is a vehicle that is not the subject vehicle
                # then this point is occupied by some other vehicle and we can break
                if subject_vehicle_id in self.grid[point][1] \
                    and len(self.grid[point][0]) > 0 and \
                    subject_vehicle_id not in self.grid[point][0]:
                    break



    def visualize_occupancy_map_for_conference_paper(self, subject_vehicle, relevant_vehicles, occluding_vehicle=None, filename=None, title=None):

        if occluding_vehicle != None:
            other_vehicles = [occluding_vehicle] + relevant_vehicles
        else:
            other_vehicles = relevant_vehicles
        # Create figure and axes
        fig, ax = plt.subplots()

        annotated = []

        for sequence in occlusion_constants.SEGMENT_SEQUENCES:
            centreline = scenario_utilities.get_centreline_from_segment_sequence(sequence[0])
            x_points = [i[0] * occlusion_constants.MAP_SCALE for i in centreline]
            y_points = [i[1] * occlusion_constants.MAP_SCALE for i in centreline]
            ax.plot(x_points,y_points, 'black', alpha=0.5, linewidth=0.2)

        for grid_square in self.grid.items():

            # DRAW RAYCAST
            if subject_vehicle.id in grid_square[1][1]:
                plotted_grid_square = patches.Rectangle((grid_square[0][0], grid_square[0][1]), \
                    width=self.grid_size, height=self.grid_size, linewidth=1, facecolor='red')
                ax.add_patch(plotted_grid_square)

            # Plot subject vehicle
            elif subject_vehicle.id in grid_square[1][0]:
                plotted_grid_square = patches.Rectangle((grid_square[0][0], grid_square[0][1]), \
                    width=self.grid_size, height=self.grid_size, linewidth=1, facecolor='black')
                ax.add_patch(plotted_grid_square)

                # This labels each vehicle with their vehicle ID
                if grid_square[1][0][0] not in annotated:
                    plt.text(grid_square[0][0]+25, grid_square[0][1], f"ID: 1")
                    annotated.append(grid_square[1][0][0])

                self.draw_arrow(subject_vehicle)

            # Draw black to signal this is bounding box of a vehicle.
            elif grid_square[1][0]:
                plotted_grid_square = patches.Rectangle((grid_square[0][0], grid_square[0][1]), \
                    width=self.grid_size, height=self.grid_size, linewidth=1, facecolor='black')
                ax.add_patch(plotted_grid_square)

                # This labels each vehicle with their vehicle ID
                if grid_square[1][0][0] not in annotated:

                    if grid_square[1][0][0] == 192:
                        plt.text(grid_square[0][0]-60, grid_square[0][1]+50, f"ID: 2")

                    elif grid_square[1][0][0] == 189:
                        plt.text(grid_square[0][0]-90, grid_square[0][1], f"ID: 3")

                    elif grid_square[1][0][0] == -3261:
                        plt.text(grid_square[0][0]-80, grid_square[0][1]+45, f"Occluding Vehicle")
                    
                    annotated.append(grid_square[1][0][0])
                    # Draw arrow from centre of vehicle
                    vehicle_id = grid_square[1][0][0] # WARNING: This should only have one element! Two would mean that a collision occurred.

                    # Add arrows to label the direction of the vehicles
                    for v in other_vehicles:
                        if v.id == vehicle_id:
                            self.draw_arrow(v)

        plt.xlim([self.origin[0] + 150, self.width + self.origin[0] - 150])
        plt.ylim([self.origin[1] + 390, self.length + self.origin[1] - 360])

        if title != None:
            plt.title(title)

        if filename:
            plt.savefig(filename, format='pdf')
            plt.close()
        else:
            plt.show()




    def visualize_occupancy_map_for_conference_paper2(self, subject_vehicle, relevant_vehicles, plotting_collision=False, occluding_vehicle=None, filename=None, title=None, crash=False):

            if occluding_vehicle != None:
                other_vehicles = [occluding_vehicle] + relevant_vehicles
            else:
                other_vehicles = relevant_vehicles
            # Create figure and axes
            fig, ax = plt.subplots()

            annotated = []

            for sequence in occlusion_constants.SEGMENT_SEQUENCES:
                centreline = scenario_utilities.get_centreline_from_segment_sequence(sequence[0])
                x_points = [i[0] * occlusion_constants.MAP_SCALE for i in centreline]
                y_points = [i[1] * occlusion_constants.MAP_SCALE for i in centreline]
                ax.plot(x_points,y_points, 'black', alpha=0.5, linewidth=0.2)

            for grid_square in self.grid.items():

                # Draw raycast
                if subject_vehicle.id in grid_square[1][1]:
                    plotted_grid_square = patches.Rectangle((grid_square[0][0], grid_square[0][1]), \
                        width=self.grid_size, height=self.grid_size, linewidth=1, facecolor='red')
                    ax.add_patch(plotted_grid_square)

                # Plot subject vehicle
                elif subject_vehicle.id in grid_square[1][0]:
                    plotted_grid_square = patches.Rectangle((grid_square[0][0], grid_square[0][1]), \
                        width=self.grid_size, height=self.grid_size, linewidth=1, facecolor='black')
                    ax.add_patch(plotted_grid_square)

                    # This labels each vehicle with their vehicle ID

                    if crash:
                        if grid_square[1][0][0] not in annotated:
                            plt.text(grid_square[0][0]-90, grid_square[0][1]+15, f"ID: 1")
                            annotated.append(grid_square[1][0][0])

                    else:
                        if grid_square[1][0][0] not in annotated:
                            plt.text(grid_square[0][0]+35, grid_square[0][1]-15, f"ID: 1")
                            annotated.append(grid_square[1][0][0])

                    self.draw_arrow(subject_vehicle)

                # Draw black to signal this is bounding box of a vehicle.
                elif grid_square[1][0]:
                    plotted_grid_square = patches.Rectangle((grid_square[0][0], grid_square[0][1]), \
                        width=self.grid_size, height=self.grid_size, linewidth=1, facecolor='black')
                    ax.add_patch(plotted_grid_square)

                    # This labels each vehicle with their vehicle ID
                    if grid_square[1][0][0] not in annotated:

                        if crash:

                            if grid_square[1][0][0] == 79:
                                plt.text(grid_square[0][0]+20, grid_square[0][1]-12, f"ID: 2")

                            elif grid_square[1][0][0] == -2085:
                                plt.text(grid_square[0][0]-40, grid_square[0][1]+40, f"Occluding Vehicle")

                        else:

                            if grid_square[1][0][0] == 79:
                                plt.text(grid_square[0][0]-70, grid_square[0][1]+30, f"ID: 2")

                            elif grid_square[1][0][0] == -2085:
                                plt.text(grid_square[0][0]-40, grid_square[0][1]+40, f"Occluding Vehicle")

                        annotated.append(grid_square[1][0][0])
                        # Draw arrow from centre of vehicle
                        vehicle_id = grid_square[1][0][0] # WARNING: This should only have one element! Two would mean that a collision occurred.

                        # Add arrows to label the direction of the vehicles
                        for v in other_vehicles:
                            if v.id == vehicle_id:
                                self.draw_arrow(v)

            plt.xlim([self.origin[0] + 150, self.width + self.origin[0] - 150])
            plt.ylim([self.origin[1] + 390, self.length + self.origin[1] - 360])

            if title != None:
                plt.title(title)

            if filename:
                plt.savefig(filename, format='pdf')
                plt.close()
            else:
                plt.show()



    def visualize_occupancy_map_for_asymmetric_occlusion(self, subject_vehicle, relevant_vehicles, occluding_vehicle=None, filename=None, title=None):

        if occluding_vehicle != None:
            other_vehicles = [occluding_vehicle] + relevant_vehicles
        else:
            other_vehicles = relevant_vehicles
        # Create figure and axes
        fig, ax = plt.subplots()

        annotated = []

        for sequence in occlusion_constants.SEGMENT_SEQUENCES:
            centreline = scenario_utilities.get_centreline_from_segment_sequence(sequence[0])
            x_points = [i[0] * occlusion_constants.MAP_SCALE for i in centreline]
            y_points = [i[1] * occlusion_constants.MAP_SCALE for i in centreline]
            ax.plot(x_points,y_points, 'black', alpha=0.5, linewidth=0.2)

        for grid_square in self.grid.items():

            # Draw raycast
            if subject_vehicle.id in grid_square[1][1]:
                plotted_grid_square = patches.Rectangle((grid_square[0][0], grid_square[0][1]), \
                    width=self.grid_size, height=self.grid_size, linewidth=1, facecolor='red')
                ax.add_patch(plotted_grid_square)

            # Plot subject vehicle
            elif subject_vehicle.id in grid_square[1][0]:
                plotted_grid_square = patches.Rectangle((grid_square[0][0], grid_square[0][1]), \
                    width=self.grid_size, height=self.grid_size, linewidth=1, facecolor='black')
                ax.add_patch(plotted_grid_square)

                # This labels each vehicle with their vehicle ID
                if grid_square[1][0][0] not in annotated:
                    plt.text(grid_square[0][0]+20, grid_square[0][1]+20, f"ID: {grid_square[1][0][0]}")
                    annotated.append(grid_square[1][0][0])

                self.draw_arrow(subject_vehicle)

            # Draw black to signal this is bounding box of a vehicle.
            elif grid_square[1][0]:
                plotted_grid_square = patches.Rectangle((grid_square[0][0], grid_square[0][1]), \
                    width=self.grid_size, height=self.grid_size, linewidth=1, facecolor='black')
                ax.add_patch(plotted_grid_square)

                # This labels each vehicle with their vehicle ID
                if grid_square[1][0][0] not in annotated:

                    if grid_square[1][0][0] == 1:
                        plt.text(grid_square[0][0]+20, grid_square[0][1]+20, f"ID: {grid_square[1][0][0]}")

                    elif grid_square[1][0][0] == 2:
                        plt.text(grid_square[0][0]+15, grid_square[0][1]+20, f"ID: {grid_square[1][0][0]}")

                    elif grid_square[1][0][0] == 3:
                        plt.text(grid_square[0][0]-20, grid_square[0][1]-20, f"ID: {grid_square[1][0][0]}")

                    
                    annotated.append(grid_square[1][0][0])
                    # Draw arrow from centre of vehicle
                    vehicle_id = grid_square[1][0][0] # WARNING: This should only have one element! Two would mean that a collision occurred.

                    # Add arrows to label the direction of the vehicles
                    self.draw_arrow(subject_vehicle)
                    for v in other_vehicles:
                        if v.id == vehicle_id:
                            self.draw_arrow(v)

        plt.xlim([self.origin[0] + 150, self.width + self.origin[0] - 150])
        plt.ylim([self.origin[1] + 390, self.length + self.origin[1] - 360])

        if title != None:
            plt.title(title)

        if filename:
            plt.savefig(filename, format='pdf')
            plt.close()
        else:
            plt.show()




    def default_visualize_occupancy_map(self, vehicles, filename=None):

        # Create figure and axes
        fig, ax = plt.subplots()
        annotated = []
        

        for sequence in occlusion_constants.SEGMENT_SEQUENCES:
            centreline = scenario_utilities.get_centreline_from_segment_sequence(sequence[0])
            x_points = [i[0] * occlusion_constants.MAP_SCALE for i in centreline]
            y_points = [i[1] * occlusion_constants.MAP_SCALE for i in centreline]
            ax.plot(x_points,y_points, 'black', alpha=0.4, linewidth=0.2)


        for grid_square in self.grid.items():

            # Draw black to signal this is bounding box of a vehicle.
            if grid_square[1][0]:
                    plotted_grid_square = patches.Rectangle((grid_square[0][0], grid_square[0][1]), \
                        width=self.grid_size, height=self.grid_size, linewidth=1, facecolor='black')
                    ax.add_patch(plotted_grid_square)

                    # This labels each vehicle with their vehicle ID
                    if grid_square[1][0][0] not in annotated:
                        # plt.text(grid_square[0][0], grid_square[0][1], f"ID: {grid_square[1][0][0]}")
                        annotated.append(grid_square[1][0][0])


                        # Draw arrow from centre of vehicle
                        vehicle_id = grid_square[1][0][0] # WARNING: This should only have one element! Two would mean that a collision occurred.

                        # Add arrows to label the direction of the vehicles
                        for v in vehicles:
                            if v.id == vehicle_id:
                                self.draw_arrow(v)

        plt.xlim([self.origin[0] + 150, self.width + self.origin[0] - 150])
        plt.ylim([self.origin[1] + 390, self.length + self.origin[1] - 360])

        if filename:
            plt.savefig(filename, format='pdf')
            plt.close()
        else:
            plt.show()
