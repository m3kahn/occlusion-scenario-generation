

'''
The VehicleState class holds all the information needed to describe each vehicle in the dataset

Created on May 10, 2021

@author: Maximilian Kahn, Atrisha Sarkar
'''
import utilities.occlusion_constants as occlusion_constants
import math


def kph_to_mps(kph):
    return kph/3.6

class VehicleState:
    def set_track_info(self,vehicle_track_info):
        self.track_info_set = True
        self.track_id = vehicle_track_info[0,]
        self.id = int(self.track_id)
        if vehicle_track_info[1,] is None:
            print("no track for",self.track_id)
        self.x = float(vehicle_track_info[1,])
        self.y = float(vehicle_track_info[2,])
        self.speed = kph_to_mps(float(vehicle_track_info[3,]))
        self.tan_acc = float(vehicle_track_info[4,])
        self.long_acc = float(vehicle_track_info[5,])
        self.time = float(vehicle_track_info[6,])
        self.current_time = self.time
        self.yaw = float(vehicle_track_info[7,])
        self.traffic_region = vehicle_track_info[8,]

    def __str__(self):
        return f"Vehicle ID: {self.id}, CURRENT TIME: {self.current_time}\nCURRENT SEGMENT: {self.current_segment}, SEGMENT SEQ: {self.segment_seq}\n" \
               f"X: {self.x}, Y: {self.y}, ANGLE: {self.angle}\nDIRECTION: {self.direction}, TRAFFIC_LIGHT: {self.signal}" 
        
    def __init__(self, vehicle_type='car'):
        self.track_info_set = True

        self.vehicle_type = vehicle_type
        self.length, self.width = self.get_vehicle_size(vehicle_type)

        self.fov_distance = occlusion_constants.FOV_DISTANCE
        self.fov_range = occlusion_constants.FOV_RANGE

    def set_x(self, x):
        self.x = x

    def set_y(self, y):
        self.y = y

    def set_speed(self, speed):
        self.speed = speed
    
    def set_current_segment(self,segment):
        self.current_segment = segment
    
    def set_current_lane(self,current_lane):
        self.current_lane = current_lane
    
    def set_segment_seq(self,segment_seq):
        self.segment_seq = segment_seq
        
    def set_current_time(self,time):
        if time is not None:
            self.current_time = float(time)
        else:
            self.current_time = None
        
    def set_gates(self,gates):
        self.gates = gates
        
    def set_traffic_light(self,signal):
        self.signal = signal
    
    def set_entry_exit_time(self,time_tuple):
        self.entry_exit_time = time_tuple
        
    def set_id(self,id):
        self.id = id
        
    def set_gate_crossing_times(self,times):
        self.gate_crossing_times = times
        
    def set_vect_to_segment_exit(self,dist):
        self.vect_to_segment_exit= dist
        
    def set_out_of_view(self,oov):
        self.out_of_view = oov
        
    def set_full_track(self,track):
        self.track = track
    
    def set_leading_vehicle(self,leading_vehicle):
        self.leading_vehicle = leading_vehicle
    
    def set_merging_vehicle(self,merging_vehicle):
        self.merging_vehicle = merging_vehicle
    
    def set_path_origin(self,point):
        self.path_origin = point
        
    def set_current_l1_action(self,l1_action):
        self.l1_action = l1_action
        
    def set_current_l2_action(self,l2_action):
        self.l2_action = l2_action
        
    def set_direction(self,direction):
        self.direction = direction
        
    def set_task(self,task):
        self.task = task
        
    def set_time_to_next_signal(self, next_signal_change):
        self.next_signal_change = next_signal_change
        
    def set_relev_crosswalks(self,crosswalks):
        self.relev_crosswalks = crosswalks
        
    def set_relev_pedestrians(self,pedestrians):
        self.relev_pedestrians = pedestrians
        
    def set_scene_state(self,scene_state):
        self.scene_state = scene_state
    
    def set_dist_to_sub_agent(self,dist):
        self.dist_to_sv = dist

    def set_angle(self, angle):
        self.angle = angle

    def set_vehicle_type(self, vehicle_type):
        self.vehicle_type = vehicle_type

    def set_occlusion_level(self, occlusion_level):
        self.occlusion_level = occlusion_level

    def set_has_oncoming_vehicle(self, has_oncoming_vehicle):
        self.has_oncoming_vehicle = has_oncoming_vehicle

    @staticmethod
    def get_vehicle_size(vehicle_type):
        if vehicle_type == "car":
            length = occlusion_constants.OCC_MAP_CAR_LENGTH
            width = occlusion_constants.OCC_MAP_CAR_WIDTH
            
        elif vehicle_type == "medium_vehicle":
            length = occlusion_constants.OCC_MAP_MEDIUM_VEHICLE_LENGTH
            width = occlusion_constants.OCC_MAP_MEDIUM_VEHICLE_WIDTH

        elif vehicle_type == "heavy_vehicle":
            length = occlusion_constants.OCC_MAP_HEAVY_VEHICLE_LENGTH
            width = occlusion_constants.OCC_MAP_HEAVY_VEHICLE_WIDTH

        return length, width

    def get_bounding_box_points(self, for_occ_map=True):

        # If the bounding box points are for an occupancy map calculation
        if for_occ_map:
            x = self.x * occlusion_constants.MAP_SCALE
            y = self.y * occlusion_constants.MAP_SCALE
            length = self.length
            width = self.width
        else:
            x = self.x
            y = self.y

            length = self.length / occlusion_constants.MAP_SCALE
            width = self.width / occlusion_constants.MAP_SCALE

        front_left = (x + length / 2, y + width / 2)
        front_right = (x + length / 2, y - width / 2)
        back_left = (x - length / 2, y + width / 2)
        back_right = (x - length / 2, y - width / 2)

        bounding_box_points = [front_left, front_right, back_right, back_left]
        rotated_bounding_box_points = []

        for point in bounding_box_points:
            rotated_x = (point[0] - x) * math.cos(self.angle) - \
                        (point[1] - y) * math.sin(self.angle) + x
            rotated_y = (point[0] - x) * math.sin(self.angle) + \
                        (point[1] - y) * math.cos(self.angle) + y

            if for_occ_map:
                rotated_bounding_box_points.append((int(round(rotated_x)),
                                                    int(round(rotated_y))))
            else:
                rotated_bounding_box_points.append((rotated_x,rotated_y))

        return rotated_bounding_box_points
