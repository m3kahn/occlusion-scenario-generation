

'''
Useful functions when working with scenarios

Created on May 10, 2021

@author: Maximilian Kahn
'''

from utilities.vehicle_state import VehicleState
from utilities import occupancy_map 
from utilities import occlusion_constants
from utilities import regenerate_NE_scenarios
from utilities import no_SOV_risk_computation
from occlusion_scenario import occlusion_scenario_builder
from game import game_utilities
from occlusion_durations import occlusion_duration_times, straight_or_left_turn
import occlusion_durations
import statistics
from shapely.geometry.polygon import Polygon, LineString
from matplotlib import pyplot as plt
import matplotlib.animation as animation
import itertools
import sqlite3
import ast
import numpy as np
import math
import copy
import sys
import os
import csv


def angle_check(angle):
    if angle > 2 * occlusion_constants.PI:
        return angle - 2 * occlusion_constants.PI
    elif angle < 0:
        return angle + 2 * occlusion_constants.PI
    else:
        return angle

# To format db queries
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def get_merging_vehicle(veh_state):
    return None

def compute_distance(p1,p2):
    return math.hypot(abs(p2[0]-p1[0]), abs(p2[1]-p1[1]))

def kph_to_mps(kph):
    return kph/3.6

def get_relevant_vehicles(veh_state):
    relev_agents = []
    if veh_state.signal == 'R' and occlusion_constants.SEGMENT_MAP[veh_state.current_segment] in ['through-lane-entry','left-turn-lane']:
        ''' only relevant agents is the leading vehicle if present, which will be determined later'''
        return relev_agents
    conn = sqlite3.connect(occlusion_constants.DB)
    c = conn.cursor()
    path = veh_state.segment_seq
    signal = veh_state.signal
    conflicts = []
    other_agent_paths,other_agent_gates,other_agent_signal = [],[],[]
    subject_path = None
    if veh_state.gates[0] is not None and veh_state.gates[1] is not None:

        gates = veh_state.gates
        if veh_state.gate_crossing_times[1] is not None and veh_state.current_time is not None and veh_state.current_time < veh_state.gate_crossing_times[1]:
            q_string = "SELECT * FROM CONFLICT_POINTS WHERE PATH_1_GATES LIKE '"+str(gates).replace(' ','')+"' OR PATH_2_GATES LIKE '"+str(gates).replace(' ','')+"'"
        else:
            q_string = "SELECT * FROM CONFLICT_POINTS WHERE ((PATH_1_GATES LIKE '"+str(gates).replace(' ','')+"') OR (PATH_2_GATES LIKE '" +str(gates).replace(' ','')+"')) AND  POINT_LOCATION = 'AFTER_INTERSECTION'"
        
        c.execute(q_string)
        res = c.fetchall()
        for row in res:
            if row[2] == str(gates).replace(' ',''):
                subject_path = 1
                other_agent_signal = row[9][1:-1].split(',')
                other_agent_path = 'L'+'_'+row[3][4].upper()+'_'+row[3][11].upper()
                other_agents_traffic_light = get_traffic_signal(veh_state.current_time, other_agent_path)

                if other_agents_traffic_light in other_agent_signal:
                    conflicts.append((row,subject_path))
            elif row[4] == str(gates).replace(' ',''):
                subject_path = 2
                other_agent_signal = row[8][1:-1].split(',')
                other_agent_path = 'L'+'_'+row[1][4].upper()+'_'+row[3][11].upper()
                other_agents_traffic_light = get_traffic_signal(veh_state.current_time, other_agent_path)

                if other_agents_traffic_light in other_agent_signal:
                    conflicts.append((row,subject_path))

    elif path[0] != 'NA' and path[1] != 'NA':
        path_string = '['+path[0]+','+path[1]+']'
        if (veh_state.gate_crossing_times[1] is not None and veh_state.current_time is not None and veh_state.current_time < veh_state.gate_crossing_times[1]) or (veh_state.current_segment != veh_state.segment_seq[-1]):
            q_string = "SELECT * FROM CONFLICT_POINTS WHERE (PATH_1 LIKE '"+path_string+"' AND SIGNAL_STATE_PATH_1 LIKE '%"+signal \
                    +"%') OR (PATH_2 LIKE '"+path_string+"' AND SIGNAL_STATE_PATH_2 LIKE '%"+signal+"%')"
        else:
            q_string = "SELECT * FROM CONFLICT_POINTS WHERE ((PATH_1 LIKE '"+path_string+"' AND SIGNAL_STATE_PATH_1 LIKE '%"+signal \
                    +"%') OR (PATH_2 LIKE '"+path_string+"' AND SIGNAL_STATE_PATH_2 LIKE '%"+signal+"%')) AND POINT_LOCATION = 'AFTER_INTERSECTION'"
        c.execute(q_string)
        res = c.fetchall()
        for row in res:
            if row[1] == path_string:
                subject_path = 1
                other_agent_signal = row[9][1:-1].split(',')
                other_agent_path = 'L'+'_'+row[3][4].upper()+'_'+row[3][11].upper()
                other_agents_traffic_light = get_traffic_signal(veh_state.current_time, other_agent_path)
                if other_agents_traffic_light in other_agent_signal:
                    conflicts.append((row,subject_path))
            elif row[3] == path_string:
                subject_path = 2
                other_agent_signal = row[8][1:-1].split(',')
                other_agent_path = 'L'+'_'+row[1][4].upper()+'_'+row[3][11].upper()
                other_agents_traffic_light = get_traffic_signal(veh_state.current_time, other_agent_path)
                if other_agents_traffic_light in other_agent_signal:
                    conflicts.append((row,subject_path))
            
    else:
        print('cannot find relevant agents for',veh_state.id,veh_state.current_time)
    
    for c in conflicts:
        curr_conflict_agents = query_agent(c[0],c[1],veh_state)
        for c_a in curr_conflict_agents: 
            if c_a not in relev_agents:
                relev_agents.append(c_a)
    return relev_agents

def reduce_relev_agents(agent_id,time_ts,relev_agents):
    found = False

    # agent_id will never be an invalid vehicle because this is filtered out 
    # in get_traffic_agents_at_current_time()
    # However, we need to filter out invalid vehicles in relev_agents
    invalid_vehicles = get_invalid_vehicles()
    removed_relev_agents = []
    for v in relev_agents:
        if v in invalid_vehicles:
            removed_relev_agents.append(v)

    for removed_v in removed_relev_agents:
        relev_agents.remove(removed_v)

    veh_state = setup_vehicle_state(agent_id, time_ts)
    veh_state.set_dist_to_sub_agent(0.0)

    ra_states = []
    for r_a_id in relev_agents:
        found = False
        r_a_state = setup_vehicle_state(r_a_id, time_ts)

        if r_a_state == None:
            continue
            
        dist_to_sv = math.hypot(veh_state.x-r_a_state.x,veh_state.y-r_a_state.y)

        # If a vehicle is farther than this threshold than we don't include it in the scenario
        if dist_to_sv > occlusion_constants.MAXIMUM_DISTANCE_FROM_SUBJECT_VEHICLE:
            continue

        r_a_state.set_dist_to_sub_agent(dist_to_sv)

        ra_states.append(r_a_state)
    ra_map = dict()
    for k,v in occlusion_constants.RELEV_REDUCTION_MAP.items():
        for relev_agent in ra_states:

            if occlusion_constants.TASK_MAP[veh_state.direction] != k:
                break
            else:
                if k not in ra_map:
                    ra_map[k] = dict()
                for incl_desc in v:
                    if occlusion_constants.SEGMENT_MAP[relev_agent.current_segment] == incl_desc[0]:
                        if incl_desc[1] is not None:
                            if relev_agent.current_segment[-1] == incl_desc[1]:
                                if incl_desc not in ra_map[k]:
                                    ra_map[k][incl_desc] = [relev_agent]
                                else:
                                    ra_map[k][incl_desc].append(relev_agent)
                        else:
                            if incl_desc not in ra_map[k]:
                                ra_map[k][incl_desc] = [relev_agent]
                            else:
                                ra_map[k][incl_desc].append(relev_agent)
    incl_list = [veh_state.leading_vehicle.id] if veh_state.leading_vehicle is not None else []
    for dir,v_dir in ra_map.items():
        for incl_desc,ras in v_dir.items():
            ras.sort(key=lambda x: x.dist_to_sv)
            if incl_desc[3] is not None:
                ras = [x for x in ras if x.dist_to_sv<=incl_desc[3]]
            if len(ras) > 0:
                if incl_desc[2] == 1:
                    incl_list.append(ras[0].id)
                else:
                    incl_list.append(ras[0].id)
                    if len(ras) > 1:
                        incl_list.append(ras[-1].id)
            ra_map[dir][incl_desc] = incl_list
    return incl_list


''' this function interpolates track information only for real trajectories '''
def interpolate_track_info(veh_state,forward,backward,partial_track=None):
    if partial_track is not None and len(partial_track)>0:
        track_info = partial_track
    else:
        track_info = [None]*9
    veh_id,curr_time = veh_state.id,veh_state.current_time
    track_info[0],track_info[6] = veh_id,curr_time
    veh_entry_segment, veh_exit_segment = veh_state.segment_seq[0],veh_state.segment_seq[-1]
    if not hasattr(veh_state, 'track'):
        q_string = "select TIME,TRAFFIC_REGIONS,X,Y,SPEED from trajectories_0"+occlusion_constants.CURRENT_FILE_ID+" where track_id="+str(veh_id)+" order by time"
        conn = sqlite3.connect(occlusion_constants.DB)
        c = conn.cursor()
        c.execute(q_string)
        res = c.fetchall()
        traffic_regions = []
        for r in res:
            traffic_regions.append((float(r[0]),r[1]))
        veh_entry_speed, veh_exit_speed = res[0][4],res[-1][4]
        conn.close()
    else:
        for r in veh_state.track:
            traffic_regions = [(float(x[6]),x[8]) for x in veh_state.track]
        veh_entry_speed, veh_exit_speed = veh_state.track[0][3],veh_state.track[-1][3]
    for_idx,back_idx = None,None

    if not forward and not backward:
        ''' interpolate in the middle'''
        idx = [x[0] for x in traffic_regions].index(curr_time)

        for i in np.arange(idx,len(traffic_regions)):
            if traffic_regions[i][1] is not None and len(traffic_regions[i][1]) > 1:
                for_idx = i
                break
        for j in np.arange(idx,-1,-1):
            if traffic_regions[j][1] is not None and len(traffic_regions[j][1]) > 1:
                back_idx = j
                break
        if for_idx is None:
            ''' the missing entry is the last one '''
            track_info[8] = veh_exit_segment
        elif back_idx is None:
            ''' the missing entry is the first one'''
            track_info[8] = veh_entry_segment
        else:
            if abs(idx - back_idx) < abs(for_idx - idx):
                ''' missing idx is closer to a previously assigned value in the past'''
                track_info[8] = traffic_regions[back_idx][1]
            else:
                track_info[8] = traffic_regions[for_idx][1]
   
    elif forward:
        ''' extrapolate forward in time '''
        conn = sqlite3.connect(occlusion_constants.DB)
        c = conn.cursor()
        q_string = "SELECT * FROM TRAFFIC_REGIONS_DEF WHERE NAME = '"+veh_exit_segment+"' AND REGION_PROPERTY = 'center_line'"
        c.execute(q_string)
        res = c.fetchone()
        if res is None:
            sys.exit("cannot find centerline for"+veh_exit_segment)
        exit_pos_X = ast.literal_eval(res[4])
        exit_pos_Y = ast.literal_eval(res[5])
        angle_of_centerline = math.atan2(exit_pos_Y[1]-exit_pos_Y[0],exit_pos_X[1]-exit_pos_X[0])
        proj_pos_X = exit_pos_X[1] + veh_entry_speed * math.cos(angle_of_centerline) * abs(veh_state.entry_exit_time[1] - veh_state.current_time)
        proj_pos_Y = exit_pos_Y[1] + veh_entry_speed * math.sin(angle_of_centerline) * abs(veh_state.entry_exit_time[1] - veh_state.current_time)
        track_info[8] = veh_state.segment_seq[-1]
        track_info[1] = proj_pos_X
        track_info[2] = proj_pos_Y
        track_info[3] = veh_entry_speed
        track_info[4] = 0
        track_info[5] = 0
        track_info[7] = angle_of_centerline if angle_of_centerline > 0 else 2 * math.pi + angle_of_centerline 
        conn.close()
    elif backward:
        ''' extrapolate backward in time '''
        idx = 0
        conn = sqlite3.connect(occlusion_constants.DB)
        c = conn.cursor()
        q_string = "SELECT * FROM TRAFFIC_REGIONS_DEF WHERE NAME = '"+veh_entry_segment+"' AND REGION_PROPERTY = 'center_line'"
        c.execute(q_string)
        res = c.fetchone()
        entry_pos_X = ast.literal_eval(res[4])
        entry_pos_Y = ast.literal_eval(res[5])
        angle_of_centerline = math.atan2(entry_pos_Y[0]-entry_pos_Y[1],entry_pos_X[0]-entry_pos_X[1])
        proj_pos_X = entry_pos_X[0] + veh_entry_speed * math.cos(angle_of_centerline) * abs(veh_state.entry_exit_time[0] - veh_state.current_time)
        proj_pos_Y = entry_pos_Y[0] + veh_entry_speed * math.sin(angle_of_centerline) * abs(veh_state.entry_exit_time[0] - veh_state.current_time)
        track_info[8] = veh_state.segment_seq[0]
        track_info[1] = proj_pos_X
        track_info[2] = proj_pos_Y
        track_info[3] = veh_entry_speed
        track_info[4] = 0
        track_info[5] = 0
        track_info[7] = math.pi + (angle_of_centerline if angle_of_centerline > 0 else 2 * math.pi + angle_of_centerline)
        conn.close()
    return track_info 

def get_current_segment(r_a_state,r_a_track_region,r_a_track_segment_seq,curr_time):
    r_a_current_segment = assign_curent_segment(r_a_track_region,r_a_state,False)
    if hasattr(r_a_state, 'entry_exit_time') and r_a_current_segment is None:
        entry_exit_time = r_a_state.entry_exit_time
        if curr_time < entry_exit_time[0]:
            r_a_current_segment = r_a_track_segment_seq[0]
        elif curr_time > entry_exit_time[1]:
            r_a_current_segment = r_a_track_segment_seq[-1]
    if r_a_current_segment is None:
        print(r_a_track_region,r_a_track_segment_seq,r_a_state.id,curr_time)
        sys.exit('no current segment found for relev agent')
    return r_a_current_segment


def has_crossed(segment,veh_state):
    conn = sqlite3.connect(occlusion_constants.DB)
    c = conn.cursor()
    
    ''' for each segment loop and check if the vehicle has not yet crossed it'''
    
    q_string = "SELECT * FROM TRAFFIC_REGIONS_DEF WHERE NAME = '"+segment+"' AND REGION_PROPERTY = 'exit_boundary'"
    c.execute(q_string)
    res = c.fetchone()
    if res is None or len(res) < 1:
        sys.exit('exit boundary not found for '+str(segment))
    exit_pos_X = ast.literal_eval(res[4])
    exit_pos_Y = ast.literal_eval(res[5])
    m = (exit_pos_Y[1] - exit_pos_Y[0]) / (exit_pos_X[1] - exit_pos_X[0])
    c = (exit_pos_Y[0] - (m * exit_pos_X[0]))
    
    veh_pos_x, veh_pos_y = veh_state.x,veh_state.y
    if veh_state.gate_crossing_times[0] is not None:
        veh_orig_x, veh_orig_y = veh_state.track[0][1],veh_state.track[0][2]
    else:
        veh_orig_x, veh_orig_y = veh_state.path_origin[0],veh_state.path_origin[1]

    res_wrt_origin = veh_orig_y - (m*veh_orig_x) - c
    res_wrt_point = veh_pos_y - (m*veh_pos_x) - c
    
    conn.close()
    return True if np.sign(res_wrt_origin) != np.sign(res_wrt_point) else False



def assign_curent_segment(traffic_region_list,veh_state,simulation=False):
    track_segment = veh_state.segment_seq
    ''' if it is simulation, then we would have to assign a segment to the 
    point not seen in the data. '''
    assignment_from_region_failed = False
    if not simulation and traffic_region_list is not None:
        traffic_region_list = str(traffic_region_list).replace(' ','').strip(',')
        traffic_region_list = traffic_region_list.split(',')
        for segment in reversed(track_segment):
            for track_region in traffic_region_list:
                if region_equivalence(track_region, segment):
                    return segment
        ''' assigning based on region failed '''
        
        assignment_from_region_failed = True  
    if simulation or assignment_from_region_failed or traffic_region_list is None:
        curr_time = veh_state.current_time
        if simulation:
                ''' get the first segment. This should be set to the correct value for time=0 since we start the simulation
                from the real scene.'''
                prev_segment = None
                try:
                    prev_segment = veh_state.current_segment
                except AttributeError:
                    sys.exit('previous segment is not set (possibly for the initial scene)')
                
                if has_crossed(prev_segment, (veh_state.x,veh_state.y)):
                    return track_segment[track_segment.index(prev_segment)+1] 
                else:
                    return prev_segment
        else:
            if not has_crossed(track_segment[0], veh_state):
                return track_segment[0]
            elif has_crossed(track_segment[-1], veh_state):
                return track_segment[-1]
            else:
                for seg,next_seg in zip(track_segment[:-1],track_segment[1:]):
                    if not has_crossed(next_seg, veh_state) and has_crossed(seg, veh_state):
                        return next_seg
        ''' if still unable to assign, try with gate crossing times'''   
        if veh_state.gate_crossing_times[0] is not None and curr_time < veh_state.gate_crossing_times[0]:
            ''' the vehicle hasn't entered the intersection, so return the first segment.'''
            return track_segment[0]
        elif veh_state.gate_crossing_times[1] is not None and curr_time > veh_state.gate_crossing_times[1]:
            ''' the vehicle has left the intersection, so return the last segment'''
            return track_segment[-1]
        ''' everything failed'''
        return None 

def region_equivalence(track_region,track_segment):
    if track_region == track_segment or track_region.replace('-','_') == track_segment.replace('-','_'):
        return True
    if track_segment[:2] == 'ln':
        return track_segment[:-2] == track_region[:-2] if track_region[-2] == '-' else track_segment[:-1] == track_region[:-1]
    else:
        if 'int_entry_' in track_segment.replace('-','_'):
            if track_region[0:4] == 'l_'+track_segment[-1]+'_':
                return True
        return False 


def guess_track_info(veh_state,partial_track=None):
    veh_id,curr_time = veh_state.id,veh_state.current_time
    curr_time = float(curr_time)
    entry_time,exit_time = veh_state.entry_exit_time[0],veh_state.entry_exit_time[1]
    veh_state.set_entry_exit_time((entry_time,exit_time))
    if entry_time <= curr_time <= exit_time:
        ''' need to interpolate '''
        track_info = interpolate_track_info(veh_state,False,False,partial_track)
    elif curr_time > exit_time:
        ''' need to extrapolate forward in time'''
        track_info = interpolate_track_info(veh_state,True,False,partial_track)
    elif curr_time < entry_time:
        ''' need to extrapolate backward in time'''
        track_info = interpolate_track_info(veh_state,False,True,partial_track)
    return np.asarray(track_info)


def setup_vehicle_state(veh_id,time_ts):

    r_a_state = VehicleState()
    r_a_state.set_id(veh_id)
    r_a_state.set_current_time(time_ts)
    r_a_track = get_track(r_a_state,time_ts)
    r_a_track_segment_seq = get_track_segment_seq(veh_id)
    r_a_state.set_segment_seq(r_a_track_segment_seq)
    r_a_state.action_plans = dict()
    r_a_state.set_current_time(time_ts)
    entry_exit_time = get_entry_exit_time(r_a_state.id)
    r_a_state.set_entry_exit_time(entry_exit_time)
    if len(r_a_track) == 0:
        ''' this agent is out of the view currently'''
        r_a_state.set_out_of_view(True)
        r_a_track = None
    else:
        r_a_state.set_out_of_view(False)
        r_a_state.set_track_info(r_a_track[0,])
        r_a_track = r_a_track[0,]
    
    if r_a_state.out_of_view or r_a_track[11] is None:

        r_a_track_info = guess_track_info(r_a_state,r_a_track)

        # if this triggers then we cannot generate track_info information for this vehicle and
        # so we skip it
        if r_a_track_info[1] == None or r_a_track_info[2] == None:
            return None

        # We are setting position and angle based on projected x. y, and angle.
        # The vehicle here is outside of the map!
        r_a_state.set_x(float(r_a_track_info[1]))
        r_a_state.set_y(float(r_a_track_info[2]))
        r_a_state.set_angle(float(r_a_track_info[7]))

        if r_a_track_info[1,] is None:
            brk = 1
        r_a_state.set_track_info(r_a_track_info)
        r_a_track_region = r_a_track_info[8,]
        if r_a_track_region is None:
            sys.exit('need to guess traffic region for relev agent')
        r_a_current_segment = get_current_segment(r_a_state,r_a_track_region,r_a_track_segment_seq,time_ts)
    else:
        r_a_current_segment = r_a_track[11]

        position = get_vehicle_position(veh_id, time_ts)

        r_a_state.set_x(position[0][0])
        r_a_state.set_y(position[0][1])
        
        angle = get_angle(r_a_state)
        r_a_state.set_angle(angle) 
   

    # r_a_state meanign relevant agent state I believe
    r_a_state.set_current_segment(r_a_current_segment)
    ''' 
    #for now we will only take into account the leading vehicles of the subject agent's relevant vehicles when constructing the possible actions.'''
    lead_vehicle = get_leading_vehicles(r_a_state)
    r_a_state.set_leading_vehicle(lead_vehicle)
    merging_vehicle = get_merging_vehicle(r_a_state)
    r_a_state.set_merging_vehicle(merging_vehicle)
    r_a_direction = 'L_'+r_a_track_segment_seq[0][3].upper()+'_'+r_a_track_segment_seq[-1][3].upper()
    r_a_task = occlusion_constants.TASK_MAP[r_a_direction]
    r_a_state.set_task(r_a_task)
    r_a_traffic_light = get_traffic_signal(time_ts, r_a_direction)

    r_a_state.set_traffic_light(r_a_traffic_light)
    r_a_state.set_direction(r_a_direction)
    next_signal_change = get_time_to_next_signal(time_ts, r_a_direction, r_a_traffic_light)
    r_a_state.set_time_to_next_signal(next_signal_change)
    return r_a_state


def gate_crossing_times(veh_state):
    entry_gate,exit_gate = veh_state.gates[0],veh_state.gates[1]
    conn = sqlite3.connect(occlusion_constants.DB)
    c = conn.cursor()
    entry_time,exit_time = None,None
    if entry_gate is not None:
        q_string = "select TIME from GATE_CROSSING_EVENTS WHERE GATE_ID = "+str(entry_gate)+" AND TRACK_ID = "+str(veh_state.id)
        c.execute(q_string)
        res = c.fetchone()
        entry_time = res[0] if res is not None else None
    if exit_gate is not None:
        q_string = "select TIME from GATE_CROSSING_EVENTS WHERE GATE_ID = "+str(exit_gate)+" AND TRACK_ID = "+str(veh_state.id)
        c.execute(q_string)
        res = c.fetchone()
        exit_time = res[0] if res is not None else None
    return (entry_time,exit_time)



def get_path_gates_direction(agent_id):
    path = ['NA','NA']
        
    '''return the gates '''

    conn = sqlite3.connect(occlusion_constants.DB)
    c = conn.cursor()
    gates = [None,None]
    o,d = None,None    
    if path[0] != 'NA' and path[1] != 'NA':
        o,d = path[0][3],path[1][3]
    q_string = "SELECT ENTRY_GATE,EXIT_GATE FROM TRAJECTORY_MOVEMENTS WHERE TRACK_ID = "+str(agent_id)
    c.execute(q_string)
    res = c.fetchall()
    e_g,ex_g = [],[]
    for row in res:
        if row[0] is not None:
            e_g = [int(row[0])]
            for k,v in occlusion_constants.GATE_MAP.items():
                if int(row[0]) in v:
                    o = k[0]
        if row[1] is not None:
            ex_g = [int(row[1])]
            for k,v in occlusion_constants.GATE_MAP.items():
                if int(row[1]) in v:
                    d = k[0]
    ''' if only one gate present, try to acquire it from the path information '''
    if e_g is None and path[0] != 'NA':
        _dir = path[0][3]
        _sign = '_entry' if path[0][-2] == '-' else '_exit'
        _key = _dir + _sign
        e_g = occlusion_constants.GATE_MAP[_key]
    if ex_g is None and path[1] != 'NA':
        _dir = path[1][3]
        _sign = '_entry' if path[1][-2] == '-' else '_exit'
        _key = _dir + _sign
        ex_g = occlusion_constants.GATE_MAP[_key]
    for _eg in e_g:
        for _exg in ex_g:
            gates[0]=_eg
            gates[1]=_exg  
    gates[0] = e_g[0] if len(e_g) > 0 else None
    gates[1] = ex_g[0] if len(ex_g) > 0 else None
    if o is not None and d is not None:
        direction = 'L_'+o.upper()+'_'+d.upper()
    else:
        direction = None
    return path,gates,direction


def get_traffic_signal(time,direction,file_id=None):
    conn = sqlite3.connect(occlusion_constants.DB)
    if direction == 'ALL':
        conn.row_factory = dict_factory
        c = conn.cursor()
        q_string = "SELECT * FROM TRAFFIC_LIGHTS WHERE TIME - "+str(time)+" > 0 AND FILE_ID = "+occlusion_constants.CURRENT_FILE_ID+" ORDER BY TIME"
        c.execute(q_string)
        res = c.fetchall()
        return res
    else:
        c = conn.cursor()
        signal = None
        q_string = "SELECT MAX(TIME),"+direction+" FROM TRAFFIC_LIGHTS WHERE TIME - "+str(time)+" <= 0 AND FILE_ID = " +occlusion_constants.CURRENT_FILE_ID
        c.execute(q_string)
        res = c.fetchone()
        signal = res[1]
        conn.close()
        return signal


def get_time_to_next_signal(time_ts,direction,curr_signal):
    conn = sqlite3.connect(occlusion_constants.DB)
    c = conn.cursor()
    q_string = "SELECT * FROM TRAFFIC_LIGHTS WHERE TIME - "+str(time_ts)+" > 0 AND "+direction+" <> '"+curr_signal+"' order by time"
    curr = c.execute(q_string)
    res = c.fetchone()
    all_directions = [description[0] for description in curr.description]
    dir_idx = all_directions.index(direction)
    if res is None:
        return (None,None)
    else:
        next_signal = res[dir_idx]
        time_to_change = float(res[-1]) - time_ts
        return (time_to_change,next_signal)


def query_agent(has_conflict,subject_path,veh_state):
    vehicles = []
    conn = sqlite3.connect(occlusion_constants.DB)
    c = conn.cursor()
    curr_time = float(veh_state.current_time)
    other_agent_gates,other_agent_path = [],[]
    if subject_path == 1:
        other_agent_gates = has_conflict[4]
        other_agent_path= has_conflict[3]
    elif subject_path == 2:
        other_agent_gates = has_conflict[2]
        other_agent_path = has_conflict[1]
    gates = ast.literal_eval(other_agent_gates)
    path = other_agent_path[1:-1].split(',')
    entry_gate,exit_gate = gates[0],gates[1]
    veh_intersection_exit_time = veh_state.gate_crossing_times[1]
    veh_scene_exit_time = veh_state.entry_exit_time[1]
    if has_conflict[-1] == 'ON_INTERSECTION':
        end_time = curr_time+occlusion_constants.RELEV_VEHS_TIME_THRESH if veh_intersection_exit_time is None else veh_intersection_exit_time+occlusion_constants.RELEV_VEHS_TIME_THRESH
    else:
        end_time = curr_time+occlusion_constants.RELEV_VEHS_TIME_THRESH if veh_scene_exit_time is None else veh_scene_exit_time
    q_string = "SELECT DISTINCT TRAJECTORIES_0"+occlusion_constants.CURRENT_FILE_ID+".TRACK_ID FROM TRAJECTORIES_0"+occlusion_constants.CURRENT_FILE_ID+",TRACKS WHERE TRAJECTORIES_0"+occlusion_constants.CURRENT_FILE_ID+".TRACK_ID=TRACKS.TRACK_ID AND (TIME BETWEEN "+str(curr_time)+" AND "+str(end_time)+") AND TRACKS.TYPE <> 'Pedestrian' AND TRACKS.TRACK_ID IN (SELECT DISTINCT TRACK_ID FROM TRAJECTORY_MOVEMENTS WHERE TRAFFIC_SEGMENT_SEQ LIKE '%"+path[0][:-1]+"%"+path[1][:-2]+"%' ORDER BY TRACK_ID)"
    c.execute(q_string)
    res = c.fetchall()
    if len(res) < 1:
        print('no relevant agents for query:',q_string)
    elif len(res) > 7:
        ''' too many agents. we can reduce the number by restricting the time threshold without significant impact '''
        q_string = "SELECT DISTINCT TRAJECTORIES_0"+occlusion_constants.CURRENT_FILE_ID+".TRACK_ID FROM TRAJECTORIES_0"+occlusion_constants.CURRENT_FILE_ID+",TRACKS WHERE TRAJECTORIES_0"+occlusion_constants.CURRENT_FILE_ID+".TRACK_ID=TRACKS.TRACK_ID AND TIME = "+str(curr_time)+" AND TRACKS.TYPE <> 'Pedestrian' AND TRACKS.TRACK_ID IN (SELECT DISTINCT TRACK_ID FROM TRAJECTORY_MOVEMENTS WHERE TRAFFIC_SEGMENT_SEQ LIKE '%"+path[0][:-1]+"%"+path[1][:-2]+"%' ORDER BY TRACK_ID)"
        c.execute(q_string)
        res = c.fetchall()
    
    for row in res:
        vehicles.append(row[0])
    conn.close()
    return vehicles


def get_entry_exit_time(track_id,file_id=occlusion_constants.CURRENT_FILE_ID):
    conn = sqlite3.connect(occlusion_constants.DB)
    c = conn.cursor()
    q_string = "select * from v_TIMES where track_id="+str(track_id)
    c.execute(q_string)
    res = c.fetchone()
    time_tuple = (float(res[1]),float(res[2]))
    conn.close()
    return time_tuple


def get_leading_vehicles(veh_state):
    path = veh_state.segment_seq
    time_ts = veh_state.current_time
    current_segment = veh_state.current_segment
    if current_segment not in path:
        print(veh_state.id)
    next_segment_idx = veh_state.segment_seq.index(current_segment)+1
    next_segment = veh_state.segment_seq[next_segment_idx] if next_segment_idx < len(veh_state.segment_seq) else veh_state.segment_seq[-1]
    veh_id = veh_state.id
    veh_pos_x = float(veh_state.x)
    veh_pos_y = float(veh_state.y)
    conn = sqlite3.connect(occlusion_constants.DB)
    c = conn.cursor()
    ''' find the exit boundaries of the current and next segment. This will help calculate which vehicles are ahead.'''
    q_string = "SELECT * FROM TRAFFIC_REGIONS_DEF WHERE (NAME like '"+current_segment+"%' OR NAME like '"+next_segment+"') and REGION_PROPERTY = 'exit_boundary'"
    ex_b_positions = dict()
    c.execute(q_string)
    res_exit_b = c.fetchall()
    for ex_b in res_exit_b:
        ex_b_positions[ex_b[0]] = (ast.literal_eval(ex_b[4]),ast.literal_eval(ex_b[5]))

    veh_vect_to_segment_exit = [((ex_b_positions[current_segment][0][0] - veh_pos_x) + (ex_b_positions[current_segment][0][1] - veh_pos_x))/2,\
                                ((ex_b_positions[current_segment][1][0] - veh_pos_y) + (ex_b_positions[current_segment][1][1] - veh_pos_y))/2]
    ''' find the vehicles that are in the current segment or the next and appears within the window of the subject vehicle '''
    if next_segment[-2] == '-':
        q_string = "SELECT T.TRACK_ID FROM TRAJECTORY_MOVEMENTS T, v_TIMES V WHERE (T.TRAFFIC_SEGMENT_SEQ LIKE '%''"+current_segment+"''%' OR T.TRAFFIC_SEGMENT_SEQ LIKE '%''"+next_segment[:-1]+"%') AND T.TRACK_ID = V.TRACK_ID AND (V.ENTRY_TIME <= "+str(time_ts)+" AND V.EXIT_TIME >= "+str(time_ts)+") AND T.TRACK_ID <> "+str(veh_id)
    else:
        q_string = "SELECT T.TRACK_ID FROM TRAJECTORY_MOVEMENTS T, v_TIMES V WHERE (T.TRAFFIC_SEGMENT_SEQ LIKE '%''"+current_segment+"''%' OR T.TRAFFIC_SEGMENT_SEQ LIKE '%''"+next_segment+"''%') AND T.TRACK_ID = V.TRACK_ID AND (V.ENTRY_TIME <= "+str(time_ts)+" AND V.EXIT_TIME >= "+str(time_ts)+") AND T.TRACK_ID <> "+str(veh_id)
    c.execute(q_string)
    res = c.fetchall()
    potential_lead_vehicles = []

    if len(res) > 0:
        for row in res:
            leading_vehicle_id = row[0]
            ''' find the position of the potential lead vehicle in the current time '''

            q_string = "select * from trajectories_0"+occlusion_constants.CURRENT_FILE_ID+",trajectories_0"+occlusion_constants.CURRENT_FILE_ID+"_ext where trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".track_id=trajectories_0"+occlusion_constants.CURRENT_FILE_ID+"_ext.track_id and trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".time=trajectories_0"+occlusion_constants.CURRENT_FILE_ID+"_ext.time and trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".track_id="+str(leading_vehicle_id)+" and trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".time = "+str(time_ts)
            c.execute(q_string)
            pt_res = c.fetchone()

            l_v_state = VehicleState()
            if pt_res is None:
                ''' this means that there is no entry for this vehicle in trajectories_0"+occlusion_constants.CURRENT_FILE_ID+"_ext yet'''
                continue
            l_v_state.set_id(pt_res[0])
            l_v_state.set_current_time(time_ts)
            l_v_track = get_track(l_v_state,time_ts)
            l_v_state.set_track_info(l_v_track[0,])
            l_v_track_segment_seq = get_track_segment_seq(l_v_state.id)
            l_v_state.set_segment_seq(l_v_track_segment_seq)
            l_v_current_segment = pt_res[11]
            l_v_state.set_current_segment(l_v_current_segment)
            l_v_state.set_current_l1_action(pt_res[12])
            if l_v_current_segment not in ex_b_positions.keys():
                ''' potential lead vehicle is not in the path (current or the next segment), so ignore '''
                continue
            else:
                lead_vehicle_pos = (float(pt_res[1]),float(pt_res[2]))
                l_v_segment_ex_b = ex_b_positions[l_v_current_segment]

                                    
                l_v_vect_to_segment_exit = [((ex_b_positions[l_v_current_segment][0][0] - lead_vehicle_pos[0]) + (ex_b_positions[l_v_current_segment][0][1] - lead_vehicle_pos[0]))/2,\
                                ((ex_b_positions[l_v_current_segment][1][0] - lead_vehicle_pos[1]) + (ex_b_positions[l_v_current_segment][1][1] - lead_vehicle_pos[1]))/2]
                l_v_state.set_vect_to_segment_exit(l_v_vect_to_segment_exit)
                if l_v_current_segment == current_segment and np.linalg.norm(l_v_vect_to_segment_exit) > np.linalg.norm(veh_vect_to_segment_exit):
                    ''' this vehicle is behind the subject vehicle '''
                    continue
                elif math.hypot(lead_vehicle_pos[0]-veh_pos_x,lead_vehicle_pos[1]-veh_pos_y) > occlusion_constants.LEAD_VEH_DIST_THRESH:
                    ''' this vehicle is too far '''
                    continue
                else:
                    potential_lead_vehicles.append(l_v_state)

        if len(potential_lead_vehicles) > 1:

            lv_idx,min_dist = 0,np.inf
            for idx, lv in enumerate(potential_lead_vehicles):
                dist_from_subject = math.hypot(float(lv.x)-veh_pos_x, float(lv.y)-veh_pos_y)
                if dist_from_subject < min_dist:
                    min_dist = dist_from_subject
                    lv_idx = idx
            return potential_lead_vehicles[lv_idx]  
        else:
            return potential_lead_vehicles[0] if len(potential_lead_vehicles) ==1 else None
    else:
        return None 


def get_all_vehicles_at_current_time(current_time):
    with sqlite3.connect(occlusion_constants.DB) as conn:
        c = conn.cursor()
        elements = "TRACK_ID"
        start_time = max(current_time - occlusion_constants.DB_TIME_INTERVAL/2, 0)
        end_time = current_time + occlusion_constants.DB_TIME_INTERVAL/2

        # It looks like the recorded track ids in TRAJECTORY_{}_EXT already exclude pedestrian trajectories and cars that are "invalid"
        execute_cmd = f"SELECT {elements} FROM TRAJECTORIES_0{occlusion_constants.CURRENT_FILE_ID}_EXT WHERE TIME BETWEEN {start_time} AND {end_time}"
        c.execute(execute_cmd)

        # query_results looks like: [(1,), (3,), (4,), (6,), (7,), ...]
        query_results = c.fetchall()
        vehicles = []
        invalid_vehicles = get_invalid_vehicles()

        for query in query_results:
            # If vehicle is invalid (e.g., possesses a segment seq not in segment sequences) then skip
            if query[0] in invalid_vehicles:
                continue

            # query[0] is the id for vehicle, v.
            v = setup_vehicle_state(query[0], current_time)
            path, gates, direction = get_path_gates_direction(query[0])

            v.set_gates(gates)
            gct = gate_crossing_times(v)
            v.set_gate_crossing_times(gct)

            entry_exit_time = get_entry_exit_time(v.id)
            v.set_entry_exit_time(entry_exit_time)
            vehicles.append(v)

    return vehicles

    
def get_vehicle_position(track_id, current_time):
    with sqlite3.connect(occlusion_constants.DB) as conn:
        c = conn.cursor()
        elements = "X, Y"
        start_time = max(current_time - occlusion_constants.DB_TIME_INTERVAL/2, 0)
        end_time = current_time + occlusion_constants.DB_TIME_INTERVAL/2
        execute_cmd = f"SELECT {elements} FROM TRAJECTORIES_0{occlusion_constants.CURRENT_FILE_ID} WHERE TRACK_ID={track_id} AND TIME BETWEEN {start_time} AND {end_time} ORDER BY TIME"
        c.execute(execute_cmd)
        vehicle_position = c.fetchall()
    return vehicle_position


def get_track(veh_state,curr_time,from_current=None):
    agent_id = veh_state.id
    conn = sqlite3.connect(occlusion_constants.DB)
    c = conn.cursor()
    if curr_time is not None:
        if from_current is None:
            q_string = "select * from trajectories_0"+occlusion_constants.CURRENT_FILE_ID+",trajectories_0"+occlusion_constants.CURRENT_FILE_ID+"_ext where trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".track_id=trajectories_0"+occlusion_constants.CURRENT_FILE_ID+"_ext.track_id and trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".time=trajectories_0"+occlusion_constants.CURRENT_FILE_ID+"_ext.time and trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".track_id="+str(agent_id)+" and trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".time="+str(curr_time)+" order by trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".time"
        else:
            q_string = "select * from trajectories_0"+occlusion_constants.CURRENT_FILE_ID+",trajectories_0"+occlusion_constants.CURRENT_FILE_ID+"_ext where trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".track_id=trajectories_0"+occlusion_constants.CURRENT_FILE_ID+"_ext.track_id and trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".time=trajectories_0"+occlusion_constants.CURRENT_FILE_ID+"_ext.time and trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".track_id="+str(agent_id)+" and trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".time >="+str(curr_time)+" order by trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".time"
    else:
        q_string = "select * from trajectories_0"+occlusion_constants.CURRENT_FILE_ID+",trajectories_0"+occlusion_constants.CURRENT_FILE_ID+"_ext where trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".track_id=trajectories_0"+occlusion_constants.CURRENT_FILE_ID+"_ext.track_id and trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".time=trajectories_0"+occlusion_constants.CURRENT_FILE_ID+"_ext.time and trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".track_id="+str(agent_id)+" order by trajectories_0"+occlusion_constants.CURRENT_FILE_ID+".time"
    c.execute(q_string)
    res = c.fetchall()
    l = []   
    for row in res:
        l.append(row)
    conn.close()
    return np.asarray(l)

def get_track_segment_seq(track_id):
    conn = sqlite3.connect(occlusion_constants.DB)
    c = conn.cursor()
    q_string = "SELECT TRAFFIC_SEGMENT_SEQ FROM TRAJECTORY_MOVEMENTS WHERE TRACK_ID = "+str(track_id)
    c.execute(q_string)
    res = c.fetchall()
    seq = []
    for row in res:
        seq = row[0]
    conn.close()
    return ast.literal_eval(seq) if seq is not None else None


def get_current_segment_for_db_vehicles(track_id, current_time):
    conn = sqlite3.connect(occlusion_constants.DB)
    c = conn.cursor()
    q_string = f"SELECT ASSIGNED_SEGMENT FROM TRAJECTORIES_0{occlusion_constants.CURRENT_FILE_ID}_EXT WHERE TRACK_ID={track_id} AND TIME={current_time}"
    c.execute(q_string)
    res = c.fetchall()
    return res[0][0]


''' add two parallel lines to line at a lateral distance on either side of line'''
def add_parallel(line, dist, dist2=None):
    if dist2 == None:
        dist2 = dist
    pointline = True if len(line) < 2 else False
    p_line_lat1, p_line_lat2 = [],[]
    if not pointline:
        for l_idx in np.arange(1,len(line)):
            pt1 = line[l_idx-1]
            pt2 = line[l_idx]
            cl_angle = math.atan2(pt2[1]-pt1[1], pt2[0]-pt1[0])
            cl_angle = cl_angle if cl_angle > 0 else (2*math.pi) - abs(cl_angle)
            cl_normal = (cl_angle + (math.pi/2))%(2*math.pi)
            ''' add the point normal to pt1 and pt2 on both sides '''
            pt1_lat1 = (pt1[0] + dist*np.cos(cl_normal), pt1[1] + dist*np.sin(cl_normal))
            pt1_lat2 = (pt1[0] - dist2*np.cos(cl_normal), pt1[1] - dist2*np.sin(cl_normal))
            
            pt2_lat1 = (pt2[0] + dist*np.cos(cl_normal), pt2[1] + dist*np.sin(cl_normal))
            pt2_lat2 = (pt2[0] - dist2*np.cos(cl_normal), pt2[1] - dist2*np.sin(cl_normal))
            if l_idx == 1:
                p_line_lat1.append(pt1_lat1)
                p_line_lat2.append(pt1_lat2)
            p_line_lat1.append(pt2_lat1)
            p_line_lat2.append(pt2_lat2)
            
    return p_line_lat1, p_line_lat2


def get_angle(veh_state):
    track_id = veh_state.id
    current_time = veh_state.current_time
    conn = sqlite3.connect(occlusion_constants.DB)
    c = conn.cursor()
    elements = "ANGLE"
    q_string = f"SELECT {elements} FROM TRAJECTORIES_0{occlusion_constants.CURRENT_FILE_ID} WHERE TRACK_ID={track_id} AND TIME={current_time}"
    c.execute(q_string)
    angle = c.fetchall()
    angle = angle[0][0]
    conn.close()
    return angle


def get_available_actions(vehicle):
    segment = vehicle.current_segment
    segment = occlusion_constants.SEGMENT_MAP[segment]

    conn = sqlite3.connect(occlusion_constants.DB)
    cur = conn.cursor()
    # Since occluding vehicles are only spawned during green or yellow traffic lights

    if segment in occlusion_constants.RED_LIGHT_PROCEED_LANES and vehicle.signal == "R":
        command = f'SELECT L1_ACTION FROM ACTIONS WHERE SEGMENT="{segment}" AND (TRAFFIC_SIGNAL="G" OR TRAFFIC_SIGNAL="Y" OR TRAFFIC_SIGNAL="R" OR TRAFFIC_SIGNAL="*")'

    elif vehicle.id < 0:
        command = f'SELECT L1_ACTION FROM ACTIONS WHERE SEGMENT="{segment}" AND (TRAFFIC_SIGNAL="G" OR TRAFFIC_SIGNAL="Y" OR TRAFFIC_SIGNAL="*")'
    else:
        if vehicle.signal == "G" or vehicle.signal == "Y":
            command = f'SELECT L1_ACTION FROM ACTIONS WHERE SEGMENT="{segment}" AND (TRAFFIC_SIGNAL="G" OR TRAFFIC_SIGNAL="Y" OR TRAFFIC_SIGNAL="*")'
        elif vehicle.signal == "R":
            command = f'SELECT L1_ACTION FROM ACTIONS WHERE SEGMENT="{segment}" AND (TRAFFIC_SIGNAL="R" OR TRAFFIC_SIGNAL="*")'

    cur.execute(command)
    query_results = cur.fetchall()
    actions = [a[0] for a in query_results]

    if not actions:
        raise Exception(f"There are no actions available for vehicle {vehicle.id} at time {vehicle.current_time}, with signal {vehicle.signal}, in segment {vehicle.current_segment}.")
    return actions


def get_centreline_from_segment_sequence(lane_segment_sequence):
    conn = sqlite3.connect(occlusion_constants.DB)
    cur = conn.cursor()
    command = f'SELECT X_POSITIONS, Y_POSITIONS FROM TRAFFIC_REGIONS_DEF WHERE NAME="{lane_segment_sequence}" AND REGION_PROPERTY="segment_seq_center_line"'
    cur.execute(command)
    centreline_unformatted = cur.fetchall()

    x_points_unformatted = centreline_unformatted[0][0]
    y_points_unformatted = centreline_unformatted[0][1]

    x_points = ast.literal_eval(x_points_unformatted)
    y_points = ast.literal_eval(y_points_unformatted)

    centreline = [(x,y) for x,y in zip(x_points, y_points)]
    return centreline

def get_sequences_and_centrelines(subject_vehicle, current_time):
    with sqlite3.connect(occlusion_constants.DB) as conn:
        conn.row_factory = dict_factory
        c = conn.cursor()
        execute_cmd = f"SELECT * FROM TRAFFIC_LIGHTS WHERE TIME - {current_time} <= 0 ORDER BY TIME"
        c.execute(execute_cmd)
        traffic_signals = c.fetchall()
        # current_traffic_state is a dict where key is direction and value is the signal.
        # get the last entry to get the light signal state we are in.
        current_traffic_state = traffic_signals[-1]

    sequences_and_centrelines = []
    for sequence in occlusion_constants.SEGMENT_SEQUENCES:
        if (sequence[0] != subject_vehicle.segment_seq) and (current_traffic_state[sequence[1]] in ['Y','G']):
           centreline = get_centreline_from_segment_sequence(sequence[0])

           sequences_and_centrelines.append((sequence[0], centreline))
    return sequences_and_centrelines


# This returns a list of vehicles (for a particular db) that are in a segment sequence that we do not support in this work
# Therefore they are invalid vehicles in this work
def get_invalid_vehicles():
    conn = sqlite3.connect(occlusion_constants.DB)
    c = conn.cursor()

    execute_cmd = f"SELECT TRACK_ID FROM TRAJECTORIES_0{occlusion_constants.CURRENT_FILE_ID}_EXT"
    c.execute(execute_cmd)

    query_results = c.fetchall()
    all_track_ids = set(query_results)

    invalid_vehicles = []
    segment_sequences = [i[0] for i in occlusion_constants.SEGMENT_SEQUENCES]

    for track_id in all_track_ids:
        segment_seq = get_track_segment_seq(track_id[0])
        if segment_seq not in segment_sequences:
            invalid_vehicles.append(track_id[0])

    return invalid_vehicles


def generate_time_list():
    conn = sqlite3.connect(occlusion_constants.DB)
    c = conn.cursor()
    execute_cmd = f"SELECT MAX(TIME) FROM TRAJECTORIES_0{occlusion_constants.CURRENT_FILE_ID}_EXT"
    c.execute(execute_cmd)
    query_result = c.fetchall()
    max_time = query_result[0][0]

    time_list = [0]
    below_max_time = True
    increment = 1.001
    time = 0

    while below_max_time:
        time += increment
        time = round(time, 3)

        if time > max_time:
            below_max_time = False
        else:
            time_list.append(time)
    return time_list


def retrieve_relevant_vehicles(track_id, current_time):
    conn = sqlite3.connect(occlusion_constants.DB)
    cur = conn.cursor()
    execute_cmd = f"SELECT RELEV_AGENT_IDS FROM RELEVANT_AGENTS WHERE TIME={current_time} AND TRACK_ID={track_id} AND RELEV_AGENT_IDS IS NOT NULL"
    cur.execute(execute_cmd)
    query_result = cur.fetchall()

    if not query_result:
        return query_result

    relevant_agent_ids = query_result[0][0]
    relevant_agents_ids = ast.literal_eval(relevant_agent_ids)

    relevant_agents = []
    for id in relevant_agents_ids:
        relevant_agent = setup_vehicle_for_occlusion_scenario(id, current_time)
        if relevant_agent == None:
            continue
        relevant_agents.append(relevant_agent)
    return relevant_agents


# Returns list of subject vehicles for time
def retrieve_subject_vehicle_list(current_time):

    conn = sqlite3.connect(occlusion_constants.DB)
    cur = conn.cursor()
    execute_cmd = f"SELECT TRACK_ID FROM RELEVANT_AGENTS WHERE TIME={current_time} AND RELEV_AGENT_IDS IS NOT NULL"
    cur.execute(execute_cmd)
    query_result = cur.fetchall()
    conn.close()
    return query_result


def setup_vehicle_for_occlusion_scenario(track_id, current_time):
    segment_seq = get_track_segment_seq(track_id)
    valid_segment_sequences = [i[0] for i in occlusion_constants.SEGMENT_SEQUENCES]
    # For example some relevant vehicles may travel from East to North. We don't include those vehicles.
    if segment_seq not in valid_segment_sequences:
        print(f"Vehicle {track_id} is in unsupported lane segment {segment_seq}. Returning None.")
        return None

    position = get_vehicle_position(track_id, current_time)
    # The vehicle requires interpolation to be set up.
    if not position:
        vehicle = setup_vehicle_state(track_id, current_time)
        vehicle.set_has_oncoming_vehicle(False)
        return vehicle
    else:
        vehicle = VehicleState()
        vehicle.set_id(track_id)
        vehicle.set_current_time(current_time)
        vehicle.set_segment_seq(segment_seq)
        current_segment = get_current_segment_for_db_vehicles(track_id, current_time)
        vehicle.set_current_segment(current_segment)
        vehicle.set_x(position[0][0])
        vehicle.set_y(position[0][1])
        angle = get_angle(vehicle)
        vehicle.set_angle(angle) 

        # Get direction
        debug_flag = False
        for sequence_direction in occlusion_constants.SEGMENT_SEQUENCES:
            if segment_seq == sequence_direction[0]:
                debug_flag = True
                vehicle.set_direction(sequence_direction[1])

        vehicle.set_task(occlusion_constants.TASK_MAP[vehicle.direction])

        if debug_flag == False:
            raise Exception(f"Vehicle ID: {track_id} with segment sequence {segment_seq} at time {current_time}, current segment {current_segment}, x: {position[0][0]}, y: {position[0][1]}")

        signal = get_traffic_signal(current_time,vehicle.direction)
        vehicle.set_traffic_light(signal)
        vehicle.set_has_oncoming_vehicle(False)

        if vehicle.id > 0:
            speed = get_speed(track_id, current_time)
            vehicle.set_speed(speed[0])

        return vehicle


# This will rewrite the assigned segment values in the TRAJECTORIES_0{db_id}_EXT table
def fix_agent_assigned_segment(track_id):

    conn = sqlite3.connect(occlusion_constants.DB)
    cur = conn.cursor()
    execute_cmd = f"SELECT TIME FROM TRAJECTORIES_0{occlusion_constants.CURRENT_FILE_ID}_EXT WHERE TRACK_ID={track_id}"

    cur.execute(execute_cmd)
    query_result = cur.fetchall()
    time_list = [time[0] for time in query_result]

    for time in time_list:
        veh_state = VehicleState()
        veh_state.set_id(track_id)
        veh_state.set_current_time(time)

        position = get_vehicle_position(track_id, time)
        
        veh_state.set_x(position[0][0])
        veh_state.set_y(position[0][1])

        veh_track_segment_seq = get_track_segment_seq(track_id)
        veh_state.set_segment_seq(veh_track_segment_seq)
        veh_state.action_plans = dict()
        veh_state.set_current_time(time)
        entry_exit_time = get_entry_exit_time(veh_state.id)
        veh_state.set_entry_exit_time(entry_exit_time)
        path,gates,direction = get_path_gates_direction(veh_state.id)
        task = occlusion_constants.TASK_MAP[direction]
        veh_state.set_gates(gates)
        gct = gate_crossing_times(veh_state)
        veh_state.set_gate_crossing_times(gct)

        fulltrack = get_track(veh_state,None)
        veh_state.set_full_track(fulltrack)

        assigned_segment = assign_curent_segment(None,veh_state)

        cur.execute(f"UPDATE TRAJECTORIES_0{occlusion_constants.CURRENT_FILE_ID}_EXT SET ASSIGNED_SEGMENT='{assigned_segment}' WHERE TIME={time} AND TRACK_ID={track_id}")
        conn.commit()

        print(f"FOR TIME: {time}, ASSIGNED SEGMENT: {assigned_segment}")
    conn.close()

def retrieve_occluding_vehicles(subject_vehicle_id, current_time):
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()
    execute_cmd = f"SELECT * FROM OCCLUDING_VEHICLES WHERE TIME={current_time} AND SUBJECT_VEHICLE_ID={subject_vehicle_id}"
    cur.execute(execute_cmd)
    query_result = cur.fetchall()
    if not query_result:
        return query_result
    occluding_vehicles = []
    for row in query_result:
        occluding_vehicle = setup_occluding_vehicle_from_db(row)
        occluding_vehicles.append(occluding_vehicle)
    return occluding_vehicles


def setup_occluding_vehicle_from_db(row):
    vehicle = VehicleState()
    vehicle.set_id(row[0])
    vehicle_type = row[9]
    vehicle.set_current_time(row[1])
    vehicle.set_x(row[2])
    vehicle.set_y(row[3])
    vehicle.set_angle(row[4])
    vehicle.set_current_segment(row[5])
    segment_seq = ast.literal_eval(row[6])
    vehicle.set_segment_seq(segment_seq)
    vehicle.set_direction(row[7])
    vehicle.set_task(occlusion_constants.TASK_MAP[vehicle.direction])
    vehicle.set_has_oncoming_vehicle(False)
    vehicle.set_traffic_light(row[8])
    occlusion_level = ast.literal_eval(row[11])
    vehicle.set_occlusion_level(occlusion_level)
    return vehicle


def retrieve_occlusion_scenarios(current_time):
    # A scenario is a list of lists:
    # [[[subject_vehicle], [relevant_vehicles], [occluding_vehicle]], [[subject_vehicle], [relevant_vehicles], [occluding_vehicle]]
    #                 -------Scenario 0-------                                        -------Scenario 1-------
    scenario_list = []
    subject_vehicle_list = retrieve_subject_vehicle_list(current_time)   

    # TODO: Put all scenarios with same subject vehicle under same key so that we don't have to regenerate trajectories
    # Or even better, subject vehicle ID key, list of lists, first list base scenario, second list, list of occluding vehicles
    for subject_vehicle_id in subject_vehicle_list:
        subject_vehicle = setup_vehicle_for_occlusion_scenario(subject_vehicle_id[0], current_time)

        occluding_vehicles = retrieve_occluding_vehicles(subject_vehicle_id[0], current_time)
        # If there are no occluding vehicles
        if not occluding_vehicles:
            continue
        relevant_vehicles = retrieve_relevant_vehicles(subject_vehicle_id[0], current_time)
        if not relevant_vehicles:
            continue

        scenario = []
        scenario.append(subject_vehicle)
        scenario.append(relevant_vehicles)

        filtered_occluding_vehicles = []

        for occluding_vehicle in occluding_vehicles:
            # We don't care about occlusion cases where all vehicles are in the same sequence!
            seq = occluding_vehicle.segment_seq
            all_part_of_same_sequence = True
            for v in relevant_vehicles:
                if v.segment_seq != seq:
                    all_part_of_same_sequence = False
                    break
            if subject_vehicle.segment_seq != seq:
                all_part_of_same_sequence = False
            if all_part_of_same_sequence:
                continue

            filtered_occluding_vehicles.append(occluding_vehicle)

        scenario.append(filtered_occluding_vehicles)
        scenario_list.append(scenario)

    return scenario_list


def get_reasonable_velocities(seg,direction=None):
    seg_type = occlusion_constants.SEGMENT_MAP[seg]
    target_vels = occlusion_constants.PROCEED_VEL_RANGES[seg_type] 
    if direction is not None and direction in ['L_W_S','L_W_N'] and seg_type=='exit-lane':
        target_vels = (target_vels[0]/2,target_vels[1]/2)
    return target_vels



def get_speed_range_for_segment(segment, direction, insert_into_table):

    insert_section = f"(SEGMENT, TRAFFIC_LIGHT, DEDICATED_GREEN_FLAG, HAS_CONFLICT_FLAG, JUST_TURNED_GREEN_FLAG, MIN_SPEED, " \
                     f"MAX_SPEED, MEAN_SPEED, MEDIAN_SPEED, STANDARD_DEVIATION, TOTAL_COUNT)"

    green_light_speeds = []
    yellow_light_speeds = []
    dedicated_green_light_speeds = []
    dedicated_green_light_speeds_without_delay = []
    dedicated_green_light_speeds_with_delay = []
    green_light_speeds_conflict = []
    green_light_speeds_no_conflict = []
    yellow_light_speeds_conflict = []
    yellow_light_speeds_no_conflict = []
    green_light_speeds_delay = []

    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:
        db = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
        conn = sqlite3.connect(db)
        cur = conn.cursor()
        execute_cmd = f"SELECT {direction}, TIME FROM TRAFFIC_LIGHTS"
        cur.execute(execute_cmd)
        query_result = cur.fetchall()

        green_light_times = []
        yellow_light_times = []

        green_light_indices = []
        yellow_light_indices = [] 

        for i in range(len(query_result)):
            signal_time = query_result[i]
            if signal_time[0] == 'G':
                green_light_indices.append(i)
            elif signal_time[0] == 'Y':
                yellow_light_indices.append(i)

        for index in green_light_indices:
            if index - 1 not in green_light_indices:
                start_time = query_result[index][1]
            if index + 1 in green_light_indices:
                continue
            else:
                if index + 1 < len(query_result):
                    end_time = query_result[index+1][1]
                else:
                    end_time = query_result[index][1]
            green_light_times.append((start_time, end_time))

        for index in yellow_light_indices:
            if index - 1 not in yellow_light_indices:
                start_time = query_result[index][1]
            if index + 1 in yellow_light_indices:
                continue
            else:
                if index + 1 < len(query_result):
                    end_time = query_result[index+1][1]
                else:
                    end_time = query_result[index][1]
            yellow_light_times.append((start_time, end_time))

 
        if occlusion_constants.SEGMENT_MAP[segment] in ['exec-right-turn','through-lane','exit-lane', 'prep-right-turn','right-turn-lane']:
            print(f"THIS SHOULD ACTIVATE ONE OF ['exec-right-turn','through-lane','exit-lane', 'prep-right-turn','right-turn-lane'] ---> {occlusion_constants.SEGMENT_MAP[segment]}")
            for times in green_light_times:
                speeds = get_speeds(current_file_id, segment, times)
                if speeds:
                    speeds = [i[0] for i in speeds]
                    green_light_speeds += speeds        
   
            for times in yellow_light_times:
                speeds = get_speeds(current_file_id, segment, times)
                if speeds:
                    speeds = [i[0] for i in speeds]
                    yellow_light_speeds += speeds

        elif occlusion_constants.SEGMENT_MAP[segment] == 'prep-left-turn':
            print(f"THIS SHOULD ACTIVATE 'prep-left-turn' ---> {occlusion_constants.SEGMENT_MAP[segment]}")
            conflict_direction = occlusion_constants.CONFLICT_MAP[direction]

            # Get all unique segments in that direction that 
            conflict_segments = []
            for segment_sequence in occlusion_constants.SEGMENT_SEQUENCES:
                if conflict_direction == segment_sequence[1]:
                    for seg in segment_sequence[0]:
                        if occlusion_constants.SEGMENT_MAP[seg] != 'exit-lane':
                            conflict_segments.append(seg)
            conflict_segments = set(conflict_segments)
            for times in green_light_times:
                if dedicated_green_check(current_file_id, conflict_direction, times[0]):
                    speeds = get_speeds(current_file_id, segment, times)
                    # By definition there is no has_conflict for dedicated greens
                    if speeds:
                        speeds = [i[0] for i in speeds]
                        dedicated_green_light_speeds += speeds
                else:
                    speeds = get_speeds(current_file_id, segment, times)
                    for speed_time in speeds:
                        if conflict_vehicle_present(current_file_id, conflict_segments, speed_time[1]):
                            green_light_speeds_conflict.append(speed_time[0])
                        else: 
                            green_light_speeds_no_conflict.append(speed_time[0])
            for times in yellow_light_times:
                speeds = get_speeds(current_file_id, segment, times)
                for speed_time in speeds:
                    if conflict_vehicle_present(current_file_id, conflict_segments, speed_time[1]):
                        yellow_light_speeds_conflict.append(speed_time[0])
                    else:
                        yellow_light_speeds_no_conflict.append(speed_time[0])


        elif occlusion_constants.SEGMENT_MAP[segment] == 'left-turn-lane':
            print(f"THIS SHOULD ACTIVATE 'left-turn-lane' ---> {occlusion_constants.SEGMENT_MAP[segment]}")
            conflict_direction = occlusion_constants.CONFLICT_MAP[direction]
            # Get all unique segments in that direction that 
            conflict_segments = []
            for segment_sequence in occlusion_constants.SEGMENT_SEQUENCES:
                if conflict_direction == segment_sequence[1]:
                    for seg in segment_sequence[0]:
                        if occlusion_constants.SEGMENT_MAP[seg] != 'exit-lane':
                            conflict_segments.append(seg)
            conflict_segments = set(conflict_segments)
            for times in green_light_times:
                
                if dedicated_green_check(current_file_id, conflict_direction, times[0]):
                    speeds, speeds_with_delay = get_speeds(current_file_id, segment, times, times[0] + occlusion_constants.LEFT_TURN_GREEN_LIGHT_DELAY)
                    # By definition there is no has_conflict for dedicated greens
                    if speeds:
                        speeds = [i[0] for i in speeds]
                        dedicated_green_light_speeds_without_delay += speeds

                    if speeds_with_delay:
                        speeds_with_delay = [i[0] for i in speeds_with_delay]
                        dedicated_green_light_speeds_with_delay += speeds_with_delay

                else:
                    speeds = get_speeds(current_file_id, segment, times)
                    for speed_time in speeds:
                        if conflict_vehicle_present(current_file_id, conflict_segments, speed_time[1]):
                            green_light_speeds_conflict.append(speed_time[0])
                        else: 
                            green_light_speeds_no_conflict.append(speed_time[0])
            for times in yellow_light_times:
                speeds = get_speeds(current_file_id, segment, times)
                for speed_time in speeds:
                    if conflict_vehicle_present(current_file_id, conflict_segments, speed_time[1]):
                        yellow_light_speeds_conflict.append(speed_time[0])
                    else:
                        yellow_light_speeds_no_conflict.append(speed_time[0])

        elif occlusion_constants.SEGMENT_MAP[segment] in ['exec-left-turn']:
            print(f"THIS SHOULD ACTIVATE ONE OF ['exec-left-turn'] ---> {occlusion_constants.SEGMENT_MAP[segment]}")
            conflict_direction = occlusion_constants.CONFLICT_MAP[direction]
            # Get all unique segments in that direction that 
            conflict_segments = []
            for segment_sequence in occlusion_constants.SEGMENT_SEQUENCES:
                if conflict_direction == segment_sequence[1]:
                    for seg in segment_sequence[0]:
                        if occlusion_constants.SEGMENT_MAP[seg] != 'exit-lane':
                            conflict_segments.append(seg)
            conflict_segments = set(conflict_segments)
            for times in green_light_times:
                speeds = get_speeds(current_file_id, segment, times)
                for speed_time in speeds:
                    if conflict_vehicle_present(current_file_id, conflict_segments, speed_time[1]):
                        green_light_speeds_conflict.append(speed_time[0])
                    else: 
                        green_light_speeds_no_conflict.append(speed_time[0]) 
            for times in yellow_light_times:
                speeds = get_speeds(current_file_id, segment, times)
                for speed_time in speeds:
                    if conflict_vehicle_present(current_file_id, conflict_segments, speed_time[1]):
                        yellow_light_speeds_conflict.append(speed_time[0])
                    else:
                        yellow_light_speeds_no_conflict.append(speed_time[0])


        elif occlusion_constants.SEGMENT_MAP[segment] == 'through-lane-entry':
            print(f"THIS SHOULD ACTIVATE 'through-lane-entry' ---> {occlusion_constants.SEGMENT_MAP[segment]}")
            for times in green_light_times:
                speeds, speeds_with_delay = get_speeds(current_file_id, segment, times, times[0] + occlusion_constants.STRAIGHT_THROUGH_GREEN_LIGHT_DELAY)
                if speeds:
                    speeds = [i[0] for i in speeds]
                    green_light_speeds += speeds
                if speeds_with_delay:
                    speeds_with_delay = [i[0] for i in speeds_with_delay]
                    green_light_speeds_delay += speeds_with_delay          
            for times in yellow_light_times:
                speeds = get_speeds(current_file_id, segment, times)
                if speeds:
                    speeds = [i[0] for i in speeds]
                    yellow_light_speeds += speeds

        print(f"FINISHED: {current_file_id}")



    if occlusion_constants.SEGMENT_MAP[segment] in ['exec-right-turn','through-lane','exit-lane', 'prep-right-turn','right-turn-lane']:
        print(f"SAVING MODE: ['exec-right-turn','through-lane','exit-lane', 'prep-right-turn','right-turn-lane'] ---> {occlusion_constants.SEGMENT_MAP[segment]}")
        conn = sqlite3.connect(occlusion_constants.OCC_DB)
        cur = conn.cursor()

        title = f"GREEN LIGHT: {segment}"
        filename = f"Speed Distributions/{segment}/GREEN LIGHT: {segment}"
        plot_and_save_histogram(green_light_speeds, segment, title, filename)
        if insert_into_table:
            green_light_speeds = np.asarray(green_light_speeds)
            signal = 'G'
            dedicated_green = 0
            has_conflict = 0
            just_turned_green = 0
            min_speed = min(green_light_speeds) if len(green_light_speeds) != 0 else None
            max_speed = max(green_light_speeds) if len(green_light_speeds) != 0 else None
            mean_speed = np.mean(green_light_speeds) if len(green_light_speeds) != 0 else None
            median_speed = np.median(green_light_speeds) if len(green_light_speeds) != 0 else None
            std = green_light_speeds.std() if len(green_light_speeds) != 0 else None
            total_count = len(green_light_speeds) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))       

        title = f"YELLOW LIGHT: {segment}"
        filename = f"Speed Distributions/{segment}/YELLOW LIGHT: {segment}"
        plot_and_save_histogram(yellow_light_speeds, segment, title, filename)
        if insert_into_table:
            yellow_light_speeds = np.asarray(yellow_light_speeds)
            signal = 'Y'
            dedicated_green = 0
            has_conflict = 0
            just_turned_green = 0
            min_speed = min(yellow_light_speeds) if len(yellow_light_speeds) != 0 else None
            max_speed = max(yellow_light_speeds) if len(yellow_light_speeds) != 0 else None
            mean_speed = np.mean(yellow_light_speeds) if len(yellow_light_speeds) != 0 else None
            median_speed = np.median(yellow_light_speeds) if len(yellow_light_speeds) != 0 else None
            std = yellow_light_speeds.std() if len(yellow_light_speeds) != 0 else None
            total_count = len(yellow_light_speeds) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))       

            conn.commit()
            conn.close()

    elif occlusion_constants.SEGMENT_MAP[segment] == 'prep-left-turn':
        print(f"SAVING MODE: 'prep-left-turn' ---> {occlusion_constants.SEGMENT_MAP[segment]}")
        conn = sqlite3.connect(occlusion_constants.OCC_DB)
        cur = conn.cursor()

        title = f"DEDICATED GREEN LIGHT: {segment}"
        filename = f"Speed Distributions/{segment}/DEDICATED GREEN LIGHT: {segment}"
        plot_and_save_histogram(dedicated_green_light_speeds, segment, title, filename)
        if insert_into_table:
            dedicated_green_light_speeds = np.asarray(dedicated_green_light_speeds)
            signal = 'G'
            dedicated_green = 1
            has_conflict = 0
            just_turned_green = 0
            min_speed = min(dedicated_green_light_speeds) if len(dedicated_green_light_speeds) != 0 else None
            max_speed = max(dedicated_green_light_speeds) if len(dedicated_green_light_speeds) != 0 else None
            mean_speed = np.mean(dedicated_green_light_speeds) if len(dedicated_green_light_speeds) != 0 else None
            median_speed = np.median(dedicated_green_light_speeds) if len(dedicated_green_light_speeds) != 0 else None
            std = dedicated_green_light_speeds.std() if len(dedicated_green_light_speeds) != 0 else None
            total_count = len(dedicated_green_light_speeds) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))


        title = f"GREEN LIGHT WITH CONFLICT: {segment}"
        filename = f"Speed Distributions/{segment}/GREEN LIGHT WITH CONFLICT: {segment}"
        plot_and_save_histogram(green_light_speeds_conflict, segment, title, filename)
        if insert_into_table:
            green_light_speeds_conflict = np.asarray(green_light_speeds_conflict)
            signal = 'G'
            dedicated_green = 0
            has_conflict = 1
            just_turned_green = 0
            min_speed = min(green_light_speeds_conflict) if len(green_light_speeds_conflict) != 0 else None
            max_speed = max(green_light_speeds_conflict) if len(green_light_speeds_conflict) != 0 else None
            mean_speed = np.mean(green_light_speeds_conflict) if len(green_light_speeds_conflict) != 0 else None
            median_speed = np.median(green_light_speeds_conflict) if len(green_light_speeds_conflict) != 0 else None
            std = green_light_speeds_conflict.std() if len(green_light_speeds_conflict) != 0 else None
            total_count = len(green_light_speeds_conflict) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))


        title = f"GREEN LIGHT WITHOUT CONFLICT: {segment}"
        filename = f"Speed Distributions/{segment}/GREEN LIGHT WITHOUT CONFLICT: {segment}"
        plot_and_save_histogram(green_light_speeds_no_conflict, segment, title, filename)
        if insert_into_table:
            green_light_speeds_no_conflict = np.asarray(green_light_speeds_no_conflict)
            signal = 'G'
            dedicated_green = 0
            has_conflict = 0
            just_turned_green = 0
            min_speed = min(green_light_speeds_no_conflict) if len(green_light_speeds_no_conflict) != 0 else None
            max_speed = max(green_light_speeds_no_conflict) if len(green_light_speeds_no_conflict) != 0 else None
            mean_speed = np.mean(green_light_speeds_no_conflict) if len(green_light_speeds_no_conflict) != 0 else None
            median_speed = np.median(green_light_speeds_no_conflict) if len(green_light_speeds_no_conflict) != 0 else None
            std = green_light_speeds_no_conflict.std() if len(green_light_speeds_no_conflict) != 0 else None
            total_count = len(green_light_speeds_no_conflict) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))


        title = f"YELLOW LIGHT WITH CONFLICT: {segment}"
        filename = f"Speed Distributions/{segment}/YELLOW LIGHT WITH CONFLICT: {segment}"
        plot_and_save_histogram(yellow_light_speeds_conflict, segment, title, filename)
        if insert_into_table:
            yellow_light_speeds_conflict = np.asarray(yellow_light_speeds_conflict)
            signal = 'Y'
            dedicated_green = 0
            has_conflict = 1
            just_turned_green = 0
            min_speed = min(yellow_light_speeds_conflict) if len(yellow_light_speeds_conflict) != 0 else None
            max_speed = max(yellow_light_speeds_conflict) if len(yellow_light_speeds_conflict) != 0 else None
            mean_speed = np.mean(yellow_light_speeds_conflict) if len(yellow_light_speeds_conflict) != 0 else None
            median_speed = np.median(yellow_light_speeds_conflict) if len(yellow_light_speeds_conflict) != 0 else None
            std = yellow_light_speeds_conflict.std() if len(yellow_light_speeds_conflict) != 0 else None
            total_count = len(yellow_light_speeds_conflict) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))


        title = f"YELLOW LIGHT WITHOUT CONFLICT: {segment}"
        filename = f"Speed Distributions/{segment}/YELLOW LIGHT WITHOUT CONFLICT: {segment}"
        plot_and_save_histogram(yellow_light_speeds_no_conflict, segment, title, filename)
        if insert_into_table:
            yellow_light_speeds_no_conflict = np.asarray(yellow_light_speeds_no_conflict)
            signal = 'Y'
            dedicated_green = 0
            has_conflict = 0
            just_turned_green = 0
            min_speed = min(yellow_light_speeds_no_conflict) if len(yellow_light_speeds_no_conflict) != 0 else None
            max_speed = max(yellow_light_speeds_no_conflict) if len(yellow_light_speeds_no_conflict) != 0 else None
            mean_speed = np.mean(yellow_light_speeds_no_conflict) if len(yellow_light_speeds_no_conflict) != 0 else None
            median_speed = np.median(yellow_light_speeds_no_conflict) if len(yellow_light_speeds_no_conflict) != 0 else None
            std = yellow_light_speeds_no_conflict.std() if len(yellow_light_speeds_no_conflict) != 0 else None
            total_count = len(yellow_light_speeds_no_conflict) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))

            conn.commit()
            conn.close()

    elif occlusion_constants.SEGMENT_MAP[segment] == 'left-turn-lane':
        print(f"SAVING MODE: 'left-turn-lane' ---> {occlusion_constants.SEGMENT_MAP[segment]}")
        conn = sqlite3.connect(occlusion_constants.OCC_DB)
        cur = conn.cursor()

        title = f"DEDICATED GREEN LIGHT WITHOUT DELAY: {segment}"
        filename = f"Speed Distributions/{segment}/DEDICATED GREEN LIGHT WITHOUT DELAY: {segment}"
        plot_and_save_histogram(dedicated_green_light_speeds_without_delay, segment, title, filename)
        if insert_into_table:
            dedicated_green_light_speeds_without_delay = np.asarray(dedicated_green_light_speeds_without_delay)
            signal = 'G'
            dedicated_green = 1
            has_conflict = 0
            just_turned_green = 0
            min_speed = min(dedicated_green_light_speeds_without_delay) if len(dedicated_green_light_speeds_without_delay) != 0 else None
            max_speed = max(dedicated_green_light_speeds_without_delay) if len(dedicated_green_light_speeds_without_delay) != 0 else None
            mean_speed = np.mean(dedicated_green_light_speeds_without_delay) if len(dedicated_green_light_speeds_without_delay) != 0 else None
            median_speed = np.median(dedicated_green_light_speeds_without_delay) if len(dedicated_green_light_speeds_without_delay) != 0 else None
            std = dedicated_green_light_speeds_without_delay.std() if len(dedicated_green_light_speeds_without_delay) != 0 else None
            total_count = len(dedicated_green_light_speeds_without_delay) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))


        title = f"DEDICATED GREEN LIGHT WITH DELAY: {segment}"
        filename = f"Speed Distributions/{segment}/DEDICATED GREEN LIGHT WITH DELAY: {segment}"
        plot_and_save_histogram(dedicated_green_light_speeds_with_delay, segment, title, filename)
        if insert_into_table:
            dedicated_green_light_speeds_with_delay = np.asarray(dedicated_green_light_speeds_with_delay)
            signal = 'G'
            dedicated_green = 1
            has_conflict = 0
            just_turned_green = 1
            min_speed = min(dedicated_green_light_speeds_with_delay) if len(dedicated_green_light_speeds_with_delay) != 0 else None
            max_speed = max(dedicated_green_light_speeds_with_delay) if len(dedicated_green_light_speeds_with_delay) != 0 else None
            mean_speed = np.mean(dedicated_green_light_speeds_with_delay) if len(dedicated_green_light_speeds_with_delay) != 0 else None
            median_speed = np.median(dedicated_green_light_speeds_with_delay) if len(dedicated_green_light_speeds_with_delay) != 0 else None
            std = dedicated_green_light_speeds_with_delay.std() if len(dedicated_green_light_speeds_with_delay) != 0 else None
            total_count = len(dedicated_green_light_speeds_with_delay) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))


        title = f"GREEN LIGHT WITH CONFLICT: {segment}"
        filename = f"Speed Distributions/{segment}/GREEN LIGHT WITH CONFLICT: {segment}"
        plot_and_save_histogram(green_light_speeds_conflict, segment, title, filename)
        if insert_into_table:
            green_light_speeds_conflict = np.asarray(green_light_speeds_conflict)
            signal = 'G'
            dedicated_green = 0
            has_conflict = 1
            just_turned_green = 0
            min_speed = min(green_light_speeds_conflict) if len(green_light_speeds_conflict) != 0 else None
            max_speed = max(green_light_speeds_conflict) if len(green_light_speeds_conflict) != 0 else None
            mean_speed = np.mean(green_light_speeds_conflict) if len(green_light_speeds_conflict) != 0 else None
            median_speed = np.median(green_light_speeds_conflict) if len(green_light_speeds_conflict) != 0 else None
            std = green_light_speeds_conflict.std() if len(green_light_speeds_conflict) != 0 else None
            total_count = len(green_light_speeds_conflict) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))


        title = f"GREEN LIGHT WITHOUT CONFLICT: {segment}"
        filename = f"Speed Distributions/{segment}/GREEN LIGHT WITHOUT CONFLICT: {segment}"
        plot_and_save_histogram(green_light_speeds_no_conflict, segment, title, filename)
        if insert_into_table:
            green_light_speeds_no_conflict = np.asarray(green_light_speeds_no_conflict)
            signal = 'G'
            dedicated_green = 0
            has_conflict = 0
            just_turned_green = 0
            min_speed = min(green_light_speeds_no_conflict) if len(green_light_speeds_no_conflict) != 0 else None
            max_speed = max(green_light_speeds_no_conflict) if len(green_light_speeds_no_conflict) != 0 else None
            mean_speed = np.mean(green_light_speeds_no_conflict) if len(green_light_speeds_no_conflict) != 0 else None
            median_speed = np.median(green_light_speeds_no_conflict) if len(green_light_speeds_no_conflict) != 0 else None
            std = green_light_speeds_no_conflict.std() if len(green_light_speeds_no_conflict) != 0 else None
            total_count = len(green_light_speeds_no_conflict) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))


        title = f"YELLOW LIGHT WITH CONFLICT: {segment}"
        filename = f"Speed Distributions/{segment}/YELLOW LIGHT WITH CONFLICT: {segment}"
        plot_and_save_histogram(yellow_light_speeds_conflict, segment, title, filename)
        if insert_into_table:
            yellow_light_speeds_conflict = np.asarray(yellow_light_speeds_conflict)
            signal = 'Y'
            dedicated_green = 0
            has_conflict = 1
            just_turned_green = 0
            min_speed = min(yellow_light_speeds_conflict) if len(yellow_light_speeds_conflict) != 0 else None
            max_speed = max(yellow_light_speeds_conflict) if len(yellow_light_speeds_conflict) != 0 else None
            mean_speed = np.mean(yellow_light_speeds_conflict) if len(yellow_light_speeds_conflict) != 0 else None
            median_speed = np.median(yellow_light_speeds_conflict) if len(yellow_light_speeds_conflict) != 0 else None
            std = yellow_light_speeds_conflict.std() if len(yellow_light_speeds_conflict) != 0 else None
            total_count = len(yellow_light_speeds_conflict) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))


        title = f"YELLOW LIGHT WITHOUT CONFLICT: {segment}"
        filename = f"Speed Distributions/{segment}/YELLOW LIGHT WITHOUT CONFLICT: {segment}"
        plot_and_save_histogram(yellow_light_speeds_no_conflict, segment, title, filename)
        if insert_into_table:
            yellow_light_speeds_no_conflict = np.asarray(yellow_light_speeds_no_conflict)
            signal = 'Y'
            dedicated_green = 0
            has_conflict = 0
            just_turned_green = 0
            min_speed = min(yellow_light_speeds_no_conflict) if len(yellow_light_speeds_no_conflict) != 0 else None
            max_speed = max(yellow_light_speeds_no_conflict) if len(yellow_light_speeds_no_conflict) != 0 else None
            mean_speed = np.mean(yellow_light_speeds_no_conflict) if len(yellow_light_speeds_no_conflict) != 0 else None
            median_speed = np.median(yellow_light_speeds_no_conflict) if len(yellow_light_speeds_no_conflict) != 0 else None
            std = yellow_light_speeds_no_conflict.std() if len(yellow_light_speeds_no_conflict) != 0 else None
            total_count = len(yellow_light_speeds_no_conflict) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))

            conn.commit()
            conn.close()
 
    elif occlusion_constants.SEGMENT_MAP[segment] in ['exec-left-turn']:
    
        print(f"SAVING MODE: ['exec-left-turn'] ---> {occlusion_constants.SEGMENT_MAP[segment]}")

        conn = sqlite3.connect(occlusion_constants.OCC_DB)
        cur = conn.cursor()

        title = f"GREEN LIGHT WITH CONFLICT: {segment}"
        filename = f"Speed Distributions/{segment}/GREEN LIGHT WITH CONFLICT: {segment}"
        plot_and_save_histogram(green_light_speeds_conflict, segment, title, filename)
        if insert_into_table:
            green_light_speeds_conflict = np.asarray(green_light_speeds_conflict)
            signal = 'G'
            dedicated_green = 0
            has_conflict = 1
            just_turned_green = 0
            min_speed = min(green_light_speeds_conflict) if len(green_light_speeds_conflict) != 0 else None
            max_speed = max(green_light_speeds_conflict) if len(green_light_speeds_conflict) != 0 else None
            mean_speed = np.mean(green_light_speeds_conflict) if len(green_light_speeds_conflict) != 0 else None
            median_speed = np.median(green_light_speeds_conflict) if len(green_light_speeds_conflict) != 0 else None
            std = green_light_speeds_conflict.std() if len(green_light_speeds_conflict) != 0 else None
            total_count = len(green_light_speeds_conflict) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))


        title = f"GREEN LIGHT WITHOUT CONFLICT: {segment}"
        filename = f"Speed Distributions/{segment}/GREEN LIGHT WITHOUT CONFLICT: {segment}"
        plot_and_save_histogram(green_light_speeds_no_conflict, segment, title, filename)
        if insert_into_table:
            green_light_speeds_no_conflict = np.asarray(green_light_speeds_no_conflict)
            signal = 'G'
            dedicated_green = 0
            has_conflict = 0
            just_turned_green = 0
            min_speed = min(green_light_speeds_no_conflict) if len(green_light_speeds_no_conflict) != 0 else None
            max_speed = max(green_light_speeds_no_conflict) if len(green_light_speeds_no_conflict) != 0 else None
            mean_speed = np.mean(green_light_speeds_no_conflict) if len(green_light_speeds_no_conflict) != 0 else None
            median_speed = np.median(green_light_speeds_no_conflict) if len(green_light_speeds_no_conflict) != 0 else None
            std = green_light_speeds_no_conflict.std() if len(green_light_speeds_no_conflict) != 0 else None
            total_count = len(green_light_speeds_no_conflict) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))

        
        title = f"YELLOW LIGHT WITH CONFLICT: {segment}"
        filename = f"Speed Distributions/{segment}/YELLOW LIGHT WITH CONFLICT: {segment}"
        plot_and_save_histogram(yellow_light_speeds_conflict, segment, title, filename)
        if insert_into_table:
            yellow_light_speeds_conflict = np.asarray(yellow_light_speeds_conflict)
            signal = 'Y'
            dedicated_green = 0
            has_conflict = 1
            just_turned_green = 0
            min_speed = min(yellow_light_speeds_conflict) if len(yellow_light_speeds_conflict) != 0 else None
            max_speed = max(yellow_light_speeds_conflict) if len(yellow_light_speeds_conflict) != 0 else None
            mean_speed = np.mean(yellow_light_speeds_conflict) if len(yellow_light_speeds_conflict) != 0 else None
            median_speed = np.median(yellow_light_speeds_conflict) if len(yellow_light_speeds_conflict) != 0 else None
            std = yellow_light_speeds_conflict.std() if len(yellow_light_speeds_conflict) != 0 else None
            total_count = len(yellow_light_speeds_conflict) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))


        title = f"YELLOW LIGHT WITHOUT CONFLICT: {segment}"
        filename = f"Speed Distributions/{segment}/YELLOW LIGHT WITHOUT CONFLICT: {segment}"
        plot_and_save_histogram(yellow_light_speeds_no_conflict, segment, title, filename)
        if insert_into_table:
            yellow_light_speeds_no_conflict = np.asarray(yellow_light_speeds_no_conflict)
            signal = 'Y'
            dedicated_green = 0
            has_conflict = 0
            just_turned_green = 0
            min_speed = min(yellow_light_speeds_no_conflict) if len(yellow_light_speeds_no_conflict) != 0 else None
            max_speed = max(yellow_light_speeds_no_conflict) if len(yellow_light_speeds_no_conflict) != 0 else None
            mean_speed = np.mean(yellow_light_speeds_no_conflict) if len(yellow_light_speeds_no_conflict) != 0 else None
            median_speed = np.median(yellow_light_speeds_no_conflict) if len(yellow_light_speeds_no_conflict) != 0 else None
            std = yellow_light_speeds_no_conflict.std() if len(yellow_light_speeds_no_conflict) != 0 else None
            total_count = len(yellow_light_speeds_no_conflict) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))

            conn.commit()
            conn.close()


    elif occlusion_constants.SEGMENT_MAP[segment] == 'through-lane-entry':
        print(f"SAVING MODE: 'through-lane-entry' ---> {occlusion_constants.SEGMENT_MAP[segment]}")
        conn = sqlite3.connect(occlusion_constants.OCC_DB)
        cur = conn.cursor()

        title = f"GREEN LIGHT WITHOUT DELAY: {segment}"
        filename = f"Speed Distributions/{segment}/GREEN LIGHT WITHOUT DELAY: {segment}"
        plot_and_save_histogram(green_light_speeds, segment, title, filename)
        if insert_into_table:
            green_light_speeds = np.asarray(green_light_speeds)
            signal = 'G'
            dedicated_green = 0
            has_conflict = 0
            just_turned_green = 0
            min_speed = min(green_light_speeds) if len(green_light_speeds) != 0 else None
            max_speed = max(green_light_speeds) if len(green_light_speeds) != 0 else None
            mean_speed = np.mean(green_light_speeds) if len(green_light_speeds) != 0 else None
            median_speed = np.median(green_light_speeds) if len(green_light_speeds) != 0 else None
            std = green_light_speeds.std() if len(green_light_speeds) != 0 else None
            total_count = len(green_light_speeds) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))


        title = f"GREEN LIGHT WITH DELAY: {segment}"
        filename = f"Speed Distributions/{segment}/GREEN LIGHT WITH DELAY: {segment}"
        plot_and_save_histogram(green_light_speeds_delay, segment, title, filename)
        if insert_into_table:
            green_light_speeds_delay = np.asarray(green_light_speeds_delay)
            signal = 'G'
            dedicated_green = 0
            has_conflict = 0
            just_turned_green = 1
            min_speed = min(green_light_speeds_delay) if len(green_light_speeds_delay) != 0 else None
            max_speed = max(green_light_speeds_delay) if len(green_light_speeds_delay) != 0 else None
            mean_speed = np.mean(green_light_speeds_delay) if len(green_light_speeds_delay) != 0 else None
            median_speed = np.median(green_light_speeds_delay) if len(green_light_speeds_delay) != 0 else None
            std = green_light_speeds_delay.std() if len(green_light_speeds_delay) != 0 else None
            total_count = len(green_light_speeds_delay) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))


        title = f"YELLOW LIGHT: {segment}"
        filename = f"Speed Distributions/{segment}/YELLOW LIGHT: {segment}"
        plot_and_save_histogram(yellow_light_speeds, segment, title, filename)
        if insert_into_table:
            yellow_light_speeds = np.asarray(yellow_light_speeds)
            signal = 'Y'
            dedicated_green = 0
            has_conflict = 0
            just_turned_green = 0
            min_speed = min(yellow_light_speeds) if len(yellow_light_speeds) != 0 else None
            max_speed = max(yellow_light_speeds) if len(yellow_light_speeds) != 0 else None
            mean_speed = np.mean(yellow_light_speeds) if len(yellow_light_speeds) != 0 else None
            median_speed = np.median(yellow_light_speeds) if len(yellow_light_speeds) != 0 else None
            std = yellow_light_speeds.std() if len(yellow_light_speeds) != 0 else None
            total_count = len(yellow_light_speeds) 
            cur.execute(f"INSERT INTO INITIAL_SPEEDS {insert_section} VALUES (?,?,?,?,?,?,?,?,?,?,?)", (segment, signal, dedicated_green, has_conflict, just_turned_green, min_speed, max_speed, mean_speed, median_speed, std, total_count))   

            conn.commit()
            conn.close()


def create_initial_speed_table():
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()
    cmd = f"CREATE TABLE INITIAL_INITIAL_SPEEDS (SEGMENT TEXT, TRAFFIC_LIGHT TEXT, ADVANCE_GREEN INT, CONFLICT INT, DELAY INT, MIN_SPEED NUMERIC, " \
          f"MAX_SPEED NUMERIC, MEAN_SPEED NUMERIC, MEDIAN_SPEED NUMERIC, STANDARD_DEVIATION NUMERIC, TOTAL_COUNT INT)"
    cur.execute(cmd)
    conn.commit()
    conn.close()

def get_speed(track_id, current_time, current_file_id=None):

    if current_file_id:
        occlusion_constants.CURRENT_FILE_ID = current_file_id
        occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"

    conn = sqlite3.connect(occlusion_constants.DB)
    cur = conn.cursor()
    execute_cmd = f"SELECT SPEED FROM TRAJECTORIES_0{occlusion_constants.CURRENT_FILE_ID} WHERE TIME={current_time} AND TRACK_ID={track_id}"
    cur.execute(execute_cmd)
    query_result = cur.fetchall()
    if not query_result:
        print(f"Could not find speed data for ID {track_id} at time {current_time}.")
        return None
    kph = query_result[0][0]
    mps = kph_to_mps(kph)
    return (mps, current_time)

# This function was used to construct the speed distributions for occluding vehicles
# green light just_turned_green must be start of green light plus some buffer time
def get_speeds(current_file_id, segment, times, green_light_delay=None):
    db = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    conn = sqlite3.connect(db)
    cur = conn.cursor()

    execute_cmd = f"SELECT TRACK_ID, TIME FROM TRAJECTORIES_0{current_file_id}_EXT WHERE ASSIGNED_SEGMENT='{segment}' AND TIME BETWEEN {times[0]} AND {times[1]}"
    cur.execute(execute_cmd)
    query_result = cur.fetchall()
    speeds = []
    speeds_with_delay = []
    for id_time_tuple in query_result:
        speed_time = get_speed(id_time_tuple[0], id_time_tuple[1], current_file_id)
        if speed_time != None:

            if green_light_delay:
                if speed_time[1] > green_light_delay:
                    speeds_with_delay.append(speed_time)
                else: 
                    speeds.append(speed_time)

            else:
                speeds.append(speed_time)
    # The output is a list of tuples of speed and time
    if green_light_delay:
        return speeds, speeds_with_delay
    else:
        return speeds

def conflict_vehicle_present(current_file_id, conflict_segments, current_time):
    db = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    conn = sqlite3.connect(db)
    cur = conn.cursor()
    conflict_present = False
    for segment in conflict_segments:
        execute_cmd = f"SELECT TRACK_ID FROM TRAJECTORIES_0{current_file_id}_EXT WHERE ASSIGNED_SEGMENT='{segment}' AND TIME={current_time}"
        cur.execute(execute_cmd)
        query_result = cur.fetchall()
        if query_result:
            conflict_present = True
            break
    return conflict_present


def dedicated_green_check(current_file_id, conflict_direction, current_time):
    db = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    conn = sqlite3.connect(db)
    cur = conn.cursor()

    # Need to get list of traffic light times and see which time is the closest and below or equal to current time.
    execute_cmd = f"SELECT TIME FROM TRAFFIC_LIGHTS"
    cur.execute(execute_cmd)
    query_result = cur.fetchall()
    times = [i[0] for i in query_result]

    index = 0
    for i in range(len(times)):
        time = times[i]
        # Case where current_time is greater than all times in times list
        if current_time >= time and i == len(times) - 1:
            break
        elif time >= current_time and index > 0:
            index -= 1
            break
        elif time >= current_time and index == 0:
            break
        else:
            index += 1

    time = times[index]

    execute_cmd = f"SELECT {conflict_direction} FROM TRAFFIC_LIGHTS WHERE TIME = {time}"
    cur.execute(execute_cmd)
    query_result = cur.fetchall()

    if not query_result:
        raise Exception(f"Query result returned nothing. Current file ID: {current_file_id}, has_conflict direction: {conflict_direction}, current time: {current_time}")

    if query_result[0][0] == 'R':
        return True
    else:
        return False

def plot_and_save_histogram(data, segment, title, filename):
    path = os.getcwd()

    if not os.path.isdir(f"{path}/Speed Distributions/{segment}"):
        os.mkdir(f"{path}/Speed Distributions/{segment}")

    fig, ax = plt.subplots()
    plt.title(title)
    plt.ylabel('Count')
    plt.xlabel('Speed (m/s)')
    plt.hist(data, 100)
    plt.savefig(filename, format='pdf') 
    plt.close()


def get_leader_id(vehicle, scenario):
    sequence = vehicle.segment_seq
    potential_leaders = []

    current_segment_index = None
    for i in range(len(sequence)):
        if vehicle.current_segment == sequence[i]:
            current_segment_index = i
            break
    if current_segment_index == None:
        raise Exception(f"Current segment index is None. This is only possible if the vehicle's current segment is not a part of its sequence.")

    if current_segment_index < len(sequence) - 1: 
        next_segment_index = current_segment_index + 1
    else:
        next_segment_index = current_segment_index

    for v in scenario:
        # We include the input vehicle in potential leaders because we need to calculate it's index along the centreline
        if v.current_segment == sequence[current_segment_index] or v.current_segment == sequence[next_segment_index]:
            potential_leaders.append(v)

    if not potential_leaders or (len(potential_leaders) == 1 and potential_leaders[0].id == vehicle.id):
        return None

        # Now we have all the vehicles on the centreline. Now we find their closest point and take rank distance along path to get order.
    indices = {}
    centreline = get_centreline_from_segment_sequence(sequence) 

    for v in potential_leaders:
        minimum_distance = math.inf
        v_position = (v.x,v.y)
        for i in range(len(centreline)):
            point = centreline[i]
            distance_to_centreline = compute_distance(point, v_position)
            
            if distance_to_centreline < minimum_distance and i == len(centreline) - 1:

                indices[v.id] = i

            elif distance_to_centreline < minimum_distance:
                minimum_distance = distance_to_centreline
                continue

            elif distance_to_centreline > minimum_distance:
                if i == 0:
                    indices[v.id] = 0
                else:
                    indices[v.id] = i - 1
                break


    input_vehicle_index = indices[vehicle.id]
    minimum_index_difference = math.inf
    leader = None

    for vehicle_id, centreline_index in indices.items():

        if indices[vehicle_id] - input_vehicle_index > 0:

            # Note: I am not currently handling case where both vehicles are on the same index in the centreline
            if indices[vehicle_id] - input_vehicle_index < minimum_index_difference:

                minimum_index_difference = indices[vehicle_id] - input_vehicle_index
                leader = vehicle_id

    return leader





# The variable, scenario, can just be the whole list of vehicles, though ideally it should be every car except the car in question
def conflict_present(conflict_direction, scenario):

    conflict_segments = []
    for segment_sequence in occlusion_constants.SEGMENT_SEQUENCES:
        if conflict_direction == segment_sequence[1]:
            for seg in segment_sequence[0]:
                if occlusion_constants.SEGMENT_MAP[seg] != 'exit-lane':
                    conflict_segments.append(seg)
    conflict_segments = set(conflict_segments)

    conflict_present = 0

    # I believe out of bounds vehicles do not have an assigned segment
    for v in scenario:
        if hasattr(v, 'current_segment'):
            if v.current_segment in conflict_segments:
                conflict_present = 1
                break

    return conflict_present



def retrieve_traffic_conditions_for_initial_speed(current_file_id, vehicle, scenario):

    conflict_direction = occlusion_constants.CONFLICT_MAP[vehicle.direction] if vehicle.direction in list(occlusion_constants.CONFLICT_MAP.keys()) else None
    has_conflict = conflict_present(conflict_direction, scenario)

    if vehicle.signal == 'G':
        dedicated_green = dedicated_green_check(current_file_id, conflict_direction, vehicle.current_time) if conflict_direction else 0
        dedicated_green = 1 if dedicated_green else 0
    else:
        dedicated_green = 0

    # Computing if there is a just_turned_green on green light
    if (occlusion_constants.SEGMENT_MAP[vehicle.current_segment] == 'left-turn-lane' or occlusion_constants.SEGMENT_MAP[vehicle.current_segment] == 'through-lane-entry') and vehicle.signal == 'G':
        db = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
        conn = sqlite3.connect(db)
        cur = conn.cursor()
        execute_cmd = f"SELECT {vehicle.direction}, TIME FROM TRAFFIC_LIGHTS"
        cur.execute(execute_cmd)
        query_result = cur.fetchall()

        green_light_indices = []
        green_light_times = []
        for i in range(len(query_result)):
            signal_time = query_result[i]
            if signal_time[0] == 'G':
                green_light_indices.append(i)
        for index in green_light_indices:
            if index - 1 not in green_light_indices:
                start_time = query_result[index][1]
            if index + 1 in green_light_indices:
                continue
            else:
                if index + 1 < len(query_result):
                    end_time = query_result[index+1][1]
                else:
                    end_time = query_result[index][1]
            green_light_times.append((start_time, end_time))

        current_time_index = None

        index = 0
        for start_time, end_time in green_light_times:
            if vehicle.current_time >= start_time and vehicle.current_time <= end_time:
                current_time_index = index
                break
            elif vehicle.current_time > end_time and index == len(green_light_times) - 1:
                current_time_index = index
                break
            index += 1

        if current_time_index == None:
            print(f"GREEN LIGHT TIMES: {green_light_times}")
            print(f"VEHICLE TIME: {vehicle.current_time}")
            raise Exception(f"Could not find green light associated with current time.")

        if occlusion_constants.SEGMENT_MAP[vehicle.current_segment] == 'left-turn-lane':
            time_with_delay = green_light_times[current_time_index][0] + occlusion_constants.LEFT_TURN_GREEN_LIGHT_DELAY

        elif occlusion_constants.SEGMENT_MAP[vehicle.current_segment] == 'through-lane-entry':
            time_with_delay = green_light_times[current_time_index][0] + occlusion_constants.STRAIGHT_THROUGH_GREEN_LIGHT_DELAY 

        if time_with_delay < vehicle.current_time:
            just_turned_green = 0
        else:
            just_turned_green = 1
    else:
        just_turned_green = 0

    return dedicated_green, has_conflict, just_turned_green 



def get_segment_from_position(position, path_origin, segment_seq, current_time):
    v = VehicleState()
    v.set_x(position[0])
    v.set_y(position[1])
    v.set_segment_seq(segment_seq)
    v.set_current_time(current_time)
    v.set_path_origin(path_origin)
    v.set_gate_crossing_times((None, None))
    traffic_region_list = None

    return assign_curent_segment(traffic_region_list,v,simulation=False)


def ag_obj_ingredients(vehicle):

    centreline = get_centreline_from_segment_sequence(vehicle.segment_seq)

    position = (vehicle.x, vehicle.y)
    index = closest_index_to_point(position, centreline)

    path = centreline[index:]
    waypoints = [(x[0],x[1]) for idx,x in enumerate(path) if idx in [int(y) for y in np.linspace(start=0, stop=len(path)-1, num=10)]]

    waypoint_segments = []
    path_origin = centreline[0]
    for point in path:
        # This was segment = get_segment_from_position((vehicle.x,vehicle.y), path_origin, vehicle.segment_seq, vehicle.current_time)
        # which I believe is incorrect
        segment = get_segment_from_position((point[0],point[1]), path_origin, vehicle.segment_seq, vehicle.current_time)
        waypoint_segments.append(segment)

    if len(waypoints) == 1:

        x_direction = (centreline[-1][0] - centreline[-2][0]) / (math.sqrt(centreline[-1][0] ** 2 + centreline[-2][0] ** 2))
        y_direction = (centreline[-1][1] - centreline[-2][1]) / (math.sqrt(centreline[-1][1] ** 2 + centreline[-2][1] ** 2))

        next_x_path_point = centreline[-1][0] + x_direction * 10
        next_y_path_point = centreline[-1][1] + y_direction * 10

        waypoints.append((next_x_path_point, next_y_path_point))

        waypoint_segments.append(waypoint_segments[-1])

    return waypoints, waypoint_segments

# Takes in the vehicle's full path and index where it should start emergency braking
def get_ag_obj_ingredients_for_emergency_braking(vehicle, path, start_emergency_braking_index):

    path_origin = path[0] 
    waypoints = path[start_emergency_braking_index:]
    waypoints = [(x[0],x[1]) for idx,x in enumerate(waypoints) if idx in [int(y) for y in np.linspace(start=0, stop=len(path)-1, num=10)]]

    waypoint_segments = []
    for point in waypoints:
        segment = get_segment_from_position((point[0],point[1]), path_origin, vehicle.segment_seq, vehicle.current_time)
        waypoint_segments.append(segment)

    if len(waypoints) == 1:
        centreline = get_centreline_from_segment_sequence(vehicle.segment_seq)
        x_direction = (centreline[-1][0] - centreline[-2][0]) / (math.sqrt(centreline[-1][0] ** 2 + centreline[-2][0] ** 2))
        y_direction = (centreline[-1][1] - centreline[-2][1]) / (math.sqrt(centreline[-1][1] ** 2 + centreline[-2][1] ** 2))
        next_x_path_point = centreline[-1][0] + x_direction * 10
        next_y_path_point = centreline[-1][1] + y_direction * 10
        waypoints.append((next_x_path_point, next_y_path_point))
        waypoint_segments.append(waypoint_segments[-1])

    return waypoints, waypoint_segments
    

def get_initial_speed_from_database(speed_setting, segment, traffic_light, dedicated_green, has_conflict, just_turned_green):

    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()
    
    include_dedicated_green = True if type(dedicated_green) == int else False
    include_has_conflict = True if type(has_conflict) == int else False
    include_just_turned_green = True if type(just_turned_green) == int else False

    if type(speed_setting) == str:
        dedicated_green_condition = f" AND DEDICATED_GREEN_FLAG={dedicated_green}" if dedicated_green else f""
        has_conflict_condition = f" AND HAS_CONFLICT_FLAG={has_conflict}" if has_conflict else f""
        just_turned_green_condition = f" AND JUST_TURNED_GREEN_FLAG={just_turned_green}" if just_turned_green else f""

        if speed_setting == 'gaussian':
                execute_cmd = f"SELECT INITIAL_SPEED_KEY, MEAN_SPEED, STANDARD_DEVIATION FROM INITIAL_SPEEDS WHERE SEGMENT='{segment}' AND TRAFFIC_LIGHT='{traffic_light}'{dedicated_green_condition}{has_conflict_condition}{just_turned_green_condition}"
                cur.execute(execute_cmd)
                query_result = cur.fetchall()
                query_result = query_result[0]
                initial_speed_key = query_result[0]
                mean = query_result[1]
                std = query_result[2]
                speed = np.random.normal(mean, std)
                return initial_speed_key, speed

        elif speed_setting == 'exponential':
            execute_cmd = f"SELECT INITIAL_SPEED_KEY, MEAN_SPEED FROM INITIAL_SPEEDS WHERE SEGMENT='{segment}' AND TRAFFIC_LIGHT='{traffic_light}'{dedicated_green_condition}{has_conflict_condition}{just_turned_green_condition}"
            cur.execute(execute_cmd)
            query_result = cur.fetchall()
            query_result = query_result[0]
            initial_speed_key = query_result[0]
            mean = query_result[1]
            speed = np.random.exponential(mean)
            return initial_speed_key, speed

        elif speed_setting == 'uniform':
            execute_cmd = f"SELECT INITIAL_SPEED_KEY, MIN_SPEED, MAX_SPEED FROM INITIAL_SPEEDS WHERE SEGMENT='{segment}' AND TRAFFIC_LIGHT='{traffic_light}'{dedicated_green_condition}{has_conflict_condition}{just_turned_green_condition}"
            cur.execute(execute_cmd)
            query_result = cur.fetchall()
            query_result = query_result[0]
            initial_speed_key = query_result[0]
            min_value = query_result[1]
            max_value = query_result[2]
            speed = np.random.uniform(min_value, max_value)
            return initial_speed_key, speed
    else:
        raise Exception(f"Input speed_setting must be a string. Instead, speed_setting was {speed_setting} which is of type {type(speed_setting)}.")


def speed_setting_check(speed_setting, segment, traffic_light, dedicated_green, has_conflict, just_turned_green):
    if not speed_setting:
        raise Exception(f"Could not find the speed profile for the segment: {segment}, traffic light: {traffic_light}, "
                        f"dedicated_green: {dedicated_green}, has_conflict: {has_conflict}, just_turned_green: {just_turned_green}")

def speed_quality_check(speed):
    if speed > occlusion_constants.MAX_INITIAL_SPEED_FOR_OCCLUDING_VEHICLES:
        speed = occlusion_constants.MAX_INITIAL_SPEED_FOR_OCCLUDING_VEHICLES
    elif speed < 0:
        speed = 0
    return speed



def initial_speed_helper(speed_setting, segment, traffic_light, dedicated_green, has_conflict, just_turned_green):
    if type(speed_setting) == int:
        go_speed = speed_setting
        stop_speed = speed_setting

        go_speed_initial_key = None
        stop_speed_initial_key = None

    elif type(speed_setting) == str:
        speed = get_initial_speed_from_database(speed_setting, segment, traffic_light, dedicated_green, has_conflict, just_turned_green)

        go_speed = speed[1]
        stop_speed = speed[1]

        go_speed_initial_key = speed[0]
        stop_speed_initial_key = speed[0]

    elif type(speed_setting) == dict:
        if type(speed_setting['go']) == int:
            go_speed = speed_setting['go']
            go_speed_initial_key = None

        else:
            speed = get_initial_speed_from_database(speed_setting['go'], segment, traffic_light, dedicated_green, has_conflict, just_turned_green)
            go_speed_initial_key = speed[0]
            go_speed = speed[1]

        if type(speed_setting['stop']) == int:
            stop_speed = speed_setting['stop']
            stop_speed_initial_key = None

        else:
            speed = get_initial_speed_from_database(speed_setting['stop'], segment, traffic_light, dedicated_green, has_conflict, just_turned_green)
            stop_speed_initial_key = speed[0]
            stop_speed = speed[1]

    go_speed = speed_quality_check(go_speed)
    stop_speed = speed_quality_check(stop_speed)
    return {'go_speed_initial_key': go_speed_initial_key, 'stop_speed_initial_key': stop_speed_initial_key, 'go': go_speed, 'stop': stop_speed}



def get_initial_speed_for_occluding_vehicle(segment, traffic_light, dedicated_green, has_conflict, just_turned_green):

    speed_profiles = occlusion_constants.SEGMENT_SPEED_MODELS[segment]

    speed_profile = None
    if segment in ['ln_n_1', 'ln_e_1', 'ln_s_1', 'ln_w_1']:
        if traffic_light == 'G' and dedicated_green == 1:
            for _, speed_profile in speed_profiles.items():
                if speed_profile['light'] == traffic_light and speed_profile['dedicated_green'] ==  dedicated_green and speed_profile['just_turned_green'] == just_turned_green:
                    speed_setting = speed_profile['speed']
                    break
            speed_setting_check(speed_setting, segment, traffic_light, dedicated_green, has_conflict, just_turned_green)
            return initial_speed_helper(speed_setting, segment, traffic_light, dedicated_green, None, just_turned_green)
 
        else:
            for _, speed_profile in speed_profiles.items():
                if speed_profile['light'] == traffic_light and speed_profile['has_conflict'] == has_conflict:
                    speed_setting = speed_profile['speed']
                    break
            speed_setting_check(speed_setting, segment, traffic_light, dedicated_green, has_conflict, just_turned_green)
            return initial_speed_helper(speed_setting, segment, traffic_light, None, has_conflict, None)

    elif segment in ['rt_prep-turn_s', 'rt_prep-turn_w', 'rt_exec-turn_s', 'rt_exec-turn_w', 'l_s_n_r', 'l_s_n_l', 'l_n_s_r',  \
                     'l_n_s_l', 'l_w_e_r', 'l_w_e_l', 'l_e_w_r', 'l_e_w_l', 'ln_s_4', 'ln_w_4', 'ln_e_-1', 'ln_e_-2', 'ln_n_-1', \
                     'ln_n_-2', 'ln_w_-1', 'ln_w_-2', 'ln_s_-1', 'ln_s_-2']:

        for _, speed_profile in speed_profiles.items():
            if speed_profile['light'] == traffic_light:
                speed_setting = speed_profile['speed']
                break
        speed_setting_check(speed_setting, segment, traffic_light, dedicated_green, has_conflict, just_turned_green)
        return initial_speed_helper(speed_setting, segment, traffic_light, None, None, None)

    elif segment in ['ln_s_2', 'ln_s_3', 'ln_e_2', 'ln_e_3', 'ln_n_2', 'ln_n_3', 'ln_w_2', 'ln_w_3']:

        if traffic_light == 'G':
            for _, speed_profile in speed_profiles.items():
                if speed_profile['light'] == traffic_light and speed_profile['just_turned_green'] == just_turned_green:
                    speed_setting = speed_profile['speed']
                    break
            speed_setting_check(speed_setting, segment, traffic_light, dedicated_green, has_conflict, just_turned_green)
            return initial_speed_helper(speed_setting, segment, traffic_light, None, None, just_turned_green)

        elif traffic_light == 'Y':
            for _, speed_profile in speed_profiles.items():
                if speed_profile['light'] == traffic_light:
                    speed_setting = speed_profile['speed']
                    break
            speed_setting_check(speed_setting, segment, traffic_light, dedicated_green, has_conflict, just_turned_green)
            return initial_speed_helper(speed_setting, segment, traffic_light, None, None, None)

    elif segment in ['exec-turn_n', 'exec-turn_e', 'exec-turn_s', 'exec-turn_w']:
        for _, speed_profile in speed_profiles.items():
            if speed_profile['light'] == traffic_light and speed_profile['has_conflict'] == has_conflict:
                speed_setting = speed_profile['speed']
                break
        speed_setting_check(speed_setting, segment, traffic_light, dedicated_green, has_conflict, just_turned_green)
        return initial_speed_helper(speed_setting, segment, traffic_light, None, has_conflict, None)

    elif segment in ['prep-turn_n', 'prep-turn_e', 'prep-turn_s', 'prep-turn_w']:
        if traffic_light == 'G' and dedicated_green == 1:
            for _, speed_profile in speed_profiles.items():
                if speed_profile['light'] == traffic_light and speed_profile['dedicated_green'] == dedicated_green:
                    speed_setting = speed_profile['speed']
                    break
            speed_setting_check(speed_setting, segment, traffic_light, dedicated_green, has_conflict, just_turned_green)
            return initial_speed_helper(speed_setting, segment, traffic_light, dedicated_green, None, None)

        else:
            for _, speed_profile in speed_profiles.items():
                if speed_profile['light'] == traffic_light and speed_profile['has_conflict'] == has_conflict:
                    speed_setting = speed_profile['speed']
                    break
            speed_setting_check(speed_setting, segment, traffic_light, dedicated_green, has_conflict, just_turned_green)
            return initial_speed_helper(speed_setting, segment, traffic_light, None, has_conflict, None)



def last_nonzero_index_for_sort(trajectory):
    nonzero_value_indices = [index for index, timestep in enumerate(trajectory) if timestep[3] != 0]
    if nonzero_value_indices:
        return nonzero_value_indices[-1]
    else:
        return 0



# TIME X Y SPEED ACCELERATION JERK CURVATURE YAW
# Index 0 is time index from 0 to trajectory horizon
# [0.0, 538830.52, 4814012.16, 16.994444444444444, -2.053959551935696, 0, 0.04318783942461197, -0.98052867145024]
def collision_check(traj_a, traj_b, type_a='car', type_b='car'):
    index_of_collision = 0
    for timestep_a, timestep_b in zip(traj_a, traj_b):
        v_a = VehicleState(type_a)
        v_b = VehicleState(type_b)


        v_a.set_x(timestep_a[1])
        v_a.set_y(timestep_a[2])
        angle_a = angle_check(timestep_a[7])
        v_a.set_angle(angle_a)

        v_b.set_x(timestep_b[1])
        v_b.set_y(timestep_b[2])
        angle_b = angle_check(timestep_b[7])
        v_b.set_angle(angle_b)

        bbox_a = Polygon(v_a.get_bounding_box_points(for_occ_map=False))
        bbox_b = Polygon(v_b.get_bounding_box_points(for_occ_map=False))


        if bbox_a.distance(bbox_b) <= 0.0:
            return timestep_a[3], timestep_b[3], index_of_collision

        index_of_collision += 1
    return None


def calculate_severity_class(v_impact):
    if v_impact >= 0.0 and v_impact <= 5.3:
        return f"S0"
    elif v_impact >= 5.3 and v_impact <= 7.8:
        return f"S1"
    elif v_impact >= 7.8 and v_impact <= 10.3:
        return f"S2"
    elif v_impact >= 10.3:
        return f"S3"



def get_rule_action(veh_state):
    DB = f"{occlusion_constants.PATH}/uni_weber/database_files/769/uni_weber_769.db"
    conn = sqlite3.connect(DB)
    c = conn.cursor()
    q_string = "select * from ACTIONS where IS_RULE='Y'"
    c.execute(q_string)
    res = c.fetchall()                
    ''' (segment,signal,lead vehicle,task, pedestrian,state_string(lead,pedestrian,oncoming))):action'''
    rules = {(row[0],row[3],row[4],row[5],row[7],row[8]):row[1] for row in res}
    key_tuples = [[occlusion_constants.SEGMENT_MAP[veh_state.current_segment]], [veh_state.signal,'*']]
    state_str = []
    if veh_state.leading_vehicle is not None:
        key_tuples.append(['Y','*'])
    else:
        key_tuples.append(['N','*'])
    key_tuples.append([veh_state.task])
    key_tuples.append(['N','*'])
    if veh_state.has_oncoming_vehicle:
        oncoming_veh = ['Y','*']
    else:
        oncoming_veh = ['N','*']
    state_str = [','.join(x) for x in itertools.product(key_tuples[2],key_tuples[4],oncoming_veh)]
    all_keys = list(itertools.product(key_tuples[0],key_tuples[1],key_tuples[2],key_tuples[3],key_tuples[4],state_str))
    action = []
    for k in all_keys:
        if k in rules:
            action.append(rules[k])
    if len(action) > 1:
        all_wait_actions = all(a in occlusion_constants.WAIT_ACTIONS for a in action)
        if all_wait_actions:
            chosen_action = None
            for a in reversed(occlusion_constants.WAIT_ACTIONS):
                if a in action:
                    chosen_action = a
                    break
            if chosen_action is None:
                sys.exit('No action in '+str(action)+' found in the list of wait actions')
            else:
                action = [chosen_action]
        else:
            if 'wait_for_lead_to_cross' in action and 'Y' in oncoming_veh :
                action = ['wait_for_lead_to_cross']
            else:
                if 'follow_lead_into_intersection' in action and 'Y' in oncoming_veh:
                    action.remove('follow_lead_into_intersection')
    return action



# Computes the conflict points (i.e., intersection of centrelines) for the uni-weber map
# The conflict points are stored in CONFLICT_SEQUENCES in occlusion_constants.py
def compute_conflict_points_for_segment_sequences():
    
    conflict_points = []
    for segment_seq_data in occlusion_constants.CONFLICT_SEQUENCES:
        segment_seq_pair = segment_seq_data[0]
        segment_seq_1 = segment_seq_pair[0]
        segment_seq_2 = segment_seq_pair[1]


        centreline_1 = get_centreline_from_segment_sequence(segment_seq_1)
        centreline_2 = get_centreline_from_segment_sequence(segment_seq_2)

        conflict_point = None
        for i in range(0, len(centreline_1)-1):

            line_1 = LineString([centreline_1[i], centreline_1[i+1]])

            for j in range(0, len(centreline_2)-1):

                line_2 = LineString([centreline_2[j], centreline_2[j+1]])

                if line_1.distance(line_2) < 0.2 and segment_seq_1 == ['ln_s_4', 'rt_prep-turn_s', 'rt_exec-turn_s', 'ln_e_-2']:
                    print(f"Special Case: Working on: {segment_seq_1} and {segment_seq_2}")
                    print(f"Distance is: {line_1.distance(line_2)}")
                    conflict_point = centreline_1[i]
                    fig, ax = plt.subplots()
                    ax.plot(conflict_point[0], conflict_point[1], 'ro')
                    print(f"Conflict point: {conflict_point}")

                    x_points = [i[0] for i in centreline_1]
                    y_points = [i[1] for i in centreline_1]
                    ax.plot(x_points,y_points, 'green', linewidth=0.2)
                    x_points = [i[0] for i in centreline_2]
                    y_points = [i[1] for i in centreline_2]
                    ax.plot(x_points,y_points, 'green', linewidth=0.2)
                    plt.show()
                    break  

                elif line_1.intersects(line_2):
                    print(f"Working on: {segment_seq_1} and {segment_seq_2}")
                    conflict_point = line_1.intersection(line_2)
                    conflict_point = (conflict_point.x, conflict_point.y)
                    fig, ax = plt.subplots()
                    ax.plot(conflict_point[0], conflict_point[1], 'ro')
                    print(f"Conflict point: {conflict_point}")

                    x_points = [i[0] for i in centreline_1]
                    y_points = [i[1] for i in centreline_1]
                    ax.plot(x_points,y_points, 'green', linewidth=0.2)
                    x_points = [i[0] for i in centreline_2]
                    y_points = [i[1] for i in centreline_2]
                    ax.plot(x_points,y_points, 'green', linewidth=0.2)
                    plt.show()
                    break  

            if conflict_point:
                conflict_points.append([segment_seq_pair, conflict_point])
                break

    print(conflict_points)


# Determine if target vehicle is an oncoming vehicle
def compute_has_oncoming_vehicle(subject_vehicle, target_vehicle):

    # If either vehicle is the other's leading vehicle, then neither is an oncoming vehicle for each other
    if subject_vehicle.leading_vehicle:
        if subject_vehicle.leading_vehicle.id == target_vehicle.id:
            return False

    if target_vehicle.leading_vehicle:
        if target_vehicle.leading_vehicle.id == subject_vehicle.id:
            return False

    subject_vehicle.segment_seq
    target_vehicle.segment_seq

    subject_vehicle_in_conflict = False
    target_vehicle_in_conflict = False
    has_oncoming_vehicle = False

    input_segment_seq_pair = set([tuple(subject_vehicle.segment_seq), tuple(target_vehicle.segment_seq)])

    for entry in occlusion_constants.CONFLICT_SEQUENCES:
        segment_seq_pair = entry[0]
        segment_seq_pair = [tuple(i) for i in segment_seq_pair]
        conflict_point = entry[1]  

        if input_segment_seq_pair == set(segment_seq_pair):

            # If at least one of these is true then oncoming vehicle is false
            # If both false, set both to true
            subject_vehicle_centreline = get_centreline_from_segment_sequence(subject_vehicle.segment_seq)

            # Compute the index along the centreline of the input segment sequence that is closest to the conflict point
            subject_vehicle_conflict_point_index = closest_index_to_point(conflict_point, subject_vehicle_centreline)
            subject_vehicle_position = (subject_vehicle.x, subject_vehicle.y)
            subject_vehicle_centreline_index = closest_index_to_point(subject_vehicle_position, subject_vehicle_centreline)
            # Compare distances along centreline for subject vehicle
            subject_vehicle_distance_along_curve = distance_along_curve(subject_vehicle_centreline, subject_vehicle_centreline_index)
            conflict_distance_along_curve = distance_along_curve(subject_vehicle_centreline, subject_vehicle_conflict_point_index)

            if subject_vehicle_distance_along_curve - conflict_distance_along_curve < 2.0:
                subject_vehicle_in_conflict = True

            target_vehicle_centreline = get_centreline_from_segment_sequence(target_vehicle.segment_seq)
            target_vehicle_conflict_point_index = closest_index_to_point(conflict_point, target_vehicle_centreline)

            target_vehicle_position = (target_vehicle.x, target_vehicle.y)
            target_vehicle_centreline_index = closest_index_to_point(target_vehicle_position, target_vehicle_centreline)

            target_vehicle_distance_along_curve = distance_along_curve(target_vehicle_centreline, target_vehicle_centreline_index)
            conflict_distance_along_curve = distance_along_curve(target_vehicle_centreline, target_vehicle_conflict_point_index)

            if target_vehicle_distance_along_curve - conflict_distance_along_curve < 2.0:
                target_vehicle_in_conflict = True

    if subject_vehicle_in_conflict and target_vehicle_in_conflict:
        has_oncoming_vehicle = True

    return has_oncoming_vehicle

# Returns distance from index 0 to input index along the curve
def distance_along_curve(curve, index=None):
    total_distance = 0
    for i in range(0, len(curve)-1):
        if i == index:
            break
        distance = compute_distance(curve[i], curve[i+1])
        total_distance += distance
    return total_distance

# Returns the closest index on centreline to point
def closest_index_to_point(point, centreline):
    minimum_distance = math.inf
    for i in range(len(centreline)):
        distance_to_centreline = compute_distance(point, centreline[i])
        if distance_to_centreline < minimum_distance and i == len(centreline) - 1:
            index = i
        elif distance_to_centreline < minimum_distance:
            minimum_distance = distance_to_centreline
        elif distance_to_centreline > minimum_distance:
            if i == 0:
                index = 0
            else:
                index = i - 1
            break
    return index


def filter_trajectories(trajectories, maneuver):
    if not trajectories:
        return trajectories
    sorted_trajectories = []
    for level in list(trajectories.keys()):
        for trajectory in trajectories[level]:
            sorted_trajectories.append(trajectory)

    if maneuver in occlusion_constants.WAIT_ACTIONS and len(sorted_trajectories) >= 3:
        sorted_trajectories.sort(key=lambda x: last_nonzero_index_for_sort(x))

    elif maneuver not in occlusion_constants.WAIT_ACTIONS and len(sorted_trajectories) >= 3:
        # First index 1, because its (level, trajectory)
        # Then index terminal velocity 
        # Then index velocity, which is the 4th position in traj timestep, (index 3)
        sorted_trajectories.sort(key=lambda x:x[-1][3])

    if len(sorted_trajectories) >= 3:
        selected_indices = [0, round((len(sorted_trajectories) - 1)/2), len(sorted_trajectories) - 1]

        sorted_trajectories = [sorted_trajectories[i] for i in selected_indices]

    return sorted_trajectories


# If filter_follow_maneuvers is True, we also filter out all follow maneuvers
def filter_wait_maneuvers(maneuvers, filter_follow_maneuvers=False):

    wait_maneuver_included = False
    removed_wait_maneuvers = []
    for maneuver in maneuvers:
        if maneuver in occlusion_constants.WAIT_ACTIONS and not wait_maneuver_included:
            wait_maneuver_included = True
        elif maneuver in occlusion_constants.WAIT_ACTIONS and wait_maneuver_included:
            removed_wait_maneuvers.append(maneuver)

    for wait_maneuver in removed_wait_maneuvers:
        maneuvers.remove(wait_maneuver)

    if filter_follow_maneuvers:
        removed_follow_maneuvers = []
        for maneuver in maneuvers:
            if maneuver in occlusion_constants.FOLLOW_ACTIONS:
                removed_follow_maneuvers.append(maneuver)
        for follow_maneuver in removed_follow_maneuvers:
            maneuvers.remove(follow_maneuver)

    return maneuvers


# Note: selected_trajectories is a dict of track_id and then trajectory key-value pairs 
def animate_collision_helper(current_file_id, scenario_key, collision_identifier, collision_id, selected_trajectories, time_to_collision_index, colliding_vehicle_ids, collision_configuration):
    x_values_dict = {}
    y_values_dict = {}
    angles_dict = {}
    car_list = []

    x = occlusion_constants.UNI_WEBER_INTERSECTION_DIMENSIONS['x']
    y = occlusion_constants.UNI_WEBER_INTERSECTION_DIMENSIONS['y']
    intersection_box = Polygon([[x[0],y[0]], [x[0],y[1]], [x[1],y[1]], [x[1],y[0]]])

    for track_id, trajectory in selected_trajectories.items():
        # print(trajectory)
        car, = plt.plot([], [], 'k')
        car_list.append(car)
        x_values = [timestep[1] for timestep in trajectory]
        y_values = [timestep[2] for timestep in trajectory]
        angles = [angle_check(timestep[-2]) for timestep in trajectory]

        x_values_dict[track_id] = x_values
        y_values_dict[track_id] = y_values
        angles_dict[track_id] = angles

    fig, ax = plt.subplots()

    for sequence in occlusion_constants.SEGMENT_SEQUENCES:
        centreline = get_centreline_from_segment_sequence(sequence[0])
        x_points = [i[0] for i in centreline]
        y_points = [i[1] for i in centreline]
        ax.plot(x_points,y_points, 'black', alpha=0.4, linewidth=0.2)


    def init():
        ax.set_xlim(538775, 538900)
        ax.set_ylim(4813975, 4814050)
        for sequence in occlusion_constants.SEGMENT_SEQUENCES:
            centreline = get_centreline_from_segment_sequence(sequence[0])
            x_points = [i[0] for i in centreline]
            y_points = [i[1] for i in centreline]
            ax.plot(x_points,y_points, 'black', alpha=0.4, linewidth=0.2)
        return car_list

    def update(frame):
        fig.clear()
        # plt.title(f"Scenario {scenario_key}; Collision ID: {collision_id}")
        plt.title(f"Synthetic Scenario {scenario_key}: {collision_configuration} Collision", y=1.05)
        for sequence in occlusion_constants.SEGMENT_SEQUENCES:
            centreline = get_centreline_from_segment_sequence(sequence[0])
            x_points = [i[0] for i in centreline]
            y_points = [i[1] for i in centreline]
            plt.plot(x_points,y_points, 'black', alpha=0.4, linewidth=0.2)
        
        car_list = []
        for track_id in list(selected_trajectories.keys()):
            if frame < len(x_values_dict[track_id]) - 1:
                if frame >= time_to_collision_index:
                    v = VehicleState()
                    v.set_id(track_id)
                    v.set_x(x_values_dict[track_id][time_to_collision_index])
                    v.set_y(y_values_dict[track_id][time_to_collision_index])
                    v.set_angle(angles_dict[track_id][time_to_collision_index])
                    bounding_box = Polygon(v.get_bounding_box_points(for_occ_map=False))

                    if intersection_box.contains(bounding_box):
                        x,y = bounding_box.exterior.xy

                        if track_id in colliding_vehicle_ids:
                            car, = plt.plot(x,y, 'r')
                            label = plt.text(x[0],y[0], f"{track_id}")
                            car_list.append(car)
                            car_list.append(label)
                        else:
                            car, = plt.plot(x,y, 'k')
                            label = plt.text(x[0],y[0], f"{track_id}")
                            car_list.append(car)
                            car_list.append(label)
                else:
                    v = VehicleState()
                    v.set_id(track_id)
                    v.set_x(x_values_dict[track_id][frame])
                    v.set_y(y_values_dict[track_id][frame])
                    v.set_angle(angles_dict[track_id][frame])
                    bounding_box = Polygon(v.get_bounding_box_points(for_occ_map=False))

                    if intersection_box.contains(bounding_box):
                        x,y = bounding_box.exterior.xy
                        car, = plt.plot(x,y, 'k')
                        label = plt.text(x[0],y[0], f"{track_id}")
                        car_list.append(car)
                        car_list.append(label)
        return car_list


    ani = animation.FuncAnimation(fig, update, frames=time_to_collision_index + 10,
                        init_func=init, interval=100, blit=True)

    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=500)

    if not os.path.isdir(f"collision_animations/{current_file_id}"):
        os.makedirs(f"collision_animations/{current_file_id}")
    ani.save(f'collision_animations/{current_file_id}/Collision_{scenario_key}_{collision_id}_{collision_identifier}.mp4', writer=writer)
    plt.close()

def retrieve_trajectories(current_file_id, scenario_key, vehicle_ids, vehicle_maneuvers):
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    conn = sqlite3.connect(occlusion_constants.OCC_DB)
    cur = conn.cursor()

    # Stores all the trajectories
    all_trajectories_dict = {}
    # Save trajectories from DB in a dict
    for track_id in vehicle_ids:
        maneuvers = vehicle_maneuvers[track_id]
        all_trajectories_dict[track_id] = {}
        
        for maneuver in maneuvers:
            maneuver_trajectory_dict = {}
             # keys are levels, values are lists of trajectories
            if track_id < 0:
                execute_cmd = f"SELECT TRAJECTORIES FROM OCCLUSION_SCENARIO_TRAJECTORIES WHERE SCENARIO_KEY={scenario_key} AND TRACK_ID={track_id} AND MANEUVER='{maneuver}'"
            elif track_id > 0 and maneuver in occlusion_constants.FOLLOW_ACTIONS:
                execute_cmd = f"SELECT TRAJECTORIES FROM OCCLUSION_SCENARIO_TRAJECTORIES WHERE SCENARIO_KEY={scenario_key} AND TRACK_ID={track_id} AND MANEUVER='{maneuver}'"
            else:
                execute_cmd = f"SELECT TRAJECTORIES FROM BASE_SCENARIO_TRAJECTORIES WHERE MIN_SCENARIO_KEY <= {scenario_key} AND MAX_SCENARIO_KEY >= {scenario_key} AND TRACK_ID={track_id} AND MANEUVER='{maneuver}'"
            
            cur.execute(execute_cmd)
            query_result = cur.fetchall()
            trajectories = ast.literal_eval(query_result[0][0])
            
            all_trajectories_dict[track_id][maneuver] = trajectories
    return all_trajectories_dict



# Gets all the maneuvers that have trajectories for each vehicle in the given scenario
# Returns a dict of the form: {1: [track_speed, follow_lead], -2: [track_speed], 3: [wait_for_oncoming_vehicle]}
def retrieve_all_maneuvers(current_file_id, scenario_key):
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

    conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
    cur_occ_db = conn_occ_db.cursor()

    conn_db = sqlite3.connect(occlusion_constants.DB)
    cur_db = conn_db.cursor()

    execute_cmd = f"SELECT SUBJECT_VEHICLE_ID, OCCLUDING_VEHICLE_ID, TIME FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={scenario_key}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()

    subject_vehicle_id = query_result[0][0]
    occluding_vehicle_id = query_result[0][1]
    current_time = query_result[0][2]

    execute_cmd = f"SELECT RELEV_AGENT_IDS FROM RELEVANT_AGENTS WHERE TIME={current_time} AND TRACK_ID={subject_vehicle_id}"
    cur_db.execute(execute_cmd)
    query_result = cur_db.fetchall()
    relevant_vehicle_ids = ast.literal_eval(query_result[0][0])

    ordered_vehicle_ids = [subject_vehicle_id, occluding_vehicle_id]
    for track_id in relevant_vehicle_ids:
        ordered_vehicle_ids.append(track_id)

    # Get all maneuvers
    execute_cmd = f"SELECT TRACK_ID, MANEUVER FROM OCCLUSION_SCENARIO_TRAJECTORIES WHERE SCENARIO_KEY={scenario_key}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    track_id_maneuver_pairs_occlusion_scenario = list(set(query_result))


    execute_cmd = f"SELECT TRACK_ID, MANEUVER FROM BASE_SCENARIO_TRAJECTORIES WHERE MIN_SCENARIO_KEY <= {scenario_key} AND MAX_SCENARIO_KEY >= {scenario_key}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    track_id_maneuver_pairs_base_scenario = list(set(query_result))

    track_id_maneuver_pairs = track_id_maneuver_pairs_occlusion_scenario + track_id_maneuver_pairs_base_scenario

    vehicle_maneuvers = {}
    for track_id in ordered_vehicle_ids:
        vehicle_maneuvers[track_id] = []
        for track_id_maneuver in track_id_maneuver_pairs:
            if track_id_maneuver[0] == track_id:
                vehicle_maneuvers[track_id].append(track_id_maneuver[1])

    return current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs



# Animates all collisions that occur for scenario key
def animate_factored_game_collision(current_file_id, scenario_key, collision_configuration):

    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
    cur_occ_db = conn_occ_db.cursor()


    # Animate all factored game collisions
    execute_cmd = f"SELECT FACTORED_GAME_COLLISIONS FROM COLLISIONS WHERE SCENARIO_KEY={scenario_key}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    all_factored_game_collisions = ast.literal_eval(query_result[0][0])

    # Get trajectory indices for each vehicle in scenario
    execute_cmd = f"SELECT FACTORED_GAME_TRAJ_INDICES FROM COLLISIONS WHERE SCENARIO_KEY={scenario_key}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    all_factored_game_traj_indices = ast.literal_eval(query_result[0][0])

    # Get emergency braking trajectories for both colliding vehicles
    execute_cmd = f"SELECT FACTORED_GAME_EMERGENCY_BRAKING_TRAJS FROM COLLISIONS WHERE SCENARIO_KEY={scenario_key}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    all_factored_game_emergency_braking_trajs = ast.literal_eval(query_result[0][0])


    for collision_identifier, factored_game_collisions in all_factored_game_collisions.items():
        factored_game_traj_indices = all_factored_game_traj_indices[collision_identifier]
        factored_game_emergency_braking_trajs = all_factored_game_emergency_braking_trajs[collision_identifier]

        for collision in factored_game_collisions:

            track_id_a = collision[0]
            track_id_b = collision[1]
            collision_id = collision[2]

            selected_trajectories = {}

            selected_trajectories[track_id_a] = factored_game_emergency_braking_trajs[collision_id][0]
            selected_trajectories[track_id_b] = factored_game_emergency_braking_trajs[collision_id][1]

            collision_details = collision_check(selected_trajectories[track_id_a], selected_trajectories[track_id_b])

            # time_to_collision_index = int(collision[-1] * 10)
            time_to_collision_index = collision_details[-1]
            print(f"TIME TO COLLISION INDEX: {time_to_collision_index}")

            # DEBUG
            # collision_details = collision_check(factored_game_emergency_braking_trajs[collision_id][0], factored_game_emergency_braking_trajs[collision_id][1], type_a='car', type_b='car')
            #  time, x, y, speed, acceleration, jerk, lateral acceleration, yaw, trajectory length
            # DEBUG

            # Get trajectories for other vehicles
            current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = retrieve_all_maneuvers(current_file_id, scenario_key)
            all_trajectories = retrieve_trajectories(current_file_id, scenario_key, ordered_vehicle_ids, vehicle_maneuvers)

            for track_id in ordered_vehicle_ids:
                # If scenario key is 236 don't use emergency braking trajectory for vehicle a as it is stopped and the emergency braking traj is not well formed.
                if current_file_id == "771" and scenario_key == 236:
                    if track_id != track_id_b:
                        traj_maneuver_and_index = [manv_index for manv_index in factored_game_traj_indices if manv_index[0] == track_id][0]
                        vehicle_maneuver = occlusion_constants.L1_MANEUVERS[traj_maneuver_and_index[1]]
                        selected_trajectories[track_id] = all_trajectories[track_id][vehicle_maneuver][traj_maneuver_and_index[-1]]
                else:
                    if track_id != track_id_a and track_id != track_id_b:
                        traj_maneuver_and_index = [manv_index for manv_index in factored_game_traj_indices if manv_index[0] == track_id][0]
                        vehicle_maneuver = occlusion_constants.L1_MANEUVERS[traj_maneuver_and_index[1]]
                        selected_trajectories[track_id] = all_trajectories[track_id][vehicle_maneuver][traj_maneuver_and_index[-1]]

            animate_collision_helper(current_file_id, scenario_key, collision_identifier, collision_id, selected_trajectories, time_to_collision_index, (track_id_a, track_id_b), collision_configuration)      



def setup_scenario(current_file_id, current_time, subject_vehicle_id, relevant_vehicle_ids, occluding_vehicle_id=None, NE=False):
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)
    NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

    if NE:
        conn_occ_db = sqlite3.connect(NE_OCC_DB)
        cur_occ_db = conn_occ_db.cursor()
    else:
        conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
        cur_occ_db = conn_occ_db.cursor()

    scenario = []
    subject_vehicle = setup_vehicle_for_occlusion_scenario(subject_vehicle_id, current_time)
    scenario.append(subject_vehicle)
    # Create occluding vehicle from db
    if occluding_vehicle_id != None:
        execute_cmd = f"SELECT * FROM OCCLUDING_VEHICLES WHERE TRACK_ID={occluding_vehicle_id}"
        cur_occ_db.execute(execute_cmd)
        query_result = cur_occ_db.fetchall()
        occluding_vehicle = setup_occluding_vehicle_from_db(query_result[0])
        scenario.append(occluding_vehicle)
    # Create relevant_vehicles from db
    relevant_vehicles = []
    for track_id in relevant_vehicle_ids:
        relevant_vehicle = setup_vehicle_for_occlusion_scenario(track_id, current_time)
        relevant_vehicles.append(relevant_vehicle)
    scenario += relevant_vehicles

    return scenario



def setup_any_vehicle(track_id, current_time, cur):
    if track_id > 0:
        v = setup_vehicle_for_occlusion_scenario(track_id, current_time)
    else:
        execute_cmd = f"SELECT * FROM OCCLUDING_VEHICLES WHERE TRACK_ID={track_id}"
        cur.execute(execute_cmd)
        query_result = cur.fetchall()
        v = setup_occluding_vehicle_from_db(query_result[0])
    return v


# To analyze where occlusion occurs in the Uni-Weber intersection
def generate_colliding_vehicle_position_map_for_conference_paper():

    direction_colours = {('L_E_S', 'L_W_E'): 'gold', ('L_S_W', 'L_N_S'): 'cyan', ('L_W_N', 'L_E_W'): 'lawngreen', ('L_S_E', 'L_W_E'): 'darkmagenta', ('L_W_S', 'L_N_S'): 'hotpink', ('L_N_E', 'L_S_N'): 'orangered'}
    direction_markers = {('L_E_S', 'L_W_E'): 'o', ('L_S_W', 'L_N_S'): 'd', ('L_W_N', 'L_E_W'): '*', ('L_S_E', 'L_W_E'): 'D', ('L_W_S', 'L_N_S'): 's', ('L_N_E', 'L_S_N'): 'X'}

    vehicle_direction_to_direction = {('L_E_S', 'L_W_E'): ('ES : WE'), ('L_S_W', 'L_N_S'): ('SW : NS'), ('L_W_N', 'L_E_W'): ('WN : EW'), ('L_S_E', 'L_W_E'): ('SE : WE'), ('L_W_S', 'L_N_S'): ('WS : NS'), ('L_N_E', 'L_S_N'): ('NE : SN')}
    direction_in_legend = []
    legend_markers = []
    legend_directions = []

    fig, ax = plt.subplots()

    for sequence in occlusion_constants.SEGMENT_SEQUENCES:
        centreline = get_centreline_from_segment_sequence(sequence[0])
        x_points = [i[0] for i in centreline]
        y_points = [i[1] for i in centreline]
        ax.plot(x_points,y_points, 'black', alpha=0.5, linewidth=0.2)

    directions = []
    occluding_vehicle_positions = []
    with open('Occlusion-Caused Collisions.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for occlusion_caused_collision in reader:
        
            current_file_id = occlusion_caused_collision['Database']
            occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
            occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
            occlusion_constants.CURRENT_FILE_ID = str(current_file_id)
            NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

            if int(occlusion_caused_collision['NE_Scenario']) == 1:
                conn_occ_db = sqlite3.connect(NE_OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            else:
                conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            execute_cmd = f"SELECT TIME FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            current_time = query_result[0][0]


            execute_cmd = f"SELECT FACTORED_GAME_COLLISIONS FROM COLLISIONS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            collisions_for_maneuver_combo = ast.literal_eval(query_result[0][0])
            collision_identifier_coded = ast.literal_eval(occlusion_caused_collision['Collision_Identifier'])
            collisions = collisions_for_maneuver_combo[collision_identifier_coded]
            colliding_vehicle_a_id = collisions[0][0]
            colliding_vehicle_b_id = collisions[0][1]

            v_a = setup_any_vehicle(colliding_vehicle_a_id, current_time, cur_occ_db)
            v_b = setup_any_vehicle(colliding_vehicle_b_id, current_time, cur_occ_db)

            collision_time = int(collisions[0][-1] * 10)

            execute_cmd = f"SELECT FACTORED_GAME_EMERGENCY_BRAKING_TRAJS FROM COLLISIONS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            emergency_braking_trajs = ast.literal_eval(query_result[0][0])
            traj_key = list(emergency_braking_trajs[collision_identifier_coded].keys())[0]
            emergency_braking_trajs = emergency_braking_trajs[collision_identifier_coded][traj_key]
            colliding_vehicle_a_traj = emergency_braking_trajs[0]
            colliding_vehicle_b_traj = emergency_braking_trajs[1]

            pos_a = colliding_vehicle_a_traj[collision_time][1:3]
            pos_b = colliding_vehicle_b_traj[collision_time][1:3]

            colour = direction_colours[(v_a.direction, v_b.direction)]
            marker = direction_markers[(v_a.direction, v_b.direction)]
            plt_marker, = plt.plot(pos_a[0], pos_a[1], color=colour, marker=marker, linestyle="None", ms=7, mec='k', mew=1)
            plt.plot(pos_b[0], pos_b[1], color=colour, marker=marker, linestyle="None", ms=7, mec='k', mew=1)

            
            if (v_a.direction, v_b.direction) not in direction_in_legend:
                direction_in_legend.append((v_a.direction, v_b.direction))
                legend_markers.append(plt_marker)
                d = vehicle_direction_to_direction[(v_a.direction, v_b.direction)]
                legend_directions.append(d)

            print(f"Finished with scenario: {occlusion_caused_collision['Scenario_Key']}")


    plt.legend(legend_markers, legend_directions, title='Conflict Directions')
    plt.title("Colliding Vehicle Positions")
    fig.savefig('Colliding Vehicle Positions.pdf', format='pdf')

def generate_no_SOV_occluding_vehicle_position_map_for_conference_paper():
    direction_colours = {('L_E_S', 'L_W_E'): 'gold', ('L_S_W', 'L_N_S'): 'cyan', ('L_W_N', 'L_E_W'): 'lawngreen', ('L_S_E', 'L_W_E'): 'darkmagenta', ('L_W_S', 'L_N_S'): 'hotpink', ('L_N_E', 'L_S_N'): 'orangered'}
    direction_markers = {('L_E_S', 'L_W_E'): 'o', ('L_S_W', 'L_N_S'): 'd', ('L_W_N', 'L_E_W'): '*', ('L_S_E', 'L_W_E'): 'D', ('L_W_S', 'L_N_S'): 's', ('L_N_E', 'L_S_N'): 'X'}

    vehicle_direction_to_direction = {('L_E_S', 'L_W_E'): ('ES : WE'), ('L_S_W', 'L_N_S'): ('SW : NS'), ('L_W_N', 'L_E_W'): ('WN : EW'), ('L_S_E', 'L_W_E'): ('SE : WE'), ('L_W_S', 'L_N_S'): ('WS : NS'), ('L_N_E', 'L_S_N'): ('NE : SN')}
    direction_in_legend = []
    legend_markers = []
    legend_directions = []

    collisions = [[781, 84, 21.021, 39, 35, 49], [785, 107, 117.117, 132, 117, 133]]
    fig, ax = plt.subplots()

    for sequence in occlusion_constants.SEGMENT_SEQUENCES:
        centreline = get_centreline_from_segment_sequence(sequence[0])
        x_points = [i[0] for i in centreline]
        y_points = [i[1] for i in centreline]
        ax.plot(x_points,y_points, 'black', alpha=0.5, linewidth=0.2)

    directions = []
    occluding_vehicle_positions = []

    for collision in collisions:
        current_file_id = collision[0]
        occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
        NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
        occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
        occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

        conn_db = sqlite3.connect(occlusion_constants.DB)
        cur_db = conn_db.cursor()


        occ_v = setup_any_vehicle(collision[3], collision[2], cur_db)
        v_a = setup_any_vehicle(collision[4], collision[2], cur_db)
        v_b = setup_any_vehicle(collision[5], collision[2], cur_db)

        colour = direction_colours[(v_a.direction, v_b.direction)]
        marker = direction_markers[(v_a.direction, v_b.direction)]
        plt_marker, = plt.plot(occ_v.x, occ_v.y, color=colour, marker=marker, linestyle="None", ms=7, mec='k', mew=1)
        
        if (v_a.direction, v_b.direction) not in direction_in_legend:
            direction_in_legend.append((v_a.direction, v_b.direction))
            legend_markers.append(plt_marker)
            d = vehicle_direction_to_direction[(v_a.direction, v_b.direction)]
            legend_directions.append(d)

    plt.legend(legend_markers, legend_directions, title='Conflict Directions', loc ="lower right")
    plt.title("Occluding Vehicle Positions")
    fig.savefig('No SOV Occluding Vehicle Positions.pdf', format='pdf')



def generate_no_SOV_colliding_vehicle_positions_map_for_conference_paper():
    direction_colours = {('L_E_S', 'L_W_E'): 'gold', ('L_S_W', 'L_N_S'): 'cyan', ('L_W_N', 'L_E_W'): 'lawngreen', ('L_S_E', 'L_W_E'): 'darkmagenta', ('L_W_S', 'L_N_S'): 'hotpink', ('L_N_E', 'L_S_N'): 'orangered'}
    direction_markers = {('L_E_S', 'L_W_E'): 'o', ('L_S_W', 'L_N_S'): 'd', ('L_W_N', 'L_E_W'): '*', ('L_S_E', 'L_W_E'): 'D', ('L_W_S', 'L_N_S'): 's', ('L_N_E', 'L_S_N'): 'X'}

    vehicle_direction_to_direction = {('L_E_S', 'L_W_E'): ('ES : WE'), ('L_S_W', 'L_N_S'): ('SW : NS'), ('L_W_N', 'L_E_W'): ('WN : EW'), ('L_S_E', 'L_W_E'): ('SE : WE'), ('L_W_S', 'L_N_S'): ('WS : NS'), ('L_N_E', 'L_S_N'): ('NE : SN')}
    direction_in_legend = []
    legend_markers = []
    legend_directions = []

    collisions = [[781, 84, 21.021, 39, 35, 49, 4.0, ((37, 2), (35, 2), (39, 3), (45, 4), (42, 3), (46, 4), (48, 3), (49, 4))], [785, 107, 117.117, 132, 117, 133, ((135, 7), (117, 2), (130, 3), (132, 3), (133, 3), (134, 4))]]
    fig, ax = plt.subplots()

    for sequence in occlusion_constants.SEGMENT_SEQUENCES:
        centreline = get_centreline_from_segment_sequence(sequence[0])
        x_points = [i[0] for i in centreline]
        y_points = [i[1] for i in centreline]
        ax.plot(x_points,y_points, 'black', alpha=0.5, linewidth=0.2)

    directions = []
    occluding_vehicle_positions = []

    for collision in collisions:
        current_file_id = collision[0]

        occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
        NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
        occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
        occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

        conn_db = sqlite3.connect(occlusion_constants.DB)
        cur_db = conn_db.cursor()
        v_a = setup_any_vehicle(collision[4], collision[2], cur_db)
        v_b = setup_any_vehicle(collision[5], collision[2], cur_db)


        if current_file_id == 781:
            v_a_x = 538847.2834892704
            v_a_y = 4814013.420988619
            v_b_x = 538846.213751154
            v_b_y = 4814009.891399092

        elif current_file_id == 785:
            v_a_x = 538826.0346922802
            v_a_y = 4814008.892016907
            v_b_x = 538827.7987825369
            v_b_y = 4814012.300235969

        print(f"VEHICLE A X AND Y: {v_a_x}; {v_a_y}")
        print(f"VEHICLE B X AND Y: {v_b_x}; {v_b_y}")

        colour = direction_colours[(v_a.direction, v_b.direction)]
        marker = direction_markers[(v_a.direction, v_b.direction)]

        plt_marker, = plt.plot(v_a_x, v_a_y, color=colour, marker=marker, linestyle="None", ms=7, mec='k', mew=1)
        plt.plot(v_b_x, v_b_y, color=colour, marker=marker, linestyle="None", ms=7, mec='k', mew=1)
        
        if (v_a.direction, v_b.direction) not in direction_in_legend:
            direction_in_legend.append((v_a.direction, v_b.direction))
            legend_markers.append(plt_marker)
            d = vehicle_direction_to_direction[(v_a.direction, v_b.direction)]
            legend_directions.append(d)

    plt.legend(legend_markers, legend_directions, title='Conflict Directions', loc="lower right")
    plt.title("Colliding Vehicle Positions")
    fig.savefig('No SOV Colliding Vehicle Positions.pdf', format='pdf')


def regenerate_all_collision_animations_for_conference_paper():

    with open('Occlusion-Caused Collisions.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for occlusion_caused_collision in reader:

            current_file_id = occlusion_caused_collision['Database']
            scenario_key = int(occlusion_caused_collision['Scenario_Key'])

            collision_configuration = occlusion_caused_collision['Collision_Configuration']

            occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
            occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
            occlusion_constants.CURRENT_FILE_ID = str(current_file_id)
            NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

            if int(occlusion_caused_collision['NE_Scenario']) == 1:
                regenerate_NE_scenarios.animate_factored_game_collision(current_file_id, scenario_key, collision_configuration)
            else:
                animate_factored_game_collision(current_file_id, scenario_key, collision_configuration)


# To analyze where occlusion occurs in the Uni-Weber intersection
def generate_occluding_vehicle_position_map_for_conference_paper():

    direction_colours = {('L_E_S', 'L_W_E'): 'gold', ('L_S_W', 'L_N_S'): 'cyan', ('L_W_N', 'L_E_W'): 'lawngreen', ('L_S_E', 'L_W_E'): 'darkmagenta', ('L_W_S', 'L_N_S'): 'hotpink', ('L_N_E', 'L_S_N'): 'orangered'}
    direction_markers = {('L_E_S', 'L_W_E'): 'o', ('L_S_W', 'L_N_S'): 'd', ('L_W_N', 'L_E_W'): '*', ('L_S_E', 'L_W_E'): 'D', ('L_W_S', 'L_N_S'): 's', ('L_N_E', 'L_S_N'): 'X'}

    vehicle_direction_to_direction = {('L_E_S', 'L_W_E'): ('ES : WE'), ('L_S_W', 'L_N_S'): ('SW : NS'), ('L_W_N', 'L_E_W'): ('WN : EW'), ('L_S_E', 'L_W_E'): ('SE : WE'), ('L_W_S', 'L_N_S'): ('WS : NS'), ('L_N_E', 'L_S_N'): ('NE : SN')}
    direction_in_legend = []
    legend_markers = []
    legend_directions = []

    fig, ax = plt.subplots()

    for sequence in occlusion_constants.SEGMENT_SEQUENCES:
        centreline = get_centreline_from_segment_sequence(sequence[0])
        x_points = [i[0] for i in centreline]
        y_points = [i[1] for i in centreline]
        ax.plot(x_points,y_points, 'black', alpha=0.5, linewidth=0.2)

    directions = []
    occluding_vehicle_positions = []
    with open('Occlusion-Caused Collisions.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for occlusion_caused_collision in reader:
        
            current_file_id = occlusion_caused_collision['Database']
            occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
            NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
            occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
            occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

            if int(occlusion_caused_collision['NE_Scenario']) == 1:
                conn_occ_db = sqlite3.connect(NE_OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            else:
                conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            execute_cmd = f"SELECT TIME FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            current_time = query_result[0][0]
            occluding_vehicle_id = int(occlusion_caused_collision['Occluding_Vehicle'])

            occ_v = setup_any_vehicle(occluding_vehicle_id, current_time, cur_occ_db)
            occluding_vehicle_positions.append((occ_v.x, occ_v.y))

            if occlusion_caused_collision['Second_Occluding_Vehicle'] != 'None':
                occluding_vehicle_id = int(occlusion_caused_collision['Second_Occluding_Vehicle'])
                v2 = setup_any_vehicle(occluding_vehicle_id, current_time, cur_occ_db)
                occluding_vehicle_positions.append((v2.x, v2.y))

            execute_cmd = f"SELECT FACTORED_GAME_COLLISIONS FROM COLLISIONS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            collisions_for_maneuver_combo = ast.literal_eval(query_result[0][0])
            collisions = collisions_for_maneuver_combo[list(collisions_for_maneuver_combo.keys())[0]]
            colliding_vehicle_a_id = collisions[0][0]
            colliding_vehicle_b_id = collisions[0][1]

            v_a = setup_any_vehicle(colliding_vehicle_a_id, current_time, cur_occ_db)
            v_b = setup_any_vehicle(colliding_vehicle_b_id, current_time, cur_occ_db)

            colour = direction_colours[(v_a.direction, v_b.direction)]
            marker = direction_markers[(v_a.direction, v_b.direction)]
            plt_marker, = plt.plot(occ_v.x, occ_v.y, color=colour, marker=marker, linestyle="None", ms=7, mec='k', mew=1)
            
            if (v_a.direction, v_b.direction) not in direction_in_legend:
                direction_in_legend.append((v_a.direction, v_b.direction))
                legend_markers.append(plt_marker)
                d = vehicle_direction_to_direction[(v_a.direction, v_b.direction)]
                legend_directions.append(d)


            if occlusion_caused_collision['Second_Occluding_Vehicle'] != 'None':
                plt.plot(v2.x, v2.y, color=colour, marker=marker, linestyle="None", ms=7, mec='k', mew=1)
            print(f"Finished with scenario: {occlusion_caused_collision['Scenario_Key']}")


    plt.legend(legend_markers, legend_directions, title='Conflict Directions')
    plt.title("Occluding Vehicle Positions")
    fig.savefig('Occluding Vehicle Positions.pdf', format='pdf')



def generate_follower_distance_distribution_for_conference_paper():
    following_distances = []
    with open('Occlusion-Caused Collisions.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for occlusion_caused_collision in reader:

            current_file_id = occlusion_caused_collision['Database']
            occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
            occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
            occlusion_constants.CURRENT_FILE_ID = str(current_file_id)
            NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

            if int(occlusion_caused_collision['NE_Scenario']) == 1:
                conn_occ_db = sqlite3.connect(NE_OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            else:
                conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            execute_cmd = f"SELECT TIME FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            current_time = query_result[0][0]
            occluding_vehicle_id = int(occlusion_caused_collision['Occluding_Vehicle'])

            occ_v = setup_any_vehicle(occluding_vehicle_id, current_time, cur_occ_db)

            # Setup follower vehicle if it exists
            if occlusion_caused_collision['Follower_Vehicle'] != 'None':
                follower_vehicle_id = int(occlusion_caused_collision['Follower_Vehicle'])

                follower_v = setup_any_vehicle(follower_vehicle_id, current_time, cur_occ_db)

                following_distance_1 = compute_distance((occ_v.x,occ_v.y), (follower_v.x, follower_v.y))

                following_distances.append(following_distance_1)

            if occlusion_caused_collision['Second_Occluding_Vehicle'] != "None" and occlusion_caused_collision['Second_Follower_Vehicle'] != 'None':

                occluding_vehicle_2_id = int(occlusion_caused_collision['Second_Occluding_Vehicle'])
                occ_v_2 = setup_any_vehicle(occluding_vehicle_2_id, current_time, cur_occ_db)

                follower_2_vehicle_id = int(occlusion_caused_collision['Second_Follower_Vehicle'])
                follower_v_2 = setup_any_vehicle(follower_2_vehicle_id, current_time, cur_occ_db)

                following_distance_2 = compute_distance((occ_v_2.x, occ_v_2.y), (follower_v_2.x, follower_v_2.y))

                following_distances.append(following_distance_2)

    fig, ax = plt.subplots()
    plt.hist(following_distances, bins=100, edgecolor = "black", color = 'lightblue')
    plt.xlabel("Lead-Follower Distance (m)")
    plt.ylabel("Count")
    # plt.show()
    fig.savefig('lead_follow_distances', format='pdf')


def generate_severity_distribution_for_conference_paper():
    severity_distribution = []

    severity_distribution_split = {('L_E_S', 'L_W_E'): [], ('L_S_W', 'L_N_S'): [], ('L_W_N', 'L_E_W'): [], ('L_S_E', 'L_W_E'): [], ('L_W_S', 'L_N_S'): [], ('L_N_E', 'L_S_N'): []}

    with open('Occlusion-Caused Collisions.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for occlusion_caused_collision in reader:
        
            current_file_id = occlusion_caused_collision['Database']
            occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
            occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
            occlusion_constants.CURRENT_FILE_ID = str(current_file_id)
            NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

            if int(occlusion_caused_collision['NE_Scenario']) == 1:
                conn_occ_db = sqlite3.connect(NE_OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            else:
                conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            execute_cmd = f"SELECT TIME FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            current_time = query_result[0][0]

            execute_cmd = f"SELECT FACTORED_GAME_COLLISIONS FROM COLLISIONS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            collisions_for_maneuver_combo = ast.literal_eval(query_result[0][0])
            collisions = collisions_for_maneuver_combo[list(collisions_for_maneuver_combo.keys())[0]]

            if occlusion_caused_collision['Collision_Configuration'] == 'Front-to-Front':

                v_impact = collisions[0][8] + collisions[0][7]

            elif occlusion_caused_collision['Collision_Configuration'] == 'Front-to-Rear':

                v_impact = abs(collisions[0][8] - collisions[0][7])

            elif occlusion_caused_collision['Collision_Configuration'] == 'Angle':

                v_impact = max(collisions[0][7], collisions[0][8])

            elif occlusion_caused_collision['Collision_Configuration'] == 'Sideswipe':

                v_impact = collisions[0][8] + collisions[0][7]

            colliding_vehicle_a_id = collisions[0][0]
            colliding_vehicle_b_id = collisions[0][1]

            v_a = setup_any_vehicle(colliding_vehicle_a_id, current_time, cur_occ_db)
            v_b = setup_any_vehicle(colliding_vehicle_b_id, current_time, cur_occ_db)

            severity_class = calculate_severity_class(v_impact)
            severity_distribution.append(severity_class)

            severity_distribution_split[(v_a.direction, v_b.direction)].append(severity_class)

    fig, ax = plt.subplots()
    plt.hist(severity_distribution, edgecolor = "black", color = 'lightblue')
    plt.xlabel("Severity Class")
    plt.ylabel("Count")
    plt.title("Severity Distribution")
    fig.savefig('Severity Distribution.pdf', format='pdf')



def generate_max_impact_versus_colliding_vehicle_distance_for_conference_paper():
    colliding_vehicle_initial_distances = []
    max_collision_impact_list = []
    with open('Occlusion-Caused Collisions.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for occlusion_caused_collision in reader:

            current_file_id = occlusion_caused_collision['Database']
            occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
            occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
            occlusion_constants.CURRENT_FILE_ID = str(current_file_id)
            NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

            if int(occlusion_caused_collision['NE_Scenario']) == 1:
                conn_occ_db = sqlite3.connect(NE_OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            else:
                conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            execute_cmd = f"SELECT TIME FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            current_time = query_result[0][0]

            execute_cmd = f"SELECT FACTORED_GAME_COLLISIONS FROM COLLISIONS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            collisions_for_maneuver_combo = ast.literal_eval(query_result[0][0])
            collisions = collisions_for_maneuver_combo[list(collisions_for_maneuver_combo.keys())[0]]

            colliding_vehicle_a_id = collisions[0][0]
            colliding_vehicle_b_id = collisions[0][1]

            v_a = setup_any_vehicle(colliding_vehicle_a_id, current_time, cur_occ_db)
            v_b = setup_any_vehicle(colliding_vehicle_b_id, current_time, cur_occ_db)

            initial_distance = compute_distance((v_a.x, v_a.y), (v_b.x, v_b.y))
            colliding_vehicle_initial_distances.append(initial_distance)

            max_collision_impact = max(collisions[0][7], collisions[0][8])
            max_collision_impact_list.append(max_collision_impact)

    fig, ax = plt.subplots()
    plt.plot(colliding_vehicle_initial_distances, max_collision_impact_list, 'ro')
    plt.xlabel("Colliding Vehicles Initial Distance (m)")
    plt.ylabel("Maximum Impact (m/s)")
    # plt.show()
    fig.savefig('Colliding Vehicles Initial Distance vs. Maximum Impact.png')


def save_scenario_collision_example_for_conference_paper(current_file_id, scenario_key):

    current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = retrieve_all_maneuvers(current_file_id, scenario_key)

    subject_vehicle_id = ordered_vehicle_ids[0]
    occluding_vehicle_id = ordered_vehicle_ids[1]
    relevant_vehicle_ids = ordered_vehicle_ids[2:]

    scenario = setup_scenario(current_file_id, current_time, subject_vehicle_id, relevant_vehicle_ids, occluding_vehicle_id=occluding_vehicle_id)

    subject_vehicle = scenario[0]
    occluding_vehicle = scenario[1]
    relevant_vehicles = scenario[2:]
    other_vehicles = [occluding_vehicle] + relevant_vehicles
    
    # Plot occlusion situation (I use "situation" and "scenario" interchangeably in this project)
    occ_map = occupancy_map.OccupancyMap()
    occ_map.make_occupancy_map(scenario, pass_bb_box_back=False)
    occ_map.visualize_occupancy_map_for_conference_paper2(subject_vehicle=subject_vehicle, relevant_vehicles=relevant_vehicles, occluding_vehicle=occluding_vehicle, filename=f"Occlusion Scenario {current_file_id} {scenario_key} Initial State.pdf", title="Occlusion Scenario Initial State")


    with open('Occlusion-Caused Collisions.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for occlusion_caused_collision in reader:
            if int(occlusion_caused_collision['Scenario_Key']) == scenario_key:
                break

    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

    conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
    cur_occ_db = conn_occ_db.cursor()

    execute_cmd = f"SELECT TIME FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    current_time = query_result[0][0]

    execute_cmd = f"SELECT FACTORED_GAME_COLLISIONS FROM COLLISIONS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    collisions_for_maneuver_combo = ast.literal_eval(query_result[0][0])
    collision_identifier_coded = ast.literal_eval(occlusion_caused_collision['Collision_Identifier'])

    collisions = collisions_for_maneuver_combo[collision_identifier_coded]
    colliding_vehicle_a_id = collisions[0][0]
    colliding_vehicle_b_id = collisions[0][1]
    collision_time_index = int(collisions[0][-1] * 10)

    execute_cmd = f"SELECT FACTORED_GAME_EMERGENCY_BRAKING_TRAJS FROM COLLISIONS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    emergency_braking_trajs = ast.literal_eval(query_result[0][0])

    traj_key = list(emergency_braking_trajs[collision_identifier_coded].keys())[0]

    emergency_braking_trajs = emergency_braking_trajs[collision_identifier_coded][traj_key]

    colliding_vehicle_a_traj = emergency_braking_trajs[0]
    colliding_vehicle_b_traj = emergency_braking_trajs[1]


    collision_identifier = []
    for id_maneuver in collision_identifier_coded:
        collision_identifier.append((id_maneuver[0], occlusion_constants.L1_MANEUVERS[id_maneuver[1]]))
    collision_identifier = tuple(collision_identifier)

    all_maneuver_to_trajectory_dict = game_utilities.compute_risk_metric(current_file_id, start_scenario_key=scenario_key, end_scenario_key=scenario_key+1, return_factored_game_info=True)
    current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = retrieve_all_maneuvers(current_file_id, scenario_key)
    trajectories_dict = retrieve_trajectories(current_file_id, scenario_key, ordered_vehicle_ids, vehicle_maneuvers)

    selected_trajectories = {}

    maneuver_to_trajectory_dict = all_maneuver_to_trajectory_dict[collision_identifier]

    selected_trajectories[colliding_vehicle_a_id] = colliding_vehicle_a_traj
    selected_trajectories[colliding_vehicle_b_id] = colliding_vehicle_b_traj

    for id_maneuver, id_index in maneuver_to_trajectory_dict.items():
        maneuver = id_maneuver[1]
        index = id_index[1]

        track_id = id_maneuver[0]
        if track_id != colliding_vehicle_a_id and track_id != colliding_vehicle_b_id:
            selected_trajectories[track_id] = trajectories_dict[track_id][maneuver][index]

    vehicles_at_collision_time = []

    subject_vehicle = None
    relevant_vehicles = []
    occluding_vehicle = None


    for track_id in ordered_vehicle_ids:
        v = VehicleState()
        timestep = selected_trajectories[track_id][collision_time_index]

        v.set_id(track_id)
        v.set_x(timestep[1])
        v.set_y(timestep[2])
        v.set_angle(timestep[-2])

        vehicles_at_collision_time.append(v)

        if track_id == ordered_vehicle_ids[0]:
            subject_vehicle = v  
        elif track_id == track_id == ordered_vehicle_ids[1]:
            occluding_vehicle = v
        else:
            relevant_vehicles.append(v)

    occ_map = occupancy_map.OccupancyMap()
    occ_map.make_occupancy_map(vehicles_at_collision_time, pass_bb_box_back=False)
    occ_map.visualize_occupancy_map_for_conference_paper2(subject_vehicle=subject_vehicle, relevant_vehicles=relevant_vehicles, occluding_vehicle=occluding_vehicle, filename=f"Occlusion Scenario {current_file_id} {scenario_key} Collision State.pdf", title="Occlusion Scenario Collision State", crash=True)


def save_scenario_figure_for_conference_paper(current_file_id, scenario_key):

    current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = retrieve_all_maneuvers(current_file_id, scenario_key)

    subject_vehicle_id = ordered_vehicle_ids[0]
    occluding_vehicle_id = ordered_vehicle_ids[1]
    relevant_vehicle_ids = ordered_vehicle_ids[2:]

    scenario = setup_scenario(current_file_id, current_time, subject_vehicle_id, relevant_vehicle_ids, occluding_vehicle_id=occluding_vehicle_id)

    subject_vehicle = scenario[0]
    occluding_vehicle = scenario[1]
    relevant_vehicles = scenario[2:]
    other_vehicles = [occluding_vehicle] + relevant_vehicles
    
    # Plot occlusion situation
    occ_map = occupancy_map.OccupancyMap()
    occ_map.make_occupancy_map(scenario, pass_bb_box_back=False)
    occ_map.visualize_occupancy_map_for_conference_paper(subject_vehicle=subject_vehicle, relevant_vehicles=relevant_vehicles, occluding_vehicle=occluding_vehicle, filename=f'occlusion_scenario_{current_file_id}_{scenario_key}')

    # Plot base situation
    base_scenario = [subject_vehicle] + relevant_vehicles
    occ_map = occupancy_map.OccupancyMap()
    occ_map.make_occupancy_map(base_scenario, pass_bb_box_back=False)
    occ_map.visualize_occupancy_map_for_conference_paper(subject_vehicle=subject_vehicle, relevant_vehicles=relevant_vehicles, occluding_vehicle=None, filename=f'base_scenario_{current_file_id}_{scenario_key}')

    # Computing attention values for each vehicle in the scenario excluding all out of bounds vehicles
    relevant_vehicle_attention = {}
    if len(relevant_vehicles) > 1:
        minimum_distance_to_sv = math.inf
        # Stores relevant vehicle distances to the subject vehicle
        relevant_vehicle_distances = {}
        distance_summed_to_subject_vehicle = 0
        relevant_vehicle_distances_order = []
        for v in relevant_vehicles:
            # Here we only compute attention for those vehicles in the map
            dist_to_sv = compute_distance((subject_vehicle.x, subject_vehicle.y), (v.x, v.y))
            relevant_vehicle_distances[v.id] = dist_to_sv
            distance_summed_to_subject_vehicle += dist_to_sv
            if dist_to_sv < minimum_distance_to_sv:
                minimum_distance_to_sv = dist_to_sv
   
        # Catch if all relevant agents are too far away
        if minimum_distance_to_sv >= occlusion_constants.MAXIMUM_DISTANCE_FROM_SUBJECT_VEHICLE:
            raise Exception(f"Closest relevant vehicle is farther away than the maximum distance, {occlusion_constants.MAXIMUM_DISTANCE_FROM_SUBJECT_VEHICLE}, to the subject vehicle.")

        attention_total = 0
        for id, distance in relevant_vehicle_distances.items():
            attention = (distance_summed_to_subject_vehicle - distance) / distance_summed_to_subject_vehicle
            attention_total += attention
            relevant_vehicle_attention[id] = attention

        # Normalize attention values
        for id, attention in relevant_vehicle_attention.items():
            relevant_vehicle_attention[id] = relevant_vehicle_attention[id] / attention_total
    else:
        relevant_vehicle_attention[relevant_vehicles[0].id] = 1

    subject_vehicle_raycasts = []
    raycast_polygon_points = []

    for v in relevant_vehicles:
        subject_vehicle_raycast, right_point_index, left_point_index = occlusion_scenario_builder.compute_raycast(subject_vehicle, v, relevant_vehicle_attention[v.id])
        raycast_polygon_points.append([subject_vehicle_raycast[right_point_index][-1], subject_vehicle_raycast[left_point_index][-1]])
        subject_vehicle_raycasts += subject_vehicle_raycast

    base_scenario = [subject_vehicle] + relevant_vehicles
    occ_map = occupancy_map.OccupancyMap()
    occ_map.make_occupancy_map(base_scenario, pass_bb_box_back=False)
    occ_map.add_raycast(subject_vehicle_raycasts, subject_vehicle.id)

    occ_map.visualize_occupancy_map_for_conference_paper(subject_vehicle=subject_vehicle, relevant_vehicles=relevant_vehicles, occluding_vehicle=None, filename=f'Partial Scenario {current_file_id} {scenario_key} With Raycasts.pdf')

    occlusion_scenario = [subject_vehicle, occluding_vehicle] + relevant_vehicles
    occ_map = occupancy_map.OccupancyMap()
    occ_map.make_occupancy_map(occlusion_scenario, pass_bb_box_back=False)
    occ_map.add_raycast(subject_vehicle_raycasts, subject_vehicle.id)

    occ_map.visualize_occupancy_map_for_conference_paper(subject_vehicle=subject_vehicle, relevant_vehicles=relevant_vehicles, occluding_vehicle=occluding_vehicle, filename=f'Occlusion Scenario {current_file_id} {scenario_key} With Raycasts.pdf')

def compute_time_to_collison_after_occlusion_ends_for_conference_paper():
    time_to_collision_list = []
    with open('Occlusion-Caused Collisions.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for occlusion_caused_collision in reader:
            current_file_id = occlusion_caused_collision['Database']
            scenario_key = int(occlusion_caused_collision['Scenario_Key'])
            print(current_file_id)
            print(scenario_key)

            occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
            occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
            occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

            NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

            if int(occlusion_caused_collision['NE_Scenario']) == 1:
                conn_occ_db = sqlite3.connect(NE_OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            else:
                conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            collision_identifier_coded = ast.literal_eval(occlusion_caused_collision['Collision_Identifier'])

            execute_cmd = f"SELECT FACTORED_GAME_COLLISIONS FROM COLLISIONS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            collisions_for_maneuver_combo = ast.literal_eval(query_result[0][0])
            collisions = collisions_for_maneuver_combo[collision_identifier_coded]
            
            collision_time = collisions[0][-1]

            occlusion_duration = occlusion_duration_times[(current_file_id, scenario_key)]['both_unnocluded']
            time_to_collision = collision_time - occlusion_duration

            time_to_collision_list.append(time_to_collision)

    
    fig, ax = plt.subplots()
    plt.hist(time_to_collision_list, bins=100, edgecolor = "black", color = 'lightblue')
    plt.xlabel("Time to Collision (s)")
    plt.ylabel("Count")
    plt.title("Time to Collision After Occlusion Ends")
    fig.savefig('Time to Collision After Occlusion.pdf', format='pdf')


def compute_occlusion_durations_for_conference_paper():
    occlusion_timings = {}

    with open('Occlusion-Caused Collisions.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for occlusion_caused_collision in reader:

            print(f"OCCLUSION TIMINGS: {occlusion_timings}")
            current_file_id = occlusion_caused_collision['Database']
            scenario_key = int(occlusion_caused_collision['Scenario_Key'])
            occlusion_timings[(current_file_id, scenario_key)] = {}

            occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
            occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
            occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

            NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

            if int(occlusion_caused_collision['NE_Scenario']) == 1:
                conn_occ_db = sqlite3.connect(NE_OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            else:
                conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            execute_cmd = f"SELECT TIME FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            current_time = query_result[0][0]

            execute_cmd = f"SELECT FACTORED_GAME_COLLISIONS FROM COLLISIONS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            collisions_for_maneuver_combo = ast.literal_eval(query_result[0][0])
            collision_identifier_coded = ast.literal_eval(occlusion_caused_collision['Collision_Identifier'])

            collisions = collisions_for_maneuver_combo[collision_identifier_coded]
            colliding_vehicle_a_id = collisions[0][0]
            colliding_vehicle_b_id = collisions[0][1]

            execute_cmd = f"SELECT FACTORED_GAME_EMERGENCY_BRAKING_TRAJS FROM COLLISIONS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            emergency_braking_trajs = ast.literal_eval(query_result[0][0])

            traj_key = list(emergency_braking_trajs[collision_identifier_coded].keys())[0]
      
            emergency_braking_trajs = emergency_braking_trajs[collision_identifier_coded][traj_key]

            colliding_vehicle_a_traj = emergency_braking_trajs[0]
            colliding_vehicle_b_traj = emergency_braking_trajs[1]

            collision_identifier = []
            for id_maneuver in collision_identifier_coded:
                collision_identifier.append((id_maneuver[0], occlusion_constants.L1_MANEUVERS[id_maneuver[1]]))
            collision_identifier = tuple(collision_identifier)

            if int(occlusion_caused_collision['NE_Scenario']) == 1:
                all_maneuver_to_trajectory_dict = regenerate_NE_scenarios.compute_NE_risk_metric(current_file_id, start_scenario_key=scenario_key, end_scenario_key=scenario_key+1, return_factored_game_info=True)
                current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = regenerate_NE_scenarios.retrieve_NE_all_maneuvers(current_file_id, scenario_key)
                trajectories_dict = regenerate_NE_scenarios.retrieve_NE_trajectories(current_file_id, scenario_key, ordered_vehicle_ids, vehicle_maneuvers)
            else:
                all_maneuver_to_trajectory_dict = game_utilities.compute_risk_metric(current_file_id, start_scenario_key=scenario_key, end_scenario_key=scenario_key+1, return_factored_game_info=True)
                current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = retrieve_all_maneuvers(current_file_id, scenario_key)
                trajectories_dict = retrieve_trajectories(current_file_id, scenario_key, ordered_vehicle_ids, vehicle_maneuvers)

            selected_trajectories = {}

            maneuver_to_trajectory_dict = all_maneuver_to_trajectory_dict[collision_identifier]

            selected_trajectories[colliding_vehicle_a_id] = colliding_vehicle_a_traj
            selected_trajectories[colliding_vehicle_b_id] = colliding_vehicle_b_traj

            for id_maneuver, id_index in maneuver_to_trajectory_dict.items():
                maneuver = id_maneuver[1]
                index = id_index[1]

                track_id = id_maneuver[0]
                if track_id != colliding_vehicle_a_id and track_id != colliding_vehicle_b_id:
                    selected_trajectories[track_id] = trajectories_dict[track_id][maneuver][index]

                    reference_trajectory = trajectories_dict[track_id][maneuver][index]

            scenario = []
            for track_id in ordered_vehicle_ids:
                v = VehicleState()
                v.set_id(track_id)
                scenario.append(v)

            track_id_a = colliding_vehicle_a_id
            track_id_b = colliding_vehicle_b_id

            # Check occlusions
            for i in range(len(emergency_braking_trajs[0])):
                time = reference_trajectory[i][0]
                selected_trajectories[track_id]

                # Update vehicle states for new time
                for v in scenario:
                    trajectory = selected_trajectories[v.id]
                    timestep = trajectory[i]
                    v.set_x(timestep[1])
                    v.set_y(timestep[2])
                    angle = angle_check(timestep[-2])
          
                    v.set_angle(angle)

                # Compute updated factored game for vehicle a
                other_vehicles_a = []
                for v in scenario:
                    if v.id != track_id_a:
                        other_vehicles_a.append(v)
                    else:
                        subject_vehicle_a = v
                hit_counts_a = occlusion_scenario_builder.compute_hit_counts(subject_vehicle_a, other_vehicles_a)
                factored_game_a = game_utilities.compute_factored_game(hit_counts_a)

                # Compute updated factored game for vehicle b
                other_vehicles_b = []
                for v in scenario:
                    if v.id != track_id_b:
                        other_vehicles_b.append(v)
                    else:
                        subject_vehicle_b = v
                hit_counts_b = occlusion_scenario_builder.compute_hit_counts(subject_vehicle_b, other_vehicles_b)
                factored_game_b = game_utilities.compute_factored_game(hit_counts_b)

                if track_id_a not in factored_game_b and track_id_b not in factored_game_a:
                    occlusion_timings[(current_file_id, scenario_key)]['both_unnocluded'] = time
                    break

                elif track_id_a not in factored_game_b:
                    if 'one_unnocluded' not in occlusion_timings[(current_file_id, scenario_key)]:
                        occlusion_timings[(current_file_id, scenario_key)]['one_unnocluded'] = time

                elif track_id_b not in factored_game_a:
                    if 'one_unnocluded' not in occlusion_timings[(current_file_id, scenario_key)]:
                        occlusion_timings[(current_file_id, scenario_key)]['one_unnocluded'] = time

                elif i == len(emergency_braking_trajs[0]) - 1:
                    raise Exception(f"This means that both colliding vehicles never see each other which should never happen.")


    print(f"\n")
    print(f"FINAL OCCLUSION TIMINGS: {occlusion_timings}")
    print(f"\n")


# To plot occlusion durations 
def plot_occlusion_durations_for_conference_paper():

    one_unnocluded_durations = []
    both_unnocluded_durations = []

    for db_scenario_key, timings in occlusion_duration_times.items():

      for timing_type, timing in timings.items():
          if timing_type == 'one_unnocluded':
              one_unnocluded_durations.append(timing)

          elif timing_type == 'both_unnocluded':
              both_unnocluded_durations.append(timing)

    fig, ax = plt.subplots()
    plt.hist(one_unnocluded_durations, bins=100, edgecolor = "black", color = 'lightblue')
    plt.xlabel("Occlusion Duration (s)")
    plt.ylabel("Count")
    plt.title("Occlusion Duration - One Colliding Vehicle Occluded")
    fig.savefig('Occlusion Duration - One Colliding Vehicle Occluded', format='pdf')

    fig, ax = plt.subplots()
    plt.hist(both_unnocluded_durations, bins=100, edgecolor = "black", color = 'lightblue')
    plt.xlabel("Occlusion Duration (s)")
    plt.ylabel("Count")
    plt.title("Occlusion Duration - Both Colliding Vehicles Occluded")
    fig.savefig('Occlusion Duration - Both Colliding Vehicles Occluded', format='pdf')


def occlusion_duration_by_situation_category():

    both_tag_on_duration = []
    left_tag_on_duration = []
    straight_tag_on_duration = []
    reveal_duration = []

    with open('Occlusion-Caused Collisions.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for occlusion_caused_collision in reader:

            # Run to debug what the issue is for NE situations
            current_file_id = occlusion_caused_collision['Database']
            scenario_key = int(occlusion_caused_collision['Scenario_Key'])

            occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
            occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
            occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

            NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

            if int(occlusion_caused_collision['NE_Scenario']) == 1:
                conn_occ_db = sqlite3.connect(NE_OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            else:
                conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
                cur_occ_db = conn_occ_db.cursor()


            timings = occlusion_duration_times[(current_file_id, scenario_key)]
            occlusion_duration = timings['both_unnocluded']


            if occlusion_caused_collision['Taxonomy'] == 'Both_tag_on':
                both_tag_on_duration.append(occlusion_duration)

            elif occlusion_caused_collision['Taxonomy'] == 'Left_turn_tag_on':
                left_tag_on_duration.append(occlusion_duration)

            elif occlusion_caused_collision['Taxonomy'] == 'Straight_tag_on':
               straight_tag_on_duration.append(occlusion_duration)

            elif occlusion_caused_collision['Taxonomy'] == 'Reveal':
                reveal_duration.append(occlusion_duration)

    print(f"MEAN: BOTH TAG ON: {np.mean(both_tag_on_duration)} + {np.var(both_tag_on_duration)} ; MAX: {max(both_tag_on_duration)}, MIN: {min(both_tag_on_duration)}")
    print(f"MEAN: LEFT TAG ON: {np.mean(left_tag_on_duration)} + {np.var(left_tag_on_duration)} ; MAX: {max(left_tag_on_duration)}, MINP: {min(left_tag_on_duration)}")
    print(f"MEAN: STRAIGHT TAG ON: {np.mean(straight_tag_on_duration)} + {np.var(straight_tag_on_duration)} ; MAX: {max(straight_tag_on_duration)}, MIN: {min(straight_tag_on_duration)}")
    print(f"MEAN: REVEAL: {np.mean(reveal_duration)} + {np.var(reveal_duration)} ; MAX: {max(reveal_duration)}, MIN: {min(reveal_duration)}")



def occlusion_duration_by_crash_configuration():

    front_to_front_duration = []
    angle_duration = []
    sideswipe_duration = []
    front_to_rear_duration = []

    with open('Occlusion-Caused Collisions.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for occlusion_caused_collision in reader:

            # Run to debug what the issue is for NE situations
            current_file_id = occlusion_caused_collision['Database']
            scenario_key = int(occlusion_caused_collision['Scenario_Key'])

            occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
            occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
            occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

            NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

            if int(occlusion_caused_collision['NE_Scenario']) == 1:
                conn_occ_db = sqlite3.connect(NE_OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            else:
                conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
                cur_occ_db = conn_occ_db.cursor()


            timings = occlusion_duration_times[(current_file_id, scenario_key)]
            occlusion_duration = timings['both_unnocluded']

            if occlusion_caused_collision['Collision_Configuration'] == 'Front-to-Front':
                front_to_front_duration.append(occlusion_duration)

            elif occlusion_caused_collision['Collision_Configuration'] == 'Angle':
                angle_duration.append(occlusion_duration)

            elif occlusion_caused_collision['Collision_Configuration'] == 'Sideswipe':
               sideswipe_duration.append(occlusion_duration)

            elif occlusion_caused_collision['Collision_Configuration'] == 'Front-to-Rear':
                front_to_rear_duration.append(occlusion_duration)

    print(f"MEAN: FRONT-TO-FRONT: {np.mean(front_to_front_duration)} + {np.var(front_to_front_duration)} ; MAX: {max(front_to_front_duration)}, MIN: {min(front_to_front_duration)}")
    print(f"MEAN: ANGLE: {np.mean(angle_duration)} + {np.var(angle_duration)} ; MAX: {max(angle_duration)}, MINP: {min(angle_duration)}")
    print(f"MEAN: SIDESWIPE: {np.mean(sideswipe_duration)} + {np.var(sideswipe_duration)} ; MAX: {max(sideswipe_duration)}, MIN: {min(sideswipe_duration)}")
    print(f"MEAN: FRONT-TO-REAR: {np.mean(front_to_rear_duration)} + {np.var(front_to_rear_duration)} ; MAX: {max(front_to_rear_duration)}, MIN: {min(front_to_rear_duration)}")





def visualize_occlusion_scenario(current_file_id, scenario_key):

    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

    conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
    cur_occ_db = conn_occ_db.cursor()

    conn_db = sqlite3.connect(occlusion_constants.DB)
    cur_db = conn_db.cursor()


    current_time = 229.229
    t = get_all_vehicles_at_current_time(current_time)
    for v in t:
        
        if v.id == 220:
            print(current_time)
            print(v.id)
            relevant_vehicles = get_relevant_vehicles(v)

            print(v.current_segment)
            print(relevant_vehicles)
        

            reduced_relevant_vehicles_ids = reduce_relev_agents(v.id, current_time, relevant_vehicles)
            print(reduced_relevant_vehicles_ids)




def plot_asymmetric_occlusion():
    subject_vehicle = VehicleState()
    subject_vehicle.set_id(1)
    subject_vehicle.set_x(538837.5)
    subject_vehicle.set_y(4814004.5)
    subject_vehicle.set_angle(2.46554846095504)

    occluding_vehicle = VehicleState()
    occluding_vehicle.set_id(2)
    occluding_vehicle.set_x(538838.679878051)
    occluding_vehicle.set_y(4813998.7)
    occluding_vehicle.set_angle(5.51417151606882)

    relevant_vehicle = VehicleState()
    relevant_vehicle.set_id(3)
    relevant_vehicle.set_x(538836.776819228)
    relevant_vehicle.set_y(4813992.76076176)
    relevant_vehicle.set_angle(5.63869724709955)

    scenario = [subject_vehicle, occluding_vehicle, relevant_vehicle]
    relevant_vehicles = [occluding_vehicle, relevant_vehicle]

    subject_vehicle_raycasts = []
    raycast_polygon_points = []

    subject_vehicle_raycast, right_point_index, left_point_index = occlusion_scenario_builder.compute_raycast(subject_vehicle, relevant_vehicle, 1.0)
    raycast_polygon_points.append([subject_vehicle_raycast[right_point_index][-1], subject_vehicle_raycast[left_point_index][-1]])
    subject_vehicle_raycasts += subject_vehicle_raycast

    # Plot occlusion situation
    occ_map = occupancy_map.OccupancyMap()
    occ_map.make_occupancy_map(scenario, pass_bb_box_back=False)
    occ_map.add_raycast(subject_vehicle_raycasts, subject_vehicle.id)
    occ_map.visualize_occupancy_map_for_asymmetric_occlusion(subject_vehicle, relevant_vehicles, filename="asymmetric_occlusion_1", title=None)


    subject_vehicle_raycasts = []
    raycast_polygon_points = []

    subject_vehicle_raycast, right_point_index, left_point_index = occlusion_scenario_builder.compute_raycast(relevant_vehicle, subject_vehicle, 1.0)
    raycast_polygon_points.append([subject_vehicle_raycast[right_point_index][-1], subject_vehicle_raycast[left_point_index][-1]])
    subject_vehicle_raycasts += subject_vehicle_raycast


    # Plot occlusion situation
    occ_map = occupancy_map.OccupancyMap()
    occ_map.make_occupancy_map(scenario, pass_bb_box_back=False)
    occ_map.add_raycast(subject_vehicle_raycasts,relevant_vehicle.id)
    t = [subject_vehicle, occluding_vehicle]
    occ_map.visualize_occupancy_map_for_asymmetric_occlusion(relevant_vehicle, [subject_vehicle], occluding_vehicle=occluding_vehicle, filename="asymmetric_occlusion_2", title=None)



def severity_by_taxonomy():

    both_tag_on = []
    left_tag_on = []
    straight_tag_on = []
    reveal = []

    both_tag_on_class = []
    left_tag_on_class = []
    straight_tag_on_class = []
    reveal_class = []

    both_tag_on_dict = {}
    left_tag_on_dict = {}
    straight_tag_on_dict = {}
    reveal_dict = {}

    with open('Occlusion-Caused Collisions.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for occlusion_caused_collision in reader:

            current_file_id = occlusion_caused_collision['Database']
            scenario_key = int(occlusion_caused_collision['Scenario_Key'])

            occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
            occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
            occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

            NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

            if int(occlusion_caused_collision['NE_Scenario']) == 1:
                conn_occ_db = sqlite3.connect(NE_OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            else:
                conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
                cur_occ_db = conn_occ_db.cursor()


            execute_cmd = f"SELECT TIME FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            current_time = query_result[0][0]

            execute_cmd = f"SELECT FACTORED_GAME_COLLISIONS FROM COLLISIONS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            collisions_for_maneuver_combo = ast.literal_eval(query_result[0][0])
            collisions = collisions_for_maneuver_combo[list(collisions_for_maneuver_combo.keys())[0]]

            if occlusion_caused_collision['Collision_Configuration'] == 'Front-to-Front':
                v_impact = collisions[0][8] + collisions[0][7]

            elif occlusion_caused_collision['Collision_Configuration'] == 'Front-to-Rear':
                v_impact = abs(collisions[0][8] - collisions[0][7])

            elif occlusion_caused_collision['Collision_Configuration'] == 'Angle':
                v_impact = max(collisions[0][7], collisions[0][8])

            elif occlusion_caused_collision['Collision_Configuration'] == 'Sideswipe':
                v_impact = collisions[0][8] + collisions[0][7]

            colliding_vehicle_a_id = collisions[0][0]
            colliding_vehicle_b_id = collisions[0][1]

            severity_class = calculate_severity_class(v_impact)

            if occlusion_caused_collision['Taxonomy'] == 'Both_tag_on':
                both_tag_on.append(v_impact)
                both_tag_on_class.append(severity_class)

                if occlusion_caused_collision['Collision_Configuration'] in both_tag_on_dict:
                    both_tag_on_dict[occlusion_caused_collision['Collision_Configuration']] += 1
                else:
                    both_tag_on_dict[occlusion_caused_collision['Collision_Configuration']] = 1    

            elif occlusion_caused_collision['Taxonomy'] == 'Left_turn_tag_on':
                left_tag_on.append(v_impact)
                left_tag_on_class.append(severity_class)

                if occlusion_caused_collision['Collision_Configuration'] in left_tag_on_dict:
                    left_tag_on_dict[occlusion_caused_collision['Collision_Configuration']] += 1
                else:
                    left_tag_on_dict[occlusion_caused_collision['Collision_Configuration']] = 1 

            elif occlusion_caused_collision['Taxonomy'] == 'Straight_tag_on':
                straight_tag_on.append(v_impact)
                straight_tag_on_class.append(severity_class)

                if occlusion_caused_collision['Collision_Configuration'] in straight_tag_on_dict:
                    straight_tag_on_dict[occlusion_caused_collision['Collision_Configuration']] += 1
                else:
                    straight_tag_on_dict[occlusion_caused_collision['Collision_Configuration']] = 1

            elif occlusion_caused_collision['Taxonomy'] == 'Reveal':
                reveal.append(v_impact)
                reveal_class.append(severity_class)

                if occlusion_caused_collision['Collision_Configuration'] in reveal_dict:
                    reveal_dict[occlusion_caused_collision['Collision_Configuration']] += 1
                else:
                    reveal_dict[occlusion_caused_collision['Collision_Configuration']] = 1


    print(f"MEAN: BOTH TAG ON: {np.mean(both_tag_on)} + {np.var(both_tag_on)} ; MAX: {max(both_tag_on)}, MIN: {min(both_tag_on)}")
    print(f"MEAN: LEFT TAG ON: {np.mean(left_tag_on)} + {np.var(left_tag_on)} ; MAX: {max(left_tag_on)}, MINP: {min(left_tag_on)}")
    print(f"MEAN: STRAIGHT TAG ON: {np.mean(straight_tag_on)} + {np.var(straight_tag_on)} ; MAX: {max(straight_tag_on)}, MIN: {min(straight_tag_on)}")
    print(f"MEAN: REVEAL: {np.mean(reveal)} + {np.var(reveal)} ; MAX: {max(reveal)}, MINP: {min(reveal)}")



def compute_occlusion_ending_order():
    occlusion_timings = {}

    with open('Occlusion-Caused Collisions.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for occlusion_caused_collision in reader:

            print(f"OCCLUSION TIMINGS: {occlusion_timings}")
            current_file_id = occlusion_caused_collision['Database']
            scenario_key = int(occlusion_caused_collision['Scenario_Key'])
            occlusion_timings[(current_file_id, scenario_key)] = {}

            occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
            occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
            occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

            NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

            

            if int(occlusion_caused_collision['NE_Scenario']) == 1:
                conn_occ_db = sqlite3.connect(NE_OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            else:
                conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            conn_db = sqlite3.connect(occlusion_constants.DB)
            cur_db = conn_occ_db.cursor()

            execute_cmd = f"SELECT TIME FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={scenario_key}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()

            current_time = query_result[0][0]

            execute_cmd = f"SELECT TIME FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            current_time = query_result[0][0]

            execute_cmd = f"SELECT FACTORED_GAME_COLLISIONS FROM COLLISIONS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            collisions_for_maneuver_combo = ast.literal_eval(query_result[0][0])
            collision_identifier_coded = ast.literal_eval(occlusion_caused_collision['Collision_Identifier'])

            collisions = collisions_for_maneuver_combo[collision_identifier_coded]
            colliding_vehicle_a_id = collisions[0][0]
            colliding_vehicle_b_id = collisions[0][1]

            execute_cmd = f"SELECT FACTORED_GAME_EMERGENCY_BRAKING_TRAJS FROM COLLISIONS WHERE SCENARIO_KEY={occlusion_caused_collision['Scenario_Key']}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            emergency_braking_trajs = ast.literal_eval(query_result[0][0])

            traj_key = list(emergency_braking_trajs[collision_identifier_coded].keys())[0]
      
            emergency_braking_trajs = emergency_braking_trajs[collision_identifier_coded][traj_key]

            colliding_vehicle_a_traj = emergency_braking_trajs[0]
            colliding_vehicle_b_traj = emergency_braking_trajs[1]

            collision_identifier = []
            for id_maneuver in collision_identifier_coded:
                collision_identifier.append((id_maneuver[0], occlusion_constants.L1_MANEUVERS[id_maneuver[1]]))
            collision_identifier = tuple(collision_identifier)

            if int(occlusion_caused_collision['NE_Scenario']) == 1:
                all_maneuver_to_trajectory_dict = regenerate_NE_scenarios.compute_NE_risk_metric(current_file_id, start_scenario_key=scenario_key, end_scenario_key=scenario_key+1, return_factored_game_info=True)
                current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = regenerate_NE_scenarios.retrieve_NE_all_maneuvers(current_file_id, scenario_key)
                trajectories_dict = regenerate_NE_scenarios.retrieve_NE_trajectories(current_file_id, scenario_key, ordered_vehicle_ids, vehicle_maneuvers)
            else:
                all_maneuver_to_trajectory_dict = game_utilities.compute_risk_metric(current_file_id, start_scenario_key=scenario_key, end_scenario_key=scenario_key+1, return_factored_game_info=True)
                current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = retrieve_all_maneuvers(current_file_id, scenario_key)
                trajectories_dict = retrieve_trajectories(current_file_id, scenario_key, ordered_vehicle_ids, vehicle_maneuvers)

            selected_trajectories = {}

            maneuver_to_trajectory_dict = all_maneuver_to_trajectory_dict[collision_identifier]

            selected_trajectories[colliding_vehicle_a_id] = colliding_vehicle_a_traj
            selected_trajectories[colliding_vehicle_b_id] = colliding_vehicle_b_traj

            for id_maneuver, id_index in maneuver_to_trajectory_dict.items():
                maneuver = id_maneuver[1]
                index = id_index[1]

                track_id = id_maneuver[0]
                if track_id != colliding_vehicle_a_id and track_id != colliding_vehicle_b_id:
                    selected_trajectories[track_id] = trajectories_dict[track_id][maneuver][index]

                    reference_trajectory = trajectories_dict[track_id][maneuver][index]

            scenario = []
            for track_id in ordered_vehicle_ids:
                v = VehicleState()
                v.set_id(track_id)
                scenario.append(v)

            track_id_a = colliding_vehicle_a_id
            track_id_b = colliding_vehicle_b_id
         
            # Retrieve left-turn, right-turn, or straight-through
            if track_id_a < 0:
                v_a = setup_any_vehicle(track_id_a, current_time, cur_occ_db)

            elif track_id_a >= 0:
                v_a = setup_any_vehicle(track_id_a, current_time, cur_db)

            if track_id_b < 0:
                v_b = setup_any_vehicle(track_id_b, current_time, cur_occ_db)

            elif track_id_b >= 0:
                v_b = setup_any_vehicle(track_id_b, current_time, cur_db)


            for segment_sequence, task in occlusion_constants.SEGMENT_SEQUENCES:
                task = occlusion_constants.TASK_MAP[task]
                if segment_sequence == v_a.segment_seq:
                    v_a_task = task
                if segment_sequence == v_b.segment_seq:
                    v_b_task = task


            # Check occlusions
            for i in range(len(emergency_braking_trajs[0])):
                time = reference_trajectory[i][0]
                selected_trajectories[track_id]

                # Update vehicle states for new time
                for v in scenario:
                    trajectory = selected_trajectories[v.id]
                    timestep = trajectory[i]
                    v.set_x(timestep[1])
                    v.set_y(timestep[2])
                    angle = angle_check(timestep[-2])
          
                    v.set_angle(angle)

                # Compute updated factored game for vehicle a
                other_vehicles_a = []
                for v in scenario:
                    if v.id != track_id_a:
                        other_vehicles_a.append(v)
                    else:
                        subject_vehicle_a = v
                hit_counts_a = occlusion_scenario_builder.compute_hit_counts(subject_vehicle_a, other_vehicles_a)
                factored_game_a = game_utilities.compute_factored_game(hit_counts_a)

                # Compute updated factored game for vehicle b
                other_vehicles_b = []
                for v in scenario:
                    if v.id != track_id_b:
                        other_vehicles_b.append(v)
                    else:
                        subject_vehicle_b = v
                hit_counts_b = occlusion_scenario_builder.compute_hit_counts(subject_vehicle_b, other_vehicles_b)
                factored_game_b = game_utilities.compute_factored_game(hit_counts_b)

                if track_id_a not in factored_game_b and track_id_b not in factored_game_a:
                    occlusion_timings[(current_file_id, scenario_key)]['both_unnocluded'] = time
                    break

                # From vehicle b's perspective, vehicle a is no longer occluded
                elif track_id_a not in factored_game_b:
                    if 'one_unnocluded' not in occlusion_timings[(current_file_id, scenario_key)]:
                        occlusion_timings[(current_file_id, scenario_key)]['one_unnocluded'] = v_b_task

                # From vehicle a's perspective, vehicle a is no longer occluded
                elif track_id_b not in factored_game_a:
                    if 'one_unnocluded' not in occlusion_timings[(current_file_id, scenario_key)]:
                        occlusion_timings[(current_file_id, scenario_key)]['one_unnocluded'] = v_a_task

                elif i == len(emergency_braking_trajs[0]) - 1:
                    raise Exception(f"This means that both colliding vehicles never see each other which should never happen.")


    print(f"\n")
    print(f"FINAL OCCLUSION TIMINGS: {occlusion_timings}")
    print(f"\n")


# Counting whether left-turn vehicle becomes unnocluded first or straight-through vehicle
# for reveal angle collisions
def unnocluded_task_count():

    both_front_to_front_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}
    left_turn_front_to_front_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}
    straight_front_to_front_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}
    reveal_front_to_front_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}

    both_angle_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}
    left_turn_angle_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}
    straight_angle_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}
    reveal_angle_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}

    both_sideswipe_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}
    left_turn_sideswipe_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}
    straight_sideswipe_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}
    reveal_sideswipe_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}  

    both_front_to_rear_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}
    left_turn_front_to_rear_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}
    straight_front_to_rear_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}
    reveal_front_to_rear_count  = {"Straight": 0, "Left-turn": 0, 'Right-turn': 0}



    with open('Occlusion-Caused Collisions.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for occlusion_caused_collision in reader:

            current_file_id = occlusion_caused_collision['Database']
            scenario_key = int(occlusion_caused_collision['Scenario_Key'])

            if 'one_unnocluded' not in straight_or_left_turn[(current_file_id, scenario_key)]:
                continue

            occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
            occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
            occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

            NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

            if int(occlusion_caused_collision['NE_Scenario']) == 1:
                conn_occ_db = sqlite3.connect(NE_OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            else:
                conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
                cur_occ_db = conn_occ_db.cursor()

            if occlusion_caused_collision['Collision_Configuration'] == 'Angle':
                if occlusion_caused_collision['Taxonomy'] == 'Both_tag_on':
                    
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        both_angle_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        both_angle_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        both_angle_count['Right-turn'] += 1

                elif occlusion_caused_collision['Taxonomy'] == 'Left_turn_tag_on':
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        left_turn_angle_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        left_turn_angle_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        left_turn_angle_count['Right-turn'] += 1

                elif occlusion_caused_collision['Taxonomy'] == 'Straight_tag_on':
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        straight_angle_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        straight_angle_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        straight_angle_count['Right-turn'] += 1

                elif occlusion_caused_collision['Taxonomy'] == 'Reveal':
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        reveal_angle_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        reveal_angle_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        reveal_angle_count['Right-turn'] += 1

            if occlusion_caused_collision['Collision_Configuration'] == 'Front-to-Front':

                if occlusion_caused_collision['Taxonomy'] == 'Both_tag_on':
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        both_front_to_front_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        both_front_to_front_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        both_front_to_front_count['Right-turn'] += 1

                elif occlusion_caused_collision['Taxonomy'] == 'Left_turn_tag_on':
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        left_turn_front_to_front_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        left_turn_front_to_front_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        left_turn_front_to_front_count['Right-turn'] += 1

                elif occlusion_caused_collision['Taxonomy'] == 'Straight_tag_on':
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        straight_front_to_front_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        straight_front_to_front_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        straight_front_to_front_count['Right-turn'] += 1

                elif occlusion_caused_collision['Taxonomy'] == 'Reveal':
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        reveal_front_to_front_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        reveal_front_to_front_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        reveal_front_to_front_count['Right-turn'] += 1

            if occlusion_caused_collision['Collision_Configuration'] == 'Sideswipe':

                if occlusion_caused_collision['Taxonomy'] == 'Both_tag_on':
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        both_sideswipe_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        both_sideswipe_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        both_sideswipe_count['Right-turn'] += 1

                elif occlusion_caused_collision['Taxonomy'] == 'Left_turn_tag_on':
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        left_turn_sideswipe_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        left_turn_sideswipe_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        left_turn_sideswipe_count['Right-turn'] += 1

                elif occlusion_caused_collision['Taxonomy'] == 'Straight_tag_on':
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        straight_sideswipe_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        straight_sideswipe_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        straight_sideswipe_count['Right-turn'] += 1

                elif occlusion_caused_collision['Taxonomy'] == 'Reveal':
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        reveal_sideswipe_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        reveal_sideswipe_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        reveal_sideswipe_count['Right-turn'] += 1


            if occlusion_caused_collision['Collision_Configuration'] == 'Front-to-Rear':

                if occlusion_caused_collision['Taxonomy'] == 'Both_tag_on':
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        both_front_to_rear_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        both_front_to_rear_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        both_front_to_rear_count['Right-turn'] += 1

                elif occlusion_caused_collision['Taxonomy'] == 'Left_turn_tag_on':
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        left_turn_front_to_rear_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        left_turn_front_to_rear_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        left_turn_front_to_rear_count['Right-turn'] += 1

                elif occlusion_caused_collision['Taxonomy'] == 'Straight_tag_on':
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        straight_front_to_rear_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        straight_front_to_rear_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        straight_front_to_rear_count['Right-turn'] += 1

                elif occlusion_caused_collision['Taxonomy'] == 'Reveal':
                    if straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'STRAIGHT':
                        reveal_front_to_rear_count['Straight'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'LEFT_TURN':
                        reveal_front_to_rear_count['Left-turn'] += 1
                    elif straight_or_left_turn[(current_file_id, scenario_key)]['one_unnocluded'] == 'RIGHT_TURN':
                        reveal_front_to_rear_count['Right-turn'] += 1


    print(f"BOTH front_to_front: {both_front_to_front_count}")
    print(f"LEFT-TURN front_to_front: {left_turn_front_to_front_count}")
    print(f"STRAIGHT front_to_front: {straight_front_to_front_count}")
    print(f"REVEAL front_to_front: {reveal_front_to_front_count}")

    print(f"BOTH ANGLE: {both_angle_count}")
    print(f"LEFT-TURN ANGLE: {left_turn_angle_count}")
    print(f"STRAIGHT ANGLE: {straight_angle_count}")
    print(f"REVEAL ANGLE: {reveal_angle_count}")

    print(f"BOTH sideswipe: {both_sideswipe_count}")
    print(f"LEFT-TURN sideswipe: {left_turn_sideswipe_count}")
    print(f"STRAIGHT sideswipe: {straight_sideswipe_count}")
    print(f"REVEAL sideswipe: {reveal_sideswipe_count}")

    print(f"BOTH front_to_rear: {both_front_to_rear_count}")
    print(f"LEFT-TURN front_to_rear: {left_turn_front_to_rear_count}")
    print(f"STRAIGHT front_to_rear: {straight_front_to_rear_count}")
    print(f"REVEAL front_to_rear: {reveal_front_to_rear_count}")


def plot_occlusion_scenario(current_file_id, scenario_key, NE=False):
    
    if NE:
        current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = regenerate_NE_scenarios.retrieve_NE_all_maneuvers(current_file_id, scenario_key)
    else:
        current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = retrieve_all_maneuvers(current_file_id, scenario_key)

    subject_vehicle_id = ordered_vehicle_ids[0]
    occluding_vehicle_id = ordered_vehicle_ids[1]
    relevant_vehicle_ids = ordered_vehicle_ids[2:]

    scenario = setup_scenario(current_file_id, current_time, subject_vehicle_id, relevant_vehicle_ids, occluding_vehicle_id=occluding_vehicle_id, NE=NE)
    
    # Plot occlusion situation
    occ_map = occupancy_map.OccupancyMap()
    occ_map.make_occupancy_map(scenario, pass_bb_box_back=False)
    occ_map.default_visualize_occupancy_map(scenario, filename=f"Occlusion Scenario {current_file_id} {scenario_key} Initial State.pdf")
