'''
Generating occlusion scenarios and risk identification for North-East left-turns specifically (since these were skipped over)

Created on August 30th, 2021

@author: Maximilian Kahn
'''

from equilibrium import utilities
from equilibria import equilibria_core
from utilities import occlusion_constants

from game import game_utilities
from utilities import occupancy_map, scenario_utilities
from occlusion_scenario import occlusion_scenario_builder

from trajectories import trajectory_generation

from shapely.geometry.polygon import Polygon, LineString
from matplotlib import pyplot as plt
import matplotlib.animation as animation
from utilities.vehicle_state import VehicleState

import ast
import itertools
from copy import deepcopy
import sqlite3
import math
import numpy as np
import os

def recompute_relevant_vehicles_for_NE_turning_vehicles():

    north_east_left_turn_lanes = ('ln_n_1','prep-turn_n','exec-turn_n')

    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS[7:]:


        occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
        occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
        occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

        conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
        cur_occ_db = conn_occ_db.cursor()
        conn_db = sqlite3.connect(occlusion_constants.DB)
        cur_db = conn_db.cursor()

        # Get all subject vehicle ids from relevant agents table at time current_time
        time_list = tuple(scenario_utilities.generate_time_list())
        invalid_vehicles = scenario_utilities.get_invalid_vehicles() 

        execute_cmd = f"SELECT TRACK_ID, TIME FROM TRAJECTORIES_0{current_file_id}_EXT WHERE TIME IN {time_list} AND ASSIGNED_SEGMENT IN {north_east_left_turn_lanes}"
        cur_db.execute(execute_cmd)
        query_result = cur_db.fetchall()

        for index, id_time_tuple in enumerate(query_result):
            subject_vehicle_id = id_time_tuple[0]
            current_time = id_time_tuple[1]

            if subject_vehicle_id in invalid_vehicles:
                continue
        
            v = scenario_utilities.setup_vehicle_state(subject_vehicle_id, current_time)
            path, gates, direction = scenario_utilities.get_path_gates_direction(subject_vehicle_id)

            v.set_gates(gates)
            gct = scenario_utilities.gate_crossing_times(v)
            v.set_gate_crossing_times(gct)

            entry_exit_time = scenario_utilities.get_entry_exit_time(v.id)
            v.set_entry_exit_time(entry_exit_time)
            
            relevant_vehicles = scenario_utilities.get_relevant_vehicles(v)
            reduced_relevant_vehicles = scenario_utilities.reduce_relev_agents(v.id, current_time, relevant_vehicles)

            print(f"---DB: {current_file_id}; CURRENT TIME: {current_time}; SUBJECT VEHICLE ID: {subject_vehicle_id}, INDEX: {index}---")
            print(f"REDUCED RELEVANT VEHICLES: {reduced_relevant_vehicles}\n")

            # Save relevant agents if they exist
            if reduced_relevant_vehicles:
                cmd = f"UPDATE main.RELEVANT_AGENTS SET RELEV_AGENT_IDS='{str(reduced_relevant_vehicles)}' WHERE TRACK_ID={subject_vehicle_id} AND TIME={current_time}"
                conn_db.execute(cmd)
                conn_db.commit()

        conn_db.close()

            

def generate_and_save_NE_occluding_vehicles(current_file_id):
    vehicle_type = 'car'
    north_east_left_turn_lanes = ('ln_n_1','prep-turn_n','exec-turn_n')
    NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

    conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
    cur_occ_db = conn_occ_db.cursor()
    conn_db = sqlite3.connect(occlusion_constants.DB)
    cur_db = conn_db.cursor()

    NE_conn_occ_db = sqlite3.connect(NE_OCC_DB)
    NE_cur_occ_db = NE_conn_occ_db.cursor()


    cmd = f"CREATE TABLE OCCLUDING_VEHICLES(TIME NUMERIC, X NUMERIC, Y NUMERIC, ANGLE NUMERIC, " \
          f"ASSIGNED_SEGMENT TEXT, SEGMENT_SEQ TEXT, DIRECTION TEXT, TRAFFIC_SIGNAL TEXT, TYPE TEXT, SUBJECT_VEHICLE_ID INT, OCCLUSION_LEVEL TEXT)"
    NE_cur_occ_db.execute(cmd)


    # Get all subject vehicle ids from relevant agents table at time current_time
    time_list = tuple(scenario_utilities.generate_time_list())
    invalid_vehicles = scenario_utilities.get_invalid_vehicles() 

    execute_cmd = f"SELECT TRACK_ID, TIME FROM TRAJECTORIES_0{current_file_id}_EXT WHERE TIME IN {time_list} AND ASSIGNED_SEGMENT IN {north_east_left_turn_lanes}"
    cur_db.execute(execute_cmd)
    query_result = cur_db.fetchall()


    for index, id_time_tuple in enumerate(query_result):

        subject_vehicle_id = id_time_tuple[0]
        current_time = id_time_tuple[1]
        if subject_vehicle_id in invalid_vehicles:
            continue
        subject_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(subject_vehicle_id, current_time)
        relevant_vehicles = scenario_utilities.retrieve_relevant_vehicles(subject_vehicle_id, current_time)

        # If there are no valid relevant_vehicles (i.e., we do not include vehicles that move from East to North in this analysis)
        if not relevant_vehicles:
            continue
                
        occluding_vehicles, occlusion_levels = occlusion_scenario_builder.spawn_occluding_vehicle(subject_vehicle, relevant_vehicles, current_time)

        if occluding_vehicles:
            for occluding_vehicle in occluding_vehicles:

                occlusion_level = occlusion_levels[(occluding_vehicle.x, occluding_vehicle.y)]
                NE_cur_occ_db.execute("INSERT INTO OCCLUDING_VEHICLES(TIME, X, Y, ANGLE, ASSIGNED_SEGMENT, SEGMENT_SEQ, DIRECTION, TRAFFIC_SIGNAL, \
                             TYPE, SUBJECT_VEHICLE_ID, OCCLUSION_LEVEL) VALUES (?,?,?,?,?,?,?,?,?,?,?)", 
                             (occluding_vehicle.current_time, occluding_vehicle.x, occluding_vehicle.y, occluding_vehicle.angle, occluding_vehicle.current_segment, \
                              str(occluding_vehicle.segment_seq), occluding_vehicle.direction, occluding_vehicle.signal, vehicle_type, subject_vehicle.id, str(occlusion_level)))

            NE_conn_occ_db.commit()

        print(f"FINISHED COMPUTING OCCLUSION SCENARIO FOR TIME {current_time}; {round(index / len(query_result) * 100, 2)}% PERCENT COMPLETED DATABASE ID {occlusion_constants.CURRENT_FILE_ID}.")
        
    NE_conn_occ_db.close()



def generate_NE_initial_speeds_table():
    insert_section = f"(INITIAL_SPEED_KEY INTEGER, SEGMENT TEXT, TRAFFIC_LIGHT TEXT, DEDICATED_GREEN_FLAG INTEGER, HAS_CONFLICT_FLAG INTEGER, JUST_TURNED_GREEN_FLAG INTEGER, MIN_SPEED NUMERIC, " \
                     f"MAX_SPEED NUMERIC, MEAN_SPEED NUMERIC, MEDIAN_SPEED NUMERIC, STANDARD_DEVIATION NUMERIC, TOTAL_COUNT INTEGER)"
    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:
        NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
        conn = sqlite3.connect(NE_OCC_DB)
        cur = conn.cursor()
        cmd = f"CREATE TABLE INITIAL_SPEEDS {insert_section}"
        cur.execute(cmd)
        conn.commit()
        conn.close()


def copy_NE_initial_speeds_table():
    base_table_id = "769" # We are copying contents of INITIAL_SPEEDS table from 769 to other databases
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{base_table_id}/uni_weber_{base_table_id}_occluding_vehicles.db"
    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:
        file_name = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
        conn = sqlite3.connect(occlusion_constants.OCC_DB)
        cur = conn.cursor()
        print(current_file_id)
        print(file_name)
        cmd = f"ATTACH DATABASE '{file_name}' AS new_db"
        cur.execute(cmd)
        cmd = f"INSERT INTO new_db.INITIAL_SPEEDS SELECT * FROM main.INITIAL_SPEEDS;"
        cur.execute(cmd)
        conn.commit()
        cmd = f"DETACH new_db"
        cur.execute(cmd)
        conn.close()


# Used to add track id values to the OCCLUDING VEHICLES table
def add_NE_track_id_for_occluding_vehicles():
    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:
        NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
        conn = sqlite3.connect(NE_OCC_DB)
        cur = conn.cursor()
        cur.execute(f"SELECT TIME FROM OCCLUDING_VEHICLES")
        query_result = cur.fetchall()
        track_id = -1
        for i in range(1, len(query_result)+1):
            cur.execute(f"UPDATE main.OCCLUDING_VEHICLES SET TRACK_ID={track_id} WHERE _rowid_='{i}'")
            track_id -= 1
        conn.commit()
        conn.close()


# ----GENERATING TRAJECTORIES----
def generate_NE_trajectories_tables():
    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:
        NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
        conn = sqlite3.connect(NE_OCC_DB)
        cur = conn.cursor()
        cmd = f"CREATE TABLE BASE_SCENARIO_TRAJECTORIES (MIN_SCENARIO_KEY INTEGER, MAX_SCENARIO_KEY INTEGER, TRACK_ID INTEGER, MANEUVER TEXT, TRAJECTORIES TEXT)"
        cur.execute(cmd)
        conn.commit()
        conn.close()

    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:
        NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
        conn = sqlite3.connect(NE_OCC_DB)
        cur = conn.cursor()
        cmd = f"CREATE TABLE OCCLUSION_SCENARIO_TRAJECTORIES (SCENARIO_KEY INTEGER, TRACK_ID INTEGER, MANEUVER TEXT, TRAJECTORIES TEXT)"
        cur.execute(cmd)
        conn.commit()
        conn.close()


def generate_NE_synthetic_scenarios_table():
    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:
        NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
        conn = sqlite3.connect(NE_OCC_DB)
        cur = conn.cursor()
        cmd = f"CREATE TABLE ALL_SYNTHETIC_SCENARIOS (SCENARIO_KEY INTEGER, SUBJECT_VEHICLE_ID INTEGER, OCCLUDING_VEHICLE_ID INTEGER, TIME NUMERIC, INITIAL_SPEED_KEY INTEGER, OCCLUDING_VEHICLE_SPEED NUMERIC, IS_VALID_OCCLUSION_SCENARIO INTEGER, VEHICLE_HIT_COUNTS TEXT)"
        cur.execute(cmd)
        conn.commit()
        conn.close()


def save_NE_base_scenario_trajectories(min_scenario_key, max_scenario_key, track_id, maneuver, trajectories, NE_OCC_DB):
    conn = sqlite3.connect(NE_OCC_DB)
    cur = conn.cursor()
    cur.execute("INSERT INTO BASE_SCENARIO_TRAJECTORIES (MIN_SCENARIO_KEY, MAX_SCENARIO_KEY, TRACK_ID, MANEUVER, TRAJECTORIES) VALUES (?,?,?,?,?)", 
               (min_scenario_key, max_scenario_key, track_id, maneuver, str(trajectories)))
    conn.commit()
    conn.close()


def save_NE_occlusion_scenario_trajectories(scenario_key, track_id, maneuver, trajectories, NE_OCC_DB):
    conn = sqlite3.connect(NE_OCC_DB)
    cur = conn.cursor()
    cur.execute("INSERT INTO OCCLUSION_SCENARIO_TRAJECTORIES (SCENARIO_KEY, TRACK_ID, MANEUVER, TRAJECTORIES) VALUES (?,?,?,?)", 
               (scenario_key, track_id, maneuver, str(trajectories)))
    conn.commit()
    conn.close()


        
def save_NE_synthetic_scenario(scenario_key, subject_vehicle_id, occluding_vehicle_id, time, initial_speed_key, occluding_vehicle_speed, NE_OCC_DB, is_valid_occlusion_scenario=None, vehicle_hit_counts=None):
    conn = sqlite3.connect(NE_OCC_DB)
    cur = conn.cursor()
    cur.execute("INSERT INTO ALL_SYNTHETIC_SCENARIOS (SCENARIO_KEY, SUBJECT_VEHICLE_ID, OCCLUDING_VEHICLE_ID, TIME, INITIAL_SPEED_KEY, OCCLUDING_VEHICLE_SPEED, IS_VALID_OCCLUSION_SCENARIO, VEHICLE_HIT_COUNTS) VALUES (?,?,?,?,?,?,?,?)", 
               (scenario_key, subject_vehicle_id, occluding_vehicle_id, time, initial_speed_key, occluding_vehicle_speed, is_valid_occlusion_scenario, vehicle_hit_counts))
    conn.commit()
    conn.close()



def generate_and_save_NE_trajectories(current_file_id, start_time_index=0, end_time_index=None, scenario_key=0):
    north_east_left_turn_lanes = ('ln_n_1','prep-turn_n','exec-turn_n')
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)
    NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

    conn_db = sqlite3.connect(occlusion_constants.DB)
    cur_db = conn_db.cursor()

    NE_conn_occ_db = sqlite3.connect(NE_OCC_DB)
    NE_cur_occ_db = NE_conn_occ_db.cursor()

    time_list = tuple(scenario_utilities.generate_time_list())
    execute_cmd = f"SELECT TRACK_ID, TIME FROM TRAJECTORIES_0{current_file_id}_EXT WHERE TIME IN {time_list} AND ASSIGNED_SEGMENT IN {north_east_left_turn_lanes}"
    cur_db.execute(execute_cmd)
    subject_time_list = cur_db.fetchall()


    def take_second(elem):
        return elem[1]

    subject_time_list = list(set(subject_time_list))

    subject_time_list.sort(key=take_second)

    for index, subject_vehicle_id_time_tuple in enumerate(subject_time_list):

        starting_scenario_key = scenario_key
        subject_vehicle_id = subject_vehicle_id_time_tuple[0]
        current_time =  subject_vehicle_id_time_tuple[1]

        print(f"STARTING NEW BASE SCENARIO;  STARTING SCENARIO KEY: {starting_scenario_key}")
        print(f"{round(index / len(subject_time_list) * 100, 2)}% PERCENT COMPLETED DB {current_file_id}")
        print(f"SUBJECT VEHICLE ID: {subject_vehicle_id}; CURRENT_TIME: {current_time}")

        subject_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(subject_vehicle_id, current_time)
        relevant_vehicles = scenario_utilities.retrieve_relevant_vehicles(subject_vehicle_id, current_time)
        if not relevant_vehicles:
            continue

        execute_cmd = f"SELECT * FROM OCCLUDING_VEHICLES WHERE TIME={current_time} AND SUBJECT_VEHICLE_ID={subject_vehicle_id}"
        NE_cur_occ_db.execute(execute_cmd)
        query_result = NE_cur_occ_db.fetchall()
        if not query_result:
            continue
        occluding_vehicles = []
        for row in query_result:
            occluding_vehicle = scenario_utilities.setup_occluding_vehicle_from_db(row)
            occluding_vehicles.append(occluding_vehicle)
        if not occluding_vehicles:
            continue

        base_scenario = [subject_vehicle] + relevant_vehicles

        # Stores the trajectories described below
        base_scenario_trajectories = {}

        # Generate trajectories for subject and relevant vehicles that don't depend on the 
        # occluding vehicle (all maneuvers except for follow maneuvers)
        for v in base_scenario:
     
            v.set_leading_vehicle(None)

            base_scenario_trajectories[v.id] = {}
            maneuvers = scenario_utilities.get_available_actions(v)
            print(f"-----------BELOW IS FOR VEHICLE: {v.id}----------")
            print(f"VEHICLE CURRENT SEGMENT: {v.current_segment}")
            print(f"VEHICLE SEGMENT SEQ: {v.segment_seq}")
            print(f"VEHICLE SIGNAL: {v.signal}")
            print(f"BASE SCENARIO MANEUVERS: {maneuvers}")
            maneuvers = scenario_utilities.filter_wait_maneuvers(maneuvers, filter_follow_maneuvers=True)
            print(f"BASE SCENARIO MANEUVERS AFTER FILTERING: {maneuvers}")

            for maneuver in maneuvers:
                print(f"Working on vehicle {v.id}, maneuver: {maneuver}")
                if maneuver == 'cut-in':
                    continue

                trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, None)
                # Relax constraints if we failed to generate trajectories
                if not trajectories:
                    trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, None, relaxed_constraints=True)
                # Set speed to something reasonable if we failed to generate trajectories 
                if not trajectories:
                    if maneuver in occlusion_constants.WAIT_ACTIONS:
                        new_speed = 2.0
                    else:
                        new_speed = 8.0 
                    trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, None, relaxed_constraints=True, new_speed=new_speed)

                if not trajectories:
                    raise Exception(f"Could not generate trajectories for file id: {current_file_id}, time: {v.current_time}, vehicle: {v.id} for maneuver: {maneuver}, vehicle segment: {v.current_segment}, speed: {v.speed}")
                sorted_trajectories = scenario_utilities.filter_trajectories(trajectories, maneuver)
                base_scenario_trajectories[v.id][maneuver] = sorted_trajectories
                
        for occluding_vehicle in occluding_vehicles:

            print(f"\n***---NEW OCCLUSION SCENARIO: TIME {current_time}, FILE ID: {current_file_id}---***")
            vehicle_list = []
            vehicle_list.append(subject_vehicle)
            vehicle_list.append(occluding_vehicle)
            vehicle_list += relevant_vehicles

            # Set leaders for each vehicle in scenario
            for v in vehicle_list:
                leader_id = scenario_utilities.get_leader_id(v, vehicle_list)
                if leader_id:
                    for veh in vehicle_list:
                        if veh.id == leader_id:
                            v.set_leading_vehicle(veh)
                            break
                else:
                    v.set_leading_vehicle(None)

            # Retrieve traffic information for occluding vehicle speed generation
            dedicated_green, has_conflict, just_turned_green = scenario_utilities.retrieve_traffic_conditions_for_initial_speed(current_file_id, occluding_vehicle, vehicle_list)
            speed = scenario_utilities.get_initial_speed_for_occluding_vehicle(occluding_vehicle.current_segment, occluding_vehicle.signal, dedicated_green, has_conflict, just_turned_green)

            # Create scenarios based on the occluding vehicle's initial speed
            occluding_vehicle_initial_speeds = []

            # NOTE: Could generate more speeds for more scenarios later
            # Stores the occluding vehicle initial speeds in a list of tuples of the form:
            # [(speed_key, speed)]. The speed key tells you what entry in the initial speeds 
            # table to look at to generate the speed.
            if type(speed) == dict and speed['go'] != speed['stop']:
                occluding_vehicle_initial_speeds.append((speed['go_speed_initial_key'], speed['go']))
                occluding_vehicle_initial_speeds.append((speed['stop_speed_initial_key'], speed['stop']))
            else:
                occluding_vehicle_initial_speeds.append((speed['go_speed_initial_key'], speed['go']))
           
            # This is true if all maneuvers could be generated for at least one of the occluding vehicle speeds
            successful_occlusion_scenario = False

            # Store the initial speeds tested 
            for key_speed_tuple in occluding_vehicle_initial_speeds:
                initial_speed_key = key_speed_tuple[0]
                occluding_vehicle_initial_speed = key_speed_tuple[1]

                # If occluding vehicle has a lead vehicle, occluding vehicle's speed 
                # cannot be greater than lead vehicle's speed.
                # This will hopefully prevent collisions where the occluding vehicle
                # rear-ends the lead vehicle.
                if occluding_vehicle.leading_vehicle and occluding_vehicle_initial_speed > occluding_vehicle.leading_vehicle.speed:
                        occluding_vehicle_initial_speed = occluding_vehicle.leading_vehicle.speed
                occluding_vehicle.set_speed(occluding_vehicle_initial_speed)

                occlusion_scenario_trajectories = {}
                for v in vehicle_list:
                    occlusion_scenario_trajectories[v.id] = {}

                for v in vehicle_list:
                    maneuvers = scenario_utilities.get_available_actions(v)

                    if v.id < 0:
                        maneuvers = scenario_utilities.filter_wait_maneuvers(maneuvers, filter_follow_maneuvers=False)
                    else:
                        maneuvers = [maneuver for maneuver in maneuvers if maneuver in occlusion_constants.FOLLOW_ACTIONS]

                    original_speed = occluding_vehicle_initial_speed if v.id < 0 else v.speed
                 
                    for maneuver in maneuvers:
                        print(f"Working on vehicle {v.id}, maneuver: {maneuver}")
                        if maneuver == 'cut-in':
                            continue
                        if maneuver in occlusion_constants.FOLLOW_ACTIONS and not v.leading_vehicle:
                            continue

                        print(f"FIRST ATTEMPT")
                        trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, occluding_vehicle_initial_speed)
                        
                        if not trajectories:
                            print(f"SECOND ATTEMPT")
                            trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, occluding_vehicle_initial_speed, relaxed_constraints=True)
                    
                        # Set speed to something reasonable if we failed to generate trajectories 
                        if not trajectories:
                            print(f"THIRD ATTEMPT")
                            changed_lead_vehicle_speed = False

                            if maneuver in occlusion_constants.WAIT_ACTIONS:
                                reasonable_speed = 2.0
                                if v.id < 0:
                                    occluding_vehicle_initial_speed = reasonable_speed

                                trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, occluding_vehicle_initial_speed, relaxed_constraints=True, new_speed=reasonable_speed)

                            elif maneuver in occlusion_constants.FOLLOW_ACTIONS and v.leading_vehicle:

                                trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, v.leading_vehicle.speed, relaxed_constraints=True, third_attempt=True)

                            else:
                                reasonable_speed = 8.0 # approx. 29 km/h
                                if v.id < 0:
                                    occluding_vehicle_initial_speed = reasonable_speed

                                trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, occluding_vehicle_initial_speed, relaxed_constraints=True, new_speed=reasonable_speed)
                            
                            if not trajectories:
                                raise Exception(f"Could not generate trajectories for file id: {current_file_id}, time: {v.current_time}, vehicle: {v.id} for maneuver: {maneuver}, vehicle segment: {v.current_segment}, speed: {v.speed}")
                            
                            if v.id < 0:
                                occluding_vehicle_initial_speed = original_speed

                        sorted_trajectories = scenario_utilities.filter_trajectories(trajectories, maneuver)
                        occlusion_scenario_trajectories[v.id][maneuver] = sorted_trajectories

                # # Save occlusion scenario trajectories
                for v in vehicle_list:
                    # Save occlusion scenario trajectories
                    occlusion_scenario_maneuvers = list(occlusion_scenario_trajectories[v.id].keys())
                    for maneuver in occlusion_scenario_maneuvers:
                        trajectories = occlusion_scenario_trajectories[v.id][maneuver]

                        save_format_trajectories = [] 
                        # Convert weird array in timestep to float
                        for i in range(len(trajectories)):
                            traj = trajectories[i]
                            traj = [list(timestep) for timestep in traj]
                            for j in range(len(traj)):
                                timestep = traj[j]
                                traj[j][4] = float(timestep[4])
                            save_format_trajectories.append(traj)
                        save_NE_occlusion_scenario_trajectories(scenario_key, v.id, maneuver, save_format_trajectories, NE_OCC_DB)
                save_NE_synthetic_scenario(scenario_key, subject_vehicle.id, occluding_vehicle.id, current_time, initial_speed_key, occluding_vehicle_initial_speed, NE_OCC_DB)
                
                scenario_key += 1

        # Save base scenario trajectories
        base_scenario_track_ids = list(base_scenario_trajectories.keys())

        for track_id in base_scenario_track_ids:
            maneuvers = list(base_scenario_trajectories[track_id].keys())

            for maneuver in maneuvers:
                trajectories = base_scenario_trajectories[track_id][maneuver]
                save_format_trajectories = [] 
                # Convert weird array in timestep to float
                for i in range(len(trajectories)):
                    traj = trajectories[i]
                    traj = [list(timestep) for timestep in traj]
                    for j in range(len(traj)):
                        timestep = traj[j]
                        traj[j][4] = float(timestep[4])
                    save_format_trajectories.append(traj)
                save_NE_base_scenario_trajectories(starting_scenario_key, scenario_key-1, track_id, maneuver, save_format_trajectories, NE_OCC_DB)




def retrieve_NE_all_maneuvers(current_file_id, scenario_key):
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

    NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"

    conn_db = sqlite3.connect(occlusion_constants.DB)
    cur_db = conn_db.cursor()

    conn_occ_db = sqlite3.connect(NE_OCC_DB)
    cur_occ_db = conn_occ_db.cursor()

    execute_cmd = f"SELECT SUBJECT_VEHICLE_ID, OCCLUDING_VEHICLE_ID, TIME FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={scenario_key}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()

    subject_vehicle_id = query_result[0][0]
    occluding_vehicle_id = query_result[0][1]
    current_time = query_result[0][2]

    execute_cmd = f"SELECT RELEV_AGENT_IDS FROM RELEVANT_AGENTS WHERE TIME={current_time} AND TRACK_ID={subject_vehicle_id}"
    cur_db.execute(execute_cmd)
    query_result = cur_db.fetchall()
    relevant_vehicle_ids = ast.literal_eval(query_result[0][0])

    ordered_vehicle_ids = [subject_vehicle_id, occluding_vehicle_id]
    for track_id in relevant_vehicle_ids:
        ordered_vehicle_ids.append(track_id)

    # Get all maneuvers
    execute_cmd = f"SELECT TRACK_ID, MANEUVER FROM OCCLUSION_SCENARIO_TRAJECTORIES WHERE SCENARIO_KEY={scenario_key}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    track_id_maneuver_pairs_occlusion_scenario = list(set(query_result))


    execute_cmd = f"SELECT TRACK_ID, MANEUVER FROM BASE_SCENARIO_TRAJECTORIES WHERE MIN_SCENARIO_KEY <= {scenario_key} AND MAX_SCENARIO_KEY >= {scenario_key}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    track_id_maneuver_pairs_base_scenario = list(set(query_result))

    track_id_maneuver_pairs = track_id_maneuver_pairs_occlusion_scenario + track_id_maneuver_pairs_base_scenario

    vehicle_maneuvers = {}
    for track_id in ordered_vehicle_ids:
        vehicle_maneuvers[track_id] = []
        for track_id_maneuver in track_id_maneuver_pairs:
            if track_id_maneuver[0] == track_id:
                vehicle_maneuvers[track_id].append(track_id_maneuver[1])

    return current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs


def setup_NE_scenario(current_file_id, current_time, subject_vehicle_id, relevant_vehicle_ids, occluding_vehicle_id=None):
    NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)
    conn_occ_db = sqlite3.connect(NE_OCC_DB)
    cur_occ_db = conn_occ_db.cursor()

    scenario = []
    subject_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(subject_vehicle_id, current_time)
    scenario.append(subject_vehicle)
    # Create occluding vehicle from db
    if occluding_vehicle_id != None:
        execute_cmd = f"SELECT * FROM OCCLUDING_VEHICLES WHERE TRACK_ID={occluding_vehicle_id}"
        cur_occ_db.execute(execute_cmd)
        query_result = cur_occ_db.fetchall()
        occluding_vehicle = scenario_utilities.setup_occluding_vehicle_from_db(query_result[0])
        scenario.append(occluding_vehicle)
    # Create relevant_vehicles from db
    relevant_vehicles = []
    for track_id in relevant_vehicle_ids:
        relevant_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(track_id, current_time)
        relevant_vehicles.append(relevant_vehicle)
    scenario += relevant_vehicles
    return scenario



def retrieve_NE_trajectories(current_file_id, scenario_key, vehicle_ids, vehicle_maneuvers):
    NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
    conn = sqlite3.connect(NE_OCC_DB)
    cur = conn.cursor()

    # Stores all the trajectories
    all_trajectories_dict = {}
    # Save trajectories from DB in a dict
    for track_id in vehicle_ids:
        maneuvers = vehicle_maneuvers[track_id]
        all_trajectories_dict[track_id] = {}
        
        for maneuver in maneuvers:
            maneuver_trajectory_dict = {}
             # keys are levels, values are lists of trajectories
            if track_id < 0:
                execute_cmd = f"SELECT TRAJECTORIES FROM OCCLUSION_SCENARIO_TRAJECTORIES WHERE SCENARIO_KEY={scenario_key} AND TRACK_ID={track_id} AND MANEUVER='{maneuver}'"
            elif track_id > 0 and maneuver in occlusion_constants.FOLLOW_ACTIONS:
                execute_cmd = f"SELECT TRAJECTORIES FROM OCCLUSION_SCENARIO_TRAJECTORIES WHERE SCENARIO_KEY={scenario_key} AND TRACK_ID={track_id} AND MANEUVER='{maneuver}'"
            else:
                execute_cmd = f"SELECT TRAJECTORIES FROM BASE_SCENARIO_TRAJECTORIES WHERE MIN_SCENARIO_KEY <= {scenario_key} AND MAX_SCENARIO_KEY >= {scenario_key} AND TRACK_ID={track_id} AND MANEUVER='{maneuver}'"
            
            cur.execute(execute_cmd)
            query_result = cur.fetchall()
            trajectories = ast.literal_eval(query_result[0][0])
            all_trajectories_dict[track_id][maneuver] = trajectories
    return all_trajectories_dict



def compute_and_save_NE_is_valid_occlusion_scenario(current_file_id, start_scenario_key=None):
    NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)
    conn_occ_db = sqlite3.connect(NE_OCC_DB)
    cur_occ_db = conn_occ_db.cursor()
    conn_db = sqlite3.connect(occlusion_constants.DB)
    cur_db = conn_db.cursor()

    execute_cmd = f"SELECT SCENARIO_KEY FROM OCCLUSION_SCENARIO_TRAJECTORIES"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    query_result = set(query_result)
    scenario_keys = [i[0] for i in query_result]
    scenario_keys.sort()

    if start_scenario_key:
        scenario_keys = scenario_keys[start_scenario_key:]

    # Retrieve all trajectories for the scenario
    for scenario_key in scenario_keys:
        print(f"----CURRENT FILE ID: {current_file_id}; SCENARIO KEY: {scenario_key}----")

        current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = retrieve_NE_all_maneuvers(current_file_id, scenario_key)

        subject_vehicle_id = ordered_vehicle_ids[0]
        occluding_vehicle_id = ordered_vehicle_ids[1]
        relevant_vehicle_ids = ordered_vehicle_ids[2:]
        hypergame_length = len(ordered_vehicle_ids)

        scenario = setup_NE_scenario(current_file_id, current_time, subject_vehicle_id, relevant_vehicle_ids, occluding_vehicle_id=occluding_vehicle_id)

        # Assign leading vehicle attribute
        for vehicle in scenario:
            lead_vehicle_id = scenario_utilities.get_leader_id(vehicle, scenario)
            if lead_vehicle_id:
                for v in scenario:
                    if v.id == lead_vehicle_id:
                        vehicle.set_leading_vehicle(v)
            else:
                vehicle.set_leading_vehicle(None)

        # Assign has_oncoming_vehicle attribute for vehicles in scenario
        for i in range(0, len(scenario)-1):
            for j in range(i, len(scenario)):
                vehicle_a, vehicle_b = scenario[i], scenario[j]
                has_oncoming_vehicle = scenario_utilities.compute_has_oncoming_vehicle(vehicle_a, vehicle_b)
                if has_oncoming_vehicle:
                    if not vehicle_a.has_oncoming_vehicle:
                        vehicle_a.set_has_oncoming_vehicle(True)
                    if not vehicle_b.has_oncoming_vehicle:
                        vehicle_b.set_has_oncoming_vehicle(True)
                        
        hypergame_maneuver_combinations = itertools.combinations(track_id_maneuver_pairs, hypergame_length)
        hypergame_maneuver_combinations = [i for i in hypergame_maneuver_combinations if not game_utilities.repeated_track_ids_in_maneuver_combination(i, hypergame_length)]

        ordered_hypergame_maneuver_combinations = []
        # elements of hypergame_maneuver_combinations are not ordered and they must be ordered for 
        # the Nash Equilibrium solver to work.
        # E.g., hypergame_maneuver_combinations: [((1, 'follow_lead'), (-1, 'proceed-turn'), (3, 'track_speed')), 
        #                                         ((-1, 'proceed-turn'), (1, 'track_speed'), (3, 'track_speed'))
        for maneuver_combination in hypergame_maneuver_combinations:
            ordered_maneuver_combination = []
            for track_id in ordered_vehicle_ids:
                for id_maneuver in maneuver_combination:
                    if id_maneuver[0] == track_id:
                        ordered_maneuver_combination.append(id_maneuver)
                if ordered_maneuver_combination not in ordered_hypergame_maneuver_combinations:
                    ordered_hypergame_maneuver_combinations.append(ordered_maneuver_combination)

        hypergame_payoff_dict = {}
        all_maneuver_to_trajectory_index_dict = {}

        for hypergame_maneuver_set in ordered_hypergame_maneuver_combinations:
            # A hypergame maneuver set is a combination of maneuvers for each vehicle in the scenario:
            # E.g., ((1,2), (2,3), (-3,1)) could be one combination of maneuvers for each vehicle in the scenario with vehicles 1, 2, and -3.
            vehicle_maneuver_dict = {}
            for id_maneuver in hypergame_maneuver_set:
                vehicle_maneuver_dict[id_maneuver[0]] = [id_maneuver[1]]
   
            # Retrieve trajectories for this set of maneuvers
            trajectories_dict = retrieve_NE_trajectories(current_file_id, scenario_key, ordered_vehicle_ids, vehicle_maneuver_dict)

            payoff_dict_key, payoff_dict_value, chosen_trajectories = game_utilities.compute_chosen_trajectories(current_file_id, trajectories_dict, subject_vehicle_id, ordered_vehicle_ids, vehicle_maneuver_dict)

            # maneuver_to_trajectory_index_dict looks like: {(1, 'follow_lead'): (1, 'aggressive', 0), (-6, 'wait_for_lead_to_cross'): (-6, 'aggressive', 1), (3, 'track_speed'): (3, 'normal', 0)}
            # The key is a track_id, maneuver tuple, the value is a track_id, level, index tuple
            # The point of this dict is to recover the chosen trajectory for the maneuver to do a collision check and compute collision details
            maneuver_to_trajectory_index_dict = {}
            for id_maneuver, chosen_trajectory_key in zip(hypergame_maneuver_set, list(chosen_trajectories.keys())):
                if id_maneuver not in list(maneuver_to_trajectory_index_dict.keys()):
                    maneuver_to_trajectory_index_dict[id_maneuver] = chosen_trajectory_key

            hypergame_payoff_dict[payoff_dict_key] = payoff_dict_value

            all_maneuver_to_trajectory_index_dict[tuple(hypergame_maneuver_set)] = maneuver_to_trajectory_index_dict

        num_players = len(ordered_vehicle_ids)
        player_actions = [list(set([k[i] for k in hypergame_payoff_dict.keys()])) for i in np.arange(num_players)]  
   
        eq = equilibria_core.EquilibriaCore(num_players,hypergame_payoff_dict,len(hypergame_payoff_dict),player_actions[0],False)
        nash_equilibria = eq.calc_pure_strategy_nash_equilibrium_exhaustive()

        is_valid_occlusion_scenario = False
        # Skip the scenario if there is no nash equilibria for the hypergame
        if not nash_equilibria:
            save_NE_is_valid_occlusion_scenario(scenario_key, 0, NE_OCC_DB)
            print(f"COULD NOT GENERATE NASH EQUILIBRIA FOR THIS SCENARIO\n")
            continue

        # This stores every vehicles' maneuvers in the form
        # e.g., {1: ['track_speed', 'follow-lead'], 2: ['follow-lead']}
        vehicle_maneuvers = {}
        # Since the order is fixed, the index tells us the position of particular vehicle
        for index in range(len(ordered_vehicle_ids)):
            track_id = ordered_vehicle_ids[index]
            vehicle_maneuvers[track_id] = []
            for maneuver_combination in ordered_hypergame_maneuver_combinations:
                if maneuver_combination[index][1] not in vehicle_maneuvers[track_id]:
                    vehicle_maneuvers[track_id].append(maneuver_combination[index][1])

        # vehicle maneuvers looks like: {1: ['track_speed', 'follow-lead'], 2: ['wait_for_oncoming_vehicle'], -3: ['follow-lead']}
        occlusion_scenario_trajectories = retrieve_NE_trajectories(current_file_id, scenario_key, ordered_vehicle_ids, vehicle_maneuvers)

        # In order for an occlusion scenario to be valid, it must have either no collision in its hypergame nash equilibria, or,
        # have no collisions involving the occluding vehicle, or, have at least one nash equilibrium where there is at least one 
        # collision pair that does not involve the occluding vehicle.
        # E.g., collision_pairs = [] is a valid occlusion scenario because there are no collision pairs
        #       collision_pairs = [(-1,3), (2,4)] is valid because there is one collision pair that does not involve the occluding vehicle, -1.
        #       collision_pairs = [(-1,3)] is not valid.
        hypergame_ground_truth_maneuvers = game_utilities.compute_ground_truth_maneuvers(ordered_vehicle_ids, scenario, nash_equilibria, hypergame_payoff_dict, factored_game_subject_id=None)
        
        # Take first ground-truth maneuver
        first_maneuver_hypergame_ground_truth_maneuvers = {}
        for track_id, maneuvers in hypergame_ground_truth_maneuvers.items():
            first_maneuver_hypergame_ground_truth_maneuvers[track_id] = maneuvers.pop()

        hypergame_ground_truth_maneuvers_set = set([(track_id, maneuver) for track_id, maneuver in first_maneuver_hypergame_ground_truth_maneuvers.items()])

        # For hypergame, find ground_truth_maneuver_to_trajectory_index_dict
        for maneuver_combination, maneuver_to_trajectory_index_dict in all_maneuver_to_trajectory_index_dict.items():
            if hypergame_ground_truth_maneuvers_set == set(maneuver_combination):
                ground_truth_maneuver_to_trajectory_index_dict = maneuver_to_trajectory_index_dict
                break

        # Compute all collisions   
        all_collisions = []
        # Construct selected trajectories dict which stores the selected trajectory for each vehicle
        # Eg. {track_id: traj, ...}
        selected_trajectories = {}
        for track_id in ordered_vehicle_ids:
            maneuver = first_maneuver_hypergame_ground_truth_maneuvers[track_id]
            _, index = ground_truth_maneuver_to_trajectory_index_dict[(track_id, maneuver)]
            trajectory = occlusion_scenario_trajectories[track_id][maneuver][index]
            selected_trajectories[track_id] = trajectory

        all_collisions = []
        collision_id = 0
        for i in range(0, len(ordered_vehicle_ids) - 1):
            track_id_a = ordered_vehicle_ids[i]
            maneuver_a = first_maneuver_hypergame_ground_truth_maneuvers[track_id_a]
            for j in range(i+1, len(ordered_vehicle_ids)):
                track_id_b = ordered_vehicle_ids[j]
                maneuver_b = first_maneuver_hypergame_ground_truth_maneuvers[track_id_b]

                trajectory_a = selected_trajectories[track_id_a]
                trajectory_b = selected_trajectories[track_id_b]
                collision_impact_details = scenario_utilities.collision_check(trajectory_a, trajectory_b)

                if collision_impact_details:
                    impact_velocity_a, impact_velocity_b, original_time_to_collision = collision_impact_details
                    collision_details = [track_id_a, track_id_b, collision_id, maneuver_a, maneuver_b, maneuver_a, maneuver_b, impact_velocity_a, impact_velocity_b, original_time_to_collision]
                    all_collisions.append(collision_details)
                    collision_id += 1

        hypergame_filtered_collisions = game_utilities.filter_collisions(ordered_vehicle_ids, all_collisions, emergency_braking_trajectories=None)
        print(f"HYPERGAME FILTERED COLLISIONS: {hypergame_filtered_collisions}")

        # Stores tuples of track_ids involved in each accident, e.g., hypergame_collision_pairs = [(-1,3)]
        hypergame_collision_pairs = [(collision[0], collision[1]) for collision in hypergame_filtered_collisions]
        collisions_not_involving_occluding_vehicle = [collision_pair for collision_pair in hypergame_collision_pairs if occluding_vehicle_id not in collision_pair]
        
        if not hypergame_filtered_collisions:
            save_NE_is_valid_occlusion_scenario(scenario_key, 1, NE_OCC_DB) 
            print(f"IS VALID OCCLUSION SCENARIO: 1\n")

        # In this case there must be at least one collision pair that does not involve the one occluding vehicle
        elif len(collisions_not_involving_occluding_vehicle) >= 1:
            save_NE_is_valid_occlusion_scenario(scenario_key, 1, NE_OCC_DB) 
            print(f"IS VALID OCCLUSION SCENARIO: 1\n")
        # If we get to this point it must be because there is a collision that involves the occluding vehicle
        # We are okay with any collision as long as it happens after some time horizon threshold as this threshold 
        # means the vehicles in the scenario have time to react
        elif hypergame_filtered_collisions[0][-1] >= occlusion_constants.OCCLUSION_SCENARIO_INCLUSION_TIME_HORIZON_THRESHOLD:
            save_NE_is_valid_occlusion_scenario(scenario_key, 1, NE_OCC_DB) 
            print(f"IS VALID OCCLUSION SCENARIO: 1\n")
        else:
            save_NE_is_valid_occlusion_scenario(scenario_key, 0, NE_OCC_DB) 
            print(f"IS VALID OCCLUSION SCENARIO: 0\n")


def save_NE_is_valid_occlusion_scenario(scenario_key, is_valid_occlusion_scenario, NE_OCC_DB):
    conn = sqlite3.connect(NE_OCC_DB)
    cur = conn.cursor()
    cmd = f"UPDATE main.ALL_SYNTHETIC_SCENARIOS SET IS_VALID_OCCLUSION_SCENARIO={is_valid_occlusion_scenario} WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    conn.commit()
    conn.close()


def retrieve_NE_is_valid_occlusion_scenario(scenario_key, NE_OCC_DB):
    conn = sqlite3.connect(NE_OCC_DB)
    cur = conn.cursor()
    cmd = f"SELECT IS_VALID_OCCLUSION_SCENARIO FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    query_result = cur.fetchall()
    conn.close()
    return query_result[0][0]


def compute_and_save_NE_vehicle_hit_counts(current_file_id, start_scenario_key=None):
    NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)
    conn_occ_db = sqlite3.connect(NE_OCC_DB)
    cur_occ_db = conn_occ_db.cursor()

    execute_cmd = f"SELECT SCENARIO_KEY FROM OCCLUSION_SCENARIO_TRAJECTORIES"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    query_result = set(query_result)
    scenario_keys = [i[0] for i in query_result]
    scenario_keys.sort()

    if start_scenario_key:
        scenario_keys = scenario_keys[start_scenario_key:]

    # Retrieve all trajectories for the scenario
    for scenario_key in scenario_keys:
        is_valid_occlusion_scenario = retrieve_NE_is_valid_occlusion_scenario(scenario_key, NE_OCC_DB)
        if is_valid_occlusion_scenario == 0:
            continue

        print(f"SCENARIO KEY: {scenario_key}, CURRENT_FILE_ID: {current_file_id}")

        current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = retrieve_NE_all_maneuvers(current_file_id, scenario_key)

        subject_vehicle_id = ordered_vehicle_ids[0]
        occluding_vehicle_id = ordered_vehicle_ids[1]
        relevant_vehicle_ids = ordered_vehicle_ids[2:]

        subject_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(subject_vehicle_id, current_time)

        print(F"{subject_vehicle.id}: {subject_vehicle.x}, {subject_vehicle.y}")

        # Create occluding vehicle from db
        execute_cmd = f"SELECT * FROM OCCLUDING_VEHICLES WHERE TRACK_ID={occluding_vehicle_id}"
        cur_occ_db.execute(execute_cmd)
        query_result = cur_occ_db.fetchall()
        occluding_vehicle = scenario_utilities.setup_occluding_vehicle_from_db(query_result[0])
        # Create relevant_vehicles from db
        relevant_vehicles = []
        for track_id in relevant_vehicle_ids:
            relevant_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(track_id, current_time)
            relevant_vehicles.append(relevant_vehicle)
        
        # E.g., vehicle_hit_counts = {2: {1: 23, 3: 10}, 1: {2: 1, 3: 0}} shows how many rays hit each vehicle 
        vehicle_hit_counts = game_utilities.compute_all_hit_counts_for_scenario(subject_vehicle, occluding_vehicle, relevant_vehicles)
        print(f"vehicle hit counts: {vehicle_hit_counts}\n")
        save_NE_vehicle_hit_counts(scenario_key, vehicle_hit_counts, NE_OCC_DB)

    print(f"Finished computing vehicle hit counts for database {current_file_id}.")



def save_NE_vehicle_hit_counts(scenario_key, vehicle_hit_counts, NE_OCC_DB):
    conn = sqlite3.connect(NE_OCC_DB)
    cur = conn.cursor()
    cmd = f"UPDATE main.ALL_SYNTHETIC_SCENARIOS SET VEHICLE_HIT_COUNTS='{str(vehicle_hit_counts)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    conn.commit()
    conn.close()



def generate_NE_collisions_tables():
    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:

        NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
        conn = sqlite3.connect(NE_OCC_DB)
        cur = conn.cursor()
        # Create relevant agents table in database
        cmd = f"CREATE TABLE COLLISIONS (SCENARIO_KEY INTEGER, HYPERGAME_COLLISIONS TEXT, FACTORED_GAME_COLLISIONS TEXT, OCCLUSION_CAUSED_COLLISIONS TEXT, MANEUVER_PROBABILITIES TEXT, \
                HYPERGAME_TRAJ_INDICES TEXT, FACTORED_GAME_TRAJ_INDICES TEXT, HYPERGAME_EMERGENCY_BRAKING_TRAJS TEXT, FACTORED_GAME_EMERGENCY_BRAKING_TRAJS TEXT)"   
        cur.execute(cmd)
        conn.commit()
        conn.close()


def save_NE_scenario_key_in_collisions(scenario_key, NE_OCC_DB):
    conn = sqlite3.connect(NE_OCC_DB)
    cur = conn.cursor()
    cmd = f"SELECT SCENARIO_KEY FROM COLLISIONS"
    cur.execute(cmd)
    query_result = cur.fetchall()
    if not query_result:
        cur.execute("INSERT INTO COLLISIONS (SCENARIO_KEY) VALUES (?)", (str(scenario_key),))
        conn.commit()
        conn.close()
    else: 
        scenario_keys = [i[0] for i in query_result]
        # If scenario key already in the table, do nothing
        if scenario_key in scenario_keys:
            return
        cur.execute("INSERT INTO COLLISIONS (SCENARIO_KEY) VALUES (?)", (str(scenario_key),))
        conn.commit()
        conn.close()


def save_NE_hypergame_collision_details(scenario_key, hypergame_collisions, hypergame_traj_indices, hypergame_emergency_breaking_trajs, NE_OCC_DB):
    conn = sqlite3.connect(NE_OCC_DB)
    cur = conn.cursor()
    cmd = f"UPDATE main.COLLISIONS SET HYPERGAME_COLLISIONS='{str(hypergame_collisions)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    cmd = f"UPDATE main.COLLISIONS SET HYPERGAME_TRAJ_INDICES='{str(hypergame_traj_indices)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    cmd = f"UPDATE main.COLLISIONS SET HYPERGAME_EMERGENCY_BRAKING_TRAJS='{str(hypergame_emergency_breaking_trajs)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    conn.commit()
    conn.close()


def save_NE_factored_game_collision_details(scenario_key, factored_game_collisions, factored_game_traj_indices, factored_game_emergency_breaking_trajs, occlusion_caused_collisions, NE_OCC_DB):
    conn = sqlite3.connect(NE_OCC_DB)
    cur = conn.cursor()
    cmd = f"UPDATE main.COLLISIONS SET FACTORED_GAME_COLLISIONS='{str(factored_game_collisions)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    cmd = f"UPDATE main.COLLISIONS SET FACTORED_GAME_TRAJ_INDICES='{str(factored_game_traj_indices)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    cmd = f"UPDATE main.COLLISIONS SET FACTORED_GAME_EMERGENCY_BRAKING_TRAJS='{str(factored_game_emergency_breaking_trajs)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    cmd = f"UPDATE main.COLLISIONS SET OCCLUSION_CAUSED_COLLISIONS='{str(occlusion_caused_collisions)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    conn.commit()
    conn.close()


def save_NE_maneuver_probabilities(scenario_key, maneuver_probabilities, NE_OCC_DB):
    conn = sqlite3.connect(NE_OCC_DB)
    cur = conn.cursor()
    cmd = f"UPDATE main.COLLISIONS SET MANEUVER_PROBABILITIES='{str(maneuver_probabilities)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    conn.commit()
    conn.close()


def save_NE_occlusion_caused_collisions(scenario_key, occlusion_caused_collisions, NE_OCC_DB):
    conn = sqlite3.connect(NE_OCC_DB)
    cur = conn.cursor()
    cmd = f"UPDATE main.COLLISIONS SET OCCLUSION_CAUSED_COLLISIONS='{str(occlusion_caused_collisions)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    conn.commit()
    conn.close()


def retrieve_NE_vehicle_hit_counts(scenario_key, NE_OCC_DB):
    conn = sqlite3.connect(NE_OCC_DB)
    cur = conn.cursor()
    cmd = f"SELECT VEHICLE_HIT_COUNTS FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    query_result = cur.fetchall()

    if query_result[0][0]:
        query_result = ast.literal_eval(query_result[0][0])
    else:
        query_result = query_result[0][0]
    conn.close()
    return query_result




def compute_NE_risk_metric(current_file_id, start_scenario_key=None, end_scenario_key=None, return_factored_game_info=False):
    NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

    conn_occ_db = sqlite3.connect(NE_OCC_DB)
    cur_occ_db = conn_occ_db.cursor()

    conn_db = sqlite3.connect(occlusion_constants.DB)
    cur_db = conn_db.cursor()

    execute_cmd = f"SELECT SCENARIO_KEY FROM OCCLUSION_SCENARIO_TRAJECTORIES"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    query_result = set(query_result)
    scenario_keys = [i[0] for i in query_result]
    scenario_keys.sort()

    if start_scenario_key != None:
        scenario_keys = scenario_keys[start_scenario_key:]

    # Retrieve all trajectories for the scenario
    for scenario_key in scenario_keys:

        if end_scenario_key != None and scenario_key >= end_scenario_key:
            return

        is_valid_occlusion_scenario = retrieve_NE_is_valid_occlusion_scenario(scenario_key, NE_OCC_DB)
        if is_valid_occlusion_scenario == 0:
            continue

        print(f"\n----SCENARIO KEY: {scenario_key}; CURRENT_FILE_ID: {current_file_id}----")
        # print(f"Note: Need to uncomment save_scenario_key_in_collisions.")
        save_NE_scenario_key_in_collisions(scenario_key, NE_OCC_DB)

        current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = retrieve_NE_all_maneuvers(current_file_id, scenario_key)

        subject_vehicle_id = ordered_vehicle_ids[0]
        occluding_vehicle_id = ordered_vehicle_ids[1]
        relevant_vehicle_ids = ordered_vehicle_ids[2:]

        hypergame_length = len(ordered_vehicle_ids)
        hypergame_maneuver_combinations = itertools.combinations(track_id_maneuver_pairs, hypergame_length)
        hypergame_maneuver_combinations = [i for i in hypergame_maneuver_combinations if not game_utilities.repeated_track_ids_in_maneuver_combination(i, hypergame_length)]

        scenario = setup_NE_scenario(current_file_id, current_time, subject_vehicle_id, relevant_vehicle_ids, occluding_vehicle_id=occluding_vehicle_id)

        # Retrieve occlusion scenarios
        # E.g., factored_occlusion_scenarios = {1: [3], -1: [], 3: [1]}, means vehicle 3 is 
        # occluded from 1 and vehicle 1 is occluded from 3, no vehicles are occluded from -1.


        factored_games = compute_NE_factored_games(scenario_key, NE_OCC_DB)

        # Remove any vehicle that is too far away
        # If a vehicle is too far then we don't include it in any of the games. This is fine for safety as it is too far away to effect
        # the outcome of the games.
        vehicles_too_far_away = []
        for vehicle in scenario:
            if len(factored_games[vehicle.id]) == len(ordered_vehicle_ids) - 1:
                vehicles_too_far_away.append(vehicle)

        if vehicles_too_far_away:   
            for vehicle in vehicles_too_far_away:
                scenario.remove(vehicle)
                ordered_vehicle_ids.remove(vehicle.id)
                factored_games.pop(vehicle.id)
            vehicles_too_far_away_ids = [vehicle.id for vehicle in vehicles_too_far_away]
            
            # Need to recompute factored games, taking into account the removed vehicles
            for track_id in ordered_vehicle_ids:
                factored_games[track_id] = [veh_id for veh_id in factored_games[track_id] if veh_id not in vehicles_too_far_away_ids]

        ordered_hypergame_maneuver_combinations = []
        # appended_maneuver_combinations
        # E.g., hypergame_maneuver_combinations: [((1, 'follow_lead'), (-1, 'proceed-turn'), (3, 'track_speed')), ((-1, 'proceed-turn'), (1, 'track_speed'), (3, 'track_speed'))
        for maneuver_combination in hypergame_maneuver_combinations:
            ordered_maneuver_combination = []
            for track_id in ordered_vehicle_ids:
                for id_maneuver in maneuver_combination:
                    if id_maneuver[0] == track_id:
                        ordered_maneuver_combination.append(id_maneuver)
                if ordered_maneuver_combination not in ordered_hypergame_maneuver_combinations:
                    ordered_hypergame_maneuver_combinations.append(ordered_maneuver_combination)

        # First we calculate the risk with the hypergame, where all vehicles are included:
        hypergame_payoff_dict = {}

        # This is dict where the key is the hypergame_maneuver_set and the value is the maneuver_to_trajectory_index_dict
        # We need this because the specific trajectory to represent each maneuver depends on the maneuver combination that the 
        # vehicles take.
        all_maneuver_to_trajectory_index_dict = {}
        for hypergame_maneuver_set in ordered_hypergame_maneuver_combinations:

            maneuver_to_trajectory_index_dict = {}
            vehicle_maneuver_dict = {}

            # A hypergame maneuver set is a combination of maneuvers for each vehicle in the scenario:
            # E.g., ((1,2), (2,3), (-3,1)) could be one combination of maneuvers for each vehicle in the scenario with vehicles 1, 2, and -3.

            for id_maneuver in hypergame_maneuver_set:
                vehicle_maneuver_dict[id_maneuver[0]] = [id_maneuver[1]]

            # vehicle maneuvers looks like: {1: ['track_speed', 'follow-lead'], 2: ['wait_for_oncoming_vehicle'], -3: ['follow-lead']}
            trajectories_dict = retrieve_NE_trajectories(current_file_id, scenario_key, ordered_vehicle_ids, vehicle_maneuver_dict)

            payoff_dict_key, payoff_dict_value, chosen_trajectories = game_utilities.compute_chosen_trajectories(current_file_id, trajectories_dict, subject_vehicle_id, ordered_vehicle_ids, vehicle_maneuver_dict)

            # This dictionary looks like: {(1, 'follow_lead'): (1, 'aggressive', 0), (-6, 'wait_for_lead_to_cross'): (-6, 'aggressive', 1), (3, 'track_speed'): (3, 'normal', 0)}
            # The key is a track_id, maneuver tuple, the value is a track_id, level, index tuple
            # The point of this dict is to recover the chosen trajectory for the maneuver to do a collision check and compute collision details
            for id_maneuver, chosen_trajectory_key in zip(hypergame_maneuver_set, list(chosen_trajectories.keys())):
                if id_maneuver not in list(maneuver_to_trajectory_index_dict.keys()):                        
                    maneuver_to_trajectory_index_dict[id_maneuver] = chosen_trajectory_key

            all_maneuver_to_trajectory_index_dict[tuple(hypergame_maneuver_set)] = maneuver_to_trajectory_index_dict
            hypergame_payoff_dict[payoff_dict_key] = payoff_dict_value


        if return_factored_game_info:
            return all_maneuver_to_trajectory_index_dict

        num_players = len(ordered_vehicle_ids)
        player_actions = [list(set([k[i] for k in hypergame_payoff_dict.keys()])) for i in np.arange(num_players)]  
   
        eq = equilibria_core.EquilibriaCore(num_players,hypergame_payoff_dict,len(hypergame_payoff_dict),player_actions[0],False)
        nash_equilibria = eq.calc_pure_strategy_nash_equilibrium_exhaustive()


        # We cannot play out this game since there are no nash equilibria
        if not nash_equilibria:
            print(f"No hypergame Nash Equilibria could be generated for scenario {scenario_key}. Skipping the scenario.")
            continue

        # This stores every vehicles maneuvers in the form
        # e.g., {1: ['track_speed', 'follow-lead'], 2: ['follow-lead']}
        vehicle_maneuvers = {}
        for index in range(len(ordered_vehicle_ids)):
            track_id = ordered_vehicle_ids[index]
            vehicle_maneuvers[track_id] = []
            for maneuver_combination in  ordered_hypergame_maneuver_combinations:
                if maneuver_combination[index][1] not in vehicle_maneuvers[track_id]:
                    vehicle_maneuvers[track_id].append(maneuver_combination[index][1])

        # Compute maneuver probabilities for vehicles that have no occluded vehicles 
        hypergame_maneuver_probabilities = {}
        for track_id, maneuvers in vehicle_maneuvers.items():

            if factored_games[track_id]:
                continue

            subject_vehicle_maneuvers = {}
            subject_vehicle_maneuvers[track_id] = maneuvers
            maneuver_utilities = game_utilities.compute_maneuver_utilities(subject_vehicle_maneuvers, ordered_vehicle_ids, hypergame_payoff_dict, nash_equilibria)

            vehicle_maneuver_utilities = maneuver_utilities[track_id]
            vehicle_maneuver_probabilities = game_utilities.calculate_maneuver_probability(vehicle_maneuver_utilities)

            hypergame_maneuver_probabilities[track_id] = vehicle_maneuver_probabilities


        # vehicle maneuvers looks like: {1: ['track_speed', 'follow-lead'], 2: ['wait_for_oncoming_vehicle'], -3: ['follow-lead']}
        all_trajectories_dict = retrieve_NE_trajectories(current_file_id, scenario_key, ordered_vehicle_ids, vehicle_maneuvers)
 
        # Assign leading vehicle attribute
        for vehicle in scenario:
            lead_vehicle_id = scenario_utilities.get_leader_id(vehicle, scenario)
            if lead_vehicle_id:
                for v in scenario:
                    if v.id == lead_vehicle_id:
                        vehicle.set_leading_vehicle(v)
            else:
                vehicle.set_leading_vehicle(None)

        # Note: this requires the original scenario and so needs to be above the has_oncoming assignment below
        factored_game_details = compute_NE_ground_truth_maneuvers_for_factored_games(current_file_id, scenario_key, scenario, ordered_vehicle_ids, factored_games, track_id_maneuver_pairs)
            
        # This is the case if no Nash Equilibria could be found for one of the factored games
        if not factored_game_details:
            print(f"No Nash Equilibria could be found for one of the factored games for scenario {scenario_key}. Skipping the scenario.")
            continue

        factored_ground_truth_maneuvers, factored_game_maneuver_probabilities = factored_game_details
        print(f"FACTORED GAME GROUND-TRUTH MANEUVERS: {factored_ground_truth_maneuvers}")
        factored_ground_truth_maneuver_sets = game_utilities.compute_ground_truth_maneuver_sets(factored_ground_truth_maneuvers, hypergame_length)
        print(f"BEFORE: FACTORED GROUND TRUTH MANEUVER SETS: {factored_ground_truth_maneuver_sets}")

        # Update factored_game_maneuver_probabilities so that it contains all maneuver probabilities
        factored_game_maneuver_probabilities.update(hypergame_maneuver_probabilities)
 
        # Assign has_oncoming_vehicle attribute for vehicles in scenario
        for i in range(0, len(scenario)-1):
            for j in range(i, len(scenario)):
                vehicle_a, vehicle_b = scenario[i], scenario[j]
                has_oncoming_vehicle = scenario_utilities.compute_has_oncoming_vehicle(vehicle_a, vehicle_b)
                if has_oncoming_vehicle:
                    if not vehicle_a.has_oncoming_vehicle:
                        vehicle_a.set_has_oncoming_vehicle(True)
                    if not vehicle_b.has_oncoming_vehicle:
                        vehicle_b.set_has_oncoming_vehicle(True)

        hypergame_ground_truth_maneuvers = game_utilities.compute_ground_truth_maneuvers(ordered_vehicle_ids, scenario, nash_equilibria, hypergame_payoff_dict, factored_game_subject_id=None)
        hypergame_ground_truth_maneuver_sets = game_utilities.compute_ground_truth_maneuver_sets(hypergame_ground_truth_maneuvers, hypergame_length)
        # print(f"HYPERGAME GROUND-TRUTH MANEUVERS: {hypergame_ground_truth_maneuvers}")
        # print(f"HYPERGAME GROUND-TRUTH MANEUVER SETS: {hypergame_ground_truth_maneuver_sets}")

        for factored_ground_truth_maneuvers in factored_ground_truth_maneuver_sets:
            for track_id, maneuver in factored_ground_truth_maneuvers.items():
                if maneuver == 'hypergame_maneuver':
                    factored_ground_truth_maneuvers[track_id] = hypergame_ground_truth_maneuvers[track_id].copy().pop()

        print(f"AFTER: FACTORED GAME GROUND-TRUTH MANEUVER SETS: {factored_ground_truth_maneuver_sets}")

        # Calculate all collisions for hypergame
        print(f"COMPUTING ALL HYPERGAME COLLISIONS")
        all_hypergame_filtered_collisions = {}
        all_true_hypergame_emergency_braking_trajectories = {}
        all_hypergame_maneuver_to_trajectory_index_dict = {}

        for hypergame_ground_truth_maneuvers in hypergame_ground_truth_maneuver_sets:

            # To identify different collisions caused by different ground truth maneuver sets
            # The key to the dict says the maneuver set: ((119, 2), (-2047, 6), (100, 2), (125, 3))
            collision_identifier = [(track_id, occlusion_constants.L1_ACTION_CODES[maneuver]) for track_id, maneuver in hypergame_ground_truth_maneuvers.items()]
            collision_identifier = tuple(collision_identifier)

            hypergame_ground_truth_maneuvers_set = tuple([(track_id, maneuver) for track_id, maneuver in hypergame_ground_truth_maneuvers.items()])

            # For hypergame, find ground truth maneuver_to_trajectory_index_dict: which is called hypergame_maneuver_to_trajectory_index_dict
            for maneuver_combination, maneuver_to_trajectory_index_dict in all_maneuver_to_trajectory_index_dict.items():
                if hypergame_ground_truth_maneuvers_set == maneuver_combination:
                    hypergame_maneuver_to_trajectory_index_dict = maneuver_to_trajectory_index_dict
                    break

            all_hypergame_maneuver_to_trajectory_index_dict[collision_identifier] = hypergame_maneuver_to_trajectory_index_dict
            hypergame_all_collisions, hypergame_emergency_braking_trajectories = game_utilities.compute_all_collisions(current_file_id, ordered_vehicle_ids, hypergame_ground_truth_maneuvers, all_trajectories_dict, hypergame_maneuver_to_trajectory_index_dict, scenario)
            hypergame_filtered_collisions, true_hypergame_emergency_braking_trajectories = game_utilities.filter_collisions(ordered_vehicle_ids, hypergame_all_collisions, hypergame_emergency_braking_trajectories)
            
            if hypergame_filtered_collisions:
                all_hypergame_filtered_collisions[collision_identifier] = hypergame_filtered_collisions
                all_true_hypergame_emergency_braking_trajectories[collision_identifier] = true_hypergame_emergency_braking_trajectories

        # Calculate all collisions for factored game
        print(f"COMPUTING ALL FACTORED GAME COLLISIONS")
        all_factored_game_filtered_collisions = {}
        all_true_factored_game_emergency_braking_trajectories = {}
        all_factored_game_maneuver_to_trajectory_index_dict = {}

        for factored_ground_truth_maneuvers in factored_ground_truth_maneuver_sets:

            # To identify different collisions caused by different ground truth maneuver sets
            # The key to the dict says the maneuver set: ((119, 2), (-2047, 6), (100, 2), (125, 3))
            collision_identifier = [(track_id, occlusion_constants.L1_ACTION_CODES[maneuver]) for track_id, maneuver in factored_ground_truth_maneuvers.items()]
            collision_identifier = tuple(collision_identifier)

            factored_ground_truth_maneuvers_set = tuple([(track_id, maneuver) for track_id, maneuver in factored_ground_truth_maneuvers.items()])
        
            for maneuver_combination, maneuver_to_trajectory_index_dict in all_maneuver_to_trajectory_index_dict.items():
                if factored_ground_truth_maneuvers_set == maneuver_combination:
                    factored_game_maneuver_to_trajectory_index_dict = maneuver_to_trajectory_index_dict
                    break


            all_factored_game_maneuver_to_trajectory_index_dict[collision_identifier] = factored_game_maneuver_to_trajectory_index_dict
            factored_game_all_collisions, factored_game_emergency_braking_trajectories = game_utilities.compute_all_collisions(current_file_id, ordered_vehicle_ids, factored_ground_truth_maneuvers, all_trajectories_dict, factored_game_maneuver_to_trajectory_index_dict, scenario)

            factored_game_filtered_collisions, true_factored_game_emergency_braking_trajectories = game_utilities.filter_collisions(ordered_vehicle_ids, factored_game_all_collisions, factored_game_emergency_braking_trajectories)

            if factored_game_filtered_collisions:
                all_factored_game_filtered_collisions[collision_identifier] = factored_game_filtered_collisions
                all_true_factored_game_emergency_braking_trajectories[collision_identifier] = true_factored_game_emergency_braking_trajectories

        print(f"ALL FACTORED GAME FILTERED COLLISIONS")
        print(all_factored_game_filtered_collisions)

        if all_hypergame_filtered_collisions:

            all_hypergame_trajectory_indices = {}
            for collision_identifier, hypergame_filtered_collisions in all_hypergame_filtered_collisions.items():
                # Convert maneuvers to coded form since SQL cannot store text in the form 'text'
                # [track_id_a, track_id_b, collision_id, 'emergency_braking', 'emergency_braking', maneuver_a, maneuver_b, impact_velocity_a, impact_velocity_b, time_to_collision]
                for collision in hypergame_filtered_collisions:
                    collision[3] = occlusion_constants.L1_ACTION_CODES[collision[3]]
                    collision[4] = occlusion_constants.L1_ACTION_CODES[collision[4]]
                    collision[5] = occlusion_constants.L1_ACTION_CODES[collision[5]]
                    collision[6] = occlusion_constants.L1_ACTION_CODES[collision[6]]

                # hypergame_maneuver_to_trajectory_index_dict: {(11, 'follow_lead_into_intersection'): (11, 2), (-8, 'wait_for_lead_to_cross'): (-8, 1), (3, 'track_speed'): (3, 2), (1, 'follow_lead'): (1, 1), (20, 'track_speed'): (20, 2)}
                hypergame_trajectory_indices = [] # {track_id: {maneuver_code: [traj_level_code, traj_index]}
                hypergame_maneuver_to_trajectory_index_dict = all_hypergame_maneuver_to_trajectory_index_dict[collision_identifier]
                for id_maneuver, trajectory_index in hypergame_maneuver_to_trajectory_index_dict.items():
                    track_id = id_maneuver[0]
                    coded_maneuver = occlusion_constants.L1_ACTION_CODES[id_maneuver[1]]
                    hypergame_trajectory_indices.append([track_id, coded_maneuver, trajectory_index[1]])

                all_hypergame_trajectory_indices[collision_identifier] = hypergame_trajectory_indices

            # Save hypergame collision
            save_NE_hypergame_collision_details(scenario_key, all_hypergame_filtered_collisions, all_hypergame_trajectory_indices, all_true_hypergame_emergency_braking_trajectories, NE_OCC_DB)


        if all_factored_game_filtered_collisions:

            # Same format as factored_game_maneuver_probabilities but 
            # maneuvers are coded, e.g., {track_id: {maneuver_code: prob.}, ...}
            all_coded_maneuver_probabilities = {} 
            for track_id, maneuver_probabilities in factored_game_maneuver_probabilities.items():
                coded_maneuver_probabilities = {}
                for maneuver, probability in maneuver_probabilities.items():
                    coded_maneuver = occlusion_constants.L1_ACTION_CODES[maneuver]
                    coded_maneuver_probabilities[coded_maneuver] = probability
                all_coded_maneuver_probabilities[track_id] = coded_maneuver_probabilities
            # The probabilities are not for each vehicle, only for the vehicles that have 
            # occluded vehicles, otherwise the actions for the hypergame are used.
            print(f"CODED MANEUVER PROBABILITIES: {all_coded_maneuver_probabilities}")

            occlusion_caused_collisions = {}
            all_factored_game_trajectory_indices = {}
            for collision_identifier, factored_game_filtered_collisions in all_factored_game_filtered_collisions.items():
                occlusion_caused_collisions[collision_identifier] = {}
                # Convert maneuvers to coded form since SQL cannot store text in the form 'text'
                for collision in factored_game_filtered_collisions:
                    collision[3] = occlusion_constants.L1_ACTION_CODES[collision[3]]
                    collision[4] = occlusion_constants.L1_ACTION_CODES[collision[4]]
                    collision[5] = occlusion_constants.L1_ACTION_CODES[collision[5]]
                    collision[6] = occlusion_constants.L1_ACTION_CODES[collision[6]]

                    collision_id = collision[2]
                    factored_game = compute_NE_factored_games(scenario_key, NE_OCC_DB)
                    track_id_a = collision[0]
                    track_id_b = collision[1]
                    vehicle_a_occluded_vehicles = factored_game[track_id_a]
                    vehicle_b_occluded_vehicles = factored_game[track_id_b]
                    if track_id_b in vehicle_a_occluded_vehicles or track_id_a in vehicle_b_occluded_vehicles:
                        occlusion_caused_collisions[collision_identifier][collision_id] = 1
                    else:
                        occlusion_caused_collisions[collision_identifier][collision_id] = 0

                factored_game_trajectory_indices = [] # {track_id: {maneuver_code: [traj_level_code, traj_index]}
                factored_game_maneuver_to_trajectory_index_dict = all_factored_game_maneuver_to_trajectory_index_dict[collision_identifier]

                for id_maneuver, trajectory_index in factored_game_maneuver_to_trajectory_index_dict.items():
                    track_id = id_maneuver[0]
                    coded_maneuver = occlusion_constants.L1_ACTION_CODES[id_maneuver[1]]
                    factored_game_trajectory_indices.append([track_id, coded_maneuver, trajectory_index[1]])

                all_factored_game_trajectory_indices[collision_identifier] = factored_game_trajectory_indices

            save_NE_factored_game_collision_details(scenario_key, all_factored_game_filtered_collisions, all_factored_game_trajectory_indices, \
                                                                    all_true_factored_game_emergency_braking_trajectories, occlusion_caused_collisions, NE_OCC_DB)
            save_NE_maneuver_probabilities(scenario_key, all_coded_maneuver_probabilities, NE_OCC_DB)



def compute_NE_factored_games(scenario_key, NE_OCC_DB):
    vehicle_hit_counts = retrieve_NE_vehicle_hit_counts(scenario_key, NE_OCC_DB)
    occluded_vehicles_dict = {}
    for track_id, hit_counts in vehicle_hit_counts.items():
        occluded_vehicles = game_utilities.compute_factored_game(hit_counts)
        occluded_vehicles_dict[track_id] = occluded_vehicles
    return occluded_vehicles_dict




def compute_NE_ground_truth_maneuvers_for_factored_games(current_file_id, scenario_key, scenario, ordered_vehicle_ids, factored_games, track_id_maneuver_pairs):
    ground_truth_maneuvers = {}

    # We don't allow any vehicle to use a follow maneuver if it's occluded vehicle includes it's lead vehicle
    # This may result in an error if the vehicle only can use follow maneuvers. Though they if they use follow-lead
    # they should also be able to use track_speed.
    # See if lead vehicle is occluded 

    lead_vehicle_occluded = [] # Record vehicle ids with lead vehicles occluded
    for vehicle in scenario:
        if vehicle.leading_vehicle:
            if vehicle.leading_vehicle.id in factored_games[vehicle.id]:
                lead_vehicle_occluded.append(vehicle.id)

    maneuver_probabilities = {}

    for subject_vehicle_id, occluded_vehicles in factored_games.items():
        # print(f"subject_vehicle_id: {subject_vehicle_id}")
        # print(f"occluded_vehicles: {occluded_vehicles}")

        if not occluded_vehicles:
            ground_truth_maneuvers[subject_vehicle_id] = {'hypergame_maneuver'}
            continue

        # Get factored ordered_vehicle_ids for this particular occlusion scenario
        factored_ordered_vehicle_ids = [subject_vehicle_id]
        for track_id in ordered_vehicle_ids:
            if track_id != subject_vehicle_id and track_id not in occluded_vehicles:
                factored_ordered_vehicle_ids.append(track_id)

        game_length = len(factored_ordered_vehicle_ids)

        # Get the track_id_maneuver_pairs for this factored game
        factored_track_id_maneuver_pairs = []
        for track_id_maneuver_pair in track_id_maneuver_pairs:
            if track_id_maneuver_pair[0] in factored_ordered_vehicle_ids:

                # The vehicle can't use follow maneuvers if it's lead vehicle is occluded
                if track_id_maneuver_pair[0] in lead_vehicle_occluded and track_id_maneuver_pair[1] in occlusion_constants.FOLLOW_ACTIONS:
                   continue

                factored_track_id_maneuver_pairs.append(track_id_maneuver_pair)

        maneuver_combinations = itertools.combinations(factored_track_id_maneuver_pairs, game_length)
        maneuver_combinations = [i for i in maneuver_combinations if not game_utilities.repeated_track_ids_in_maneuver_combination(i, game_length)]

        # Order maneuver combination so order of vehicles is consistent with factored_order_vehicle_ids
        ordered_maneuver_combinations = []
        for maneuver_combination in maneuver_combinations:
            ordered_combination = []
            for track_id in factored_ordered_vehicle_ids:
                for id_maneuver in maneuver_combination:
                    if id_maneuver[0] == track_id:
                        ordered_combination.append(id_maneuver)
                if ordered_combination not in ordered_maneuver_combinations:
                    ordered_maneuver_combinations.append(ordered_combination)

        factored_payoff_dict = {}
        for maneuver_combination in ordered_maneuver_combinations:
            vehicle_maneuver_dict = {}
            for id_maneuver in maneuver_combination:
                vehicle_maneuver_dict[id_maneuver[0]] = [id_maneuver[1]]
            # vehicle maneuvers looks like: {1: ['track_speed', 'follow-lead'], 2: ['wait_for_oncoming_vehicle'], -3: ['follow-lead']}
           
            trajectories_dict = retrieve_NE_trajectories(current_file_id, scenario_key, factored_ordered_vehicle_ids, vehicle_maneuver_dict)

            factored_payoff_dict_key, factored_payoff_dict_value, chosen_trajectories = game_utilities.compute_chosen_trajectories(current_file_id, trajectories_dict, subject_vehicle_id, factored_ordered_vehicle_ids, vehicle_maneuver_dict)

            factored_payoff_dict[factored_payoff_dict_key] = factored_payoff_dict_value

        factored_num_players = len(factored_ordered_vehicle_ids)
        factored_player_actions = [list(set([k[i] for k in factored_payoff_dict.keys()])) for i in np.arange(factored_num_players)]
        
        eq = equilibria_core.EquilibriaCore(factored_num_players,factored_payoff_dict,len(factored_payoff_dict),factored_player_actions[0],False)
        factored_nash_equilibria = eq.calc_pure_strategy_nash_equilibrium_exhaustive()

        if not factored_nash_equilibria:
            return None

        # Assemble scenario
        factored_scenario = deepcopy(scenario)
        factored_scenario = [v for v in factored_scenario if v.id in factored_ordered_vehicle_ids]

        # Assign leading vehicle attribute
        for vehicle in factored_scenario:
            lead_vehicle_id = scenario_utilities.get_leader_id(vehicle, factored_scenario)
            if lead_vehicle_id:
                for v in factored_scenario:
                    if v.id == lead_vehicle_id:
                        vehicle.set_leading_vehicle(v)
            else:
                vehicle.set_leading_vehicle(None)

        for i in range(0, len(factored_scenario)-1):
            for j in range(i, len(factored_scenario)):
                vehicle_a, vehicle_b = factored_scenario[i], factored_scenario[j]
                has_oncoming_vehicle = scenario_utilities.compute_has_oncoming_vehicle(vehicle_a, vehicle_b)
                if has_oncoming_vehicle:
                    if not vehicle_a.has_oncoming_vehicle:
                        vehicle_a.set_has_oncoming_vehicle(True)
                    if not vehicle_b.has_oncoming_vehicle:
                        vehicle_b.set_has_oncoming_vehicle(True)

        computed_maneuvers = game_utilities.compute_ground_truth_maneuvers(factored_ordered_vehicle_ids, factored_scenario, factored_nash_equilibria, factored_payoff_dict, factored_game_subject_id=subject_vehicle_id)

        ground_truth_maneuvers[subject_vehicle_id] = computed_maneuvers[subject_vehicle_id]

        # Get all of subject vehicle's factored game maneuvers
        # ['76900700004', '76900700003']
        subject_vehicle_maneuver_action_codes = factored_player_actions[0]

        maneuvers = []
        for action_code in subject_vehicle_maneuver_action_codes:
            track_id, maneuver_code = game_utilities.extract_id_and_maneuver_from_action_code(action_code)
            maneuver = occlusion_constants.L1_MANEUVERS[maneuver_code]
            maneuvers.append(maneuver)

        subject_vehicle_maneuvers = {}
        subject_vehicle_maneuvers[subject_vehicle_id] = maneuvers
        maneuver_utilities = game_utilities.compute_maneuver_utilities(subject_vehicle_maneuvers, factored_ordered_vehicle_ids, factored_payoff_dict, factored_nash_equilibria)

        vehicle_maneuver_utilities = maneuver_utilities[subject_vehicle_id]
        vehicle_maneuver_probabilities = game_utilities.calculate_maneuver_probability(vehicle_maneuver_utilities)

        maneuver_probabilities[subject_vehicle_id] = vehicle_maneuver_probabilities

    return ground_truth_maneuvers, maneuver_probabilities



# Animates all collisions that occur for scenario key
def animate_factored_game_collision(current_file_id, scenario_key, collision_configuration):

    NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
    conn_occ_db = sqlite3.connect(NE_OCC_DB)
    cur_occ_db = conn_occ_db.cursor()


    # Animate all factored game collisions
    execute_cmd = f"SELECT FACTORED_GAME_COLLISIONS FROM COLLISIONS WHERE SCENARIO_KEY={scenario_key}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    all_factored_game_collisions = ast.literal_eval(query_result[0][0])

    # Get trajectory indices for each vehicle in scenario
    execute_cmd = f"SELECT FACTORED_GAME_TRAJ_INDICES FROM COLLISIONS WHERE SCENARIO_KEY={scenario_key}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    all_factored_game_traj_indices = ast.literal_eval(query_result[0][0])

    # Get emergency braking trajectories for both colliding vehicles
    execute_cmd = f"SELECT FACTORED_GAME_EMERGENCY_BRAKING_TRAJS FROM COLLISIONS WHERE SCENARIO_KEY={scenario_key}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    all_factored_game_emergency_braking_trajs = ast.literal_eval(query_result[0][0])


    for collision_identifier, factored_game_collisions in all_factored_game_collisions.items():
        factored_game_traj_indices = all_factored_game_traj_indices[collision_identifier]
        factored_game_emergency_braking_trajs = all_factored_game_emergency_braking_trajs[collision_identifier]

        for collision in factored_game_collisions:

            track_id_a = collision[0]
            track_id_b = collision[1]
            collision_id = collision[2]


            # time_to_collision_index = 50
            selected_trajectories = {}

            selected_trajectories[track_id_a] = factored_game_emergency_braking_trajs[collision_id][0]
            selected_trajectories[track_id_b] = factored_game_emergency_braking_trajs[collision_id][1]

            collision_details = scenario_utilities.collision_check(selected_trajectories[track_id_a], selected_trajectories[track_id_b])

            # time_to_collision_index = int(collision[-1] * 10)
            time_to_collision_index = collision_details[-1]
            print(f"TIME TO COLLISION INDEX: {time_to_collision_index}")

            # Get trajectories for other vehicles
            current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs = retrieve_NE_all_maneuvers(current_file_id, scenario_key)
            all_trajectories = retrieve_NE_trajectories(current_file_id, scenario_key, ordered_vehicle_ids, vehicle_maneuvers)

            for track_id in ordered_vehicle_ids:
           
                if track_id != track_id_a and track_id != track_id_b:
                    traj_maneuver_and_index = [manv_index for manv_index in factored_game_traj_indices if manv_index[0] == track_id][0]
                    vehicle_maneuver = occlusion_constants.L1_MANEUVERS[traj_maneuver_and_index[1]]
                    selected_trajectories[track_id] = all_trajectories[track_id][vehicle_maneuver][traj_maneuver_and_index[-1]]

                traj_maneuver_and_index = [manv_index for manv_index in factored_game_traj_indices if manv_index[0] == track_id][0]
                vehicle_maneuver = occlusion_constants.L1_MANEUVERS[traj_maneuver_and_index[1]]
                selected_trajectories[track_id] = all_trajectories[track_id][vehicle_maneuver][traj_maneuver_and_index[-1]]

            index = 10
            plot_NE_occlusion_scenario_at_traj_index(current_file_id, scenario_key, index, track_id_a, track_id_b, selected_trajectories)
            # animate_NE_collision_helper(current_file_id, scenario_key, collision_identifier, collision_id, selected_trajectories, time_to_collision_index, (track_id_a, track_id_b), collision_configuration) 


def plot_NE_occlusion_scenario_at_traj_index(current_file_id, scenario_key, index, track_id_a, track_id_b, selected_trajectories):

    scenario = []
    for track_id, trajectory in selected_trajectories.items():
        timestep = trajectory[index]
        v = VehicleState()
        v.set_id(track_id)
        v.set_x(timestep[1])
        v.set_y(timestep[2])
        v.set_angle(scenario_utilities.angle_check(timestep[-2]))
        scenario.append(v)

    occ_map = occupancy_map.OccupancyMap()
    occ_map.make_occupancy_map(scenario, pass_bb_box_back=False)
    occ_map.default_visualize_occupancy_map(vehicles=scenario, filename=f"Occlusion Scenario {current_file_id} {scenario_key} Initial State.pdf")

def animate_NE_collision_helper(current_file_id, scenario_key, collision_identifier, collision_id, selected_trajectories, time_to_collision_index, colliding_vehicle_ids, collision_configuration):
    x_values_dict = {}
    y_values_dict = {}
    angles_dict = {}
    car_list = []

    x = occlusion_constants.UNI_WEBER_INTERSECTION_DIMENSIONS['x']
    y = occlusion_constants.UNI_WEBER_INTERSECTION_DIMENSIONS['y']
    intersection_box = Polygon([[x[0],y[0]], [x[0],y[1]], [x[1],y[1]], [x[1],y[0]]])

    for track_id, trajectory in selected_trajectories.items():
        car, = plt.plot([], [], 'k')
        car_list.append(car)
        x_values = [timestep[1] for timestep in trajectory]
        y_values = [timestep[2] for timestep in trajectory]
        angles = [scenario_utilities.angle_check(timestep[-2]) for timestep in trajectory]

        x_values_dict[track_id] = x_values
        y_values_dict[track_id] = y_values
        angles_dict[track_id] = angles

    fig, ax = plt.subplots()

    for sequence in occlusion_constants.SEGMENT_SEQUENCES:
        centreline = scenario_utilities.get_centreline_from_segment_sequence(sequence[0])
        x_points = [i[0] for i in centreline]
        y_points = [i[1] for i in centreline]
        ax.plot(x_points,y_points, 'black', alpha=0.4, linewidth=0.2)


    def init():
        ax.set_xlim(538775, 538900)
        ax.set_ylim(4813975, 4814050)
        for sequence in occlusion_constants.SEGMENT_SEQUENCES:
            centreline = scenario_utilities.get_centreline_from_segment_sequence(sequence[0])
            x_points = [i[0] for i in centreline]
            y_points = [i[1] for i in centreline]
            ax.plot(x_points,y_points, 'black', alpha=0.4, linewidth=0.2)
        return car_list

    def update(frame):
        fig.clear()
        plt.title(f"Scenario {scenario_key}; Collision ID: {collision_id}")
        plt.title(f"Synthetic Scenario {scenario_key}: {collision_configuration} Collision", y=1.05)
        for sequence in occlusion_constants.SEGMENT_SEQUENCES:
            centreline = scenario_utilities.get_centreline_from_segment_sequence(sequence[0])
            x_points = [i[0] for i in centreline]
            y_points = [i[1] for i in centreline]
            plt.plot(x_points,y_points, 'black', alpha=0.4, linewidth=0.2)
        
        car_list = []
        for track_id in list(selected_trajectories.keys()):
            if frame < len(x_values_dict[track_id]) - 1:
                if frame >= time_to_collision_index:
                    v = VehicleState()
                    v.set_id(track_id)
                    v.set_x(x_values_dict[track_id][time_to_collision_index])
                    v.set_y(y_values_dict[track_id][time_to_collision_index])
                    v.set_angle(angles_dict[track_id][time_to_collision_index])
                    bounding_box = Polygon(v.get_bounding_box_points(for_occ_map=False))

                    if intersection_box.contains(bounding_box):
                        x,y = bounding_box.exterior.xy

                        if track_id in colliding_vehicle_ids:
                            car, = plt.plot(x,y, 'r')
                            label = plt.text(x[0],y[0], f"{track_id}")
                            car_list.append(car)
                            car_list.append(label)
                        else:
                            car, = plt.plot(x,y, 'k')
                            label = plt.text(x[0],y[0], f"{track_id}")
                            car_list.append(car)
                            car_list.append(label)
                else:
                    v = VehicleState()
                    v.set_id(track_id)
                    v.set_x(x_values_dict[track_id][frame])
                    v.set_y(y_values_dict[track_id][frame])
                    v.set_angle(angles_dict[track_id][frame])
                    bounding_box = Polygon(v.get_bounding_box_points(for_occ_map=False))

                    if intersection_box.contains(bounding_box):
                        x,y = bounding_box.exterior.xy
                        car, = plt.plot(x,y, 'k')
                        label = plt.text(x[0],y[0], f"{track_id}")
                        car_list.append(car)
                        car_list.append(label)
        return car_list


    ani = animation.FuncAnimation(fig, update, frames=time_to_collision_index + 10,
                        init_func=init, interval=100, blit=True)

    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=500)

    if not os.path.isdir(f"NE_collision_animations/{current_file_id}"):
        os.makedirs(f"NE_collision_animations/{current_file_id}")
    ani.save(f'NE_collision_animations/{current_file_id}/NE_Collision_{scenario_key}_{collision_id}_{collision_identifier}.mp4', writer=writer)
    plt.close()



def retrieve_NE_all_maneuvers(current_file_id, scenario_key):
    NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

    conn_occ_db = sqlite3.connect(NE_OCC_DB)
    cur_occ_db = conn_occ_db.cursor()

    conn_db = sqlite3.connect(occlusion_constants.DB)
    cur_db = conn_db.cursor()

    execute_cmd = f"SELECT SUBJECT_VEHICLE_ID, OCCLUDING_VEHICLE_ID, TIME FROM ALL_SYNTHETIC_SCENARIOS WHERE SCENARIO_KEY={scenario_key}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()

    subject_vehicle_id = query_result[0][0]
    occluding_vehicle_id = query_result[0][1]
    current_time = query_result[0][2]

    execute_cmd = f"SELECT RELEV_AGENT_IDS FROM RELEVANT_AGENTS WHERE TIME={current_time} AND TRACK_ID={subject_vehicle_id}"
    cur_db.execute(execute_cmd)
    query_result = cur_db.fetchall()
    relevant_vehicle_ids = ast.literal_eval(query_result[0][0])

    ordered_vehicle_ids = [subject_vehicle_id, occluding_vehicle_id]
    for track_id in relevant_vehicle_ids:
        ordered_vehicle_ids.append(track_id)

    # Get all maneuvers
    execute_cmd = f"SELECT TRACK_ID, MANEUVER FROM OCCLUSION_SCENARIO_TRAJECTORIES WHERE SCENARIO_KEY={scenario_key}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    track_id_maneuver_pairs_occlusion_scenario = list(set(query_result))


    execute_cmd = f"SELECT TRACK_ID, MANEUVER FROM BASE_SCENARIO_TRAJECTORIES WHERE MIN_SCENARIO_KEY <= {scenario_key} AND MAX_SCENARIO_KEY >= {scenario_key}"
    cur_occ_db.execute(execute_cmd)
    query_result = cur_occ_db.fetchall()
    track_id_maneuver_pairs_base_scenario = list(set(query_result))

    track_id_maneuver_pairs = track_id_maneuver_pairs_occlusion_scenario + track_id_maneuver_pairs_base_scenario

    vehicle_maneuvers = {}
    for track_id in ordered_vehicle_ids:
        vehicle_maneuvers[track_id] = []
        for track_id_maneuver in track_id_maneuver_pairs:
            if track_id_maneuver[0] == track_id:
                vehicle_maneuvers[track_id].append(track_id_maneuver[1])

    return current_time, ordered_vehicle_ids, vehicle_maneuvers, track_id_maneuver_pairs
