
'''
Created on May 10, 2021

@author: Maximilian Kahn, Atrisha Sarkar
'''

import math

SEGMENT_MAP = {
               'prep-turn_s':'prep-left-turn',
               'prep-turn_n':'prep-left-turn',
               'prep-turn_e':'prep-left-turn',
               'exec-turn_e':'exec-left-turn',
               'exec-turn_n':'exec-left-turn',
               'exec-turn_s':'exec-left-turn',
               'prep-turn_w':'prep-left-turn',
               'exec-turn_w':'exec-left-turn',
               'rt_prep-turn_s':'prep-right-turn',
               'rt_exec-turn_s':'exec-right-turn',
               'rt_prep-turn_w':'prep-right-turn',
               'rt_exec-turn_w':'exec-right-turn',
               'ln_w_-2':'exit-lane',
               'ln_w_-1':'exit-lane',
               'ln_w_1':'left-turn-lane',
               'ln_w_2':'through-lane-entry',
               'ln_w_3':'through-lane-entry',
               'ln_w_4':'right-turn-lane',
               'ln_n_-2':'exit-lane',
               'ln_n_-1':'exit-lane',
               'ln_n_1':'left-turn-lane',
               'ln_n_2':'through-lane-entry',
               'ln_n_3':'through-lane-entry',
               'ln_s_-2':'exit-lane',
               'ln_s_-1':'exit-lane',
               'ln_s_1':'left-turn-lane',
               'ln_s_2':'through-lane-entry',
               'ln_s_3':'through-lane-entry',
               'ln_s_4':'right-turn-lane',
               'ln_e_-2':'exit-lane',
               'ln_e_-1':'exit-lane',
               'ln_e_1':'left-turn-lane',
               'ln_e_2':'through-lane-entry',
               'ln_e_3':'through-lane-entry',
               'l_n_s_l':'through-lane',
               'l_n_s_r':'through-lane',
               'l_s_n_l':'through-lane',
               'l_s_n_r':'through-lane',
               'l_e_w_l':'through-lane',
               'l_e_w_r':'through-lane',
               'l_w_e_l':'through-lane',
               'l_w_e_r':'through-lane',
               }



# CONFLICT_SEQUENCES stores pairs of conflicting segment sequences and their conflict point
CONFLICT_SEQUENCES = [[[['ln_w_4', 'rt_prep-turn_w', 'rt_exec-turn_w', 'ln_s_-2'], ['ln_n_3', 'l_n_s_r', 'ln_s_-2']], (538841.0927577105, 4813990.578187239)], # Right turn: W_S / N_S
                   [[['ln_s_4', 'rt_prep-turn_s', 'rt_exec-turn_s', 'ln_e_-2'], ['ln_w_3', 'l_w_e_r', 'ln_e_-2']], (538863.0295554019, 4814018.65757729)],  # Right turn: S_E / W_E
                   [[['ln_s_1', 'prep-turn_s', 'exec-turn_s', 'ln_w_-1'], ['ln_n_2', 'l_n_s_l', 'ln_s_-1']], (538833.6347358769, 4814007.2434756635)],      # Left turn: S_W / N_S
                   [[['ln_s_1', 'prep-turn_s', 'exec-turn_s', 'ln_w_-1'], ['ln_n_3', 'l_n_s_r', 'ln_s_-2']], (538828.4254387476, 4814008.849003818)],       # Left turn: S_W / N_S
                   [[['ln_s_2', 'prep-turn_s', 'exec-turn_s', 'ln_w_-1'], ['ln_n_2', 'l_n_s_l', 'ln_s_-1']], (538834.2024242199, 4814006.428932144)],       # Left turn: S_W / N_S
                   [[['ln_s_2', 'prep-turn_s', 'exec-turn_s', 'ln_w_-1'], ['ln_n_3', 'l_n_s_r', 'ln_s_-2']], (538828.3581240346, 4814008.945906498)],       # Left turn: S_W / N_S
                   [[['ln_s_1', 'prep-turn_s', 'exec-turn_s', 'ln_w_-2'], ['ln_n_2', 'l_n_s_l', 'ln_s_-1']], (538833.2067990146, 4814007.85749778)],        # Left turn: S_W / N_S
                   [[['ln_s_1', 'prep-turn_s', 'exec-turn_s', 'ln_w_-2'], ['ln_n_3', 'l_n_s_r', 'ln_s_-2']], (538827.9055560263, 4814009.597200901)],       # Left turn: S_W / N_S
                   [[['ln_e_1', 'prep-turn_e', 'exec-turn_e', 'ln_s_-1'], ['ln_w_2', 'l_w_e_l', 'ln_e_-1']], (538842.1404339803, 4814011.999372717)],       # Left turn: E_S / W_E
                   [[['ln_e_1', 'prep-turn_e', 'exec-turn_e', 'ln_s_-1'], ['ln_w_3', 'l_w_e_r', 'ln_e_-2']], (538839.7091928337, 4814007.140550317)],       # Left turn: E_S / W_E
                   [[['ln_n_1', 'prep-turn_n', 'exec-turn_n', 'ln_e_-1'], ['ln_s_2', 'l_s_n_l', 'ln_n_-1']], (538835.9560044077, 4814015.936018244)],       # Left turn: N_E / S_N
                   [[['ln_n_1', 'prep-turn_n', 'exec-turn_n', 'ln_e_-1'], ['ln_s_3', 'l_s_n_r', 'ln_n_-2']], (538840.9359403204, 4814014.8426963845)],      # Left turn: N_E / S_N
                   [[['ln_n_1', 'prep-turn_n', 'exec-turn_n', 'ln_e_-2'], ['ln_s_2', 'l_s_n_l', 'ln_n_-1']], (538835.8981292074, 4814016.022297412)],       # Left turn: N_E / S_N
                   [[['ln_n_1', 'prep-turn_n', 'exec-turn_n', 'ln_e_-2'], ['ln_s_3', 'l_s_n_r', 'ln_n_-2']], (538840.9985219259, 4814014.746025487)],       # Left turn: N_E / S_N
                   [[['ln_w_1', 'prep-turn_w', 'exec-turn_w', 'ln_n_-1'], ['ln_e_2', 'l_e_w_l', 'ln_w_-1']], (538828.7687265406, 4814012.788464125)],       # Left turn: W_N / E_W 
                   [[['ln_w_1', 'prep-turn_w', 'exec-turn_w', 'ln_n_-1'], ['ln_e_3', 'l_e_w_r', 'ln_w_-2']], (538830.3883547819, 4814017.7229012335)],      # Left turn: W_N / E_W
                   [[['ln_w_1', 'prep-turn_w', 'exec-turn_w', 'ln_n_-2'], ['ln_e_2', 'l_e_w_l', 'ln_w_-1']], (538828.9848353311, 4814012.893624846)],       # Left turn: W_N / E_W
                   [[['ln_w_1', 'prep-turn_w', 'exec-turn_w', 'ln_n_-2'], ['ln_e_3', 'l_e_w_r', 'ln_w_-2']], (538830.2764849002, 4814017.6714544585)]]      # Left turn: W_N / E_W


# NOTE: I have removed the East to North turn lane (['ln_e_3','l_e_n','ln_n_-2'],'L_E_N')
# And the North to West turn lane (['ln_n_4','l_n_w','ln_w_-2'],'L_N_W')
# AND  (['ln_s_1','prep-turn_s','exec-turn_s','ln_w_-2'],'L_S_W') 
# IF I AM GETTING RELEVANT AGENTS LANE CHANGING THEN I SHOULD FILTER OUT RELEVANT AGENTS IN EITHER:
# (['ln_s_1','prep-turn_s','exec-turn_s','ln_w_-2'],'L_S_W') 
# OR (['ln_s_2','prep-turn_s','exec-turn_s','ln_w_-1'],'L_S_W')
SEGMENT_SEQUENCES =  [(['ln_s_4','rt_prep-turn_s','rt_exec-turn_s','ln_e_-2'], 'L_S_E'),	\
				(['ln_s_3','l_s_n_r','ln_n_-2'], 'L_S_N'),	\
				(['ln_s_2','l_s_n_l','ln_n_-1'], 'L_S_N'),
				(['ln_s_1','prep-turn_s','exec-turn_s','ln_w_-1'],'L_S_W'),	\
				(['ln_e_2','l_e_w_l','ln_w_-1'],'L_E_W'),	\
				(['ln_e_3','l_e_w_r','ln_w_-2'],'L_E_W'),	\
				(['ln_e_1','prep-turn_e','exec-turn_e','ln_s_-1'],'L_E_S'),	\
				(['ln_n_3','l_n_s_r','ln_s_-2'],'L_N_S'),	\
				(['ln_n_2','l_n_s_l','ln_s_-1'],'L_N_S'),	\
				(['ln_n_1','prep-turn_n','exec-turn_n','ln_e_-1'],'L_N_E'),	\
				(['ln_w_1','prep-turn_w','exec-turn_w','ln_n_-1'],'L_W_N'),	\
				(['ln_w_2','l_w_e_l','ln_e_-1'],'L_W_E'),	\
				(['ln_w_3','l_w_e_r','ln_e_-2'],'L_W_E'),	\
				(['ln_w_4','rt_prep-turn_w','rt_exec-turn_w','ln_s_-2'],'L_W_S'),	\
				(['ln_n_1','prep-turn_n','exec-turn_n','ln_e_-2'],'L_N_E'),	\
				(['ln_s_2','prep-turn_s','exec-turn_s','ln_w_-1'],'L_S_W'),	\
            (['ln_s_1','prep-turn_s','exec-turn_s','ln_w_-2'],'L_S_W'), \
				(['ln_w_1','prep-turn_w','exec-turn_w','ln_n_-2'],'L_W_N')]




SEGMENT_EXCEPTIONS = {'ln_w_-1':'L_E_W',
                      'ln_w_-2':'L_E_W',
                      'ln_e_-1':'L_W_E',
                      'ln_e_-2':'L_W_E',
                      'ln_s_-1':'L_N_S', 
                      'ln_s_-2':'L_N_S',
                      'ln_n_-1':'L_S_N', 
                      'ln_n_-2':'L_S_N'}


SEGMENT_SPEED_MODELS = { 'prep-turn_n':{'dedicated_green_light': {'light':'G', 'dedicated_green': 1, 'has_conflict': 0, 'just_turned_green': 0, 'speed':'exponential'},
                                       'green_light_with_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed':{'go':6, 'stop':'exponential'}},                                    
                                       'green_light_without_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed':'gaussian'}, 
                                       'yellow_light_with_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed':{'go':6, 'stop':2}}, 
                                       'yellow_light_without_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed':{'go':6, 'stop':'exponential'}}},

                        'prep-turn_w':{'dedicated_green_light': {'light':'G', 'dedicated_green': 1, 'has_conflict': 0, 'just_turned_green': 0, 'speed':'exponential'},
                                        'green_light_with_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed':{'go':6, 'stop':'exponential'}},
                                        'green_light_without_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 5}, 
                                        'yellow_light_with_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':6, 'stop':'exponential'}}, 
                                        'yellow_light_without_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':6, 'stop':2}}},

                        'prep-turn_s':{'dedicated_green_light': {'light':'G', 'dedicated_green': 1, 'has_conflict': 0, 'just_turned_green': 0, 'speed':'exponential'}, 
                                       'green_light_with_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed':{'go':6, 'stop':'exponential'}}, 
                                       'green_light_without_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                       'yellow_light_with_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':6, 'stop':2}}, 
                                       'yellow_light_without_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':6, 'stop':'exponential'}}},

                        'prep-turn_e':{'dedicated_green_light': {'light':'G', 'dedicated_green': 1, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                       'green_light_with_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':6, 'stop':'exponential'}}, 
                                       'green_light_without_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                       'yellow_light_with_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':6, 'stop':'exponential'}}, 
                                       'yellow_light_without_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':6, 'stop':2}}},

                        'exec-turn_n':{'green_light_with_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':8, 'stop':2}}, 
                                       'green_light_without_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'},
                                       'yellow_light_with_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':8, 'stop':2}}, 
                                       'yellow_light_without_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'exec-turn_e':{'green_light_with_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':8, 'stop':2}}, 
                                       'green_light_without_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'},
                                       'yellow_light_with_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':8, 'stop':2}}, 
                                       'yellow_light_without_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'exec-turn_s':{'green_light_with_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':8, 'stop':2}}, 
                                       'green_light_without_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'},
                                       'yellow_light_with_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':8, 'stop':2}}, 
                                       'yellow_light_without_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'exec-turn_w':{'green_light_with_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':8, 'stop':2}}, 
                                       'green_light_without_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'},
                                       'yellow_light_with_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':8, 'stop':2}}, 
                                       'yellow_light_without_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},


                        'rt_exec-turn_s':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                          'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'rt_exec-turn_w':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                          'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'ln_e_-1':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},
                        'ln_e_-2':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'ln_n_-1':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'},
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 14}},
                        'ln_n_-2':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 14}},

                        'ln_w_-1':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},
                        'ln_w_-2':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'ln_s_-1':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},
                        'ln_s_-2':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'ln_s_2':{'green_light_with_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 1, 'speed': 'gaussian'}, 
                                  'green_light_without_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':13, 'stop':2}}},

                        'ln_s_3':{'green_light_with_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 1, 'speed': 'gaussian'}, 
                                  'green_light_without_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':13, 'stop':2}}},

                        'ln_e_2':{'green_light_with_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 1, 'speed': 'gaussian'}, 
                                  'green_light_without_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':'gaussian', 'stop':2}}},

                        'ln_e_3':{'green_light_with_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 1, 'speed': 'gaussian'}, 
                                  'green_light_without_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':'gaussian', 'stop':2}}},

                        'ln_n_2':{'green_light_with_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 1, 'speed': 'exponential'},
                                  'green_light_without_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':13, 'stop':'exponential'}}},

                        'ln_n_3':{'green_light_with_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 1, 'speed': 'gaussian'}, 
                                  'green_light_without_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':13, 'stop':'exponential'}}},

                        'ln_w_2':{'green_light_with_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 1, 'speed': 'gaussian'}, 
                                  'green_light_without_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':'gaussian', 'stop':2}}},

                        'ln_w_3':{'green_light_with_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 1, 'speed': 'gaussian'}, 
                                  'green_light_without_delay': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':13, 'stop':2}}},

                        'l_s_n_r':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'l_s_n_l':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'l_n_s_r':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'l_n_s_l':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'l_w_e_r':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'l_w_e_l':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light':{'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'l_e_w_r':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'l_e_w_l':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}},

                        'ln_s_4':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':8, 'stop':2}}},

                        'ln_w_4':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                   'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':8, 'stop':2}}},

                        'rt_prep-turn_s':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                          'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':8, 'stop':2}}},

                        'rt_prep-turn_w':{'green_light': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                          'yellow_light': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':8, 'stop':2}}},

                        'ln_e_1':{'dedicated_green_light_with_delay': {'light':'G', 'dedicated_green': 1, 'has_conflict': 0, 'just_turned_green': 1, 'speed': 'exponential'}, 
                                  'dedicated_green_light_without_delay': {'light':'G', 'dedicated_green': 1, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'green_light_with_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'green_light_without_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'yellow_light_with_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':5, 'stop':1}}, 
                                  'yellow_light_without_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':5, 'stop':1}}},


                        'ln_s_1':{'dedicated_green_light_with_delay': {'light':'G', 'dedicated_green': 1, 'has_conflict': 0, 'just_turned_green': 1, 'speed': 'exponential'}, 
                                  'dedicated_green_light_without_delay': {'light':'G', 'dedicated_green': 1, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'green_light_with_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'green_light_without_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'yellow_light_with_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':5, 'stop':1}}, 
                                  'yellow_light_without_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':5, 'stop':1}}},

                        'ln_n_1':{'dedicated_green_light_with_delay': {'light':'G', 'dedicated_green': 1, 'has_conflict': 0, 'just_turned_green': 1, 'speed': 'exponential'}, 
                                  'dedicated_green_light_without_delay': {'light':'G', 'dedicated_green': 1, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'green_light_with_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'green_light_without_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'yellow_light_with_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':5, 'stop':1}}, 
                                  'yellow_light_without_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':5, 'stop':1}}},


                        'ln_w_1':{'dedicated_green_light_with_delay': {'light':'G', 'dedicated_green': 1, 'has_conflict': 0, 'just_turned_green': 1, 'speed': 'exponential'}, 
                                  'dedicated_green_light_without_delay': {'light':'G', 'dedicated_green': 1, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'green_light_with_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': 'exponential'}, 
                                  'green_light_without_conflict': {'light':'G', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': 'gaussian'}, 
                                  'yellow_light_with_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 1, 'just_turned_green': 0, 'speed': {'go':5, 'stop':1}}, 
                                  'yellow_light_without_conflict': {'light':'Y', 'dedicated_green': 0, 'has_conflict': 0, 'just_turned_green': 0, 'speed': {'go':5, 'stop':1}}}}



CONFLICT_MAP = {'L_N_E':'L_S_N',
                'L_W_N':'L_E_W',
                'L_S_W':'L_N_S',
                'L_E_S':'L_W_E',
                'L_W_S':'L_N_S',
                'L_S_E':'L_W_E'}

GATE_MAP = {'north_exit':[72,73],
                'south_exit':[18,130],
                'east_exit':[34,132],
                'west_exit':[63,131],
                'north_entry':[59,60,61,126],
                'south_entry':[17,28,29,127],
                'east_entry':[26,27,30,128],
                'west_entry':[64,65,125,129]}

# Removed: 'L_N_W':'DEDICATED_RIGHT_TURN'
# Removed: 'L_E_N':'DEDICATED_RIGHT_TURN'
TASK_MAP = {'L_N_S':'STRAIGHT',
            'L_N_E':'LEFT_TURN',
            'L_S_N':'STRAIGHT',
            'L_S_W':'LEFT_TURN',
            'L_S_E':'RIGHT_TURN',
            'L_E_W':'STRAIGHT',
            'L_E_S':'LEFT_TURN',
            'L_W_E':'STRAIGHT',
            'L_W_N':'LEFT_TURN',
            'L_W_S':'RIGHT_TURN',
            }

L1_ACTION_CODES = {'wait-for-oncoming':1,
                   'proceed-turn':2,
                   'track_speed':3,
                   'follow_lead':4,
                   'decelerate-to-stop':5,
                   'wait_for_lead_to_cross':6,
                   'follow_lead_into_intersection':7,
                   'wait-on-red':8,
                   'cut-in':9,
                   'yield-to-merging':10,
                   'wait-for-pedestrian':11,
                   'emergency_braking':12}

L1_MANEUVERS = {1: 'wait-for-oncoming',
                2: 'proceed-turn',
                3: 'track_speed',
                4: 'follow_lead',
                5:  'decelerate-to-stop',
                6:  'wait_for_lead_to_cross',
                7:  'follow_lead_into_intersection',
                8:  'wait-on-red',
                9: 'cut-in',
                10: 'yield-to-merging',
                11: 'wait-for-pedestrian',
                12: 'emergency_braking'}


TRAJ_LEVEL_TO_CODE = {'normal': 0, 'aggressive': 1}

CODE_TO_TRAJ_LEVEL = {0: 'normal', 1: 'aggressive'}



''' direction:[(relev_agent_segment, relev_agent_direction, number of relev agents to consider, maximum distance of the relev agent)] P.S: exit_lane is the exit lane in the current subject vehicle's direction'''
RELEV_REDUCTION_MAP = {'LEFT_TURN':[('through-lane','l',2,None),('through-lane','r',2,None), ('through-lane-entry','2',1,None), ('through-lane-entry','3',1,None)],
                       'RIGHT_TURN':[('through-lane','l',2,None),('through-lane','r',2,None), ('through-lane-entry','3',1,None),
                                     ('exec-left-turn',None,1,None),('prep-left-turn',None,1,None)]}

RED_LIGHT_PROCEED_LANES = ['through-lane', 'prep-left-turn','exec-left-turn']

WAIT_ACTIONS = ['yield-to-merging','wait_for_lead_to_cross','wait-for-oncoming','decelerate-to-stop','wait-on-red','wait-for-pedestrian']

FOLLOW_ACTIONS = ['follow_lead','follow_lead_into_intersection']

'''
Values derived from https://www.mdpi.com/2079-9292/8/9/943
'''

MAX_LONG_ACC_NORMAL = 2
MAX_LONG_ACC_AGGR = 3.6
MAX_LONG_ACC_EMG = 5

MAX_TURN_ACC_AGGR = 5
MAX_TURN_ACC_NORMAL = 3.6

MAX_TURN_JERK = 3
MAX_TURN_JERK_NORMAL = 1.5
MAX_TURN_JERK_AGGR = 2.5

MAX_LAT_ACC_NORMAL = 4
MAX_LAT_ACC_AGGR = 5.6
MAX_LAT_ACC_EMG = 7.6

MAX_LONG_DEC_NORMAL = -2
MAX_LONG_DEC_AGGR = -3.5
MAX_LONG_DEC_EMG = -5

MAX_LAT_DEC_NORMAL = -4
MAX_LAT_DEC_AGGR = -5.6
MAX_LAT_DEC_EMG = -7.6

MAX_ACC_JERK_NORMAL = 0.9
MAX_ACC_JERK_AGGR = 2

MAX_DEC_JERK_NORMAL = -0.9
MAX_DEC_JERK_AGGR = -2

STOP_LOC_STD_DEV = 1.5
STOP_VEL_TOLERANCE = 0.5


FRAME_FREQUENCY = 0.1
DEFAULT_TRAJECTORY_DURATION = 5.0
TOLERABLE_VELOCITY_RANGE = 1

# OTHER CONSTANTS
PI = math.pi

LAMBDA = 1

# Car length in metres
CAR_LENGTH = 4.1
CAR_WIDTH = 1.8

MEDIUM_VEHICLE_LENGTH = 6.8
MEDIUM_VEHICLE_WIDTH = 2.3

HEAVY_VEHICLE_LENGTH = 12.0
HEAVY_VEHICLE_WIDTH = 2.4

# VEHICLE SIZE CONSTANTS
# https://autovfix.com/average-length-of-a-car-how-long-is-the-average-car/
OCC_MAP_CAR_LENGTH = 41
OCC_MAP_CAR_WIDTH = 18

# 15' UHAUL Truck 
#https://www.uhaul.com/Truck-Rentals/15ft-Moving-Truck/
OCC_MAP_MEDIUM_VEHICLE_LENGTH = 68
OCC_MAP_MEDIUM_VEHICLE_WIDTH = 23

# https://hilbertsmazes.com/free-printable-hand-drawn-school-bus-maze/
OCC_MAP_HEAVY_VEHICLE_LENGTH = 120
OCC_MAP_HEAVY_VEHICLE_WIDTH = 24

# Width of the road (measured in google map is about 3.1 ~ 3.5m)
SEGMENT_WIDTH = 2

FOV_DISTANCE = 1000 # 100 metres
FOV_RANGE = PI / 6 #30 degrees both to the right and left which comes to 60 degrees

# DETERMINES THE NUMBER OF RAYCAST POINTS TO SPAWN
FOV_CIRCLE_POINTS_SCALE = 2
# If fov_circle_points_scale = 2, then we have fov_circle_points = 720
FOV_CIRCLE_POINTS = 360 * FOV_CIRCLE_POINTS_SCALE


# OCCUPANCY MAP CONSTANTS
OCCUPANCY_MAP_ORIGIN = (5387700,48139400)
OCCUPANCY_MAP_LENGTH = 1400
OCCUPANCY_MAP_WIDTH = 1400
OCCUPANCY_MAP_GRID_SIZE = 1


UNI_WEBER_INTERSECTION_DIMENSIONS = {'x': [538788, 538885], 'y': [4813982, 4814042]}

# If MAP_SCALE = 10, then for the occupancy map each grid is 10 decimetres wide and long.
MAP_SCALE = 10

# CONSTRUCTING SCENARIOS
MAXIMUM_DISTANCE_FROM_SUBJECT_VEHICLE = FOV_DISTANCE / MAP_SCALE # If the vehicle is more than 100 metres away from the subject vehicle we do not include it in the base scenario

MINIMUM_SPAWNING_DISTANCE = 1 # 1 metre

MAXIMUM_NUMBER_OF_OCCLUDING_VEHICLES = 5

FOV_BUDGET = PI / 6

OCCLUDING_VEHICLE_SPAWN_ATTEMPTS = 1000
OCCLUSION_THRESHOLD = 0.8
MINIMUM_HIT_COUNT_FOR_OCCLUSION = 3

# https://copradar.com/redlight/factors/
# A study found it to be 2.3s, so 1.5s is conservative
# NSC and UK standard is 3.0s
# https://www.tandfonline.com/doi/abs/10.1207/STHF0203_1 this says it is around 1.5s for unexpected objects
# 1.5s emergency braking response delay. Must be either int or some number to one decimal place
EMERGENCY_BRAKING_DELAY = 1.5 

# We filter out any occlusion scenarios where the only collision pair involves the occluding vehicle
# and the timing of the crash is less than this threshold. Set to 3s right now.
OCCLUSION_SCENARIO_INCLUSION_TIME_HORIZON_THRESHOLD = 3

# SCENARIO CONSTANTS
CENTRELINE_STEPSIZE = 0.001
CENTRELINE_POINTS_THRESHOLD_DISTANCE = 10

# TRAJECTORY SCORE CALCULATIONS
TIME_REDUCTION_FACTOR = 10


DB_TIME_INTERVAL = 0.033367


LEAD_VEH_DIST_THRESH = 50
RELEV_VEHS_TIME_THRESH = 1
VEH_ARRIVAL_HORIZON = 3

# GREEN LIGHT WAIT TIME
STRAIGHT_THROUGH_GREEN_LIGHT_DELAY = 6 # 4 - 8 seconds from 769 

# GREEN LIGHT WAIT TIME
LEFT_TURN_GREEN_LIGHT_DELAY = 5 # 5s

UNI_WEBER_DB_IDS = ["769", "770", "771", "775", "776", "777", "778", "779", "780", "781", "782", "783", "784", "785"]

CURRENT_FILE_ID = "769"

# CURRENT_FILE_ID = "770"
# CURRENT_FILE_ID = "771"
# CURRENT_FILE_ID = "775"
# CURRENT_FILE_ID = "776"
# CURRENT_FILE_ID = "777"
# CURRENT_FILE_ID = "778"
# CURRENT_FILE_ID = "779"
# CURRENT_FILE_ID = "780"
# CURRENT_FILE_ID = "781"
# CURRENT_FILE_ID = "782"
# CURRENT_FILE_ID = "783"
# CURRENT_FILE_ID = "784"
# CURRENT_FILE_ID = "785"
# CURRENT_FILE_ID = "786"

PATH = "/home/m3kahn/Desktop"

DB = f"{PATH}/uni_weber/database_files/{CURRENT_FILE_ID}/uni_weber_{CURRENT_FILE_ID}.db"

OCC_DB = f"{PATH}/uni_weber/database_files/{CURRENT_FILE_ID}/uni_weber_{CURRENT_FILE_ID}_occluding_vehicles.db"

# GAME CONSTANTS
SAFETY_THRESHOLD = 0

MAX_INITIAL_SPEED_FOR_OCCLUDING_VEHICLES = 16 # 16 m/s ~ 58 km/h

TRAJECTORY_GENERATION_ATTEMPTS = 10

TRAJECTORY_UTILITIES_PATH = f"{PATH}/trajectory_utilities/{CURRENT_FILE_ID}"



