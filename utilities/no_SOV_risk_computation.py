'''
Computing risk for naturalistc trafffic data, no synthetic occluding vehicle is used here, though occlusion is still present
in the naturalistic traffic data

Created on July 15th, 2021

@author: Maximilian Kahn
'''


from utilities.vehicle_state import VehicleState
from utilities import occupancy_map 
from utilities import occlusion_constants
from utilities import regenerate_NE_scenarios
from utilities import scenario_utilities
from occlusion_scenario import occlusion_scenario_builder
from game import game_utilities
from equilibria import equilibria_core
from occlusion_durations import occlusion_duration_times
from trajectories import trajectory_generation
from shapely.geometry.polygon import Polygon, LineString
from matplotlib import pyplot as plt
from copy import deepcopy
import matplotlib.animation as animation
import itertools
import sqlite3
import ast
import numpy as np
import math
import copy
import sys
import os
import csv



def contains_none(t):
    if t[1] == None:
        return False
    else:
        return True

def take_third(elem):
    return elem[2]


def retrieve_all_partial_scenario_information(current_file_id):
    no_sov_db =  f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_no_SOV.db"
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

    conn_no_sov_db = sqlite3.connect(no_sov_db)
    cur_no_sov_db = conn_no_sov_db.cursor()

    conn_db = sqlite3.connect(occlusion_constants.DB)
    cur_db = conn_db.cursor()

    execute_cmd = f"SELECT TRACK_ID, RELEV_AGENT_IDS, TIME FROM RELEVANT_AGENTS"
    cur_db.execute(execute_cmd)
    query_result = cur_db.fetchall()

    query_result = list(set(filter(contains_none, query_result)))
    valid_partial_scenarios = [(i[0], ast.literal_eval(i[1]), i[2]) for i in query_result if len(ast.literal_eval(i[1])) > 1]

    valid_partial_scenarios.sort(key=take_third)

    cmd = f"CREATE TABLE NO_SOV_COLLISIONS (SCENARIO_KEY INTEGER, TIME NUMERIC, SUBJECT_VEHICLE_ID INTEGER, RELEVANT_VEHICLES TEXT, VEHICLE_HIT_COUNTS TEXT, FOLLOW_TRAJECTORIES TEXT, HYPERGAME_COLLISIONS TEXT, FACTORED_GAME_COLLISIONS TEXT, OCCLUSION_CAUSED_COLLISIONS TEXT)"
    cur_no_sov_db.execute(cmd)
    conn_no_sov_db.commit()

    scenario_key = 0

    for valid_partial_scenario in valid_partial_scenarios:

        subject_vehicle_id = valid_partial_scenario[0]
        relevant_vehicle_ids = valid_partial_scenario[1]
        current_time = valid_partial_scenario[2]


        subject_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(subject_vehicle_id, current_time)

        relevant_vehicles = []
        for track_id in relevant_vehicle_ids:
            relevant_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(track_id, current_time)
            relevant_vehicles.append(relevant_vehicle)

        vehicle_hit_counts = compute_all_hit_counts_for_scenario(subject_vehicle, relevant_vehicles)
        print(vehicle_hit_counts)
        print(scenario_key)

        cur_no_sov_db.execute("INSERT INTO NO_SOV_COLLISIONS (SCENARIO_KEY, TIME, SUBJECT_VEHICLE_ID, RELEVANT_VEHICLES, VEHICLE_HIT_COUNTS) VALUES (?,?,?,?,?)", 
        (scenario_key, current_time, subject_vehicle_id, str(relevant_vehicle_ids), str(vehicle_hit_counts)))
        conn_no_sov_db.commit()

        print(f"DB {current_file_id}: FINISHED {scenario_key + 1} OUT OF {len(valid_partial_scenarios)} VALID PARTIAL SCENARIOS")

        scenario_key += 1

    conn_no_sov_db.close()


def compute_all_hit_counts_for_scenario(subject_vehicle, relevant_vehicles):
    # # Since we have already calculated occlusion levels for subject vehicle just use that
    # for track_id, occlusion_level in occlusion_levels:
    scenario = []
    scenario = [subject_vehicle] + relevant_vehicles
    scenario_ids = [v.id for v in scenario]

    vehicle_hit_counts = {}
    for vehicle in scenario:
        other_vehicles = [v for v in scenario if v.id != vehicle.id]
        hit_counts = occlusion_scenario_builder.compute_hit_counts(vehicle, other_vehicles)
        vehicle_hit_counts[vehicle.id] = hit_counts
    return vehicle_hit_counts

def compute_number_of_natural_occlusion_scenarios(current_file_id):

    NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
    no_sov_db =  f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_no_SOV.db"
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

    conn_NE_occ_db = sqlite3.connect(NE_OCC_DB)
    cur_NE_occ_db = conn_NE_occ_db.cursor()

    conn_no_sov_db = sqlite3.connect(no_sov_db)
    cur_no_sov_db = conn_no_sov_db.cursor()

    conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
    cur_occ_db = conn_occ_db.cursor()

    conn_db = sqlite3.connect(occlusion_constants.DB)
    cur_db = conn_db.cursor()

    execute_cmd = f"SELECT SCENARIO_KEY, VEHICLE_HIT_COUNTS FROM NO_SOV_COLLISIONS ORDER BY SCENARIO_KEY"
    cur_no_sov_db.execute(execute_cmd)
    all_valid_partial_scenarios = cur_no_sov_db.fetchall()

    print(f"STARTING NUMBER OF NATURAL OCCLUSION COUNTING FOR DB {current_file_id}")

    number_of_natural_occlusion_scenarios = 0
    total_number_of_scenarios = 0

    for query in all_valid_partial_scenarios:

        scenario_key = query[0]

        vehicle_hit_counts = ast.literal_eval(query[1])
        saved_vehicle_hit_counts_for_later = deepcopy(vehicle_hit_counts)
        factored_games = compute_factored_games(vehicle_hit_counts)

        execute_cmd = f"SELECT TIME, SUBJECT_VEHICLE_ID, RELEVANT_VEHICLES FROM NO_SOV_COLLISIONS WHERE SCENARIO_KEY={scenario_key}"
        cur_no_sov_db.execute(execute_cmd)
        query_result = cur_no_sov_db.fetchall()
        query_result = query_result[0]

        current_time = query_result[0]
        subject_vehicle_id = query_result[1]
        relevant_vehicle_ids = ast.literal_eval(query_result[2])
        
        subject_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(subject_vehicle_id, current_time)

        relevant_vehicles = []
        for track_id in relevant_vehicle_ids:
            relevant_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(track_id, current_time)
            relevant_vehicles.append(relevant_vehicle)

        scenario = [subject_vehicle] + relevant_vehicles
        ordered_vehicle_ids = [v.id for v in scenario]

        vehicles_too_far_away = []
        for vehicle in scenario:
            if len(factored_games[vehicle.id]) == len(ordered_vehicle_ids) - 1:
                vehicles_too_far_away.append(vehicle)

        if vehicles_too_far_away:   
            for vehicle in vehicles_too_far_away:
                scenario.remove(vehicle)
                ordered_vehicle_ids.remove(vehicle.id)
                factored_games.pop(vehicle.id)
            vehicles_too_far_away_ids = [vehicle.id for vehicle in vehicles_too_far_away]
            
            # Need to recompute factored games, taking into account the removed vehicles
            for track_id in ordered_vehicle_ids:
                factored_games[track_id] = [veh_id for veh_id in factored_games[track_id] if veh_id not in vehicles_too_far_away_ids]

        total_number_of_scenarios += 1
        if len(factored_games[subject_vehicle_id]) > 0:
            number_of_natural_occlusion_scenarios += 1

        print(f"DB {current_file_id}: FINISHED {scenario_key + 1} OUT OF {len(all_valid_partial_scenarios)} PARTIAL SCENARIOS")

    print(f"TOTAL NUMBER OF NATURAL OCCLUSION SCENARIOS: {number_of_natural_occlusion_scenarios}")
    print(f"TOTAL NUMBER OF SCENARIOS: {total_number_of_scenarios}")



def compute_collisions_for_valid_partial_scenarios(current_file_id, replay_collision_scenario_key=None, collision_identifier_input=None):
    NE_OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_NE_occluding_vehicles.db"
    no_sov_db =  f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_no_SOV.db"
    occlusion_constants.OCC_DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_occluding_vehicles.db"
    occlusion_constants.DB = f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}.db"
    occlusion_constants.CURRENT_FILE_ID = str(current_file_id)

    conn_NE_occ_db = sqlite3.connect(NE_OCC_DB)
    cur_NE_occ_db = conn_NE_occ_db.cursor()

    conn_no_sov_db = sqlite3.connect(no_sov_db)
    cur_no_sov_db = conn_no_sov_db.cursor()

    conn_occ_db = sqlite3.connect(occlusion_constants.OCC_DB)
    cur_occ_db = conn_occ_db.cursor()

    conn_db = sqlite3.connect(occlusion_constants.DB)
    cur_db = conn_db.cursor()

    execute_cmd = f"SELECT SCENARIO_KEY, VEHICLE_HIT_COUNTS FROM NO_SOV_COLLISIONS ORDER BY SCENARIO_KEY"
    cur_no_sov_db.execute(execute_cmd)
    all_valid_partial_scenarios = cur_no_sov_db.fetchall()

    print(f"STARTING COLLISION CHECKING FOR DB {current_file_id}")

    for query in all_valid_partial_scenarios:

        scenario_key = query[0]
        if replay_collision_scenario_key != None:
            if scenario_key != replay_collision_scenario_key:
                continue

        vehicle_hit_counts = ast.literal_eval(query[1])
        saved_vehicle_hit_counts_for_later = deepcopy(vehicle_hit_counts)
        factored_games = compute_factored_games(vehicle_hit_counts)

        execute_cmd = f"SELECT TIME, SUBJECT_VEHICLE_ID, RELEVANT_VEHICLES FROM NO_SOV_COLLISIONS WHERE SCENARIO_KEY={scenario_key}"
        cur_no_sov_db.execute(execute_cmd)
        query_result = cur_no_sov_db.fetchall()
        query_result = query_result[0]

        current_time = query_result[0]
        subject_vehicle_id = query_result[1]
        relevant_vehicle_ids = ast.literal_eval(query_result[2])
        
        subject_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(subject_vehicle_id, current_time)

        relevant_vehicles = []
        for track_id in relevant_vehicle_ids:
            relevant_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(track_id, current_time)
            relevant_vehicles.append(relevant_vehicle)

        scenario = [subject_vehicle] + relevant_vehicles
        ordered_vehicle_ids = [v.id for v in scenario]

        vehicles_too_far_away = []
        for vehicle in scenario:
            if len(factored_games[vehicle.id]) == len(ordered_vehicle_ids) - 1:
                vehicles_too_far_away.append(vehicle)

        if vehicles_too_far_away:   
            for vehicle in vehicles_too_far_away:
                scenario.remove(vehicle)
                ordered_vehicle_ids.remove(vehicle.id)
                factored_games.pop(vehicle.id)
            vehicles_too_far_away_ids = [vehicle.id for vehicle in vehicles_too_far_away]
            
            # Need to recompute factored games, taking into account the removed vehicles
            for track_id in ordered_vehicle_ids:
                factored_games[track_id] = [veh_id for veh_id in factored_games[track_id] if veh_id not in vehicles_too_far_away_ids]


        if len(factored_games[subject_vehicle_id]) > 0:
            for v in scenario:
                lead_vehicle_id = scenario_utilities.get_leader_id(v, scenario)

                if lead_vehicle_id != None:
                    for v2 in scenario:
                        if v2.id == lead_vehicle_id:
                            v.set_leading_vehicle(v2)
                else:
                    v.set_leading_vehicle(None)

            # Try to get trajectories from occluding vehicle db.
            execute_cmd = f"SELECT SCENARIO_KEY FROM ALL_SYNTHETIC_SCENARIOS WHERE SUBJECT_VEHICLE_ID={subject_vehicle_id} AND TIME={current_time}"
            cur_occ_db.execute(execute_cmd)
            query_result = cur_occ_db.fetchall()
            if query_result:
                occ_db_scenario_key = query_result[0][0]
            else:
                occ_db_scenario_key = None

            # May need to get them from NE DB instead.
            execute_cmd = f"SELECT SCENARIO_KEY FROM ALL_SYNTHETIC_SCENARIOS WHERE SUBJECT_VEHICLE_ID={subject_vehicle_id} AND TIME={current_time}"
            cur_NE_occ_db.execute(execute_cmd)
            query_result = cur_NE_occ_db.fetchall()
            if query_result: 
                NE_occ_db_scenario_key = query_result[0][0]
            else:
                NE_occ_db_scenario_key = None
 
            maneuvers_dict = {}
            trajectories_dict = {}

            if occ_db_scenario_key:
                for v in scenario:
                    execute_cmd = f"SELECT MANEUVER FROM BASE_SCENARIO_TRAJECTORIES WHERE MIN_SCENARIO_KEY <= {occ_db_scenario_key} AND MAX_SCENARIO_KEY >= {occ_db_scenario_key} AND TRACK_ID={v.id}"
                    cur_occ_db.execute(execute_cmd)
                    query_result = cur_occ_db.fetchall()
                    maneuvers = [i[0] for i in query_result]
                    maneuvers_dict[v.id] = maneuvers

                for v in scenario:
                    trajectories_dict[v.id] = {}
                    for maneuver in maneuvers_dict[v.id]:
                        execute_cmd = f"SELECT TRAJECTORIES FROM BASE_SCENARIO_TRAJECTORIES WHERE MIN_SCENARIO_KEY <= {occ_db_scenario_key} AND MAX_SCENARIO_KEY >= {occ_db_scenario_key} AND TRACK_ID={v.id} AND MANEUVER='{maneuver}'"
                        cur_occ_db.execute(execute_cmd)
                        query_result = cur_occ_db.fetchall()
                        trajectories = ast.literal_eval(query_result[0][0])
                        trajectories_dict[v.id][maneuver] = trajectories

            elif NE_occ_db_scenario_key:
                for v in scenario:
                    execute_cmd = f"SELECT MANEUVER FROM BASE_SCENARIO_TRAJECTORIES WHERE MIN_SCENARIO_KEY <= {NE_occ_db_scenario_key} AND MAX_SCENARIO_KEY >= {NE_occ_db_scenario_key} AND TRACK_ID={v.id}"
                    cur_NE_occ_db.execute(execute_cmd)
                    query_result = cur_NE_occ_db.fetchall()
                    maneuvers = [i[0] for i in query_result]
                    maneuvers_dict[v.id] = maneuvers

                for v in scenario:
                    trajectories_dict[v.id] = {}
                    for maneuver in maneuvers_dict[v.id]:
                        execute_cmd = f"SELECT TRAJECTORIES FROM BASE_SCENARIO_TRAJECTORIES WHERE MIN_SCENARIO_KEY <= {NE_occ_db_scenario_key} AND MAX_SCENARIO_KEY >= {NE_occ_db_scenario_key} AND TRACK_ID={v.id} AND MANEUVER='{maneuver}'"
                        cur_NE_occ_db.execute(execute_cmd)
                        query_result = cur_NE_occ_db.fetchall()
                        trajectories = ast.literal_eval(query_result[0][0])
                        trajectories_dict[v.id][maneuver] = trajectories
            else:
                for v in scenario:
                    maneuvers = scenario_utilities.get_available_actions(v)
                    maneuvers = scenario_utilities.filter_wait_maneuvers(maneuvers, filter_follow_maneuvers=True)
                    maneuvers = [m for m in maneuvers if m != 'cut-in']
                    maneuvers_dict[v.id] = maneuvers

                    trajectories_dict[v.id] = {}
                    for maneuver in maneuvers:
                        if maneuver == 'cut-in':
                            continue

                        trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, None)
                        # Relax constraints if we failed to generate trajectories
                        if not trajectories:
                            trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, None, relaxed_constraints=True)
                        # Set speed to something reasonable if we failed to generate trajectories 
                        if not trajectories:
                            if maneuver in occlusion_constants.WAIT_ACTIONS:
                                new_speed = 2.0
                            else:
                                new_speed = 8.0 
                            trajectories = trajectory_generation.generate_trajectories(current_file_id, v, maneuver, None, relaxed_constraints=True, new_speed=new_speed)

                        if not trajectories:
                            raise Exception(f"Could not generate trajectories for file id: {current_file_id}, time: {v.current_time}, vehicle: {v.id} for maneuver: {maneuver}, vehicle segment: {v.current_segment}, speed: {v.speed}")
                        
                        sorted_trajectories = scenario_utilities.filter_trajectories(trajectories, maneuver)
                        trajectories_dict[v.id][maneuver] = sorted_trajectories

            # Get any follow manoeuvres for this vehicle:
            for v in scenario:
                follow_maneuver_query_result = None
                if occ_db_scenario_key:
                    execute_cmd = f"SELECT MANEUVER FROM OCCLUSION_SCENARIO_TRAJECTORIES WHERE SCENARIO_KEY={occ_db_scenario_key} AND TRACK_ID={v.id}"
                    cur_occ_db.execute(execute_cmd)
                    follow_maneuver_query_result = cur_occ_db.fetchall()

                elif NE_occ_db_scenario_key:
                    execute_cmd = f"SELECT MANEUVER FROM OCCLUSION_SCENARIO_TRAJECTORIES WHERE SCENARIO_KEY={NE_occ_db_scenario_key} AND TRACK_ID={v.id}"
                    cur_NE_occ_db.execute(execute_cmd)
                    follow_maneuver_query_result = cur_NE_occ_db.fetchall()

                if follow_maneuver_query_result:
                    follow_maneuvers = [i[0] for i in follow_maneuver_query_result]
                else:
                    maneuvers = scenario_utilities.get_available_actions(v)
                    follow_maneuvers = [m for m in maneuvers if m in occlusion_constants.FOLLOW_ACTIONS]

                if v.leading_vehicle != None:
                    for follow_maneuver in follow_maneuvers:
                        trajectories = trajectory_generation.generate_trajectories(current_file_id, v, follow_maneuver, None, relaxed_constraints=False)

                        if not trajectories:
                            trajectories = trajectory_generation.generate_trajectories(current_file_id, v, follow_maneuver, None, relaxed_constraints=True)

                        if not trajectories:
                            print(f"Could not generate a follow maneuver for vehicle {v.id} at time {current_time} for db: {current_file_id}. The lead vehicle is: {v.leading_vehicle.id}")
                            continue

                        sorted_trajectories = scenario_utilities.filter_trajectories(trajectories, follow_maneuver)
                        trajectories_dict[v.id][follow_maneuver] = sorted_trajectories
                        maneuvers_dict[v.id].append(follow_maneuver)

            # ----------------NOW GENERATE GAME AND COMPUTE COLLISIONS------------------
            track_id_maneuver_pairs = []
            for track_id in ordered_vehicle_ids:
                for maneuver in maneuvers_dict[track_id]:
                    track_id_maneuver_pairs.append((track_id, maneuver))
       
            hypergame_length = len(ordered_vehicle_ids)
            hypergame_maneuver_combinations = itertools.combinations(track_id_maneuver_pairs, hypergame_length)
            hypergame_maneuver_combinations = [i for i in hypergame_maneuver_combinations if not game_utilities.repeated_track_ids_in_maneuver_combination(i, hypergame_length)]


            ordered_hypergame_maneuver_combinations = []
            # appended_maneuver_combinations
            # E.g., hypergame_maneuver_combinations: [((1, 'follow_lead'), (-1, 'proceed-turn'), (3, 'track_speed')), ((-1, 'proceed-turn'), (1, 'track_speed'), (3, 'track_speed'))
            for maneuver_combination in hypergame_maneuver_combinations:
                ordered_maneuver_combination = []
                for track_id in ordered_vehicle_ids:
                    for id_maneuver in maneuver_combination:
                        if id_maneuver[0] == track_id:
                            ordered_maneuver_combination.append(id_maneuver)
                    if ordered_maneuver_combination not in ordered_hypergame_maneuver_combinations:
                        ordered_hypergame_maneuver_combinations.append(ordered_maneuver_combination)
            
            hypergame_payoff_dict = {}

            # This is dict where the key is the hypergame_maneuver_set and the value is the maneuver_to_trajectory_index_dict
            # We need this because the specific trajectory to represent each maneuver depends on the maneuver combination that the 
            # vehicles take.
            all_maneuver_to_trajectory_index_dict = {}
            for hypergame_maneuver_set in ordered_hypergame_maneuver_combinations:

                maneuver_to_trajectory_index_dict = {}
                vehicle_maneuver_dict = {}

                # A hypergame maneuver set is a combination of maneuvers for each vehicle in the scenario:
                # E.g., ((1,2), (2,3), (-3,1)) could be one combination of maneuvers for each vehicle in the scenario with vehicles 1, 2, and -3.

                for id_maneuver in hypergame_maneuver_set:
                    vehicle_maneuver_dict[id_maneuver[0]] = [id_maneuver[1]]

                payoff_dict_key, payoff_dict_value, chosen_trajectories = game_utilities.compute_chosen_trajectories(current_file_id, trajectories_dict, subject_vehicle_id, ordered_vehicle_ids, vehicle_maneuver_dict)

                # This dictionary looks like: {(1, 'follow_lead'): (1, 'aggressive', 0), (-6, 'wait_for_lead_to_cross'): (-6, 'aggressive', 1), (3, 'track_speed'): (3, 'normal', 0)}
                # The key is a track_id, maneuver tuple, the value is a track_id, level, index tuple
                # The point of this dict is to recover the chosen trajectory for the maneuver to do a collision check and compute collision details
                for id_maneuver, chosen_trajectory_key in zip(hypergame_maneuver_set, list(chosen_trajectories.keys())):
                    if id_maneuver not in list(maneuver_to_trajectory_index_dict.keys()):                        
                        maneuver_to_trajectory_index_dict[id_maneuver] = chosen_trajectory_key

                all_maneuver_to_trajectory_index_dict[tuple(hypergame_maneuver_set)] = maneuver_to_trajectory_index_dict
                hypergame_payoff_dict[payoff_dict_key] = payoff_dict_value


            num_players = len(ordered_vehicle_ids)
            player_actions = [list(set([k[i] for k in hypergame_payoff_dict.keys()])) for i in np.arange(num_players)]  

            eq = equilibria_core.EquilibriaCore(num_players, hypergame_payoff_dict, len(hypergame_payoff_dict), player_actions[0], False)
            nash_equilibria = eq.calc_pure_strategy_nash_equilibrium_exhaustive()

            # We cannot play out this game since there are no nash equilibria
            if not nash_equilibria:
                print(f"No hypergame Nash Equilibria could be generated for scenario {scenario_key}. Skipping the scenario.")
                continue


            # Note: this requires the original scenario and so needs to be above the has_oncoming assignment below
            factored_game_details = compute_ground_truth_maneuvers_for_factored_games(current_file_id, scenario_key, scenario, ordered_vehicle_ids, factored_games, track_id_maneuver_pairs, trajectories_dict)
                
            # This is the case if no Nash Equilibria could be found for one of the factored games
            if not factored_game_details:
                print(f"No Nash Equilibria could be found for one of the factored games for scenario {scenario_key}. Skipping the scenario.")
                continue

            factored_ground_truth_maneuvers, factored_game_maneuver_probabilities = factored_game_details
            factored_ground_truth_maneuver_sets = game_utilities.compute_ground_truth_maneuver_sets(factored_ground_truth_maneuvers, hypergame_length)
            
            for i in range(0, len(scenario)-1):
                for j in range(i, len(scenario)):
                    vehicle_a, vehicle_b = scenario[i], scenario[j]
                    has_oncoming_vehicle = scenario_utilities.compute_has_oncoming_vehicle(vehicle_a, vehicle_b)
                    if has_oncoming_vehicle:
                        if not vehicle_a.has_oncoming_vehicle:
                            vehicle_a.set_has_oncoming_vehicle(True)
                        if not vehicle_b.has_oncoming_vehicle:
                            vehicle_b.set_has_oncoming_vehicle(True)

            hypergame_ground_truth_maneuvers = game_utilities.compute_ground_truth_maneuvers(ordered_vehicle_ids, scenario, nash_equilibria, hypergame_payoff_dict, factored_game_subject_id=None)
            hypergame_ground_truth_maneuver_sets = game_utilities.compute_ground_truth_maneuver_sets(hypergame_ground_truth_maneuvers, hypergame_length)

            for factored_ground_truth_maneuvers in factored_ground_truth_maneuver_sets:
                for track_id, maneuver in factored_ground_truth_maneuvers.items():
                    if maneuver == 'hypergame_maneuver':
                        factored_ground_truth_maneuvers[track_id] = hypergame_ground_truth_maneuvers[track_id].copy().pop()

            print(f"COMPUTING ALL HYPERGAME COLLISIONS")
            all_hypergame_filtered_collisions = {}
            all_true_hypergame_emergency_braking_trajectories = {}
            all_hypergame_maneuver_to_trajectory_index_dict = {}

            for hypergame_ground_truth_maneuvers in hypergame_ground_truth_maneuver_sets:

                # To identify different collisions caused by different ground truth maneuver sets
                # The key to the dict says the maneuver set: ((119, 2), (-2047, 6), (100, 2), (125, 3))
                collision_identifier = [(track_id, occlusion_constants.L1_ACTION_CODES[maneuver]) for track_id, maneuver in hypergame_ground_truth_maneuvers.items()]
                collision_identifier = tuple(collision_identifier)

                hypergame_ground_truth_maneuvers_set = tuple([(track_id, maneuver) for track_id, maneuver in hypergame_ground_truth_maneuvers.items()])

                # For hypergame, find ground truth maneuver_to_trajectory_index_dict: which is called hypergame_maneuver_to_trajectory_index_dict
                for maneuver_combination, maneuver_to_trajectory_index_dict in all_maneuver_to_trajectory_index_dict.items():
                    if hypergame_ground_truth_maneuvers_set == maneuver_combination:
                        hypergame_maneuver_to_trajectory_index_dict = maneuver_to_trajectory_index_dict
                        break

                all_hypergame_maneuver_to_trajectory_index_dict[collision_identifier] = hypergame_maneuver_to_trajectory_index_dict
                hypergame_all_collisions, hypergame_emergency_braking_trajectories = game_utilities.compute_all_collisions(current_file_id, ordered_vehicle_ids, hypergame_ground_truth_maneuvers, trajectories_dict, hypergame_maneuver_to_trajectory_index_dict, scenario)
                hypergame_filtered_collisions, true_hypergame_emergency_braking_trajectories = game_utilities.filter_collisions(ordered_vehicle_ids, hypergame_all_collisions, hypergame_emergency_braking_trajectories)
                
                if hypergame_filtered_collisions:
                    all_hypergame_filtered_collisions[collision_identifier] = hypergame_filtered_collisions
                    all_true_hypergame_emergency_braking_trajectories[collision_identifier] = true_hypergame_emergency_braking_trajectories

            print(f"ALL HYPERGAME FILTERED COLLISIONS: {hypergame_filtered_collisions}")

            # Calculate all collisions for factored game
            print(f"COMPUTING ALL FACTORED GAME COLLISIONS")
            all_factored_game_filtered_collisions = {}
            all_true_factored_game_emergency_braking_trajectories = {}
            all_factored_game_maneuver_to_trajectory_index_dict = {}

            for factored_ground_truth_maneuvers in factored_ground_truth_maneuver_sets:

                # To identify different collisions caused by different ground truth maneuver sets
                # The key to the dict says the maneuver set: ((119, 2), (-2047, 6), (100, 2), (125, 3))
                collision_identifier = [(track_id, occlusion_constants.L1_ACTION_CODES[maneuver]) for track_id, maneuver in factored_ground_truth_maneuvers.items()]
                collision_identifier = tuple(collision_identifier)

                factored_ground_truth_maneuvers_set = tuple([(track_id, maneuver) for track_id, maneuver in factored_ground_truth_maneuvers.items()])
            
                for maneuver_combination, maneuver_to_trajectory_index_dict in all_maneuver_to_trajectory_index_dict.items():
                    if factored_ground_truth_maneuvers_set == maneuver_combination:
                        factored_game_maneuver_to_trajectory_index_dict = maneuver_to_trajectory_index_dict
                        break


                all_factored_game_maneuver_to_trajectory_index_dict[collision_identifier] = factored_game_maneuver_to_trajectory_index_dict
                factored_game_all_collisions, factored_game_emergency_braking_trajectories = game_utilities.compute_all_collisions(current_file_id, ordered_vehicle_ids, factored_ground_truth_maneuvers, trajectories_dict, factored_game_maneuver_to_trajectory_index_dict, scenario)

                factored_game_filtered_collisions, true_factored_game_emergency_braking_trajectories = game_utilities.filter_collisions(ordered_vehicle_ids, factored_game_all_collisions, factored_game_emergency_braking_trajectories)

                if factored_game_filtered_collisions:
                    all_factored_game_filtered_collisions[collision_identifier] = factored_game_filtered_collisions
                    all_true_factored_game_emergency_braking_trajectories[collision_identifier] = true_factored_game_emergency_braking_trajectories

            print(f"ALL FACTORED GAME FILTERED COLLISIONS")
            print(all_factored_game_filtered_collisions)

            if all_hypergame_filtered_collisions:
                all_hypergame_trajectory_indices = {}
                for collision_identifier, hypergame_filtered_collisions in all_hypergame_filtered_collisions.items():
                    # Convert maneuvers to coded form since SQL cannot store text in the form 'text'
                    # [track_id_a, track_id_b, collision_id, 'emergency_braking', 'emergency_braking', maneuver_a, maneuver_b, impact_velocity_a, impact_velocity_b, time_to_collision]
                    for collision in hypergame_filtered_collisions:
                        collision[3] = occlusion_constants.L1_ACTION_CODES[collision[3]]
                        collision[4] = occlusion_constants.L1_ACTION_CODES[collision[4]]
                        collision[5] = occlusion_constants.L1_ACTION_CODES[collision[5]]
                        collision[6] = occlusion_constants.L1_ACTION_CODES[collision[6]]

                    # hypergame_maneuver_to_trajectory_index_dict: {(11, 'follow_lead_into_intersection'): (11, 2), (-8, 'wait_for_lead_to_cross'): (-8, 1), (3, 'track_speed'): (3, 2), (1, 'follow_lead'): (1, 1), (20, 'track_speed'): (20, 2)}
                    hypergame_trajectory_indices = [] # {track_id: {maneuver_code: [traj_level_code, traj_index]}
                    hypergame_maneuver_to_trajectory_index_dict = all_hypergame_maneuver_to_trajectory_index_dict[collision_identifier]
                    for id_maneuver, trajectory_index in hypergame_maneuver_to_trajectory_index_dict.items():
                        track_id = id_maneuver[0]
                        coded_maneuver = occlusion_constants.L1_ACTION_CODES[id_maneuver[1]]
                        hypergame_trajectory_indices.append([track_id, coded_maneuver, trajectory_index[1]])

                    all_hypergame_trajectory_indices[collision_identifier] = hypergame_trajectory_indices

                save_hypergame_collision_details(scenario_key, all_hypergame_filtered_collisions, no_sov_db)

    
            if all_factored_game_filtered_collisions:

                # Same format as factored_game_maneuver_probabilities but 
                # maneuvers are coded, e.g., {track_id: {maneuver_code: prob.}, ...}
                all_coded_maneuver_probabilities = {} 
                for track_id, maneuver_probabilities in factored_game_maneuver_probabilities.items():
                    coded_maneuver_probabilities = {}
                    for maneuver, probability in maneuver_probabilities.items():
                        coded_maneuver = occlusion_constants.L1_ACTION_CODES[maneuver]
                        coded_maneuver_probabilities[coded_maneuver] = probability
                    all_coded_maneuver_probabilities[track_id] = coded_maneuver_probabilities
                # The probabilities are not for each vehicle, only for the vehicles that have 
                # occluded vehicles, otherwise the actions for the hypergame are used.
                print(f"CODED MANEUVER PROBABILITIES: {all_coded_maneuver_probabilities}")

                occlusion_caused_collisions = {}
                all_factored_game_trajectory_indices = {}
                for collision_identifier, factored_game_filtered_collisions in all_factored_game_filtered_collisions.items():
                    occlusion_caused_collisions[collision_identifier] = {}
                    # Convert maneuvers to coded form since SQL cannot store text in the form 'text'
                    for collision in factored_game_filtered_collisions:
                        collision[3] = occlusion_constants.L1_ACTION_CODES[collision[3]]
                        collision[4] = occlusion_constants.L1_ACTION_CODES[collision[4]]
                        collision[5] = occlusion_constants.L1_ACTION_CODES[collision[5]]
                        collision[6] = occlusion_constants.L1_ACTION_CODES[collision[6]]

                        collision_id = collision[2]
                        factored_games = compute_factored_games(saved_vehicle_hit_counts_for_later)
                        track_id_a = collision[0]
                        track_id_b = collision[1]
                        vehicle_a_occluded_vehicles = factored_games[track_id_a]
                        vehicle_b_occluded_vehicles = factored_games[track_id_b]
                        if track_id_b in vehicle_a_occluded_vehicles or track_id_a in vehicle_b_occluded_vehicles:
                            occlusion_caused_collisions[collision_identifier][collision_id] = 1
                        else:
                            occlusion_caused_collisions[collision_identifier][collision_id] = 0

                    factored_game_trajectory_indices = [] # {track_id: {maneuver_code: [traj_level_code, traj_index]}
                    factored_game_maneuver_to_trajectory_index_dict = all_factored_game_maneuver_to_trajectory_index_dict[collision_identifier]

                    for id_maneuver, trajectory_index in factored_game_maneuver_to_trajectory_index_dict.items():
                        track_id = id_maneuver[0]
                        coded_maneuver = occlusion_constants.L1_ACTION_CODES[id_maneuver[1]]
                        factored_game_trajectory_indices.append([track_id, coded_maneuver, trajectory_index[1]])

                    all_factored_game_trajectory_indices[collision_identifier] = factored_game_trajectory_indices

                save_factored_game_collision_details(scenario_key, all_factored_game_filtered_collisions, occlusion_caused_collisions, no_sov_db)

        print(all_factored_game_filtered_collisions)
        print(f"DB {current_file_id}: FINISHED {scenario_key + 1} OUT OF {len(all_valid_partial_scenarios)} PARTIAL SCENARIOS")

        if replay_collision_scenario_key != None:
            traj_maneuver_and_index = all_factored_game_maneuver_to_trajectory_index_dict[collision_identifier_input]


            collision = all_factored_game_filtered_collisions[collision_identifier_input][0]
            track_id_a = collision[0]
            track_id_b = collision[1]
            collision_id = collision[2]

            selected_trajectories = {}
            selected_trajectories[track_id_a] = all_true_factored_game_emergency_braking_trajectories[collision_identifier_input][collision_id][0]
            selected_trajectories[track_id_b] = all_true_factored_game_emergency_braking_trajectories[collision_identifier_input][collision_id][1]

            collision_details = scenario_utilities.collision_check(selected_trajectories[track_id_a], selected_trajectories[track_id_b])
            time_to_collision_index = collision_details[-1]

            for track_id in ordered_vehicle_ids:
                # If scenario key is 236 don't use emergency braking trajectory for vehicle a as it is stopped and the emergency braking traj is not well formed.
                if track_id != track_id_a and track_id != track_id_b:
                    # traj_maneuver_and_index = [manv_index for manv_index in factored_game_traj_indices if manv_index[0] == track_id][0]
                    # vehicle_maneuver = occlusion_constants.L1_MANEUVERS[traj_maneuver_and_index[1]]

                    for track_id_maneuver, track_id_traj_index in traj_maneuver_and_index.items():
                        maneuver = None
                        traj_index = None
                        if track_id == track_id_maneuver[0]:
                            maneuver = track_id_maneuver[1]
                            traj_index = track_id_traj_index[1]
                            break

                    selected_trajectories[track_id] = trajectories_dict[track_id][maneuver][traj_index]

            animate_collision_helper(current_file_id, scenario_key, collision_identifier_input, collision_id, selected_trajectories, time_to_collision_index, (track_id_a, track_id_b))  
            return selected_trajectories




def compute_factored_games(vehicle_hit_counts):

    def compute_factored_game(hit_counts):
        occluded_vehicles = []
        for target_vehicle_id, hit_count in hit_counts.items():
            if type(hit_count) == int:
                if hit_count <= occlusion_constants.MINIMUM_HIT_COUNT_FOR_OCCLUSION:
                    occluded_vehicles.append(target_vehicle_id)
            else:
                # If hit count is equal to -1 it means that the vehicle is out of bounds (outside of the occupancy map boundary)
                # and is occluded.
                if hit_count == -1:
                    occluded_vehicles.append(target_vehicle_id)
        return occluded_vehicles


    occluded_vehicles_dict = {}
    for track_id, hit_counts in vehicle_hit_counts.items():
        occluded_vehicles = compute_factored_game(hit_counts)
        occluded_vehicles_dict[track_id] = occluded_vehicles
    return occluded_vehicles_dict




def create_traj_indices_and_emergency_braking_trajs_tables():
    for current_file_id in occlusion_constants.UNI_WEBER_DB_IDS:

        if int(current_file_id) >= 780:
            no_sov_db =  f"{occlusion_constants.PATH}/uni_weber/database_files/{current_file_id}/uni_weber_{current_file_id}_no_SOV.db"

            conn_no_sov_db = sqlite3.connect(no_sov_db)
            cur_no_sov_db = conn_no_sov_db.cursor()

            cmd = f"ALTER TABLE NO_SOV_COLLISIONS ADD FACTORED_GAME_TRAJ_INDICES TEXT"
            cur_no_sov_db.execute(cmd)
            conn_no_sov_db.commit()

            cmd = f"ALTER TABLE NO_SOV_COLLISIONS ADD FACTORED_GAME_EMERGENCY_BRAKING_TRAJS TEXT"
            cur_no_sov_db.execute(cmd)
            conn_no_sov_db.commit()

            conn_no_sov_db.close()





# Probably want to save some values here, such as maneuver probabilities 
# track_id_maneuver_pairs example : [(2, 'follow_lead_into_intersection'), (8, 'proceed-turn'), (3, 'track_speed'), (2, 'proceed-turn'), (1, 'follow_lead'), (8, 'wait-for-oncoming'), (1, 'track_speed'), (-13, 'proceed-turn'), (-13, 'wait_for_lead_to_cross')]
def compute_ground_truth_maneuvers_for_factored_games(current_file_id, scenario_key, scenario, ordered_vehicle_ids, factored_games, track_id_maneuver_pairs, trajectories_dict):
    ground_truth_maneuvers = {}
    # We don't allow any vehicle to use a follow maneuver if it's occluded vehicle includes it's lead vehicle
    # This may result in an error if the vehicle only can use follow maneuvers. Though they if they use follow-lead
    # they should also be able to use track_speed.
    # See if lead vehicle is occluded 
    lead_vehicle_occluded = [] # Record vehicle ids with lead vehicles occluded
    for vehicle in scenario:
        if vehicle.leading_vehicle:
            if vehicle.leading_vehicle.id in factored_games[vehicle.id]:
                lead_vehicle_occluded.append(vehicle.id)
    maneuver_probabilities = {}

    for subject_vehicle_id, occluded_vehicles in factored_games.items():
        if not occluded_vehicles:
            ground_truth_maneuvers[subject_vehicle_id] = {'hypergame_maneuver'}
            continue

        # Get factored ordered_vehicle_ids for this particular occlusion scenario
        factored_ordered_vehicle_ids = [subject_vehicle_id]
        for track_id in ordered_vehicle_ids:
            if track_id != subject_vehicle_id and track_id not in occluded_vehicles:
                factored_ordered_vehicle_ids.append(track_id)

        game_length = len(factored_ordered_vehicle_ids)

        # Get the track_id_maneuver_pairs for this factored game
        factored_track_id_maneuver_pairs = []
        for track_id_maneuver_pair in track_id_maneuver_pairs:
            if track_id_maneuver_pair[0] in factored_ordered_vehicle_ids:

                # The vehicle can't use follow maneuvers if it's lead vehicle is occluded
                if track_id_maneuver_pair[0] in lead_vehicle_occluded and track_id_maneuver_pair[1] in occlusion_constants.FOLLOW_ACTIONS:
                   continue

                factored_track_id_maneuver_pairs.append(track_id_maneuver_pair)

        maneuver_combinations = itertools.combinations(factored_track_id_maneuver_pairs, game_length)
        maneuver_combinations = [i for i in maneuver_combinations if not game_utilities.repeated_track_ids_in_maneuver_combination(i, game_length)]

        # Order maneuver combination so order of vehicles is consistent with factored_order_vehicle_ids
        ordered_maneuver_combinations = []
        for maneuver_combination in maneuver_combinations:
            ordered_combination = []
            for track_id in factored_ordered_vehicle_ids:
                for id_maneuver in maneuver_combination:
                    if id_maneuver[0] == track_id:
                        ordered_combination.append(id_maneuver)
                if ordered_combination not in ordered_maneuver_combinations:
                    ordered_maneuver_combinations.append(ordered_combination)

        factored_payoff_dict = {}
        for maneuver_combination in ordered_maneuver_combinations:
            vehicle_maneuver_dict = {}
            for id_maneuver in maneuver_combination:
                vehicle_maneuver_dict[id_maneuver[0]] = [id_maneuver[1]]
            # vehicle maneuvers looks like: {1: ['track_speed', 'follow-lead'], 2: ['wait_for_oncoming_vehicle'], -3: ['follow-lead']}

            factored_payoff_dict_key, factored_payoff_dict_value, chosen_trajectories = game_utilities.compute_chosen_trajectories(current_file_id, trajectories_dict, subject_vehicle_id, factored_ordered_vehicle_ids, vehicle_maneuver_dict)

            factored_payoff_dict[factored_payoff_dict_key] = factored_payoff_dict_value

        factored_num_players = len(factored_ordered_vehicle_ids)
        factored_player_actions = [list(set([k[i] for k in factored_payoff_dict.keys()])) for i in np.arange(factored_num_players)]

        eq = equilibria_core.EquilibriaCore(factored_num_players,factored_payoff_dict,len(factored_payoff_dict),factored_player_actions[0],False)
        factored_nash_equilibria = eq.calc_pure_strategy_nash_equilibrium_exhaustive()

        if not factored_nash_equilibria:
            return None

        # Assemble scenario
        factored_scenario = deepcopy(scenario)
        factored_scenario = [v for v in factored_scenario if v.id in factored_ordered_vehicle_ids]

        # Assign leading vehicle attribute
        for vehicle in factored_scenario:
            lead_vehicle_id = scenario_utilities.get_leader_id(vehicle, factored_scenario)
            if lead_vehicle_id != None:
                for v in factored_scenario:
                    if v.id == lead_vehicle_id:
                        vehicle.set_leading_vehicle(v)
            else:
                vehicle.set_leading_vehicle(None)

        for i in range(0, len(factored_scenario)-1):
            for j in range(i, len(factored_scenario)):
                vehicle_a, vehicle_b = factored_scenario[i], factored_scenario[j]
                has_oncoming_vehicle = scenario_utilities.compute_has_oncoming_vehicle(vehicle_a, vehicle_b)
                if has_oncoming_vehicle:
                    if not vehicle_a.has_oncoming_vehicle:
                        vehicle_a.set_has_oncoming_vehicle(True)
                    if not vehicle_b.has_oncoming_vehicle:
                        vehicle_b.set_has_oncoming_vehicle(True)

        computed_maneuvers = game_utilities.compute_ground_truth_maneuvers(factored_ordered_vehicle_ids, factored_scenario, factored_nash_equilibria, factored_payoff_dict, factored_game_subject_id=subject_vehicle_id)

        ground_truth_maneuvers[subject_vehicle_id] = computed_maneuvers[subject_vehicle_id]

        # Get all of subject vehicle's factored game maneuvers
        # ['76900700004', '76900700003']
        subject_vehicle_maneuver_action_codes = factored_player_actions[0]

        maneuvers = []
        for action_code in subject_vehicle_maneuver_action_codes:
            track_id, maneuver_code = game_utilities.extract_id_and_maneuver_from_action_code(action_code)
            maneuver = occlusion_constants.L1_MANEUVERS[maneuver_code]
            maneuvers.append(maneuver)

        subject_vehicle_maneuvers = {}
        subject_vehicle_maneuvers[subject_vehicle_id] = maneuvers
        maneuver_utilities = game_utilities.compute_maneuver_utilities(subject_vehicle_maneuvers, factored_ordered_vehicle_ids, factored_payoff_dict, factored_nash_equilibria)

        vehicle_maneuver_utilities = maneuver_utilities[subject_vehicle_id]
        vehicle_maneuver_probabilities = game_utilities.calculate_maneuver_probability(vehicle_maneuver_utilities)

        maneuver_probabilities[subject_vehicle_id] = vehicle_maneuver_probabilities

    return ground_truth_maneuvers, maneuver_probabilities






def save_hypergame_collision_details(scenario_key, hypergame_collisions, NO_SOV_DB):
    conn = sqlite3.connect(NO_SOV_DB)
    cur = conn.cursor()
    cmd = f"UPDATE main.NO_SOV_COLLISIONS SET HYPERGAME_COLLISIONS='{str(hypergame_collisions)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    cur.execute(cmd)
    conn.commit()
    conn.close()



def save_factored_game_collision_details(scenario_key, factored_game_collisions, occlusion_caused_collisions, NO_SOV_DB):
    conn = sqlite3.connect(NO_SOV_DB)
    cur = conn.cursor()
    cmd = f"UPDATE main.NO_SOV_COLLISIONS SET FACTORED_GAME_COLLISIONS='{str(factored_game_collisions)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    cmd = f"UPDATE main.NO_SOV_COLLISIONS SET OCCLUSION_CAUSED_COLLISIONS='{str(occlusion_caused_collisions)}' WHERE SCENARIO_KEY={scenario_key}"
    cur.execute(cmd)
    conn.commit()
    conn.close()



# Note: selected_trajectories is a dict of track_id and then trajectory key-value pairs 
def animate_collision_helper(current_file_id, scenario_key, collision_identifier, collision_id, selected_trajectories, time_to_collision_index, colliding_vehicle_ids):
    x_values_dict = {}
    y_values_dict = {}
    angles_dict = {}
    car_list = []

    x = occlusion_constants.UNI_WEBER_INTERSECTION_DIMENSIONS['x']
    y = occlusion_constants.UNI_WEBER_INTERSECTION_DIMENSIONS['y']
    intersection_box = Polygon([[x[0],y[0]], [x[0],y[1]], [x[1],y[1]], [x[1],y[0]]])

    for track_id, trajectory in selected_trajectories.items():
        car, = plt.plot([], [], 'k')
        car_list.append(car)
        x_values = [timestep[1] for timestep in trajectory]
        y_values = [timestep[2] for timestep in trajectory]
        angles = [scenario_utilities.angle_check(timestep[-2]) for timestep in trajectory]

        x_values_dict[track_id] = x_values
        y_values_dict[track_id] = y_values
        angles_dict[track_id] = angles

    fig, ax = plt.subplots()

    for sequence in occlusion_constants.SEGMENT_SEQUENCES:
        centreline = scenario_utilities.get_centreline_from_segment_sequence(sequence[0])
        x_points = [i[0] for i in centreline]
        y_points = [i[1] for i in centreline]
        ax.plot(x_points,y_points, 'black', alpha=0.4, linewidth=0.2)


    def init():
        ax.set_xlim(538775, 538900)
        ax.set_ylim(4813975, 4814050)
        for sequence in occlusion_constants.SEGMENT_SEQUENCES:
            centreline = scenario_utilities.get_centreline_from_segment_sequence(sequence[0])
            x_points = [i[0] for i in centreline]
            y_points = [i[1] for i in centreline]
            ax.plot(x_points,y_points, 'black', alpha=0.4, linewidth=0.2)
        return car_list

    def update(frame):
        fig.clear()
        plt.title(f"Naturalistic Scenario {scenario_key}: Front-to-Front Collision", y=1.05)
        for sequence in occlusion_constants.SEGMENT_SEQUENCES:
            centreline = scenario_utilities.get_centreline_from_segment_sequence(sequence[0])
            x_points = [i[0] for i in centreline]
            y_points = [i[1] for i in centreline]
            plt.plot(x_points,y_points, 'black', alpha=0.4, linewidth=0.2)
        
        car_list = []
        for track_id in list(selected_trajectories.keys()):
            if frame < len(x_values_dict[track_id]) - 1:
                if frame >= time_to_collision_index:
                    v = VehicleState()
                    v.set_id(track_id)
                    v.set_x(x_values_dict[track_id][time_to_collision_index])
                    v.set_y(y_values_dict[track_id][time_to_collision_index])
                    v.set_angle(angles_dict[track_id][time_to_collision_index])
                    bounding_box = Polygon(v.get_bounding_box_points(for_occ_map=False))

                    if intersection_box.contains(bounding_box):
                        x,y = bounding_box.exterior.xy

                        if track_id in colliding_vehicle_ids:
                            car, = plt.plot(x,y, 'r')
                            label = plt.text(x[0],y[0], f"{track_id}")
                            car_list.append(car)
                            car_list.append(label)
                        else:
                            car, = plt.plot(x,y, 'k')
                            label = plt.text(x[0],y[0], f"{track_id}")
                            car_list.append(car)
                            car_list.append(label)
                else:
                    v = VehicleState()
                    v.set_id(track_id)
                    v.set_x(x_values_dict[track_id][frame])
                    v.set_y(y_values_dict[track_id][frame])
                    v.set_angle(angles_dict[track_id][frame])
                    bounding_box = Polygon(v.get_bounding_box_points(for_occ_map=False))

                    if intersection_box.contains(bounding_box):
                        x,y = bounding_box.exterior.xy
                        car, = plt.plot(x,y, 'k')
                        label = plt.text(x[0],y[0], f"{track_id}")
                        car_list.append(car)
                        car_list.append(label)
        return car_list


    ani = animation.FuncAnimation(fig, update, frames=time_to_collision_index + 10,
                        init_func=init, interval=100, blit=True)

    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=500)

    if not os.path.isdir(f"no_SOV_collision_animations/{current_file_id}"):
        os.makedirs(f"no_SOV_collision_animations/{current_file_id}")
    ani.save(f'no_SOV_collision_animations/{current_file_id}/Collision_{scenario_key}_{collision_id}_{collision_identifier}.mp4', writer=writer)
