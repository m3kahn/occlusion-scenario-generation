'''
Created on June 30th, 2021

@author: Maximilian Kahn

@purpose: Overview of the traffic behaviour modelling project
'''


# ***-----------INTRODUCTION AND INSTALLATION STEPS-----------***

# This tutorial goes through how to set up a traffic scenario with 4 vehicles navigating the University and Weber (uni-weber)
# intersection in Waterloo, Canada. 
# This tutorial covers:
# 1) Setting up vehicles as vehicle state objects
# 2) Generating trajectories for each vehicle
# 3) Creating the payoff table (named payoff_dict in this code)
# 4) Calculating the Nash Equilibria solutions from the payoff table

# ***NOTE: THIS SCRIPT WILL NOT RUN BEFORE YOU COMPLETE THESE INSTALLATION STEPS***
# In order to run this script you will need to use the uni-weber database located on the STORINATOR (use database_files_with_relevant_agents).
# If you need help downloading the database, you can contact either myself or Atrisha.
#
# This project relies on two additional projects:
# 1. https://git.uwaterloo.ca/a9sarkar/traffic_behavior_modeling
# 2. https://git.uwaterloo.ca/a9sarkar/repeated_driving_games
#
# First, clone these projects to your local machine.
# In order to run this tutorial script (and use other functionality in this project) you need to add all three projects to your PYTHONPATH.
# These are: occlusion-scenario-generation, repeated_driving_games, and traffic_behavior_modeling.

# To add these modules to PYTHONPATH, replace "/home/yourname/" with the path to these repositories on your machine.
import sys
sys.path.insert(0, "/home/yourname/repeated_driving_games")
sys.path.insert(0, "/home/yourname/traffic_behavior_modeling")
sys.path.insert(0, "/home/yourname/occlusion-scenario-generation")

# Atrisha and I are still working on getting our repositories synced up. Unfortunately, at the moment there is a little 
# bit of work that you have to do.
# 
# In /occlusion-scenario-generation/utilities/occlusion_constants.py, change the variable PATH to the path to the top level uni-weber folder.
# In /repeated_driving_games/rg_constants.py, change UNI_WEBER_DB_HOME to be the path to the top level uni-weber folder and append "/uni_weber/database_files_with_relevant_agents/"
# to the end. For example, since I saved the uni-weber dataset to my Desktop, I set UNI_WEBER_DB_HOME = "/home/m3kahn/Desktop/uni_weber/database_files_with_relevant_agents/".


from utilities import occupancy_map, scenario_utilities, occlusion_constants
from trajectories import trajectory_generation
from game import game_utilities
from equilibria import equilibria_core
from maps import States
from planners import planning_objects
from planners.trajectory_planner import TrajectoryPlanner, VehicleTrajectoryPlanner, PedestrianTrajectoryPlanner
import sqlite3 
import numpy as np
import math
import itertools


if __name__=='__main__':

    # ***-----------CREATING SCENARIO-----------***
    print(f"\n***-----------CREATING SCENARIO-----------***")

    # The uni-weber dataset is made up of 14 database files: 769, 770, 771, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785.
    # We don't use 786. For this tutorial script we will be focusing on database 769.
    current_file_id = "769"

    # A "scenario" is a collection of vehicles in the intersection at a given time.
    # A scenario is made up of a subject vehicle and a set of "relevant" vehicles for that subject vehicle
    # Relevant vehicles are vehicles that the subject vehicle must take into account when determining its actions.
    # Typically, relevant vehicles are either vehicles that are in a conflicting lane with the subject vehicle, 
    # i.e., are in a path that crosses with the subject vehicle's path, or are a lead vehicle for the subject vehicle.

    # For this tutorial, we will instantiate a scenario involving the vehicles with track id 11, 3, 1, and 20 at time = 0. 
    # The subject vehicle is vehicle 11. We will be pulling data from the uni-weber dataset, specifically database 769.
    # The RELEVANT_AGENTS table in the database shows the subject vehicle id (TRACK_ID), the time, lead vehicle id (LEAD_VEHICLE), 
    # and relevant vehicle ids for that subject vehicle (RELEV_AGENT_IDS), in case you wanted to retrieve this information yourself.
    subject_vehicle_id = 11
    current_time = 0

    # An important datatype for this project is the VehicleState datatype.
    # You can see all the different attributes that can be set in utilities/vehicle_state.py
    subject_vehicle = scenario_utilities.setup_vehicle_for_occlusion_scenario(subject_vehicle_id, current_time)

    print(f"Printing the subject vehicle's state details.")

    # Some of the important VehicleState attributes are:
    
    # This is the vehicle's x-position
    print(f"Subject vehicle x-position: {subject_vehicle.x}")

    # This is the vehicle's y-position
    print(f"Subject vehicle y-position: {subject_vehicle.y}")

    # This is the vehicle's direction
    print(f"Subject vehicle direction: {subject_vehicle.direction}")

    # This is the lane segment the vehicle is currently in.
    # You can see where each lane segment is located using the map found here: https://git.uwaterloo.ca/a9sarkar/traffic_behavior_modeling
    print(f"Subject vehicle current_segment: {subject_vehicle.current_segment}")

    # Lane segments are contained in lane sequences.
    # Each lane sequence defines a particular route through the intersection.
    # For example: ['ln_n_2','l_n_s_l','ln_s_-1'], describes one of the routes from North to South.
    print(f"Subject vehicle segment_seq: {subject_vehicle.segment_seq}")

    # Retrieve the relevant agent ids for subject vehicle 11 at time 0.
    # This retrieves the relevant agent ids from the database and creates VehicleState objects for each relevant vehicle id.
    relevant_vehicles = scenario_utilities.retrieve_relevant_vehicles(subject_vehicle_id, current_time)

    scenario =  [subject_vehicle] + relevant_vehicles
    # If you want to visualize the scenario I would recommend using the DataFromSky Viewer.
    # You can also visualize the scenario using an occupancy map, however this is computationally expensive. 
    # occ_map = occupancy_map.OccupancyMap() # Uncomment to use
    # occ_map.make_occupancy_map(scenario) # Uncomment to use
    # occ_map.visualize_occupancy_map(subject_vehicle, relevant_vehicles, filename=None) # Uncomment to use


    # ***-----------GENERATING TRAJECTORIES-----------***
    print(f"\n***-----------GENERATING TRAJECTORIES-----------***")

    # The function, get_leader_id(), returns the track id of the lead vehicle if one exists,
    # otherwise, it returns None. We need to know if a vehicle has a lead vehicle or not 
    # because this will determine if the vehicle can perform maneuvers like "follow-lead" and 
    # "follow_lead_into_intersection". However, note that all "wait" maneuvers are the same,
    # i.e., they all involve the vehicle staying still, so a vehicle does not need a lead 
    # vehicle in order to perform the "wait-for-lead-to-cross" maneuver.
    for vehicle in scenario:
        lead_vehicle_id = scenario_utilities.get_leader_id(vehicle, scenario)
        if lead_vehicle_id:
            for v in scenario:
                if v.id == lead_vehicle_id:
                    vehicle.set_leading_vehicle(v)
        else:
            vehicle.set_leading_vehicle(None)

    # In short:
    # The dict, all_vehicle_trajectories, stores all the trajectories generated for each vehicle
    # in the format: all_vehicle_trajectories[track_id][maneuver][trajectory_aggression_level][trajectory_index]
    #
    # More specifically:
    # The format is: all_vehicle_trajectories[track_id] which is itself a dictionary with keys 
    # being each maneuver, and the values being dictionaries.
    # E.g., all_vehicle_trajectories[11]['proceed-turn'] returns a dictionary of trajectories
    # for vehicle 11 for maneuver 'proceed-turn'.
    #
    # all_vehicle_trajectories[track_id][maneuver] returns a dictionary with keys "aggressive"
    # and "normal". The values are lists of trajectories.
    #
    # Each trajectory is a list of timesteps, where each timestep looks something like this:
    # [3.8000000000000003, 538850.4943439695, 4813971.534279592, 6.499244060175133, 0.13072289176657897, 0, 0.015929392414199824, -1.0645355447896503, 28.22096952636835]
    # Each trajectory has a time-horizon of 6 seconds and each timestep increments by 0.1 seconds. 
    # The format is: [time, x, y, speed, acceleration, jerk, lateral acceleration, yaw, trajectory length]
    all_vehicle_trajectories = {}

    # This stores track id/maneuver pairs. We need this information to generate the payoff table later.
    # An example of this list is: [(11, 'proceed-turn'), (11, 'wait-for-lead-to-cross'), (1, 'proceed-turn'), ...]
    track_id_maneuver_pairs = []

    # I am using Atrisha's trajectory generation codebase. There are 3 steps to generate a trajectory:
    # 1) Create an agent object (ag_obj), which stores vehicle state information (as well as one for the lead vehicle if it exists)
    # 2) Create a trajectory constraint object, the constraints include velocity and position constraints and change depending on if the 
    # intended maneuver is a "wait" maneuver or a "proceed" maneuver.
    # 3) Generate the trajectories for that particular maneuver. The trajectory generator creates "aggressive" and 
    # "normal" trajectories. Broadly speaking, the "aggressive" level uses looser constraints than the "normal" level.
    for vehicle in scenario:
        initialize_db = False
        freq = 0.5
        scene_def = States.ScenarioDef(vehicle.id, None, current_file_id, initialize_db, vehicle.current_time, freq)
        if scene_def.time_crossed:
            raise Exception(f"Vehicle is not in intersection at time {vehicle.current_time}.")
        # Create agent object for vehicle
        ag_obj = scene_def.agent
        if vehicle.leading_vehicle:
            lead_vehicle = vehicle.leading_vehicle
            initialize_db = False
            freq = 0.5
            scene_def = States.ScenarioDef(lead_vehicle.id, None, current_file_id, initialize_db, lead_vehicle.current_time, freq)
            if scene_def.time_crossed:
                raise Exception(f"Vehicle is not in intersection at time {vehicle.current_time}.")
            # Create agent object for lead vehicle
            lead_ag_obj = scene_def.agent
        else:
            lead_ag_obj = None

        # Retrieve available maneuvers for vehicle
        maneuvers = scenario_utilities.get_available_actions(v)
        # Remove extra wait maneuvers, we only need to use one wait maneuver since they are all the same
        wait_maneuver_included = False
        removed_wait_maneuvers = []
        for maneuver in maneuvers:
            if maneuver in occlusion_constants.WAIT_ACTIONS and not wait_maneuver_included:
                wait_maneuver_included = True
            elif maneuver in occlusion_constants.WAIT_ACTIONS and wait_maneuver_included:
                removed_wait_maneuvers.append(maneuver)
        for wait_maneuver in removed_wait_maneuvers:
            maneuvers.remove(wait_maneuver)

        vehicle_trajectories = {}

        for maneuver in maneuvers:
            if maneuver == 'cut-in':
                print(f"Cut-in maneuver is not supported.")
                continue
            if lead_ag_obj == None and maneuver in occlusion_constants.FOLLOW_ACTIONS:
                print(f"Follow-lead maneuver requested but there is no lead vehicle. Returning None.")
                continue

            track_id_maneuver_pairs.append((vehicle.id, maneuver))

            # Creating constraint object for this vehicle/maneuver pair
            print(f"Creating constraint for vehicle {ag_obj.id}; Maneuver: {maneuver}")
            constr = planning_objects.TrajectoryConstraintsFactory.get_constraint_object(maneuver=maneuver, ag_obj=ag_obj, lead_ag_obj=lead_ag_obj)
            constr.set_limit_constraints()

            # Generate trajectories from constraint object and maneuver
            agent_motion = VehicleTrajectoryPlanner(traj_constr_obj=constr,maneuver=maneuver, mode=None, horizon=6)
            agent_motion.generate_trajectory(True)
            
            trajectories_list = []
            for level, trajectories in agent_motion.all_trajectories.items():
                trajectories_list += trajectories

            vehicle_trajectories[maneuver] = trajectories_list

        all_vehicle_trajectories[vehicle.id] = vehicle_trajectories


    # We order the vehicle ids because the payoff table requires that the order of vehicle ids is consistent and begins with the 
    # subject vehicle id.
    ordered_vehicle_ids = [subject_vehicle_id]
    for vehicle in relevant_vehicles:
        if vehicle.id != subject_vehicle_id:
            ordered_vehicle_ids.append(vehicle.id)


    # ***-----------GENERATING PAYOFF TABLE-----------***
    print(f"\n***-----------GENERATING PAYOFF TABLE-----------***")

    game_length = len(ordered_vehicle_ids)
    maneuver_combinations = itertools.combinations(track_id_maneuver_pairs, game_length)
    maneuver_combinations = [i for i in maneuver_combinations if not game_utilities.repeated_track_ids_in_maneuver_combination(i, game_length)]

    ordered_maneuver_combinations = []

    # E.g., maneuver_combinations: [((1, 'follow_lead'), (-1, 'proceed-turn'), (3, 'track_speed')), ((-1, 'proceed-turn'), (1, 'track_speed'), (3, 'track_speed'))
    for maneuver_combination in maneuver_combinations:
        ordered_maneuver_combination = []
        for track_id in ordered_vehicle_ids:
            for id_maneuver in maneuver_combination:
                if id_maneuver[0] == track_id:
                    ordered_maneuver_combination.append(id_maneuver)
            if ordered_maneuver_combination not in ordered_maneuver_combinations:
                ordered_maneuver_combinations.append(ordered_maneuver_combination)

    # The payoff dictionary has entries for each vehicle/maneuver combination.
    # For example, the following is an example of a key-value pair in payoff_dict
    # ('76901100006', '76901100303', '76901100102', '76901102003'): [-0.9490873829529407, -0.9193353646399667, -0.9481576905188982, -0.9493900870709402]
    # The key: ('76901100006', '76901100303', '76901100102', '76901102003') is made up of 4 action codes.
    # An example of an action code is '76901100006'. The first 3 digits tells you the database id. The next 3 digits tells you the subject vehicle
    # id, the next 3 digits are the relevant vehicle id (if the 3 digits are 000 then this refers to the subject itself). The last 2 digits are the 
    # maneuver code. You can see what each maneuver code represents by looking at L1_ACTION_CODES in occlusion_constants.
    # Anatomy of the action code: {current file id}{subject vehicle id}{relevant vehicle id}{maneuver code}
    # For example, '76901100006' represents vehicle 11, maneuver 6 ('wait_for_lead_to_cross'), while '76901102003' represents vehicle 20, maneuver 3 ('track_speed').
    # Each action code corresponds with a utility value, so vehicle 11, maneuver 6 ('wait_for_lead_to_cross') has utility value -0.9490873829529407.
    # The utility value can range from -1 to 1. A low utility value means that the maneuver is potentially unsafe while a high utility value means 
    # the maneuver is safe and the vehicle makes progress along its desired route.
    payoff_dict = {}
    for maneuver_combination in ordered_maneuver_combinations:
        vehicle_maneuver_dict = {}

        # A hypergame maneuver set is a combination of maneuvers for each vehicle in the scenario:
        # E.g., ((1,2), (2,3), (-3,1)) could be one combination of maneuvers for each vehicle in the scenario with vehicles 1, 2, and -3.
        for id_maneuver in maneuver_combination:
            vehicle_maneuver_dict[id_maneuver[0]] = [id_maneuver[1]]

        # vehicle maneuvers looks like: {1: ['track_speed'], 2: ['wait_for_oncoming_vehicle'], -3: ['follow-lead']}
        trajectories_dict = {}
        for track_id, maneuvers in vehicle_maneuver_dict.items():
            trajectories_dict[track_id] = {}
            trajectories_dict[track_id][maneuvers[0]] = all_vehicle_trajectories[track_id][maneuvers[0]]

        payoff_dict_key, payoff_dict_value, chosen_trajectories = game_utilities.compute_chosen_trajectories(current_file_id, trajectories_dict, subject_vehicle_id, ordered_vehicle_ids, vehicle_maneuver_dict)
        payoff_dict[payoff_dict_key] = payoff_dict_value


    # ***-----------COMPUTING NASH EQUILIBRIA-----------***
    print(f"\n***-----------COMPUTING NASH EQUILIBRIA-----------***")

    num_players = len(ordered_vehicle_ids)
    # Player actions collects the actions for each vehicle. We need the subject vehicle's actions 
    # in the Nash Equilibria solver. 
    player_actions = [list(set([k[i] for k in payoff_dict.keys()])) for i in np.arange(num_players)]  

    eq = equilibria_core.EquilibriaCore(num_players,payoff_dict,len(payoff_dict),player_actions[0],False)
    nash_equilibria = eq.calc_pure_strategy_nash_equilibrium_exhaustive()

    # Note that since trajectory generation is non-deterministic, we can end up with a different payoff table 
    # every time we run this script, and so the Nash Equilibria can be different for different runs.
    print(f"Finished computing Nash Equilibria.")
    print(f"Nash Equilibria: {nash_equilibria}")


