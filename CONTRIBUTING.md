Maximilian Kahn wrote the majority of the code in this project with the notable exception of the relevant vehicle extraction functions in scenario_utilities.py, which were written by Atrisha Sarkar. Maximilian and Atrisha both contributed to the VehicleState object class.

Additionally, many functions in this project rely on functionality written by Atrisha in the following project repositories: 

1. https://git.uwaterloo.ca/a9sarkar/traffic_behavior_modeling
2. https://git.uwaterloo.ca/a9sarkar/repeated_driving_games
