'''
Created on May 10, 2021

@author: Maximilian Kahn
'''

import unittest
from utilities import scenario_utilities, occlusion_constants, vehicle_state # VehicelState is Temp for testing
from planners.trajectory_planner import VehicleTrajectoryPlanner

import sqlite3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

from maps import States
from planners import planning_objects
import time # temp


def generate_trajectories(current_file_id, vehicle, maneuver, occluding_vehicle_initial_speed, relaxed_constraints=False, third_attempt=False, new_speed=None, path=None, start_emergency_braking_index=None):

    if maneuver == 'cut-in':
        print(f"Cut-in maneuver is not supported.")
        return None

    # This is used to create the emergency braking maneuver
    if path != None and start_emergency_braking_index != None:
        # Check if vehicle is stationary. If it is, need to use segment centreline as path instead to avoid errors
        if len(set(path)) != len(path):
            agent_waypoints, agent_waypoint_segments = scenario_utilities.ag_obj_ingredients(vehicle)
            initialize_db = False
            freq = 0.5
            scene_def = States.SyntheticScenarioDef(vehicle.id, occluding_vehicle_initial_speed, agent_waypoints, agent_waypoint_segments, vehicle.direction, str(current_file_id), initialize_db, vehicle.current_time, freq)
            ag_obj = scene_def.agent

        else:
            agent_waypoints, agent_waypoint_segments = scenario_utilities.get_ag_obj_ingredients_for_emergency_braking(vehicle, path, start_emergency_braking_index)
            initialize_db = False
            freq = 0.5
            scene_def = States.SyntheticScenarioDef(vehicle.id, occluding_vehicle_initial_speed, agent_waypoints, agent_waypoint_segments, vehicle.direction, str(current_file_id), initialize_db, vehicle.current_time, freq)
            ag_obj = scene_def.agent


    elif (path != None and start_emergency_braking_index == None) or (path == None and start_emergency_braking_index != None):
        raise Exception(f"Tried to generate an emergency braking maneuver but one of 1) path or 2) start_emergency_braking_index was None.")

    elif vehicle.id < 0:

        agent_waypoints, agent_waypoint_segments = scenario_utilities.ag_obj_ingredients(vehicle)

        initialize_db = False
        freq = 0.5
        scene_def = States.SyntheticScenarioDef(vehicle.id, occluding_vehicle_initial_speed, agent_waypoints, agent_waypoint_segments, vehicle.direction, str(current_file_id), initialize_db, vehicle.current_time, freq)
        ag_obj = scene_def.agent

    else:
        initialize_db = False
        freq = 0.5
        scene_def = States.ScenarioDef(vehicle.id, None, str(current_file_id), initialize_db, vehicle.current_time, freq)
        if scene_def.time_crossed:
            raise Exception(f"Vehicle is not in intersection at time {vehicle.current_time}.")
        ag_obj = scene_def.agent

    if vehicle.leading_vehicle:
        lead_vehicle = vehicle.leading_vehicle
        
        if lead_vehicle.id < 0:
            agent_waypoints, agent_waypoint_segments = scenario_utilities.ag_obj_ingredients(lead_vehicle)

            initialize_db = False
            freq = 0.5
            scene_def = States.SyntheticScenarioDef(lead_vehicle.id, occluding_vehicle_initial_speed, agent_waypoints, agent_waypoint_segments, lead_vehicle.direction, str(current_file_id), initialize_db, lead_vehicle.current_time, freq)
            lead_ag_obj = scene_def.agent

        else:
            initialize_db = False
            freq = 0.5
            scene_def = States.ScenarioDef(lead_vehicle.id, None, str(current_file_id), initialize_db, lead_vehicle.current_time, freq)
            if scene_def.time_crossed:
                raise Exception(f"Vehicle is not in intersection at time {vehicle.current_time}.")
            lead_ag_obj = scene_def.agent

    else:
        lead_ag_obj = None

    if lead_ag_obj == None and maneuver in occlusion_constants.FOLLOW_ACTIONS:
        print(f"Follow-lead maneuver requested but there is no lead vehicle. Returning None.")
        return None

    if new_speed != None:
        ag_obj.velocity = new_speed

    if third_attempt and maneuver in occlusion_constants.FOLLOW_ACTIONS:
        if lead_ag_obj.velocity > 18.0:
            lead_ag_obj.velocity = 18.0
        ag_obj.velocity = lead_ag_obj.velocity
        
        print(f"VEHICLE SPEED IS: {ag_obj.velocity}")
        print(f"LEAD VEHICLE IS: {lead_ag_obj.id}, SPEED IS: {lead_ag_obj.velocity}")

        constr = planning_objects.TrajectoryConstraintsFactory.get_constraint_object(maneuver=maneuver, ag_obj=ag_obj, lead_ag_obj=lead_ag_obj)
        constr.set_limit_constraints(max_lat_acc_lims=5.6,max_vel_lims=22,max_acc_lims=6,max_jerk_lims=3)

        agent_motion = VehicleTrajectoryPlanner(traj_constr_obj=constr,maneuver=maneuver, mode=None, horizon=6)
        agent_motion.generate_trajectory(True)

        if not agent_motion.all_trajectories:
            lead_ag_obj.velocity = lead_ag_obj.velocity * 0.5
            ag_obj.velocity = lead_ag_obj.velocity
            constr = planning_objects.TrajectoryConstraintsFactory.get_constraint_object(maneuver=maneuver, ag_obj=ag_obj, lead_ag_obj=lead_ag_obj)
            constr.set_limit_constraints(max_lat_acc_lims=5.6,max_vel_lims=22,max_acc_lims=6,max_jerk_lims=3)
            agent_motion = VehicleTrajectoryPlanner(traj_constr_obj=constr,maneuver=maneuver, mode=None, horizon=6)
            agent_motion.generate_trajectory(True)

    else:
        constr = planning_objects.TrajectoryConstraintsFactory.get_constraint_object(maneuver=maneuver, ag_obj=ag_obj, lead_ag_obj=lead_ag_obj)
        if relaxed_constraints:
            constr.set_limit_constraints(max_lat_acc_lims=5.6,max_vel_lims=22,max_acc_lims=6,max_jerk_lims=3)
        else:
            constr.set_limit_constraints()
 
        agent_motion = VehicleTrajectoryPlanner(traj_constr_obj=constr,maneuver=maneuver, mode=None, horizon=6)
        agent_motion.generate_trajectory(True)

    return agent_motion.all_trajectories




if __name__=='__main__':

    print("In main: trajectory generation.")



